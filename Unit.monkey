Import GameObject
Import Movement
Import AnimationGraph
Import Misc

Const AttitudeGroup_Humans 	:= 0
Const AttitudeGroup_Orcs 	:= 1

Class CUnit Extends CComponent
	Field Init					:= False
	Field AttitudeGroup			: Int
	Field ClosestEnemyPosition	: IVector = New IVector()
	Field Movement				: CMovement
	Field Position				: FVector = New FVector()
	Field PreviousPosition		: FVector = New FVector()
	Field ClosestEnemyDirty		:= False
	Field CurrentAnimID			:= -1
	Field SpriteID 				:= -1
	Field EnemyOnAdjacentTile	:= False
	Field AnimationSet			: IntMap< Int >
	Field Attack				:= False
	Field AnimationGraph		: CAnimationGraph
	
	Field HealthBarBackgroundWidth 	:= 28.0
	Field HealthBarBackgroundHeight := 4.0
	Field HealthBarBackgroundID		:= -1
	Field HealthBarBackgroundColor	: CColor = New CColor( 255, 255, 255 )
	Field HealthBarOffset			:= New FVector( 0.0, 0.5 )
	
	Field HealthBarWidth 	:= 26.0
	Field HealthBarHeight 	:= 2.0
	Field HealthBarID		:= -1
	Field HealthBarColor	: CColor = New CColor( 255, 0, 50 )
	
	
	
	
	Method New( SpriteID : Int, Movement : CMovement, AttitudeGroup : Int, AnimationSet : IntMap< Int >, AnimationGraph : CAnimationGraph )
		Super.New( ComponentType_Unit )
		Self.AttitudeGroup	= AttitudeGroup
		Self.Movement		= Movement
		Self.SpriteID		= SpriteID
		Self.AnimationSet 	= AnimationSet
		Self.AnimationGraph	= AnimationGraph
	End
	
	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )
		If Self.Init = False
			Self.Init = True
			'Setting up Health bar Render Item
			Self.HealthBarBackgroundID = CommandBuffer.AddCommand_CreateRenderItem( CommandType_SquareItem, UnitHealthBarBackgroundZ )
			CommandBuffer.AddCommand( Self.HealthBarBackgroundID, CommandType_ChangeColor, Self.HealthBarBackgroundColor.R, Self.HealthBarBackgroundColor.G, Self.HealthBarBackgroundColor.B )
			CommandBuffer.AddCommand( Self.HealthBarBackgroundID, CommandType_UpdateScale, Self.HealthBarBackgroundWidth * CAIManager.MapScale, Self.HealthBarBackgroundHeight * CAIManager.MapScale )
			
			Self.HealthBarID = CommandBuffer.AddCommand_CreateRenderItem( CommandType_SquareItem, UnitHealthBarZ )
			CommandBuffer.AddCommand( Self.HealthBarID, CommandType_ChangeColor, Self.HealthBarColor.R, Self.HealthBarColor.G, Self.HealthBarColor.B )
			CommandBuffer.AddCommand( Self.HealthBarID, CommandType_UpdateScale, Self.HealthBarWidth * CAIManager.MapScale, Self.HealthBarHeight * CAIManager.MapScale )
		End
	
	
		Self.Position.X = Self.Movement.CurrentPosition.X
		Self.Position.Y = Self.Movement.CurrentPosition.Y
		
		Local RoundedPositionX := Round( Self.Position.X )
		Local RoundedPositionY := Round( Self.Position.Y )
		
		Local DiffX := Self.ClosestEnemyPosition.X - RoundedPositionX
		Local DiffY := Self.ClosestEnemyPosition.Y - RoundedPositionY
		Local AbsDiffX := Abs( DiffX )
		Local AbsDiffY := Abs( DiffY )
		
		If ( AbsDiffX = 1 And AbsDiffY = 0 ) Or ( AbsDiffX = 0 And AbsDiffY = 1 ) Or ( AbsDiffX = 1 And AbsDiffY = 1 )
			Self.EnemyOnAdjacentTile = True
		Else
			Self.EnemyOnAdjacentTile = False
		End
			
		If Self.ClosestEnemyDirty
			Self.ClosestEnemyDirty = False
			
			If Self.EnemyOnAdjacentTile = False
				Self.Movement.RequestMoveTo( Self.ClosestEnemyPosition )
				Self.CurrentAnimID = -1
			End
		End
		If Self.EnemyOnAdjacentTile = True
			If Self.Movement.State <> MovementState_Idle
				Self.Movement.CancelMoveTo()
			Else
				If Self.Attack = False
					Self.Attack = True
					'Print( "Attack " + Self.GameObject.ID )
					Local Direction := New FVector( DiffX, DiffY )
					Local Length := Sqrt( Direction.X * Direction.X + Direction.Y * Direction.Y )
					Direction.X /= Length
					Direction.Y /= Length
					Local Angle := Direction.GetOrientationFromNormed(  )
					Self.AnimationGraph.SetVariable( AnimationVariable_Attack, True )
					Self.AnimationGraph.SetVariable( AnimationVariable_EnemyAngle, Angle )
				End
			End
		Else
			If Self.Attack = True
				'Print( "Stop Attack " + Self.GameObject.ID )
				Self.Attack = False
				Self.AnimationGraph.SetVariable( AnimationVariable_Attack, False )
			End
		End
		If Self.Position.X <> Self.PreviousPosition.X Or Self.Position.Y <> Self.PreviousPosition.Y 
			CommandBuffer.AddCommand( Self.HealthBarBackgroundID, CommandType_UpdatePosition, ( Self.Position.X + Self.HealthBarOffset.X + 0.5 ) * CAIManager.MapScale * CAIManager.TileSize, ( Self.Position.Y + Self.HealthBarOffset.Y + 0.5 ) * CAIManager.MapScale * CAIManager.TileSize )
			CommandBuffer.AddCommand( Self.HealthBarID, CommandType_UpdatePosition, ( Self.Position.X + Self.HealthBarOffset.X + 0.5 ) * CAIManager.MapScale * CAIManager.TileSize, ( Self.Position.Y + Self.HealthBarOffset.Y + 0.5 ) * CAIManager.MapScale * CAIManager.TileSize )
		End
		Self.PreviousPosition.X = Self.Position.X
		Self.PreviousPosition.Y = Self.Position.Y
	End
	
	Method SetClosestEnemy( ClosestEnemyPosition : FVector )
		Local EnemyX := Round( ClosestEnemyPosition.X )
		Local EnemyY := Round( ClosestEnemyPosition.Y )
		If EnemyX <> Self.ClosestEnemyPosition.X Or EnemyY <> Self.ClosestEnemyPosition.Y
			Self.ClosestEnemyPosition.X = EnemyX
			Self.ClosestEnemyPosition.Y = EnemyY
			Self.ClosestEnemyDirty 		= True
		End
	End
	
	Method RenderDebug()
		
	End
End