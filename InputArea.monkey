Import GameObject
Import Misc
Import AIManager

Const InputAreaState_Neutral 	: Int = 0
Const InputAreaState_Hovered 	: Int = 1
Const InputAreaState_Active 	: Int = 2


Const InputAreaPriority_Map 			: Int = 0
Const InputAreaPriority_InGameMenus 	: Int = 1

Const InputAreaEvent_OnHoveredStart			: Int = 0
Const InputAreaEvent_OnHoveredEnd			: Int = 1
Const InputAreaEvent_OnActivated			: Int = 2

Class CInputArea Extends CComponent
	Field TopLeftCorner 			: FVector 	= New FVector()
	Field BottomRightCorner 		: FVector 	= New FVector()
	Field Size						: FVector 
	Field State						: Int 		= InputAreaState_Neutral
	Field Priority					: Int 		= InputAreaPriority_InGameMenus
	Field LastInputGlobalPosition	: FVector	= New FVector()
	Global DebugDisplay				: Bool 		= True
	
	
	Method New( Center : FVector, Size : FVector, Priority : Int )
		Super.New( ComponentType_InputArea )
		
		Self.TopLeftCorner.X 		= Center.X - Size.X * 0.5
		Self.TopLeftCorner.Y 		= Center.Y - Size.Y * 0.5
		Self.BottomRightCorner.X	= Center.X + Size.X * 0.5
		Self.BottomRightCorner.Y	= Center.Y + Size.Y * 0.5
		Self.Size 					= Size
		Self.Priority				= Priority
	End
	
	Method RenderDebug()
		If DebugDisplay
			Local TopRightCorner 	:= New FVector( Self.TopLeftCorner.X + Self.Size.X, Self.TopLeftCorner.Y)
			Local BottomLeftCorner 	:= New FVector( Self.TopLeftCorner.X, Self.TopLeftCorner.Y + Self.Size.Y )
			
			Local ColorNeutral : CColor = New CColor( 255, 255, 255 )
			Local ColorHovered : CColor = New CColor( 255, 0, 0 )
			Local ColorClicked : CColor = New CColor( 0, 0, 255 )
			
			Select Self.State
				Case InputAreaState_Neutral
					SetColor( ColorNeutral.R, ColorNeutral.G, ColorNeutral.B )
				Case InputAreaState_Hovered
					SetColor( ColorHovered.R, ColorHovered.G, ColorHovered.B )
				Case InputAreaState_Active
					SetColor( ColorClicked.R, ColorClicked.G, ColorClicked.B )
			End
			DrawLine( Self.TopLeftCorner.X, Self.TopLeftCorner.Y, TopRightCorner.X, TopRightCorner.Y )
			DrawLine( TopRightCorner.X, TopRightCorner.Y, BottomRightCorner.X, BottomRightCorner.Y )
			DrawLine( BottomRightCorner.X, BottomRightCorner.Y, BottomLeftCorner.X, BottomLeftCorner.Y )
			DrawLine( BottomLeftCorner.X, BottomLeftCorner.Y, Self.TopLeftCorner.X, Self.TopLeftCorner.Y )
		End
	End
End