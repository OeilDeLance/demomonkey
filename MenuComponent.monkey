Import GameObject
Import Renderer

Const MenuID_HumanFootman	: Int 	= 0
Const MenuID_OrcFootman		: Int 	= 1

Const MenuState_Invalid		: Int 	= -1
Const MenuState_Neutral		: Int 	= 0
Const MenuState_Highlight 	: Int 	= 1
Const MenuState_Select 		: Int 	= 2

Const MenuEvent_SetNeutral	: Int 	= 0
Const MenuEvent_SetHighlight: Int 	= 1
Const MenuEvent_SetSelect	: Int 	= 2

Class CMenuComponent Extends CComponent
	Field MenuID			: Int		= MenuID_HumanFootman
	Field State 			: Int		= MenuState_Neutral
	Field Init 				: Bool 		= False
	Field SpriteID			: Int		= -1
	Field HighlightSquareID	: Int 		= -1
	
	Field NeutralColor 		: CColor	= New CColor( 0, 0, 200 )
	Field HighlightColor	: CColor	= New CColor( 0, 255, 200 )
	Field SelectColor		: CColor	= New CColor( 255, 200, 0 )
	
	Field MenuPosition 		: FVector 	= New FVector( 1.0, 1.0 )
	Field HighlightWidth	: Float 	= 48.0
	Field HighlightHeight	: Float 	= 40.0
	
	Field MenuIdleAnimID	: Int 		= 0
	
	Field RequestState		: Int 		= 0
	
	Method New( Position : FVector, MenuIdleAnimID : Int, MenuID : Int )
		Super.New( ComponentType_Menu )
		
		Self.MenuPosition.X = Position.X
		Self.MenuPosition.Y = Position.Y
		
		Self.MenuIdleAnimID = MenuIdleAnimID
		
		Self.MenuID			= MenuID
	End
	
	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )
		If Self.Init = False
			Self.Init = True
			
		
			
			'Setting up main sprite that defined the icon of the menu
			Self.SpriteID = CommandBuffer.AddCommand_CreateRenderItem( CommandType_CreateSprite, MenuZ )
			CommandBuffer.AddCommand( Self.SpriteID, CommandType_PlayAnimation, Self.MenuIdleAnimID )
			CommandBuffer.AddCommand( Self.SpriteID, CommandType_UpdatePosition, Self.MenuPosition.X, Self.MenuPosition.Y )
			CommandBuffer.AddCommand( Self.SpriteID, CommandType_UpdateScale, CAIManager.MapScale, CAIManager.MapScale )
			
			'Setting up Highlight Render Item
			Self.HighlightSquareID = CommandBuffer.AddCommand_CreateRenderItem( CommandType_SquareItem, MenuZ - 1.0 )
			CommandBuffer.AddCommand( Self.HighlightSquareID, CommandType_ChangeColor, Self.NeutralColor.R, Self.NeutralColor.G, Self.NeutralColor.B )
			CommandBuffer.AddCommand( Self.HighlightSquareID, CommandType_UpdatePosition, Self.MenuPosition.X, Self.MenuPosition.Y )
			CommandBuffer.AddCommand( Self.HighlightSquareID, CommandType_UpdateScale, HighlightWidth * CAIManager.MapScale, HighlightHeight * CAIManager.MapScale )
		End
	End
	
	Method OnEvent( EventID : Int )
		Select EventID
			Case MenuEvent_SetNeutral
				'Print( "Set Menu " + MenuID + " To Neutral " )
				Self.RequestState 	= MenuState_Neutral
			Case MenuEvent_SetHighlight
				'Print( "Set Menu " + MenuID + " To Highlight " )
				Self.RequestState 	= MenuState_Highlight
			Case MenuEvent_SetSelect
				'Print( "Set Menu " + MenuID + " To Select " )
				Self.RequestState 	= MenuState_Select
		End
	End
End