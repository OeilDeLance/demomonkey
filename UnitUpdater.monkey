Import GameObject
Import Unit

Class CUnitUpdater Extends CComponentUpdater
	Field UnitList : List< CUnit > = New List< CUnit >()
	Method New()
		Super.New( ComponentType_Unit )
	End
	
	Method Update( CommandBuffer : CCommandBuffer )
		Local ClosestUnitPosition := New FVector()
		For Local UnitA := Eachin Self.UnitList
			
			Local ClosestUnitDistanceSq := MAX_FLOAT
			For Local UnitB := Eachin Self.UnitList
				If UnitA <> UnitB And UnitA.AttitudeGroup <> UnitB.AttitudeGroup
					Local UnitDistanceSq := ( UnitA.Position.X - UnitB.Position.X ) * ( UnitA.Position.X - UnitB.Position.X ) + ( UnitA.Position.Y - UnitB.Position.Y ) * ( UnitA.Position.Y - UnitB.Position.Y )
					If UnitDistanceSq < ClosestUnitDistanceSq
						ClosestUnitDistanceSq 		= UnitDistanceSq
						ClosestUnitPosition.X		= UnitB.Position.X
						ClosestUnitPosition.Y		= UnitB.Position.Y
					End
				End
			End
			UnitA.SetClosestEnemy( ClosestUnitPosition )
		End
	End
	
	Method AddComponent( Component : CComponent )
		UnitList.AddLast( CUnit( Component ) )
	End
End