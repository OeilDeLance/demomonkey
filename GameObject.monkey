Import AnimationManager
Import AIComponent
Import CommandBuffer

Const ComponentType_Undefined	: Int = -1
Const ComponentType_Movement	: Int = 0
Const ComponentType_Menu 		: Int = 1
Const ComponentType_InputArea	: Int = 2
Const ComponentType_MapInterface	: Int = 3
Const ComponentType_Unit 			: Int = 4
Const ComponentType_AnimationGraph 	: Int = 5


Class CGlobalEventData
	Field GlobalEventID 	: Int
	Field TargetComponent	: CComponent
	
	Method New ( GlobalEventID : Int, TargetComponent : CComponent )
		Self.GlobalEventID 		= GlobalEventID
		Self.TargetComponent 	= TargetComponent
	End
End

Class CComponentUpdater
	Field ComponentType : Int
	
	Method New( ComponentType : Int )
		Self.ComponentType = ComponentType
	End
	
	Method Update( CommandBuffer : CCommandBuffer ) Abstract	
	Method AddComponent( Component :CComponent  ) Abstract
End

Class CComponent
	Field Type 						: Int = ComponentType_Undefined
	Field GameObject 				: CGameObject
	Field LocalToGlobalEventMap 	: IntMap< List< CGlobalEventData > >	
	
	Method New( Type : Int )
		Self.Type = Type
	End

	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )
	End
	Method RenderDebug()
	End
	
	
	Method OnEvent( EventID : Int )
	End
	Method BindEvents( LocalEventID : Int, GlobalEventID : Int, TargetComponent : CComponent )
		If Self.LocalToGlobalEventMap = Null
			Self.LocalToGlobalEventMap = New IntMap< List< CGlobalEventData > >()
		End
		Local EventDataList 	:= Self.LocalToGlobalEventMap.Get( LocalEventID )
		Local GlobalEventData 	:= New CGlobalEventData( GlobalEventID, TargetComponent )
		If EventDataList = Null
			EventDataList = New List< CGlobalEventData >()
			Self.LocalToGlobalEventMap.Add( LocalEventID, EventDataList )
		End
		
		EventDataList.AddLast( GlobalEventData )
	End
	Method RaiseEvent( LocalEventID : Int )
		If Self.LocalToGlobalEventMap
			Local EventDataList := Self.LocalToGlobalEventMap.Get( LocalEventID )
			If EventDataList
				For Local GlobalEventData := Eachin EventDataList
					GlobalEventData.TargetComponent.OnEvent( GlobalEventData.GlobalEventID )
				End
			End
		End
	End
End

Class CGameObject
	Field ID				: Int 		= -1
	Global GameObjectCount	: Int 		= 0
	Field ComponentList		: List< CComponent >	= New List< CComponent >()
	
	Method New()
		Self.ID			= GameObjectCount
		GameObjectCount	+= 1
	End
	
	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )

		For Local Component : CComponent = Eachin Self.ComponentList
			Component.Update( DeltaTime, CommandBuffer )
		End
	End
	
	Method RenderDebug()
		For Local Component : CComponent = Eachin Self.ComponentList
			Component.RenderDebug()
		End
	End
	
	Method AddComponent( Component : CComponent )
		Self.ComponentList.AddLast( Component )
		Component.GameObject = Self
	End
	
	Method GetComponent : CComponent( ComponentType : Int )
		For Local Component := Eachin Self.ComponentList
			If Component.Type = ComponentType
				Return Component
			End
		End
	End
End