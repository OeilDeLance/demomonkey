Import MenuComponent
Import InputArea
Import EventManager

Class CInputAreaUpdater Extends CComponentUpdater
	Field CurrentSelectedMenu 	: CMenuComponent
	Field ComponentList			: List< CInputArea >	= New List< CInputArea >()
	
	Method New()
		Super.New( ComponentType_InputArea )
	End
	
	Method Update( CommandBuffer : CCommandBuffer )
		Local MouseX : Float = MouseX()
		Local MouseY : Float = MouseY()
		
		Local PrioritaryInputArea : CInputArea
		For Local InputArea := Eachin Self.ComponentList			
			If PrioritaryInputArea = Null
				If InputArea.TopLeftCorner.X <= MouseX And MouseX <= InputArea.BottomRightCorner.X And InputArea.TopLeftCorner.Y <= MouseY And MouseY <= InputArea.BottomRightCorner.Y
					PrioritaryInputArea	= InputArea
					Exit					
				End
			End
		End
	
		For Local InputArea := Eachin Self.ComponentList
			If InputArea <> PrioritaryInputArea
				If InputArea.State = InputAreaState_Hovered
					'Print( "InputArea Neutral " + InputArea.Priority )
					InputArea.RaiseEvent( InputAreaEvent_OnHoveredEnd )
					InputArea.State = InputAreaState_Neutral
				End
			Else
				
				InputArea.LastInputGlobalPosition.X = MouseX
				InputArea.LastInputGlobalPosition.Y = MouseY
				If MouseDown( MOUSE_LEFT )
					If InputArea.State <> InputAreaState_Active
						'Print( "InputArea Active " + InputArea.Priority )
						InputArea.State = InputAreaState_Active
						InputArea.RaiseEvent( InputAreaEvent_OnActivated )
					End
				Else If InputArea.State <> InputAreaState_Hovered
					'Print( "InputArea Hovered " + InputArea.Priority )
					InputArea.RaiseEvent( InputAreaEvent_OnHoveredStart )
					InputArea.State = InputAreaState_Hovered
					
				End
			End
		End
		
	
	End
	
	Method AddComponent( Component : CComponent )	
		Local InputArea := CInputArea( Component )	
		' Add To the ordered Render list
		Local ComponentNode := Self.ComponentList.FirstNode
		If ComponentNode = Null
			'List is Empty
			Self.ComponentList.AddFirst( InputArea )
			Return
		End
		
		'List is not empty: finding Node with bigger Z 
		While ComponentNode 
			If InputArea.Priority > ComponentNode.Value.Priority
				If ComponentNode.PrevNode() = Null
					Self.ComponentList.AddFirst( InputArea )
				Else
					New list.Node< CInputArea >( ComponentNode, ComponentNode.PrevNode(), InputArea )
				End
				Return
			End
			ComponentNode = ComponentNode.NextNode()
		End
		
		'Node with bigger Z not found inserting last
		Self.ComponentList.AddLast( InputArea )	
	End
End