Import mojo

'Animation set IDs
Const Anim_WalkUp 		: Int = 0
Const Anim_WalkDown		: Int = 1
Const Anim_WalkRight 	: Int = 2
Const Anim_WalkLeft		: Int = 3
Const Anim_AttackUp		: Int = 4
Const Anim_AttackDown	: Int = 5
Const Anim_AttackRight	: Int = 6
Const Anim_AttackLeft	: Int = 7
Const Anim_Unused		: Int = 8
Const Anim_WalkUpRight 	: Int = 9
Const Anim_WalkUpLeft	: Int = 10
Const Anim_WalkDownRight: Int = 11
Const Anim_WalkDownLeft	: Int = 12
Const Anim_AttackUpRight 	: Int = 13
Const Anim_AttackUpLeft		: Int = 14
Const Anim_AttackDownRight	: Int = 15
Const Anim_AttackDownLeft	: Int = 16
Const Anim_IdleRight		: Int = 17
Const Anim_IdleUp			: Int = 18
Const Anim_IdleLeft			: Int = 19
Const Anim_IdleDown			: Int = 20
Const Anim_IdleUpRight		: Int = 21
Const Anim_IdleUpLeft		: Int = 22
Const Anim_IdleDownLeft		: Int = 23
Const Anim_IdleDownRight	: Int = 24

'Animations IDs
Const Anim_HumanFootmanWalkUp 		: Int = 0
Const Anim_HumanFootmanWalkDown		: Int = 1
Const Anim_HumanFootmanWalkRight 	: Int = 2
Const Anim_HumanFootmanWalkLeft		: Int = 3
Const Anim_HumanFootmanAttackUp		: Int = 4
Const Anim_HumanFootmanAttackDown	: Int = 5
Const Anim_HumanFootmanAttackRight	: Int = 6
Const Anim_HumanFootmanAttackLeft	: Int = 7
Const Anim_HumanFootmanUnused		: Int = 8
Const Anim_HumanFootmanWalkUpRight 	: Int = 9
Const Anim_HumanFootmanWalkUpLeft	: Int = 10
Const Anim_HumanFootmanWalkDownRight: Int = 11
Const Anim_HumanFootmanWalkDownLeft	: Int = 12
Const Anim_HumanFootmanAttackUpRight 	: Int = 13
Const Anim_HumanFootmanAttackUpLeft		: Int = 14
Const Anim_HumanFootmanAttackDownRight	: Int = 15
Const Anim_HumanFootmanAttackDownLeft	: Int = 16
Const Anim_HumanFootmanIdleRight		: Int = 17
Const Anim_HumanFootmanIdleUp			: Int = 18
Const Anim_HumanFootmanIdleLeft			: Int = 19
Const Anim_HumanFootmanIdleDown			: Int = 20
Const Anim_HumanFootmanIdleUpRight		: Int = 21
Const Anim_HumanFootmanIdleUpLeft		: Int = 22
Const Anim_HumanFootmanIdleDownLeft		: Int = 23
Const Anim_HumanFootmanIdleDownRight	: Int = 24

Const Anim_MenuSpawnHumanIdle		: Int = 100
Const Anim_MenuSpawnOrcIdle			: Int = 101

Const Anim_OrcFootmanWalkUp 		: Int = 200
Const Anim_OrcFootmanWalkDown		: Int = 201
Const Anim_OrcFootmanWalkRight 		: Int = 202
Const Anim_OrcFootmanWalkLeft		: Int = 203
Const Anim_OrcFootmanAttackUp		: Int = 204
Const Anim_OrcFootmanAttackDown		: Int = 205
Const Anim_OrcFootmanAttackRight	: Int = 206
Const Anim_OrcFootmanAttackLeft		: Int = 207
Const Anim_OrcFootmanUnused			: Int = 208
Const Anim_OrcFootmanWalkUpRight 	: Int = 209
Const Anim_OrcFootmanWalkUpLeft		: Int = 210
Const Anim_OrcFootmanWalkDownRight	: Int = 211
Const Anim_OrcFootmanWalkDownLeft	: Int = 212
Const Anim_OrcFootmanAttackUpRight 	: Int = 213
Const Anim_OrcFootmanAttackUpLeft	: Int = 214
Const Anim_OrcFootmanAttackDownRight: Int = 215
Const Anim_OrcFootmanAttackDownLeft	: Int = 216
Const Anim_OrcFootmanIdleRight		: Int = 217
Const Anim_OrcFootmanIdleUp			: Int = 218
Const Anim_OrcFootmanIdleLeft		: Int = 219
Const Anim_OrcFootmanIdleDown		: Int = 220
Const Anim_OrcFootmanIdleUpRight	: Int = 221
Const Anim_OrcFootmanIdleUpLeft		: Int = 222
Const Anim_OrcFootmanIdleDownLeft	: Int = 223
Const Anim_OrcFootmanIdleDownRight	: Int = 224

Class CImage
	Field Img 				: Image
	Field SpriteIndex 		: Int
	Field MirrorVertically	: Bool
	Method New( Img:Image, SpriteIndex: Int, MirrorVertically : Bool = False )
		Self.Img 				= Img
		Self.SpriteIndex 		= SpriteIndex
		Self.MirrorVertically 	= MirrorVertically
	End
End

Class CAnimation
	Field KeyframeList 		: Stack< CImage > = New Stack< CImage >()
	Field Duration 			: Float
	Field TimePerKeyframe 	: Float
	Method New( Duration : Float )
		Self.Duration = Duration
		
	End
	
	Method AddKeyframe( Img : Image, SpriteIndex: Int, MirrorVertically : Bool = False )
		Self.KeyframeList.Push( New CImage( Img, SpriteIndex, MirrorVertically ) )
		Self.TimePerKeyframe = Self.Duration / Self.KeyframeList.Length()
	End
End

Class CAnimationManager
  Global AnimationMap 		: IntMap< CAnimation > = New IntMap< CAnimation >()
  'Global HumanFootmanImage 	: Image 
 ' Global PortraitsImage 	: Image
  'Global OrcFootmanImage	: Image
  Function Init()
  	'Menus
  	Local PortraitsImage := LoadImage( "Portraits.png", 46, 38, 5, Image.MidHandle )
  	
  	Local MenuSpawnHumanIdle : CAnimation = New CAnimation( 0.5 )
  	MenuSpawnHumanIdle.AddKeyframe( PortraitsImage, 0 ) 
  	AnimationMap.Set( Anim_MenuSpawnHumanIdle, MenuSpawnHumanIdle )
  	
  	Local MenuSpawnOrcIdle : CAnimation = New CAnimation( 0.5 )
  	MenuSpawnOrcIdle.AddKeyframe( PortraitsImage, 1 ) 
  	AnimationMap.Set( Anim_MenuSpawnOrcIdle, MenuSpawnOrcIdle )
  	
  	LoadUnitAnimations( "HumanFootman.png", Anim_HumanFootmanWalkUp )
  	LoadUnitAnimations( "OrcFootman.png", Anim_OrcFootmanWalkUp )
  	
  	LoadUnitAnimations( "HumanFootman.png", Anim_HumanFootmanWalkUp )
  	LoadUnitAnimations( "OrcFootman.png", Anim_OrcFootmanWalkUp )
  	Return 
  	
  
  	
  End
  
  Function LoadUnitAnimations( ImageName : String, FirstAnimID : Int )
  	'Human
  	Local Image := LoadImage( ImageName, 64, 64, 128, Image.MidHandle )
  	
  	Local WalkAnimDuration : Float = 0.75
  	Local AttackAnimDuration : Float = 0.5
  	
  	Local HumanFootmanWalkUp : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkUp.AddKeyframe( Image, 32 ) 
  	HumanFootmanWalkUp.AddKeyframe( Image, 24 ) 
  	HumanFootmanWalkUp.AddKeyframe( Image, 16 ) 
  	HumanFootmanWalkUp.AddKeyframe( Image, 8 ) 
  	HumanFootmanWalkUp.AddKeyframe( Image, 0 ) 
  	AnimationMap.Set( Anim_HumanFootmanWalkUp + FirstAnimID, HumanFootmanWalkUp )
  	
  	Local HumanFootmanWalkDown : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkDown.AddKeyframe( Image, 4 ) 
  	HumanFootmanWalkDown.AddKeyframe( Image, 12 ) 
  	HumanFootmanWalkDown.AddKeyframe( Image, 20 ) 
  	HumanFootmanWalkDown.AddKeyframe( Image, 28 ) 
  	HumanFootmanWalkDown.AddKeyframe( Image, 36 )  
  	AnimationMap.Set( Anim_HumanFootmanWalkDown + FirstAnimID, HumanFootmanWalkDown )
  	
  	Local HumanFootmanWalkRight : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkRight.AddKeyframe( Image, 2 ) 
  	HumanFootmanWalkRight.AddKeyframe( Image, 10 ) 
  	HumanFootmanWalkRight.AddKeyframe( Image, 18 ) 
  	HumanFootmanWalkRight.AddKeyframe( Image, 26 ) 
  	HumanFootmanWalkRight.AddKeyframe( Image, 34 ) 
  	AnimationMap.Set( Anim_HumanFootmanWalkRight + FirstAnimID, HumanFootmanWalkRight )
  	
  	Local HumanFootmanWalkLeft : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkLeft.AddKeyframe( Image, 2, True ) 
  	HumanFootmanWalkLeft.AddKeyframe( Image, 10, True ) 
  	HumanFootmanWalkLeft.AddKeyframe( Image, 18, True ) 
  	HumanFootmanWalkLeft.AddKeyframe( Image, 26, True ) 
  	HumanFootmanWalkLeft.AddKeyframe( Image, 34, True ) 
  	AnimationMap.Set( Anim_HumanFootmanWalkLeft + FirstAnimID, HumanFootmanWalkLeft )
  	
  	Local HumanFootmanAttackUp : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackUp.AddKeyframe( Image, 40 ) 
  	HumanFootmanAttackUp.AddKeyframe( Image, 48 ) 
  	HumanFootmanAttackUp.AddKeyframe( Image, 56 ) 
  	HumanFootmanAttackUp.AddKeyframe( Image, 64 ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackUp + FirstAnimID, HumanFootmanAttackUp )
  	
  	Local HumanFootmanAttackDown : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackDown.AddKeyframe( Image, 44 ) 
  	HumanFootmanAttackDown.AddKeyframe( Image, 52 ) 
  	HumanFootmanAttackDown.AddKeyframe( Image, 60 ) 
  	HumanFootmanAttackDown.AddKeyframe( Image, 68 ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackDown + FirstAnimID, HumanFootmanAttackDown )
  	
  	Local HumanFootmanAttackRight : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackRight.AddKeyframe( Image, 42 ) 
  	HumanFootmanAttackRight.AddKeyframe( Image, 50 ) 
  	HumanFootmanAttackRight.AddKeyframe( Image, 58 ) 
  	HumanFootmanAttackRight.AddKeyframe( Image, 66 ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackRight + FirstAnimID, HumanFootmanAttackRight )
  	
  	Local HumanFootmanAttackLeft : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackLeft.AddKeyframe( Image, 42, True ) 
  	HumanFootmanAttackLeft.AddKeyframe( Image, 50, True ) 
  	HumanFootmanAttackLeft.AddKeyframe( Image, 58, True ) 
  	HumanFootmanAttackLeft.AddKeyframe( Image, 66, True ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackLeft + FirstAnimID, HumanFootmanAttackLeft )
  	
  	' Idles
  	Local HumanFootmanIdleRight : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleRight.AddKeyframe( Image, 2 ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleRight + FirstAnimID, HumanFootmanIdleRight )
  	
  	Local HumanFootmanIdleUp : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleUp.AddKeyframe( Image, 0 ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleUp + FirstAnimID, HumanFootmanIdleUp )
  	
  	Local HumanFootmanIdleLeft : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleLeft.AddKeyframe( Image, 2, True ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleLeft + FirstAnimID, HumanFootmanIdleLeft )
  	
  	Local HumanFootmanIdleDown : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleDown.AddKeyframe( Image, 4 ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleDown + FirstAnimID, HumanFootmanIdleDown )
  	
  	Local HumanFootmanIdleUpRight : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleUpRight.AddKeyframe( Image, 2 ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleUpRight + FirstAnimID, HumanFootmanIdleUpRight )
  	
  	Local HumanFootmanIdleUpLeft : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleUpLeft.AddKeyframe( Image, 0 ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleUpLeft + FirstAnimID, HumanFootmanIdleUpLeft )
  	
  	Local HumanFootmanIdleDownLeft : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleDownLeft.AddKeyframe( Image, 2, True ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleDownLeft + FirstAnimID, HumanFootmanIdleDownLeft )
  	
  	Local HumanFootmanIdleDownRight : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanIdleDownRight.AddKeyframe( Image, 4 ) 
  	AnimationMap.Set( Anim_HumanFootmanIdleDownRight + FirstAnimID, HumanFootmanIdleDownRight )
  	
  	'Walk sideways
  	Local HumanFootmanWalkUpRight : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkUpRight.AddKeyframe( Image, 1 ) 
  	HumanFootmanWalkUpRight.AddKeyframe( Image, 9 ) 
  	HumanFootmanWalkUpRight.AddKeyframe( Image, 17 ) 
  	HumanFootmanWalkUpRight.AddKeyframe( Image, 25 ) 
  	HumanFootmanWalkUpRight.AddKeyframe( Image, 33 ) 
  	AnimationMap.Set( Anim_HumanFootmanWalkUpRight + FirstAnimID, HumanFootmanWalkUpRight )
  	
  	Local HumanFootmanWalkUpLeft : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkUpLeft.AddKeyframe( Image, 1, True ) 
  	HumanFootmanWalkUpLeft.AddKeyframe( Image, 9, True ) 
  	HumanFootmanWalkUpLeft.AddKeyframe( Image, 17, True ) 
  	HumanFootmanWalkUpLeft.AddKeyframe( Image, 25, True ) 
  	HumanFootmanWalkUpLeft.AddKeyframe( Image, 33, True )  
  	AnimationMap.Set( Anim_HumanFootmanWalkUpLeft + FirstAnimID, HumanFootmanWalkUpLeft )
  	
  	Local HumanFootmanWalkDownRight : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkDownRight.AddKeyframe( Image, 3 ) 
  	HumanFootmanWalkDownRight.AddKeyframe( Image, 11 ) 
  	HumanFootmanWalkDownRight.AddKeyframe( Image, 19 ) 
  	HumanFootmanWalkDownRight.AddKeyframe( Image, 27 ) 
  	HumanFootmanWalkDownRight.AddKeyframe( Image, 35 ) 
  	AnimationMap.Set( Anim_HumanFootmanWalkDownRight + FirstAnimID, HumanFootmanWalkDownRight )
  	
  	Local HumanFootmanWalkDownLeft : CAnimation = New CAnimation( WalkAnimDuration )
  	HumanFootmanWalkDownLeft.AddKeyframe( Image, 3, True ) 
  	HumanFootmanWalkDownLeft.AddKeyframe( Image, 11, True ) 
  	HumanFootmanWalkDownLeft.AddKeyframe( Image, 19, True ) 
  	HumanFootmanWalkDownLeft.AddKeyframe( Image, 27, True ) 
  	HumanFootmanWalkDownLeft.AddKeyframe( Image, 35, True ) 
  	AnimationMap.Set( Anim_HumanFootmanWalkDownLeft + FirstAnimID, HumanFootmanWalkDownLeft )
  	
  	'Attack sideways
  	Local HumanFootmanAttackUpRight : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackUpRight.AddKeyframe( Image, 41 ) 
  	HumanFootmanAttackUpRight.AddKeyframe( Image, 49 ) 
  	HumanFootmanAttackUpRight.AddKeyframe( Image, 57 ) 
  	HumanFootmanAttackUpRight.AddKeyframe( Image, 65 ) 
  	'HumanFootmanAttackUpRight.AddKeyframe( Image, 73 ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackUpRight + FirstAnimID, HumanFootmanAttackUpRight )
  	
  	Local HumanFootmanAttackUpLeft : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackUpLeft.AddKeyframe( Image, 41, True ) 
  	HumanFootmanAttackUpLeft.AddKeyframe( Image, 49, True ) 
  	HumanFootmanAttackUpLeft.AddKeyframe( Image, 57, True ) 
  	HumanFootmanAttackUpLeft.AddKeyframe( Image, 65, True ) 
  	'HumanFootmanAttackUpLeft.AddKeyframe( Image, 73, True )  
  	AnimationMap.Set( Anim_HumanFootmanAttackUpLeft + FirstAnimID, HumanFootmanAttackUpLeft )
  	
  	Local HumanFootmanAttackDownRight : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackDownRight.AddKeyframe( Image, 43 ) 
  	HumanFootmanAttackDownRight.AddKeyframe( Image, 51 ) 
  	HumanFootmanAttackDownRight.AddKeyframe( Image, 59 ) 
  	HumanFootmanAttackDownRight.AddKeyframe( Image, 67 ) 
  	'HumanFootmanAttackDownRight.AddKeyframe( Image, 75 ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackDownRight + FirstAnimID, HumanFootmanAttackDownRight )
  	
  	Local HumanFootmanAttackDownLeft : CAnimation = New CAnimation( AttackAnimDuration )
  	HumanFootmanAttackDownLeft.AddKeyframe( Image, 43, True ) 
  	HumanFootmanAttackDownLeft.AddKeyframe( Image, 51, True ) 
  	HumanFootmanAttackDownLeft.AddKeyframe( Image, 59, True ) 
  	HumanFootmanAttackDownLeft.AddKeyframe( Image, 67, True ) 
  	'HumanFootmanAttackDownLeft.AddKeyframe( Image, 75, True ) 
  	AnimationMap.Set( Anim_HumanFootmanAttackDownLeft + FirstAnimID, HumanFootmanAttackDownLeft )
  End
  
  Function GetAnimation : CAnimation( AnimationName )
  	Return AnimationMap.Get( AnimationName )
  End
End