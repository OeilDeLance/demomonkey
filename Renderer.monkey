Import Map
Import CommandBuffer
Import Sprite
Import GameObject
Import SquareItem

Class CRenderItem
	Field Z 		: Float = 0.0
	Field ID 		: Int
	Field Position 	: FVector 	= New FVector()
	Field Scale_ 	: FVector 	= New FVector( 1.0, 1.0 )
	Field Color 	: CColor 	= New CColor( 255, 255, 255 )
	
	Method New( ID : Int, Z : Float = 0.0 )
		Self.ID = ID
		Self.Z	= Z
	End
	
	Method Render( )
		
	End
	
	Method Update( DeltaTime: Float )
	End
	
	Method ProcessCommand( Command : CCommand )
		Select Command.Type
			Case CommandType_UpdatePosition			
				Self.Position.X = Command.ParamFloatA
				Self.Position.Y = Command.ParamFloatB
				'Print( "Updated pos( " + Position.X + ", " + Position.Y + " ) for item " + Self.ID )
			Case CommandType_UpdateScale
				'Print( "Updated Scale( " + Self.Scale_.X + ", " + Self.Scale_.Y + " ) for item " + Self.ID )
				Self.Scale_.X = Command.ParamFloatA 
				Self.Scale_.Y = Command.ParamFloatB
			Case CommandType_ChangeColor
				Self.Color.R = Command.ParamIntA
				Self.Color.G = Command.ParamIntB
				Self.Color.B = Command.ParamIntC
		End
	End
End

Class CRenderItemList Extends List< CRenderItem >
	Method Compare : Int( a : CRenderItem, b : CRenderItem )
		If a.Z > b.Z
			Return 1
		Else If a.Z < b.Z
			Return -1
		Else 
			Return 0
		End
	End
End

Class CRenderer
	Field RenderItemList 		: CRenderItemList 			= New CRenderItemList()
	Field RenderItemMap			: IntMap< CRenderItem > 	= New IntMap< CRenderItem >
		
	Method Update( DeltaTime: Float, CommandBuffer : CCommandBuffer )
		While CommandBuffer.IsEmpty() = False
			Local Command : CCommand = CommandBuffer.PopCommand()
			Select Command.Type
				Case CommandType_CreateMap
					Local Map : CMap = New CMap( Command.ID, Command.ParamFloatA )
					AddRenderItem( Map )
				Case CommandType_CreateSprite
					Local Sprite : CSprite = New CSprite( Command.ID, Command.ParamFloatA )
					AddRenderItem( Sprite )
				Case CommandType_SquareItem
					Local SquareItem : CSquareItem = New CSquareItem( Command.ID, Command.ParamFloatA )
					AddRenderItem( SquareItem )
				Default
					Local RenderItem := Self.RenderItemMap.Get( Command.ID )
					If RenderItem
						RenderItem.ProcessCommand( Command )
					End
			End
			CommandBuffer.RecycleCommand( Command )
		End
		
		For Local RenderItem := Eachin RenderItemList
			RenderItem.Update( DeltaTime )
		End
	End
	
	Method Render( GameObjectList : List< CGameObject > )
		Cls( 0,0,0 )
		
		PushMatrix()
		'Scale( SpriteScale, SpriteScale )
		For Local RenderItem := Eachin RenderItemList
			RenderItem.Render()
		End
		CAIManager.RenderDebug()
		For Local GameObject := Eachin GameObjectList
			GameObject.RenderDebug()
		End
		PopMatrix()
	End
	
	Method AddRenderItem( RenderItem : CRenderItem )
		'Print( "RenderItem Added ID: " + RenderItem.ID )
		'Add to the map for fast acces
		Self.RenderItemMap.Add( RenderItem.ID, RenderItem )
		
		' Add To the ordered Render list
		Local RenderItemNode := Self.RenderItemList.FirstNode
		If RenderItemNode = Null
			'List is Empty
			Self.RenderItemList.AddFirst( RenderItem )
			Return
		End
		
		'List is not empty: finding Node with bigger Z 
		While RenderItemNode
			If RenderItem.Z < RenderItemNode.Value.Z
				If RenderItemNode.PrevNode() = Null
					Self.RenderItemList.AddFirst( RenderItem )
				Else
					Local NewNode := New list.Node< CRenderItem >( RenderItemNode, RenderItemNode.PrevNode(), RenderItem)
				End
				Return
			End
			RenderItemNode = RenderItemNode.NextNode()
		End
		
		'Node with bigger Z not found inserting last
		Self.RenderItemList.AddLast( RenderItem )		
	End
End