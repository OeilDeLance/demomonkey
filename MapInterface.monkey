Import GameObject
Import InputArea
Import SpawnManager

Const MapInterfaceEvent_HumanFootmanMenuActivated 	:= 0
Const MapInterfaceEvent_OrcFootmanMenuActivated 	:= 1

Class CMapInterface Extends CComponent
	Field HumanFootmanMenuActivated 	:= False
	Field OrcFootmanMenuActivated 		:= False
	Field InputArea	 					: CInputArea
	Field PreviousInputAreaState		: Int = InputAreaState_Neutral
	
	Method New( )
		Super.New( ComponentType_MapInterface )
	End
	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )
		If InputArea = Null
			InputArea = CInputArea( Self.GameObject.GetComponent( ComponentType_InputArea ) )
		End
	
	
		If Self.InputArea.State = InputAreaState_Active And Self.PreviousInputAreaState <> InputAreaState_Active
			Local SpawnPosition 	:= New FVector( Self.InputArea.LastInputGlobalPosition.X / CAIManager.TileSize / CAIManager.MapScale, Self.InputArea.LastInputGlobalPosition.Y / CAIManager.TileSize / CAIManager.MapScale )
			Local PositionI 		: IVector 	= New IVector( SpawnPosition.X, SpawnPosition.Y )
			Local TileStatusData 	:= CAIManager.GetTileStatusData( PositionI )
			
			If TileStatusData.TileStatus = TileStatus_Free
				If Self.HumanFootmanMenuActivated
					'Print( "Human" )
					CSpawnManager.RequestSpawn( TemplateID_HumanFootman, SpawnPosition )
				Else If Self.OrcFootmanMenuActivated
					'Print( "Orc" )
					CSpawnManager.RequestSpawn( TemplateID_OrcFootman, SpawnPosition )
				End
			End
		End
		Self.PreviousInputAreaState = Self.InputArea.State
	End
	
	Method OnEvent( EventID : Int )
		Select EventID
			Case MapInterfaceEvent_HumanFootmanMenuActivated
				Self.HumanFootmanMenuActivated 	= True
				Self.OrcFootmanMenuActivated 	= False
			Case MapInterfaceEvent_OrcFootmanMenuActivated
				Self.OrcFootmanMenuActivated 	= True
				Self.HumanFootmanMenuActivated 	= False
		End
	End
End