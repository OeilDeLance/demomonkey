Import Misc
Import mojo

Const PathStatus_Pending 	: Int = 0
Const PathStatus_Ready 		: Int = 1
Const PathStatus_Error 		: Int = 2

Const TileStatus_Free		: Int = 0
Const TileStatus_Occupied	: Int = 1
Const TileStatus_Obstacle	: Int = 2

Class CPath 
	Field PathStatus 	: Int 				= PathStatus_Error
	Field StartPoint 	: IVector 			= New IVector()
	Field EndPoint 		: IVector 			= New IVector()
	Field NodeList 		: List< IVector >	= New List< IVector >()
	
	Method Request( StartPoint : IVector, EndPoint : IVector )
		Self.PathStatus 	= PathStatus_Pending
		Self.StartPoint.X 	= StartPoint.X
		Self.StartPoint.Y 	= StartPoint.Y
		Self.EndPoint.X		= EndPoint.X
		Self.EndPoint.Y		= EndPoint.Y
		
		CAIManager.AddRequest( Self )
	End		
End

Class COpenListItem
	Field Position : IVector 
	Field Estimate : Float
	
	Method Init( Position : IVector, Estimate : Float )
		Self.Position = Position
		Self.Estimate = Estimate
	End
End

Class COpenListItemList Extends List< COpenListItem >
	Method Compare : Int( a : COpenListItem, b : COpenListItem )
		If a.Estimate > b.Estimate Return 1
		If a.Estimate = b.Estimate Return 0
		Return -1
	End
End

Class PathfindingMapItem
	Field Obstacle  : Int  	= 0
	Field Explored 	: Bool 	= False
	Field CostSoFar : Float = 0
	Field CameFrom 	: Int 	= -1
End

Class CTileStatusData
	Field TileStatus 	:= TileStatus_Free
	'If the tile is occupied the Unit who booked it 
	'can be accessed with UnitID
	Field UnitID		:= -1
	
	Method CTileStatusData( TileStatus : Int = 0, UnitID : Int = -1 )
		Self.TileStatus = TileStatus
		Self.UnitID		= UnitID
	End
End

Class CAIManager
	Global TerrainDescriptionArray 	: Int[]
	Global TileSize					: Int 	= 32
	Global TerrainHeight			: Int 	= 18
	Global TerrainWidth				: Int 	= 30
	Global MapScale 				: Float = 1.5
	Global TerrainTileCount 		: Int
	Global PathQueue				: Deque< CPath > = New Deque< CPath >()
	
	'These are used for pathfinding itself
	Global PathfindingMap 			: PathfindingMapItem[] 
	Global OpenList 				: COpenListItemList = New COpenListItemList()
	Global NeighbourArray 			: IVector [ 8 ]			
	Global OpenListItemPool			: COpenListItem[]
	Global OpenListItemPoolCount 	: Int = 0
	
	Global TileStatusArray			: CTileStatusData[]
	
	Global DisplayDebug				:= False
	
	Function Init()
		'Terrain description
		'0 -> Random grass tile
		TerrainDescriptionArray =  [ 	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
										0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0,
										0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0,
										0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0,
										0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0,
										0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0	]
		TerrainTileCount = TerrainWidth * TerrainHeight
		
		PathfindingMap = New PathfindingMapItem[ TerrainTileCount ]
		For Local i : Int = 0 Until TerrainTileCount
			PathfindingMap[ i ] = New PathfindingMapItem()
			PathfindingMap[ i ].Obstacle = 0
			If TerrainDescriptionArray[ i ] = 1
				PathfindingMap[ i ].Obstacle = 1
			End
		End
		
		' Right
		NeighbourArray[ 0 ] = New IVector()
		NeighbourArray[ 0 ].X = 1
		NeighbourArray[ 0 ].Y = 0
		
		' Left
		NeighbourArray[ 1 ] = New IVector()
		NeighbourArray[ 1 ].X = -1
		NeighbourArray[ 1 ].Y = 0
		
		' Bottom
		NeighbourArray[ 2 ] = New IVector()
		NeighbourArray[ 2 ].X = 0
		NeighbourArray[ 2 ].Y = 1
		
		' Top
		NeighbourArray[ 3 ] = New IVector()
		NeighbourArray[ 3 ].X = 0
		NeighbourArray[ 3 ].Y = -1
		
		' Top Right
		NeighbourArray[ 4 ] = New IVector()
		NeighbourArray[ 4 ].X = 1
		NeighbourArray[ 4 ].Y = -1
		
		' Top Left
		NeighbourArray[ 5 ] = New IVector()
		NeighbourArray[ 5 ].X = -1
		NeighbourArray[ 5 ].Y = -1
		
		' Bottom Left
		NeighbourArray[ 6 ] = New IVector()
		NeighbourArray[ 6 ].X = -1
		NeighbourArray[ 6 ].Y = 1
		
		' Bottom Right
		NeighbourArray[ 7 ] = New IVector()
		NeighbourArray[ 7 ].X = 1
		NeighbourArray[ 7 ].Y = 1
		
		OpenListItemPool = New COpenListItem[ TerrainTileCount ]
		
		TileStatusArray = New CTileStatusData[ TerrainTileCount ]
		For Local i : Int = 0 Until TerrainTileCount
			TileStatusArray[ i ] = New CTileStatusData()
			If TerrainDescriptionArray[ i ] <> 0
				TileStatusArray[ i ].TileStatus = TileStatus_Obstacle
			End
		End
	End
	
	Function AddRequest( Path : CPath )
		PathQueue.PushLast( Path )
	End
	
	Function Update( DeltaTime : Float )
		If DeltaTime <> 0.0 And PathQueue.Length() <> 0
			Local Path : CPath = PathQueue.PopFirst()
			ComputePath( Path )
		End
	End
	
	Function PoolOpenListItem( OpenListItem : COpenListItem )
		If OpenListItemPoolCount < TerrainTileCount
			OpenListItemPool[ OpenListItemPoolCount ] = OpenListItem
			OpenListItemPoolCount += 1
		End ' Else don't pool garbage collect should do its job
	End
	
	Function GetOpenListItemFromPool : COpenListItem( )
		If  OpenListItemPoolCount = 0
			Return New COpenListItem()
		End
		Local OpenListItem : COpenListItem = OpenListItemPool[ OpenListItemPoolCount - 1 ]
		OpenListItemPoolCount -= 1
		Return OpenListItem
	End
	
	Function ComputePath( Path : CPath )
		
		'Reset Map data
		For Local i : Int = 0 Until TerrainTileCount
			PathfindingMap[ i ].Explored 	= False
			PathfindingMap[ i ].CostSoFar 	= 0
			PathfindingMap[ i ].CameFrom	= -1
		End
	
		OpenList.Clear()
		Local StartPointOpenListItem : COpenListItem = GetOpenListItemFromPool()
		StartPointOpenListItem.Init( Path.StartPoint, 0 )
		OpenList.AddLast( StartPointOpenListItem )	
		
		
		While OpenList.IsEmpty() = False
			OpenList.Sort()
			Local CurrentOpenListItem : COpenListItem = OpenList.First()
			OpenList.RemoveFirst()
			If CurrentOpenListItem.Position.X = Path.EndPoint.X And CurrentOpenListItem.Position.Y = Path.EndPoint.Y
				Exit
			End
			Local CurrentNodeIndex : Int = CurrentOpenListItem.Position.X + CurrentOpenListItem.Position.Y * TerrainWidth 
			For Local i : Int = 0 Until 8
				Local NeighbourPosition : IVector = New IVector()
				NeighbourPosition.X = CurrentOpenListItem.Position.X + NeighbourArray[ i ].X
				NeighbourPosition.Y = CurrentOpenListItem.Position.Y + NeighbourArray[ i ].Y
				Local NeighbourIndex : Int = NeighbourPosition.X + NeighbourPosition.Y * TerrainWidth 
				If NeighbourPosition.X >= 0.0 And NeighbourPosition.X < TerrainWidth And NeighbourPosition.Y >= 0 And NeighbourPosition.Y < TerrainHeight And PathfindingMap[ NeighbourIndex ].Obstacle = 0 
					Local Cost := 1.0
					If NeighbourArray[ i ].X <> 0.0 And NeighbourArray[ i ].Y <> 0.0
						Cost = 1.414213562 ' Sqrt( 2 )
					End
					Local NeighbourCost : Float = PathfindingMap[ CurrentNodeIndex ].CostSoFar + Cost
					If PathfindingMap[ NeighbourIndex ].Explored = False Or NeighbourCost < PathfindingMap[ NeighbourIndex ].CostSoFar
						PathfindingMap[ NeighbourIndex ].Explored = True
						PathfindingMap[ NeighbourIndex ].CostSoFar = NeighbourCost
						Local Heuristic : Float = Sqrt( (NeighbourPosition.X - Path.EndPoint.X) * (NeighbourPosition.X - Path.EndPoint.X) + (NeighbourPosition.Y - Path.EndPoint.Y) * (NeighbourPosition.Y - Path.EndPoint.Y) )
						Local NeighbourEstimate : Float = NeighbourCost + Heuristic
						
						Local NeighbourOpenListItem : COpenListItem = GetOpenListItemFromPool()
						NeighbourOpenListItem.Init( NeighbourPosition, NeighbourEstimate )
		
						OpenList.AddLast( NeighbourOpenListItem )
						PathfindingMap[ NeighbourIndex ].CameFrom = CurrentNodeIndex
					End
				End
			End
			PoolOpenListItem( CurrentOpenListItem )
		End		
		'Moving back up the chain to work the path out:
		Path.NodeList.Clear()
		Local NodeIndex : Int = Path.EndPoint.X + Path.EndPoint.Y * TerrainWidth 
		Path.NodeList.AddFirst( Path.EndPoint )
		While True 
			Local PreviousNodeIndex : Int = PathfindingMap[ NodeIndex ].CameFrom
			If PreviousNodeIndex = -1
				Path.PathStatus = PathStatus_Error
				Return
			End
			Local NodePosition : IVector = New IVector()
			NodePosition.X = PreviousNodeIndex Mod TerrainWidth
			NodePosition.Y = PreviousNodeIndex / TerrainWidth
			Path.NodeList.AddFirst( NodePosition )
			If NodePosition.X = Path.StartPoint.X And NodePosition.Y = Path.StartPoint.Y
				Exit
			End
			NodeIndex = PreviousNodeIndex
		End
		Path.PathStatus = PathStatus_Ready
	End
	
	Function IsTileValid : Bool( Position : IVector )
		Local Index := GetTileIndexFromPosition( Position )
		If CAIManager.TerrainDescriptionArray[ Index ] = 0
			Return True
		End
		Return False
	End
	
	Function GetTileIndexFromPosition( Position : IVector )
		Return Position.X + Position.Y * TerrainWidth
	End
	
	Function GetTilePositionFromIndex : IVector ( Index : Int )
		return New IVector( Index mod TerrainWidth, Index / TerrainWidth )
	End
	
	Function GetTileStatusData : CTileStatusData ( Position : IVector )
		Local Index := GetTileIndexFromPosition( Position )
		Return TileStatusArray[ Index ]
	End
	
	Function SetTileStatus( Position : IVector, TileStatus : Int, UnitID : Int = -1 )
		Local Index := GetTileIndexFromPosition( Position )
		TileStatusArray[ Index ].TileStatus = TileStatus
		TileStatusArray[ Index ].UnitID		= UnitID
	End
	
	Function RenderDebug()
		If DisplayDebug
			For Local i := 0 Until TerrainTileCount
				Local Text : String
				Select TileStatusArray[ i ].TileStatus
					Case TileStatus_Free
						Text = "Free"
					Case TileStatus_Occupied
						Text = "Occ(" + TileStatusArray[ i ].UnitID + ")"
				End
				Local TextPosition := GetTilePositionFromIndex( i ) 
				DrawText( Text, ( TextPosition.X + 0.5 ) * TileSize * MapScale, ( TextPosition.Y + 0.5 ) * TileSize * MapScale, 0.5, 0.5 )
			End
		End
	End
End