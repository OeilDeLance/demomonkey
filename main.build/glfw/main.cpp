
#include "main.h"

//${CONFIG_BEGIN}
#define CFG_BINARY_FILES *.bin|*.dat
#define CFG_BRL_GAMETARGET_IMPLEMENTED 1
#define CFG_BRL_THREAD_IMPLEMENTED 1
#define CFG_CONFIG debug
#define CFG_CPP_GC_MODE 1
#define CFG_GLFW_SWAP_INTERVAL -1
#define CFG_GLFW_USE_MINGW 1
#define CFG_GLFW_WINDOW_FULLSCREEN 0
#define CFG_GLFW_WINDOW_HEIGHT 864
#define CFG_GLFW_WINDOW_RESIZABLE 0
#define CFG_GLFW_WINDOW_TITLE Monkey Game
#define CFG_GLFW_WINDOW_WIDTH 1440
#define CFG_HOST winnt
#define CFG_IMAGE_FILES *.png|*.jpg
#define CFG_LANG cpp
#define CFG_MOJO_AUTO_SUSPEND_ENABLED 1
#define CFG_MOJO_DRIVER_IMPLEMENTED 1
#define CFG_MOJO_IMAGE_FILTERING_ENABLED false
#define CFG_MUSIC_FILES *.wav|*.ogg
#define CFG_OPENGL_DEPTH_BUFFER_ENABLED 0
#define CFG_OPENGL_GLES20_ENABLED 0
#define CFG_SAFEMODE 0
#define CFG_SOUND_FILES *.wav|*.ogg
#define CFG_TARGET glfw
#define CFG_TEXT_FILES *.txt|*.xml|*.json
//${CONFIG_END}

//${TRANSCODE_BEGIN}

// C++ Monkey runtime.
//
// Placed into the public domain 24/02/2011.
// No warranty implied; use at your own risk.

//***** Monkey Types *****

typedef wchar_t Char;
template<class T> class Array;
class String;
class Object;

#if CFG_CPP_DOUBLE_PRECISION_FLOATS
typedef double Float;
#define FLOAT(X) X
#else
typedef float Float;
#define FLOAT(X) X##f
#endif

void dbg_error( const char *p );

#if !_MSC_VER
#define sprintf_s sprintf
#define sscanf_s sscanf
#endif

//***** GC Config *****

#define DEBUG_GC 0

// GC mode:
//
// 0 = disabled
// 1 = Incremental GC every OnWhatever
// 2 = Incremental GC every allocation
//
#ifndef CFG_CPP_GC_MODE
#define CFG_CPP_GC_MODE 1
#endif

//How many bytes alloced to trigger GC
//
#ifndef CFG_CPP_GC_TRIGGER
#define CFG_CPP_GC_TRIGGER 8*1024*1024
#endif

//GC_MODE 2 needs to track locals on a stack - this may need to be bumped if your app uses a LOT of locals, eg: is heavily recursive...
//
#ifndef CFG_CPP_GC_MAX_LOCALS
#define CFG_CPP_GC_MAX_LOCALS 8192
#endif

// ***** GC *****

#if _WIN32

int gc_micros(){
	static int f;
	static LARGE_INTEGER pcf;
	if( !f ){
		if( QueryPerformanceFrequency( &pcf ) && pcf.QuadPart>=1000000L ){
			pcf.QuadPart/=1000000L;
			f=1;
		}else{
			f=-1;
		}
	}
	if( f>0 ){
		LARGE_INTEGER pc;
		if( QueryPerformanceCounter( &pc ) ) return pc.QuadPart/pcf.QuadPart;
		f=-1;
	}
	return 0;// timeGetTime()*1000;
}

#elif __APPLE__

#include <mach/mach_time.h>

int gc_micros(){
	static int f;
	static mach_timebase_info_data_t timeInfo;
	if( !f ){
		mach_timebase_info( &timeInfo );
		timeInfo.denom*=1000L;
		f=1;
	}
	return mach_absolute_time()*timeInfo.numer/timeInfo.denom;
}

#else

int gc_micros(){
	return 0;
}

#endif

#define gc_mark_roots gc_mark

void gc_mark_roots();

struct gc_object;

gc_object *gc_object_alloc( int size );
void gc_object_free( gc_object *p );

struct gc_object{
	gc_object *succ;
	gc_object *pred;
	int flags;
	
	virtual ~gc_object(){
	}
	
	virtual void mark(){
	}
	
	void *operator new( size_t size ){
		return gc_object_alloc( size );
	}
	
	void operator delete( void *p ){
		gc_object_free( (gc_object*)p );
	}
};

gc_object gc_free_list;
gc_object gc_marked_list;
gc_object gc_unmarked_list;
gc_object gc_queued_list;	//doesn't really need to be doubly linked...

int gc_free_bytes;
int gc_marked_bytes;
int gc_alloced_bytes;
int gc_max_alloced_bytes;
int gc_new_bytes;
int gc_markbit=1;

gc_object *gc_cache[8];

int gc_ctor_nest;
gc_object *gc_locals[CFG_CPP_GC_MAX_LOCALS],**gc_locals_sp=gc_locals;

void gc_collect_all();
void gc_mark_queued( int n );

#define GC_CLEAR_LIST( LIST ) ((LIST).succ=(LIST).pred=&(LIST))

#define GC_LIST_IS_EMPTY( LIST ) ((LIST).succ==&(LIST))

#define GC_REMOVE_NODE( NODE ){\
(NODE)->pred->succ=(NODE)->succ;\
(NODE)->succ->pred=(NODE)->pred;}

#define GC_INSERT_NODE( NODE,SUCC ){\
(NODE)->pred=(SUCC)->pred;\
(NODE)->succ=(SUCC);\
(SUCC)->pred->succ=(NODE);\
(SUCC)->pred=(NODE);}

void gc_init1(){
	GC_CLEAR_LIST( gc_free_list );
	GC_CLEAR_LIST( gc_marked_list );
	GC_CLEAR_LIST( gc_unmarked_list);
	GC_CLEAR_LIST( gc_queued_list );
}

void gc_init2(){
	gc_mark_roots();
}

#if CFG_CPP_GC_MODE==2

struct gc_ctor{
	gc_ctor(){ ++gc_ctor_nest; }
	~gc_ctor(){ --gc_ctor_nest; }
};

struct gc_enter{
	gc_object **sp;
	gc_enter():sp(gc_locals_sp){
	}
	~gc_enter(){
	/*
		static int max_locals;
		int n=gc_locals_sp-gc_locals;
		if( n>max_locals ){
			max_locals=n;
			printf( "max_locals=%i\n",n );
		}
	*/
		gc_locals_sp=sp;
	}
};

#define GC_CTOR gc_ctor _c;
#define GC_ENTER gc_enter _e;

#else

struct gc_ctor{
};
struct gc_enter{
};

#define GC_CTOR
#define GC_ENTER

#endif

void gc_flush_free( int size ){

	int t=gc_free_bytes-size;
	if( t<0 ) t=0;
	
	while( gc_free_bytes>t ){
		gc_object *p=gc_free_list.succ;
		if( !p || p==&gc_free_list ){
//			printf("ERROR:p=%p gc_free_bytes=%i\n",p,gc_free_bytes);
//			fflush(stdout);
			gc_free_bytes=0;
			break;
		}
		GC_REMOVE_NODE(p);
		delete p;	//...to gc_free
	}
}

void *gc_ext_malloc( int size ){

	gc_new_bytes+=size;
	
	gc_flush_free( size );
	
	return malloc( size );
}

void gc_ext_malloced( int size ){

	gc_new_bytes+=size;
	
	gc_flush_free( size );
}

gc_object *gc_object_alloc( int size ){

	size=(size+7)&~7;
	
#if CFG_CPP_GC_MODE==1

	gc_new_bytes+=size;
	
#elif CFG_CPP_GC_MODE==2

	if( !gc_ctor_nest ){
#if DEBUG_GC
		int ms=gc_micros();
#endif
		if( gc_new_bytes+size>(CFG_CPP_GC_TRIGGER) ){
			gc_collect_all();
			gc_new_bytes=size;
		}else{
			gc_new_bytes+=size;
			gc_mark_queued( (long long)(gc_new_bytes)*(gc_alloced_bytes-gc_new_bytes)/(CFG_CPP_GC_TRIGGER)+gc_new_bytes );
		}
		
#if DEBUG_GC
		ms=gc_micros()-ms;
		if( ms>=100 ) {printf( "gc time:%i\n",ms );fflush( stdout );}
#endif
	}

#endif

	gc_flush_free( size );

	gc_object *p;
	if( size<64 && (p=gc_cache[size>>3]) ){
		gc_cache[size>>3]=p->succ;
	}else{
		p=(gc_object*)malloc( size );
	}
	
	p->flags=size|gc_markbit;
	GC_INSERT_NODE( p,&gc_unmarked_list );

	gc_alloced_bytes+=size;
	if( gc_alloced_bytes>gc_max_alloced_bytes ) gc_max_alloced_bytes=gc_alloced_bytes;
	
#if CFG_CPP_GC_MODE==2
	*gc_locals_sp++=p;
#endif

	return p;
}

void gc_object_free( gc_object *p ){

	int size=p->flags & ~7;
	gc_free_bytes-=size;
	
	if( size<64 ){
		p->succ=gc_cache[size>>3];
		gc_cache[size>>3]=p;
	}else{
		free( p );
	}
}

template<class T> void gc_mark( T *t ){

	gc_object *p=dynamic_cast<gc_object*>(t);
	
	if( p && (p->flags & 3)==gc_markbit ){
		p->flags^=1;
		GC_REMOVE_NODE( p );
		GC_INSERT_NODE( p,&gc_marked_list );
		gc_marked_bytes+=(p->flags & ~7);
		p->mark();
	}
}

template<class T> void gc_mark_q( T *t ){

	gc_object *p=dynamic_cast<gc_object*>(t);
	
	if( p && (p->flags & 3)==gc_markbit ){
		p->flags^=1;
		GC_REMOVE_NODE( p );
		GC_INSERT_NODE( p,&gc_queued_list );
	}
}

template<class T> T *gc_retain( T *t ){
#if CFG_CPP_GC_MODE==2
	*gc_locals_sp++=dynamic_cast<gc_object*>( t );
#endif	
	return t;
}

template<class T,class V> void gc_assign( T *&lhs,V *rhs ){
	gc_object *p=dynamic_cast<gc_object*>(rhs);
	if( p && (p->flags & 3)==gc_markbit ){
		p->flags^=1;
		GC_REMOVE_NODE( p );
		GC_INSERT_NODE( p,&gc_queued_list );
	}
	lhs=rhs;
}

void gc_mark_locals(){
	for( gc_object **pp=gc_locals;pp!=gc_locals_sp;++pp ){
		gc_object *p=*pp;
		if( p && (p->flags & 3)==gc_markbit ){
			p->flags^=1;
			GC_REMOVE_NODE( p );
			GC_INSERT_NODE( p,&gc_marked_list );
			gc_marked_bytes+=(p->flags & ~7);
			p->mark();
		}
	}
}

void gc_mark_queued( int n ){
	while( gc_marked_bytes<n && !GC_LIST_IS_EMPTY( gc_queued_list ) ){
		gc_object *p=gc_queued_list.succ;
		GC_REMOVE_NODE( p );
		GC_INSERT_NODE( p,&gc_marked_list );
		gc_marked_bytes+=(p->flags & ~7);
		p->mark();
	}
}

//returns reclaimed bytes
int gc_sweep(){

	int reclaimed_bytes=gc_alloced_bytes-gc_marked_bytes;
	
	if( reclaimed_bytes ){
	
		//append unmarked list to end of free list
		gc_object *head=gc_unmarked_list.succ;
		gc_object *tail=gc_unmarked_list.pred;
		gc_object *succ=&gc_free_list;
		gc_object *pred=succ->pred;
		head->pred=pred;
		tail->succ=succ;
		pred->succ=head;
		succ->pred=tail;
		
		gc_free_bytes+=reclaimed_bytes;
	}
	
	//move marked to unmarked.
	gc_unmarked_list=gc_marked_list;
	gc_unmarked_list.succ->pred=gc_unmarked_list.pred->succ=&gc_unmarked_list;
	
	//clear marked.
	GC_CLEAR_LIST( gc_marked_list );
	
	//adjust sizes
	gc_alloced_bytes=gc_marked_bytes;
	gc_marked_bytes=0;
	gc_markbit^=1;
	
	return reclaimed_bytes;
}

void gc_collect_all(){

//	printf( "Mark locals\n" );fflush( stdout );
	gc_mark_locals();

//	printf( "Mark queued\n" );fflush( stdout );
	gc_mark_queued( 0x7fffffff );

//	printf( "sweep\n" );fflush( stdout );	
	gc_sweep();

//	printf( "Mark roots\n" );fflush( stdout );
	gc_mark_roots();

#if DEBUG_GC	
	printf( "gc collected:%i\n",reclaimed );fflush( stdout );
#endif
}

void gc_collect(){

	if( gc_locals_sp!=gc_locals ){
//		printf( "GC_LOCALS error\n" );fflush( stdout );
		gc_locals_sp=gc_locals;
	}
	
#if CFG_CPP_GC_MODE==1

#if DEBUG_GC
	int ms=gc_micros();
#endif

	if( gc_new_bytes>(CFG_CPP_GC_TRIGGER) ){
		gc_collect_all();
		gc_new_bytes=0;
	}else{
		gc_mark_queued( (long long)(gc_new_bytes)*(gc_alloced_bytes-gc_new_bytes)/(CFG_CPP_GC_TRIGGER)+gc_new_bytes );
	}

#if DEBUG_GC
	ms=gc_micros()-ms;
	if( ms>=100 ) {printf( "gc time:%i\n",ms );fflush( stdout );}
#endif

#endif

}

// ***** Array *****

template<class T> T *t_memcpy( T *dst,const T *src,int n ){
	memcpy( dst,src,n*sizeof(T) );
	return dst+n;
}

template<class T> T *t_memset( T *dst,int val,int n ){
	memset( dst,val,n*sizeof(T) );
	return dst+n;
}

template<class T> int t_memcmp( const T *x,const T *y,int n ){
	return memcmp( x,y,n*sizeof(T) );
}

template<class T> int t_strlen( const T *p ){
	const T *q=p++;
	while( *q++ ){}
	return q-p;
}

template<class T> T *t_create( int n,T *p ){
	t_memset( p,0,n );
	return p+n;
}

template<class T> T *t_create( int n,T *p,const T *q ){
	t_memcpy( p,q,n );
	return p+n;
}

template<class T> void t_destroy( int n,T *p ){
}

template<class T> void gc_mark_elements( int n,T *p ){
}

template<class T> void gc_mark_elements( int n,T **p ){
	for( int i=0;i<n;++i ) gc_mark( p[i] );
}

template<class T> class Array{
public:
	Array():rep( &nullRep ){
	}

	//Uses default...
//	Array( const Array<T> &t )...
	
	Array( int length ):rep( Rep::alloc( length ) ){
		t_create( rep->length,rep->data );
	}
	
	Array( const T *p,int length ):rep( Rep::alloc(length) ){
		t_create( rep->length,rep->data,p );
	}
	
	~Array(){
	}

	//Uses default...
//	Array &operator=( const Array &t )...
	
	int Length()const{ 
		return rep->length; 
	}
	
	T &At( int index ){
		if( index<0 || index>=rep->length ) dbg_error( "Array index out of range" );
		return rep->data[index]; 
	}
	
	const T &At( int index )const{
		if( index<0 || index>=rep->length ) dbg_error( "Array index out of range" );
		return rep->data[index]; 
	}
	
	T &operator[]( int index ){
		return rep->data[index]; 
	}

	const T &operator[]( int index )const{
		return rep->data[index]; 
	}
	
	Array Slice( int from,int term )const{
		int len=rep->length;
		if( from<0 ){ 
			from+=len;
			if( from<0 ) from=0;
		}else if( from>len ){
			from=len;
		}
		if( term<0 ){
			term+=len;
		}else if( term>len ){
			term=len;
		}
		if( term<=from ) return Array();
		return Array( rep->data+from,term-from );
	}

	Array Slice( int from )const{
		return Slice( from,rep->length );
	}
	
	Array Resize( int newlen )const{
		if( newlen<=0 ) return Array();
		int n=rep->length;
		if( newlen<n ) n=newlen;
		Rep *p=Rep::alloc( newlen );
		T *q=p->data;
		q=t_create( n,q,rep->data );
		q=t_create( (newlen-n),q );
		return Array( p );
	}
	
private:
	struct Rep : public gc_object{
		int length;
		T data[0];
		
		Rep():length(0){
			flags=3;
		}
		
		Rep( int length ):length(length){
		}
		
		~Rep(){
			t_destroy( length,data );
		}
		
		void mark(){
			gc_mark_elements( length,data );
		}
		
		static Rep *alloc( int length ){
			if( !length ) return &nullRep;
			void *p=gc_object_alloc( sizeof(Rep)+length*sizeof(T) );
			return ::new(p) Rep( length );
		}
		
	};
	Rep *rep;
	
	static Rep nullRep;
	
	template<class C> friend void gc_mark( Array<C> t );
	template<class C> friend void gc_mark_q( Array<C> t );
	template<class C> friend Array<C> gc_retain( Array<C> t );
	template<class C> friend void gc_assign( Array<C> &lhs,Array<C> rhs );
	template<class C> friend void gc_mark_elements( int n,Array<C> *p );
	
	Array( Rep *rep ):rep(rep){
	}
};

template<class T> typename Array<T>::Rep Array<T>::nullRep;

template<class T> Array<T> *t_create( int n,Array<T> *p ){
	for( int i=0;i<n;++i ) *p++=Array<T>();
	return p;
}

template<class T> Array<T> *t_create( int n,Array<T> *p,const Array<T> *q ){
	for( int i=0;i<n;++i ) *p++=*q++;
	return p;
}

template<class T> void gc_mark( Array<T> t ){
	gc_mark( t.rep );
}

template<class T> void gc_mark_q( Array<T> t ){
	gc_mark_q( t.rep );
}

template<class T> Array<T> gc_retain( Array<T> t ){
#if CFG_CPP_GC_MODE==2
	gc_retain( t.rep );
#endif
	return t;
}

template<class T> void gc_assign( Array<T> &lhs,Array<T> rhs ){
	gc_mark( rhs.rep );
	lhs=rhs;
}

template<class T> void gc_mark_elements( int n,Array<T> *p ){
	for( int i=0;i<n;++i ) gc_mark( p[i].rep );
}
		
// ***** String *****

static const char *_str_load_err;

class String{
public:
	String():rep( &nullRep ){
	}
	
	String( const String &t ):rep( t.rep ){
		rep->retain();
	}

	String( int n ){
		char buf[256];
		sprintf_s( buf,"%i",n );
		rep=Rep::alloc( t_strlen(buf) );
		for( int i=0;i<rep->length;++i ) rep->data[i]=buf[i];
	}
	
	String( Float n ){
		char buf[256];
		
		//would rather use snprintf, but it's doing weird things in MingW.
		//
		sprintf_s( buf,"%.17lg",n );
		//
		char *p;
		for( p=buf;*p;++p ){
			if( *p=='.' || *p=='e' ) break;
		}
		if( !*p ){
			*p++='.';
			*p++='0';
			*p=0;
		}

		rep=Rep::alloc( t_strlen(buf) );
		for( int i=0;i<rep->length;++i ) rep->data[i]=buf[i];
	}

	String( Char ch,int length ):rep( Rep::alloc(length) ){
		for( int i=0;i<length;++i ) rep->data[i]=ch;
	}

	String( const Char *p ):rep( Rep::alloc(t_strlen(p)) ){
		t_memcpy( rep->data,p,rep->length );
	}

	String( const Char *p,int length ):rep( Rep::alloc(length) ){
		t_memcpy( rep->data,p,rep->length );
	}
	
#if __OBJC__	
	String( NSString *nsstr ):rep( Rep::alloc([nsstr length]) ){
		unichar *buf=(unichar*)malloc( rep->length * sizeof(unichar) );
		[nsstr getCharacters:buf range:NSMakeRange(0,rep->length)];
		for( int i=0;i<rep->length;++i ) rep->data[i]=buf[i];
		free( buf );
	}
#endif

#if __cplusplus_winrt
	String( Platform::String ^str ):rep( Rep::alloc(str->Length()) ){
		for( int i=0;i<rep->length;++i ) rep->data[i]=str->Data()[i];
	}
#endif

	~String(){
		rep->release();
	}
	
	template<class C> String( const C *p ):rep( Rep::alloc(t_strlen(p)) ){
		for( int i=0;i<rep->length;++i ) rep->data[i]=p[i];
	}
	
	template<class C> String( const C *p,int length ):rep( Rep::alloc(length) ){
		for( int i=0;i<rep->length;++i ) rep->data[i]=p[i];
	}
	
	int Length()const{
		return rep->length;
	}
	
	const Char *Data()const{
		return rep->data;
	}
	
	Char At( int index )const{
		if( index<0 || index>=rep->length ) dbg_error( "Character index out of range" );
		return rep->data[index]; 
	}
	
	Char operator[]( int index )const{
		return rep->data[index];
	}
	
	String &operator=( const String &t ){
		t.rep->retain();
		rep->release();
		rep=t.rep;
		return *this;
	}
	
	String &operator+=( const String &t ){
		return operator=( *this+t );
	}
	
	int Compare( const String &t )const{
		int n=rep->length<t.rep->length ? rep->length : t.rep->length;
		for( int i=0;i<n;++i ){
			if( int q=(int)(rep->data[i])-(int)(t.rep->data[i]) ) return q;
		}
		return rep->length-t.rep->length;
	}
	
	bool operator==( const String &t )const{
		return rep->length==t.rep->length && t_memcmp( rep->data,t.rep->data,rep->length )==0;
	}
	
	bool operator!=( const String &t )const{
		return rep->length!=t.rep->length || t_memcmp( rep->data,t.rep->data,rep->length )!=0;
	}
	
	bool operator<( const String &t )const{
		return Compare( t )<0;
	}
	
	bool operator<=( const String &t )const{
		return Compare( t )<=0;
	}
	
	bool operator>( const String &t )const{
		return Compare( t )>0;
	}
	
	bool operator>=( const String &t )const{
		return Compare( t )>=0;
	}
	
	String operator+( const String &t )const{
		if( !rep->length ) return t;
		if( !t.rep->length ) return *this;
		Rep *p=Rep::alloc( rep->length+t.rep->length );
		Char *q=p->data;
		q=t_memcpy( q,rep->data,rep->length );
		q=t_memcpy( q,t.rep->data,t.rep->length );
		return String( p );
	}
	
	int Find( String find,int start=0 )const{
		if( start<0 ) start=0;
		while( start+find.rep->length<=rep->length ){
			if( !t_memcmp( rep->data+start,find.rep->data,find.rep->length ) ) return start;
			++start;
		}
		return -1;
	}
	
	int FindLast( String find )const{
		int start=rep->length-find.rep->length;
		while( start>=0 ){
			if( !t_memcmp( rep->data+start,find.rep->data,find.rep->length ) ) return start;
			--start;
		}
		return -1;
	}
	
	int FindLast( String find,int start )const{
		if( start>rep->length-find.rep->length ) start=rep->length-find.rep->length;
		while( start>=0 ){
			if( !t_memcmp( rep->data+start,find.rep->data,find.rep->length ) ) return start;
			--start;
		}
		return -1;
	}
	
	String Trim()const{
		int i=0,i2=rep->length;
		while( i<i2 && rep->data[i]<=32 ) ++i;
		while( i2>i && rep->data[i2-1]<=32 ) --i2;
		if( i==0 && i2==rep->length ) return *this;
		return String( rep->data+i,i2-i );
	}

	Array<String> Split( String sep )const{
	
		if( !sep.rep->length ){
			Array<String> bits( rep->length );
			for( int i=0;i<rep->length;++i ){
				bits[i]=String( (Char)(*this)[i],1 );
			}
			return bits;
		}
		
		int i=0,i2,n=1;
		while( (i2=Find( sep,i ))!=-1 ){
			++n;
			i=i2+sep.rep->length;
		}
		Array<String> bits( n );
		if( n==1 ){
			bits[0]=*this;
			return bits;
		}
		i=0;n=0;
		while( (i2=Find( sep,i ))!=-1 ){
			bits[n++]=Slice( i,i2 );
			i=i2+sep.rep->length;
		}
		bits[n]=Slice( i );
		return bits;
	}

	String Join( Array<String> bits )const{
		if( bits.Length()==0 ) return String();
		if( bits.Length()==1 ) return bits[0];
		int newlen=rep->length * (bits.Length()-1);
		for( int i=0;i<bits.Length();++i ){
			newlen+=bits[i].rep->length;
		}
		Rep *p=Rep::alloc( newlen );
		Char *q=p->data;
		q=t_memcpy( q,bits[0].rep->data,bits[0].rep->length );
		for( int i=1;i<bits.Length();++i ){
			q=t_memcpy( q,rep->data,rep->length );
			q=t_memcpy( q,bits[i].rep->data,bits[i].rep->length );
		}
		return String( p );
	}

	String Replace( String find,String repl )const{
		int i=0,i2,newlen=0;
		while( (i2=Find( find,i ))!=-1 ){
			newlen+=(i2-i)+repl.rep->length;
			i=i2+find.rep->length;
		}
		if( !i ) return *this;
		newlen+=rep->length-i;
		Rep *p=Rep::alloc( newlen );
		Char *q=p->data;
		i=0;
		while( (i2=Find( find,i ))!=-1 ){
			q=t_memcpy( q,rep->data+i,i2-i );
			q=t_memcpy( q,repl.rep->data,repl.rep->length );
			i=i2+find.rep->length;
		}
		q=t_memcpy( q,rep->data+i,rep->length-i );
		return String( p );
	}

	String ToLower()const{
		for( int i=0;i<rep->length;++i ){
			Char t=tolower( rep->data[i] );
			if( t==rep->data[i] ) continue;
			Rep *p=Rep::alloc( rep->length );
			Char *q=p->data;
			t_memcpy( q,rep->data,i );
			for( q[i++]=t;i<rep->length;++i ){
				q[i]=tolower( rep->data[i] );
			}
			return String( p );
		}
		return *this;
	}

	String ToUpper()const{
		for( int i=0;i<rep->length;++i ){
			Char t=toupper( rep->data[i] );
			if( t==rep->data[i] ) continue;
			Rep *p=Rep::alloc( rep->length );
			Char *q=p->data;
			t_memcpy( q,rep->data,i );
			for( q[i++]=t;i<rep->length;++i ){
				q[i]=toupper( rep->data[i] );
			}
			return String( p );
		}
		return *this;
	}
	
	bool Contains( String sub )const{
		return Find( sub )!=-1;
	}

	bool StartsWith( String sub )const{
		return sub.rep->length<=rep->length && !t_memcmp( rep->data,sub.rep->data,sub.rep->length );
	}

	bool EndsWith( String sub )const{
		return sub.rep->length<=rep->length && !t_memcmp( rep->data+rep->length-sub.rep->length,sub.rep->data,sub.rep->length );
	}
	
	String Slice( int from,int term )const{
		int len=rep->length;
		if( from<0 ){
			from+=len;
			if( from<0 ) from=0;
		}else if( from>len ){
			from=len;
		}
		if( term<0 ){
			term+=len;
		}else if( term>len ){
			term=len;
		}
		if( term<from ) return String();
		if( from==0 && term==len ) return *this;
		return String( rep->data+from,term-from );
	}

	String Slice( int from )const{
		return Slice( from,rep->length );
	}
	
	Array<int> ToChars()const{
		Array<int> chars( rep->length );
		for( int i=0;i<rep->length;++i ) chars[i]=rep->data[i];
		return chars;
	}
	
	int ToInt()const{
		char buf[64];
		return atoi( ToCString<char>( buf,sizeof(buf) ) );
	}
	
	Float ToFloat()const{
		char buf[256];
		return atof( ToCString<char>( buf,sizeof(buf) ) );
	}

	template<class C> class CString{
		struct Rep{
			int refs;
			C data[1];
		};
		Rep *_rep;
	public:
		template<class T> CString( const T *data,int length ){
			_rep=(Rep*)malloc( length*sizeof(C)+sizeof(Rep) );
			_rep->refs=1;
			_rep->data[length]=0;
			for( int i=0;i<length;++i ){
				_rep->data[i]=(C)data[i];
			}
		}
		CString( const CString &c ):_rep(c._rep){
			++_rep->refs;
		}
		~CString(){
			if( !--_rep->refs ) free( _rep );
		}
		CString &operator=( const CString &c ){
			++c._rep->refs;
			if( !--_rep->refs ) free( _rep );
			_rep=c._rep;
			return *this;
		}
		operator const C*()const{ 
			return _rep->data;
		}
	};
	
	template<class C> CString<C> ToCString()const{
		return CString<C>( rep->data,rep->length );
	}

	template<class C> C *ToCString( C *p,int length )const{
		if( --length>rep->length ) length=rep->length;
		for( int i=0;i<length;++i ) p[i]=rep->data[i];
		p[length]=0;
		return p;
	}

#if __OBJC__	
	NSString *ToNSString()const{
		return [NSString stringWithCharacters:ToCString<unichar>() length:rep->length];
	}
#endif

#if __cplusplus_winrt
	Platform::String ^ToWinRTString()const{
		return ref new Platform::String( rep->data,rep->length );
	}
#endif

	bool Save( FILE *fp ){
		std::vector<unsigned char> buf;
		Save( buf );
		return buf.size() ? fwrite( &buf[0],1,buf.size(),fp )==buf.size() : true;
	}
	
	void Save( std::vector<unsigned char> &buf ){
	
		Char *p=rep->data;
		Char *e=p+rep->length;
		
		while( p<e ){
			Char c=*p++;
			if( c<0x80 ){
				buf.push_back( c );
			}else if( c<0x800 ){
				buf.push_back( 0xc0 | (c>>6) );
				buf.push_back( 0x80 | (c & 0x3f) );
			}else{
				buf.push_back( 0xe0 | (c>>12) );
				buf.push_back( 0x80 | ((c>>6) & 0x3f) );
				buf.push_back( 0x80 | (c & 0x3f) );
			}
		}
	}
	
	static String FromChars( Array<int> chars ){
		int n=chars.Length();
		Rep *p=Rep::alloc( n );
		for( int i=0;i<n;++i ){
			p->data[i]=chars[i];
		}
		return String( p );
	}

	static String Load( FILE *fp ){
		unsigned char tmp[4096];
		std::vector<unsigned char> buf;
		for(;;){
			int n=fread( tmp,1,4096,fp );
			if( n>0 ) buf.insert( buf.end(),tmp,tmp+n );
			if( n!=4096 ) break;
		}
		return buf.size() ? String::Load( &buf[0],buf.size() ) : String();
	}
	
	static String Load( unsigned char *p,int n ){
	
		_str_load_err=0;
		
		unsigned char *e=p+n;
		std::vector<Char> chars;
		
		int t0=n>0 ? p[0] : -1;
		int t1=n>1 ? p[1] : -1;

		if( t0==0xfe && t1==0xff ){
			p+=2;
			while( p<e-1 ){
				int c=*p++;
				chars.push_back( (c<<8)|*p++ );
			}
		}else if( t0==0xff && t1==0xfe ){
			p+=2;
			while( p<e-1 ){
				int c=*p++;
				chars.push_back( (*p++<<8)|c );
			}
		}else{
			int t2=n>2 ? p[2] : -1;
			if( t0==0xef && t1==0xbb && t2==0xbf ) p+=3;
			unsigned char *q=p;
			bool fail=false;
			while( p<e ){
				unsigned int c=*p++;
				if( c & 0x80 ){
					if( (c & 0xe0)==0xc0 ){
						if( p>=e || (p[0] & 0xc0)!=0x80 ){
							fail=true;
							break;
						}
						c=((c & 0x1f)<<6) | (p[0] & 0x3f);
						p+=1;
					}else if( (c & 0xf0)==0xe0 ){
						if( p+1>=e || (p[0] & 0xc0)!=0x80 || (p[1] & 0xc0)!=0x80 ){
							fail=true;
							break;
						}
						c=((c & 0x0f)<<12) | ((p[0] & 0x3f)<<6) | (p[1] & 0x3f);
						p+=2;
					}else{
						fail=true;
						break;
					}
				}
				chars.push_back( c );
			}
			if( fail ){
				_str_load_err="Invalid UTF-8";
				return String( q,n );
			}
		}
		return chars.size() ? String( &chars[0],chars.size() ) : String();
	}

private:
	
	struct Rep{
		int refs;
		int length;
		Char data[0];
		
		Rep():refs(1),length(0){
		}
		
		Rep( int length ):refs(1),length(length){
		}
		
		void retain(){
			++refs;
		}
		
		void release(){
			if( --refs || !length ) return;
			free( this );
		}

		static Rep *alloc( int length ){
			if( !length ) return &nullRep;
			void *p=malloc( sizeof(Rep)+length*sizeof(Char) );
			return new(p) Rep( length );
		}
	};
	Rep *rep;
	
	static Rep nullRep;
	
	String( Rep *rep ):rep(rep){
	}
};

String::Rep String::nullRep;

String *t_create( int n,String *p ){
	for( int i=0;i<n;++i ) new( &p[i] ) String();
	return p+n;
}

String *t_create( int n,String *p,const String *q ){
	for( int i=0;i<n;++i ) new( &p[i] ) String( q[i] );
	return p+n;
}

void t_destroy( int n,String *p ){
	for( int i=0;i<n;++i ) p[i].~String();
}

// ***** Object *****

String dbg_stacktrace();

class Object : public gc_object{
public:
	virtual bool Equals( Object *obj ){
		return this==obj;
	}
	
	virtual int Compare( Object *obj ){
		return (char*)this-(char*)obj;
	}
	
	virtual String debug(){
		return "+Object\n";
	}
};

class ThrowableObject : public Object{
#ifndef NDEBUG
public:
	String stackTrace;
	ThrowableObject():stackTrace( dbg_stacktrace() ){}
#endif
};

struct gc_interface{
	virtual ~gc_interface(){}
};

//***** Debugger *****

//#define Error bbError
//#define Print bbPrint

int bbPrint( String t );

#define dbg_stream stderr

#if _MSC_VER
#define dbg_typeof decltype
#else
#define dbg_typeof __typeof__
#endif 

struct dbg_func;
struct dbg_var_type;

static int dbg_suspend;
static int dbg_stepmode;

const char *dbg_info;
String dbg_exstack;

static void *dbg_var_buf[65536*3];
static void **dbg_var_ptr=dbg_var_buf;

static dbg_func *dbg_func_buf[1024];
static dbg_func **dbg_func_ptr=dbg_func_buf;

String dbg_type( bool *p ){
	return "Bool";
}

String dbg_type( int *p ){
	return "Int";
}

String dbg_type( Float *p ){
	return "Float";
}

String dbg_type( String *p ){
	return "String";
}

template<class T> String dbg_type( T *p ){
	return "Object";
}

template<class T> String dbg_type( Array<T> *p ){
	return dbg_type( &(*p)[0] )+"[]";
}

String dbg_value( bool *p ){
	return *p ? "True" : "False";
}

String dbg_value( int *p ){
	return String( *p );
}

String dbg_value( Float *p ){
	return String( *p );
}

String dbg_value( String *p ){
	String t=*p;
	if( t.Length()>100 ) t=t.Slice( 0,100 )+"...";
	t=t.Replace( "\"","~q" );
	t=t.Replace( "\t","~t" );
	t=t.Replace( "\n","~n" );
	t=t.Replace( "\r","~r" );
	return String("\"")+t+"\"";
}

template<class T> String dbg_value( T *t ){
	Object *p=dynamic_cast<Object*>( *t );
	char buf[64];
	sprintf_s( buf,"%p",p );
	return String("@") + (buf[0]=='0' && buf[1]=='x' ? buf+2 : buf );
}

template<class T> String dbg_value( Array<T> *p ){
	String t="[";
	int n=(*p).Length();
	for( int i=0;i<n;++i ){
		if( i ) t+=",";
		t+=dbg_value( &(*p)[i] );
	}
	return t+"]";
}

template<class T> String dbg_decl( const char *id,T *ptr ){
	return String( id )+":"+dbg_type(ptr)+"="+dbg_value(ptr)+"\n";
}

struct dbg_var_type{
	virtual String type( void *p )=0;
	virtual String value( void *p )=0;
};

template<class T> struct dbg_var_type_t : public dbg_var_type{

	String type( void *p ){
		return dbg_type( (T*)p );
	}
	
	String value( void *p ){
		return dbg_value( (T*)p );
	}
	
	static dbg_var_type_t<T> info;
};
template<class T> dbg_var_type_t<T> dbg_var_type_t<T>::info;

struct dbg_blk{
	void **var_ptr;
	
	dbg_blk():var_ptr(dbg_var_ptr){
		if( dbg_stepmode=='l' ) --dbg_suspend;
	}
	
	~dbg_blk(){
		if( dbg_stepmode=='l' ) ++dbg_suspend;
		dbg_var_ptr=var_ptr;
	}
};

struct dbg_func : public dbg_blk{
	const char *id;
	const char *info;

	dbg_func( const char *p ):id(p),info(dbg_info){
		*dbg_func_ptr++=this;
		if( dbg_stepmode=='s' ) --dbg_suspend;
	}
	
	~dbg_func(){
		if( dbg_stepmode=='s' ) ++dbg_suspend;
		--dbg_func_ptr;
		dbg_info=info;
	}
};

int dbg_print( String t ){
	static char *buf;
	static int len;
	int n=t.Length();
	if( n+100>len ){
		len=n+100;
		free( buf );
		buf=(char*)malloc( len );
	}
	buf[n]='\n';
	for( int i=0;i<n;++i ) buf[i]=t[i];
	fwrite( buf,n+1,1,dbg_stream );
	fflush( dbg_stream );
	return 0;
}

void dbg_callstack(){

	void **var_ptr=dbg_var_buf;
	dbg_func **func_ptr=dbg_func_buf;
	
	while( var_ptr!=dbg_var_ptr ){
		while( func_ptr!=dbg_func_ptr && var_ptr==(*func_ptr)->var_ptr ){
			const char *id=(*func_ptr++)->id;
			const char *info=func_ptr!=dbg_func_ptr ? (*func_ptr)->info : dbg_info;
			fprintf( dbg_stream,"+%s;%s\n",id,info );
		}
		void *vp=*var_ptr++;
		const char *nm=(const char*)*var_ptr++;
		dbg_var_type *ty=(dbg_var_type*)*var_ptr++;
		dbg_print( String(nm)+":"+ty->type(vp)+"="+ty->value(vp) );
	}
	while( func_ptr!=dbg_func_ptr ){
		const char *id=(*func_ptr++)->id;
		const char *info=func_ptr!=dbg_func_ptr ? (*func_ptr)->info : dbg_info;
		fprintf( dbg_stream,"+%s;%s\n",id,info );
	}
}

String dbg_stacktrace(){
	if( !dbg_info || !dbg_info[0] ) return "";
	String str=String( dbg_info )+"\n";
	dbg_func **func_ptr=dbg_func_ptr;
	if( func_ptr==dbg_func_buf ) return str;
	while( --func_ptr!=dbg_func_buf ){
		str+=String( (*func_ptr)->info )+"\n";
	}
	return str;
}

void dbg_throw( const char *err ){
	dbg_exstack=dbg_stacktrace();
	throw err;
}

void dbg_stop(){

#if TARGET_OS_IPHONE
	dbg_throw( "STOP" );
#endif

	fprintf( dbg_stream,"{{~~%s~~}}\n",dbg_info );
	dbg_callstack();
	dbg_print( "" );
	
	for(;;){

		char buf[256];
		char *e=fgets( buf,256,stdin );
		if( !e ) exit( -1 );
		
		e=strchr( buf,'\n' );
		if( !e ) exit( -1 );
		
		*e=0;
		
		Object *p;
		
		switch( buf[0] ){
		case '?':
			break;
		case 'r':	//run
			dbg_suspend=0;		
			dbg_stepmode=0;
			return;
		case 's':	//step
			dbg_suspend=1;
			dbg_stepmode='s';
			return;
		case 'e':	//enter func
			dbg_suspend=1;
			dbg_stepmode='e';
			return;
		case 'l':	//leave block
			dbg_suspend=0;
			dbg_stepmode='l';
			return;
		case '@':	//dump object
			p=0;
			sscanf_s( buf+1,"%p",&p );
			if( p ){
				dbg_print( p->debug() );
			}else{
				dbg_print( "" );
			}
			break;
		case 'q':	//quit!
			exit( 0 );
			break;			
		default:
			printf( "????? %s ?????",buf );fflush( stdout );
			exit( -1 );
		}
	}
}

void dbg_error( const char *err ){

#if TARGET_OS_IPHONE
	dbg_throw( err );
#endif

	for(;;){
		bbPrint( String("Monkey Runtime Error : ")+err );
		bbPrint( dbg_stacktrace() );
		dbg_stop();
	}
}

#define DBG_INFO(X) dbg_info=(X);if( dbg_suspend>0 ) dbg_stop();

#define DBG_ENTER(P) dbg_func _dbg_func(P);

#define DBG_BLOCK() dbg_blk _dbg_blk;

#define DBG_GLOBAL( ID,NAME )	//TODO!

#define DBG_LOCAL( ID,NAME )\
*dbg_var_ptr++=&ID;\
*dbg_var_ptr++=(void*)NAME;\
*dbg_var_ptr++=&dbg_var_type_t<dbg_typeof(ID)>::info;

//**** main ****

int argc;
const char **argv;

Float D2R=0.017453292519943295f;
Float R2D=57.29577951308232f;

int bbPrint( String t ){

	static std::vector<unsigned char> buf;
	buf.clear();
	t.Save( buf );
	buf.push_back( '\n' );
	buf.push_back( 0 );
	
#if __cplusplus_winrt	//winrt?

#if CFG_WINRT_PRINT_ENABLED
	OutputDebugStringA( (const char*)&buf[0] );
#endif

#elif _WIN32			//windows?

	fputs( (const char*)&buf[0],stdout );
	fflush( stdout );

#elif __APPLE__			//macos/ios?

	fputs( (const char*)&buf[0],stdout );
	fflush( stdout );
	
#elif __linux			//linux?

#if CFG_ANDROID_PRINT_ENABLED
	LOGI( (const char*)&buf[0] );
#else
	fputs( (const char*)&buf[0],stdout );
	fflush( stdout );
#endif

#endif

	return 0;
}

class BBExitApp{
};

int bbError( String err ){
	if( !err.Length() ){
#if __cplusplus_winrt
		throw BBExitApp();
#else
		exit( 0 );
#endif
	}
	dbg_error( err.ToCString<char>() );
	return 0;
}

int bbDebugLog( String t ){
	bbPrint( t );
	return 0;
}

int bbDebugStop(){
	dbg_stop();
	return 0;
}

int bbInit();
int bbMain();

#if _MSC_VER

static void _cdecl seTranslator( unsigned int ex,EXCEPTION_POINTERS *p ){

	switch( ex ){
	case EXCEPTION_ACCESS_VIOLATION:dbg_error( "Memory access violation" );
	case EXCEPTION_ILLEGAL_INSTRUCTION:dbg_error( "Illegal instruction" );
	case EXCEPTION_INT_DIVIDE_BY_ZERO:dbg_error( "Integer divide by zero" );
	case EXCEPTION_STACK_OVERFLOW:dbg_error( "Stack overflow" );
	}
	dbg_error( "Unknown exception" );
}

#else

void sighandler( int sig  ){
	switch( sig ){
	case SIGSEGV:dbg_error( "Memory access violation" );
	case SIGILL:dbg_error( "Illegal instruction" );
	case SIGFPE:dbg_error( "Floating point exception" );
#if !_WIN32
	case SIGBUS:dbg_error( "Bus error" );
#endif	
	}
	dbg_error( "Unknown signal" );
}

#endif

//entry point call by target main()...
//
int bb_std_main( int argc,const char **argv ){

	::argc=argc;
	::argv=argv;
	
#if _MSC_VER

	_set_se_translator( seTranslator );

#else
	
	signal( SIGSEGV,sighandler );
	signal( SIGILL,sighandler );
	signal( SIGFPE,sighandler );
#if !_WIN32
	signal( SIGBUS,sighandler );
#endif

#endif

	gc_init1();

	bbInit();
	
	gc_init2();

	bbMain();

	return 0;
}


//***** game.h *****

struct BBGameEvent{
	enum{
		None=0,
		KeyDown=1,KeyUp=2,KeyChar=3,
		MouseDown=4,MouseUp=5,MouseMove=6,
		TouchDown=7,TouchUp=8,TouchMove=9,
		MotionAccel=10
	};
};

class BBGameDelegate : public Object{
public:
	virtual void StartGame(){}
	virtual void SuspendGame(){}
	virtual void ResumeGame(){}
	virtual void UpdateGame(){}
	virtual void RenderGame(){}
	virtual void KeyEvent( int event,int data ){}
	virtual void MouseEvent( int event,int data,float x,float y ){}
	virtual void TouchEvent( int event,int data,float x,float y ){}
	virtual void MotionEvent( int event,int data,float x,float y,float z ){}
	virtual void DiscardGraphics(){}
};

class BBGame{
public:
	BBGame();
	virtual ~BBGame(){}
	
	static BBGame *Game(){ return _game; }
	
	virtual void SetDelegate( BBGameDelegate *delegate );
	virtual BBGameDelegate *Delegate(){ return _delegate; }
	
	virtual void SetKeyboardEnabled( bool enabled );
	virtual bool KeyboardEnabled();
	
	virtual void SetUpdateRate( int updateRate );
	virtual int UpdateRate();
	
	virtual bool Started(){ return _started; }
	virtual bool Suspended(){ return _suspended; }
	
	virtual int Millisecs();
	virtual void GetDate( Array<int> date );
	virtual int SaveState( String state );
	virtual String LoadState();
	virtual String LoadString( String path );
	virtual bool PollJoystick( int port,Array<Float> joyx,Array<Float> joyy,Array<Float> joyz,Array<bool> buttons );
	virtual void OpenUrl( String url );
	virtual void SetMouseVisible( bool visible );
	
	//***** cpp extensions *****
	virtual String PathToFilePath( String path );
	virtual FILE *OpenFile( String path,String mode );
	virtual unsigned char *LoadData( String path,int *plength );
	
	//***** INTERNAL *****
	virtual void Die( ThrowableObject *ex );
	virtual void gc_collect();
	virtual void StartGame();
	virtual void SuspendGame();
	virtual void ResumeGame();
	virtual void UpdateGame();
	virtual void RenderGame();
	virtual void KeyEvent( int ev,int data );
	virtual void MouseEvent( int ev,int data,float x,float y );
	virtual void TouchEvent( int ev,int data,float x,float y );
	virtual void MotionEvent( int ev,int data,float x,float y,float z );
	virtual void DiscardGraphics();

protected:

	static BBGame *_game;

	BBGameDelegate *_delegate;
	bool _keyboardEnabled;
	int _updateRate;
	bool _started;
	bool _suspended;
};

//***** game.cpp *****

BBGame *BBGame::_game;

BBGame::BBGame():
_delegate( 0 ),
_keyboardEnabled( false ),
_updateRate( 0 ),
_started( false ),
_suspended( false ){
	_game=this;
}

void BBGame::SetDelegate( BBGameDelegate *delegate ){
	_delegate=delegate;
}

void BBGame::SetKeyboardEnabled( bool enabled ){
	_keyboardEnabled=enabled;
}

bool BBGame::KeyboardEnabled(){
	return _keyboardEnabled;
}

void BBGame::SetUpdateRate( int updateRate ){
	_updateRate=updateRate;
}

int BBGame::UpdateRate(){
	return _updateRate;
}

int BBGame::Millisecs(){
	return 0;
}

void BBGame::GetDate( Array<int> date ){
	int n=date.Length();
	if( n>0 ){
		time_t t=time( 0 );
		
#if _MSC_VER
		struct tm tii;
		struct tm *ti=&tii;
		localtime_s( ti,&t );
#else
		struct tm *ti=localtime( &t );
#endif

		date[0]=ti->tm_year+1900;
		if( n>1 ){ 
			date[1]=ti->tm_mon+1;
			if( n>2 ){
				date[2]=ti->tm_mday;
				if( n>3 ){
					date[3]=ti->tm_hour;
					if( n>4 ){
						date[4]=ti->tm_min;
						if( n>5 ){
							date[5]=ti->tm_sec;
							if( n>6 ){
								date[6]=0;
							}
						}
					}
				}
			}
		}
	}
}

int BBGame::SaveState( String state ){
	if( FILE *f=OpenFile( "./.monkeystate","wb" ) ){
		bool ok=state.Save( f );
		fclose( f );
		return ok ? 0 : -2;
	}
	return -1;
}

String BBGame::LoadState(){
	if( FILE *f=OpenFile( "./.monkeystate","rb" ) ){
		String str=String::Load( f );
		fclose( f );
		return str;
	}
	return "";
}

String BBGame::LoadString( String path ){
	if( FILE *fp=OpenFile( path,"rb" ) ){
		String str=String::Load( fp );
		fclose( fp );
		return str;
	}
	return "";
}

bool BBGame::PollJoystick( int port,Array<Float> joyx,Array<Float> joyy,Array<Float> joyz,Array<bool> buttons ){
	return false;
}

void BBGame::OpenUrl( String url ){
}

void BBGame::SetMouseVisible( bool visible ){
}

//***** C++ Game *****

String BBGame::PathToFilePath( String path ){
	return path;
}

FILE *BBGame::OpenFile( String path,String mode ){
	path=PathToFilePath( path );
	if( path=="" ) return 0;
	
#if __cplusplus_winrt
	path=path.Replace( "/","\\" );
	FILE *f;
	if( _wfopen_s( &f,path.ToCString<wchar_t>(),mode.ToCString<wchar_t>() ) ) return 0;
	return f;
#elif _WIN32
	return _wfopen( path.ToCString<wchar_t>(),mode.ToCString<wchar_t>() );
#else
	return fopen( path.ToCString<char>(),mode.ToCString<char>() );
#endif
}

unsigned char *BBGame::LoadData( String path,int *plength ){

	FILE *f=OpenFile( path,"rb" );
	if( !f ) return 0;

	const int BUF_SZ=4096;
	std::vector<void*> tmps;
	int length=0;
	
	for(;;){
		void *p=malloc( BUF_SZ );
		int n=fread( p,1,BUF_SZ,f );
		tmps.push_back( p );
		length+=n;
		if( n!=BUF_SZ ) break;
	}
	fclose( f );
	
	unsigned char *data=(unsigned char*)malloc( length );
	unsigned char *p=data;
	
	int sz=length;
	for( int i=0;i<tmps.size();++i ){
		int n=sz>BUF_SZ ? BUF_SZ : sz;
		memcpy( p,tmps[i],n );
		free( tmps[i] );
		sz-=n;
		p+=n;
	}
	
	*plength=length;
	
	gc_ext_malloced( length );
	
	return data;
}

//***** INTERNAL *****

void BBGame::Die( ThrowableObject *ex ){
	bbPrint( "Monkey Runtime Error : Uncaught Monkey Exception" );
#ifndef NDEBUG
	bbPrint( ex->stackTrace );
#endif
	exit( -1 );
}

void BBGame::gc_collect(){
	gc_mark( _delegate );
	::gc_collect();
}

void BBGame::StartGame(){

	if( _started ) return;
	_started=true;
	
	try{
		_delegate->StartGame();
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::SuspendGame(){

	if( !_started || _suspended ) return;
	_suspended=true;
	
	try{
		_delegate->SuspendGame();
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::ResumeGame(){

	if( !_started || !_suspended ) return;
	_suspended=false;
	
	try{
		_delegate->ResumeGame();
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::UpdateGame(){

	if( !_started || _suspended ) return;
	
	try{
		_delegate->UpdateGame();
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::RenderGame(){

	if( !_started ) return;
	
	try{
		_delegate->RenderGame();
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::KeyEvent( int ev,int data ){

	if( !_started ) return;
	
	try{
		_delegate->KeyEvent( ev,data );
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::MouseEvent( int ev,int data,float x,float y ){

	if( !_started ) return;
	
	try{
		_delegate->MouseEvent( ev,data,x,y );
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::TouchEvent( int ev,int data,float x,float y ){

	if( !_started ) return;
	
	try{
		_delegate->TouchEvent( ev,data,x,y );
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::MotionEvent( int ev,int data,float x,float y,float z ){

	if( !_started ) return;
	
	try{
		_delegate->MotionEvent( ev,data,x,y,z );
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}

void BBGame::DiscardGraphics(){

	if( !_started ) return;
	
	try{
		_delegate->DiscardGraphics();
	}catch( ThrowableObject *ex ){
		Die( ex );
	}
	gc_collect();
}


//***** wavloader.h *****
//
unsigned char *LoadWAV( FILE *f,int *length,int *channels,int *format,int *hertz );

//***** wavloader.cpp *****
//
static const char *readTag( FILE *f ){
	static char buf[8];
	if( fread( buf,4,1,f )!=1 ) return "";
	buf[4]=0;
	return buf;
}

static int readInt( FILE *f ){
	unsigned char buf[4];
	if( fread( buf,4,1,f )!=1 ) return -1;
	return (buf[3]<<24) | (buf[2]<<16) | (buf[1]<<8) | buf[0];
}

static int readShort( FILE *f ){
	unsigned char buf[2];
	if( fread( buf,2,1,f )!=1 ) return -1;
	return (buf[1]<<8) | buf[0];
}

static void skipBytes( int n,FILE *f ){
	char *p=(char*)malloc( n );
	fread( p,n,1,f );
	free( p );
}

unsigned char *LoadWAV( FILE *f,int *plength,int *pchannels,int *pformat,int *phertz ){
	if( !strcmp( readTag( f ),"RIFF" ) ){
		int len=readInt( f )-8;len=len;
		if( !strcmp( readTag( f ),"WAVE" ) ){
			if( !strcmp( readTag( f ),"fmt " ) ){
				int len2=readInt( f );
				int comp=readShort( f );
				if( comp==1 ){
					int chans=readShort( f );
					int hertz=readInt( f );
					int bytespersec=readInt( f );bytespersec=bytespersec;
					int pad=readShort( f );pad=pad;
					int bits=readShort( f );
					int format=bits/8;
					if( len2>16 ) skipBytes( len2-16,f );
					for(;;){
						const char *p=readTag( f );
						if( feof( f ) ) break;
						int size=readInt( f );
						if( strcmp( p,"data" ) ){
							skipBytes( size,f );
							continue;
						}
						unsigned char *data=(unsigned char*)malloc( size );
						if( fread( data,size,1,f )==1 ){
							*plength=size/(chans*format);
							*pchannels=chans;
							*pformat=format;
							*phertz=hertz;
							return data;
						}
						free( data );
					}
				}
			}
		}
	}
	return 0;
}



//***** oggloader.h *****
unsigned char *LoadOGG( FILE *f,int *length,int *channels,int *format,int *hertz );

//***** oggloader.cpp *****
unsigned char *LoadOGG( FILE *f,int *length,int *channels,int *format,int *hertz ){

	int error;
	stb_vorbis *v=stb_vorbis_open_file( f,0,&error,0 );
	if( !v ) return 0;
	
	stb_vorbis_info info=stb_vorbis_get_info( v );
	
	int limit=info.channels*4096;
	int offset=0,total=limit;

	short *data=(short*)malloc( total*2 );
	
	for(;;){
		int n=stb_vorbis_get_frame_short_interleaved( v,info.channels,data+offset,total-offset );
		if( !n ) break;
	
		offset+=n*info.channels;
		
		if( offset+limit>total ){
			total*=2;
			data=(short*)realloc( data,total*2 );
		}
	}
	
	data=(short*)realloc( data,offset*2 );
	
	*length=offset/info.channels;
	*channels=info.channels;
	*format=2;
	*hertz=info.sample_rate;
	
	stb_vorbis_close(v);
	
	return (unsigned char*)data;
}



//***** glfwgame.h *****

struct BBGlfwVideoMode : public Object{
	int Width;
	int Height;
	int RedBits;
	int GreenBits;
	int BlueBits;
	BBGlfwVideoMode( int w,int h,int r,int g,int b ):Width(w),Height(h),RedBits(r),GreenBits(g),BlueBits(b){}
};

class BBGlfwGame : public BBGame{
public:
	BBGlfwGame();

	static BBGlfwGame *GlfwGame(){ return _glfwGame; }
	
	virtual void SetUpdateRate( int hertz );
	virtual int Millisecs();
	virtual bool PollJoystick( int port,Array<Float> joyx,Array<Float> joyy,Array<Float> joyz,Array<bool> buttons );
	virtual void OpenUrl( String url );
	virtual void SetMouseVisible( bool visible );

	virtual String PathToFilePath( String path );

	virtual unsigned char *LoadImageData( String path,int *width,int *height,int *depth );
	virtual unsigned char *LoadAudioData( String path,int *length,int *channels,int *format,int *hertz );
	
	BBGlfwVideoMode *GetGlfwDesktopMode();
	Array<BBGlfwVideoMode*> GetGlfwVideoModes();
	virtual void SetGlfwWindow( int width,int height,int red,int green,int blue,int alpha,int depth,int stencil,bool fullscreen );
	virtual void Run();
	
private:
	static BBGlfwGame *_glfwGame;

	bool _iconified;
	
	double _nextUpdate;
	double _updatePeriod;
		
protected:
	static int TransKey( int key );
	static int KeyToChar( int key );
	
	static void GLFWCALL OnKey( int key,int action );
	static void GLFWCALL OnChar( int chr,int action );
	static void GLFWCALL OnMouseButton( int button,int action );
	static void GLFWCALL OnMousePos( int x,int y );
	static int  GLFWCALL OnWindowClose();
};

//***** glfwgame.cpp *****

#define _QUOTE(X) #X
#define _STRINGIZE( X ) _QUOTE(X)

enum{
	VKEY_BACKSPACE=8,VKEY_TAB,
	VKEY_ENTER=13,
	VKEY_SHIFT=16,
	VKEY_CONTROL=17,
	VKEY_ESC=27,
	VKEY_SPACE=32,
	VKEY_PAGEUP=33,VKEY_PAGEDOWN,VKEY_END,VKEY_HOME,
	VKEY_LEFT=37,VKEY_UP,VKEY_RIGHT,VKEY_DOWN,
	VKEY_INSERT=45,VKEY_DELETE,
	VKEY_0=48,VKEY_1,VKEY_2,VKEY_3,VKEY_4,VKEY_5,VKEY_6,VKEY_7,VKEY_8,VKEY_9,
	VKEY_A=65,VKEY_B,VKEY_C,VKEY_D,VKEY_E,VKEY_F,VKEY_G,VKEY_H,VKEY_I,VKEY_J,
	VKEY_K,VKEY_L,VKEY_M,VKEY_N,VKEY_O,VKEY_P,VKEY_Q,VKEY_R,VKEY_S,VKEY_T,
	VKEY_U,VKEY_V,VKEY_W,VKEY_X,VKEY_Y,VKEY_Z,
	
	VKEY_LSYS=91,VKEY_RSYS,
	
	VKEY_NUM0=96,VKEY_NUM1,VKEY_NUM2,VKEY_NUM3,VKEY_NUM4,
	VKEY_NUM5,VKEY_NUM6,VKEY_NUM7,VKEY_NUM8,VKEY_NUM9,
	VKEY_NUMMULTIPLY=106,VKEY_NUMADD,VKEY_NUMSLASH,
	VKEY_NUMSUBTRACT,VKEY_NUMDECIMAL,VKEY_NUMDIVIDE,

	VKEY_F1=112,VKEY_F2,VKEY_F3,VKEY_F4,VKEY_F5,VKEY_F6,
	VKEY_F7,VKEY_F8,VKEY_F9,VKEY_F10,VKEY_F11,VKEY_F12,

	VKEY_LSHIFT=160,VKEY_RSHIFT,
	VKEY_LCONTROL=162,VKEY_RCONTROL,
	VKEY_LALT=164,VKEY_RALT,

	VKEY_TILDE=192,VKEY_MINUS=189,VKEY_EQUALS=187,
	VKEY_OPENBRACKET=219,VKEY_BACKSLASH=220,VKEY_CLOSEBRACKET=221,
	VKEY_SEMICOLON=186,VKEY_QUOTES=222,
	VKEY_COMMA=188,VKEY_PERIOD=190,VKEY_SLASH=191
};

BBGlfwGame *BBGlfwGame::_glfwGame;

BBGlfwGame::BBGlfwGame():
_iconified( false ){
	_glfwGame=this;
}

//***** BBGame *****

void Init_GL_Exts();

int glfwGraphicsSeq=0;

void BBGlfwGame::SetUpdateRate( int updateRate ){
	BBGame::SetUpdateRate( updateRate );
	if( _updateRate ){
		_updatePeriod=1.0/_updateRate;
		_nextUpdate=glfwGetTime()+_updatePeriod;
	}
}

int BBGlfwGame::Millisecs(){
	return int( glfwGetTime()*1000.0 );
}

bool BBGlfwGame::PollJoystick( int port,Array<Float> joyx,Array<Float> joyy,Array<Float> joyz,Array<bool> buttons ){

	int joy=GLFW_JOYSTICK_1+port;
	if( !glfwGetJoystickParam( joy,GLFW_PRESENT ) ) return false;

	int n_axes=glfwGetJoystickParam( joy,GLFW_AXES );
	int n_buttons=glfwGetJoystickParam( joy,GLFW_BUTTONS );

	float pos[6];
	memset( pos,0,sizeof(pos) );	
	glfwGetJoystickPos( joy,pos,n_axes );
	
	float t;
	switch( n_axes ){
	case 4:	//my saitek...axes=4, buttons=14
		pos[4]=pos[2];
		pos[3]=pos[3];
		pos[2]=0;
		break;
	case 5:	//xbox360...axes=5, buttons=10
		t=pos[3];
		pos[3]=pos[4];
		pos[4]=t;
		break;
	}
	
	joyx[0]=pos[0];joyx[1]=pos[3];
	joyy[0]=pos[1];joyy[1]=pos[4];
	joyz[0]=pos[2];joyz[1]=pos[5];

	//Buttons...
	//	
	unsigned char buts[32];
	memset( buts,0,sizeof(buts) );
	glfwGetJoystickButtons( port,buts,n_buttons );

	for( int i=0;i<n_buttons;++i ) buttons[i]=(buts[i]==GLFW_PRESS);
	return true;
}

void BBGlfwGame::OpenUrl( String url ){
#if _WIN32
	ShellExecute( HWND_DESKTOP,"open",url.ToCString<char>(),0,0,SW_SHOWNORMAL );
#elif __APPLE__
	if( CFURLRef cfurl=CFURLCreateWithBytes( 0,url.ToCString<UInt8>(),url.Length(),kCFStringEncodingASCII,0 ) ){
		LSOpenCFURLRef( cfurl,0 );
		CFRelease( cfurl );
	}
#elif __linux
	system( ( String( "xdg-open \"" )+url+"\"" ).ToCString<char>() );
#endif
}

void BBGlfwGame::SetMouseVisible( bool visible ){
	if( visible ){
		glfwEnable( GLFW_MOUSE_CURSOR );
	}else{
		glfwDisable( GLFW_MOUSE_CURSOR );
	}
}

String BBGlfwGame::PathToFilePath( String path ){
	if( !path.StartsWith( "monkey:" ) ){
		return path;
	}else if( path.StartsWith( "monkey://data/" ) ){
		return String("./data/")+path.Slice(14);
	}else if( path.StartsWith( "monkey://internal/" ) ){
		return String("./internal/")+path.Slice(18);
	}else if( path.StartsWith( "monkey://external/" ) ){
		return String("./external/")+path.Slice(18);
	}
	return "";
}

unsigned char *BBGlfwGame::LoadImageData( String path,int *width,int *height,int *depth ){

	FILE *f=OpenFile( path,"rb" );
	if( !f ) return 0;
	
	unsigned char *data=stbi_load_from_file( f,width,height,depth,0 );
	fclose( f );
	
	if( data ) gc_ext_malloced( (*width)*(*height)*(*depth) );
	
	return data;
}

unsigned char *BBGlfwGame::LoadAudioData( String path,int *length,int *channels,int *format,int *hertz ){

	FILE *f=OpenFile( path,"rb" );
	if( !f ) return 0;
	
	unsigned char *data=0;
	
	if( path.ToLower().EndsWith( ".wav" ) ){
		data=LoadWAV( f,length,channels,format,hertz );
	}else if( path.ToLower().EndsWith( ".ogg" ) ){
		data=LoadOGG( f,length,channels,format,hertz );
	}
	fclose( f );
	
	if( data ) gc_ext_malloced( (*length)*(*channels)*(*format) );
	
	return data;
}

//glfw key to monkey key!
int BBGlfwGame::TransKey( int key ){

	if( key>='0' && key<='9' ) return key;
	if( key>='A' && key<='Z' ) return key;

	switch( key ){

	case ' ':return VKEY_SPACE;
	case ';':return VKEY_SEMICOLON;
	case '=':return VKEY_EQUALS;
	case ',':return VKEY_COMMA;
	case '-':return VKEY_MINUS;
	case '.':return VKEY_PERIOD;
	case '/':return VKEY_SLASH;
	case '~':return VKEY_TILDE;
	case '[':return VKEY_OPENBRACKET;
	case ']':return VKEY_CLOSEBRACKET;
	case '\"':return VKEY_QUOTES;
	case '\\':return VKEY_BACKSLASH;
	
	case '`':return VKEY_TILDE;
	case '\'':return VKEY_QUOTES;

	case GLFW_KEY_LSHIFT:
	case GLFW_KEY_RSHIFT:return VKEY_SHIFT;
	case GLFW_KEY_LCTRL:
	case GLFW_KEY_RCTRL:return VKEY_CONTROL;
	
//	case GLFW_KEY_LSHIFT:return VKEY_LSHIFT;
//	case GLFW_KEY_RSHIFT:return VKEY_RSHIFT;
//	case GLFW_KEY_LCTRL:return VKEY_LCONTROL;
//	case GLFW_KEY_RCTRL:return VKEY_RCONTROL;
	
	case GLFW_KEY_BACKSPACE:return VKEY_BACKSPACE;
	case GLFW_KEY_TAB:return VKEY_TAB;
	case GLFW_KEY_ENTER:return VKEY_ENTER;
	case GLFW_KEY_ESC:return VKEY_ESC;
	case GLFW_KEY_INSERT:return VKEY_INSERT;
	case GLFW_KEY_DEL:return VKEY_DELETE;
	case GLFW_KEY_PAGEUP:return VKEY_PAGEUP;
	case GLFW_KEY_PAGEDOWN:return VKEY_PAGEDOWN;
	case GLFW_KEY_HOME:return VKEY_HOME;
	case GLFW_KEY_END:return VKEY_END;
	case GLFW_KEY_UP:return VKEY_UP;
	case GLFW_KEY_DOWN:return VKEY_DOWN;
	case GLFW_KEY_LEFT:return VKEY_LEFT;
	case GLFW_KEY_RIGHT:return VKEY_RIGHT;
	
	case GLFW_KEY_KP_0:return VKEY_NUM0;
	case GLFW_KEY_KP_1:return VKEY_NUM1;
	case GLFW_KEY_KP_2:return VKEY_NUM2;
	case GLFW_KEY_KP_3:return VKEY_NUM3;
	case GLFW_KEY_KP_4:return VKEY_NUM4;
	case GLFW_KEY_KP_5:return VKEY_NUM5;
	case GLFW_KEY_KP_6:return VKEY_NUM6;
	case GLFW_KEY_KP_7:return VKEY_NUM7;
	case GLFW_KEY_KP_8:return VKEY_NUM8;
	case GLFW_KEY_KP_9:return VKEY_NUM9;
	case GLFW_KEY_KP_DIVIDE:return VKEY_NUMDIVIDE;
	case GLFW_KEY_KP_MULTIPLY:return VKEY_NUMMULTIPLY;
	case GLFW_KEY_KP_SUBTRACT:return VKEY_NUMSUBTRACT;
	case GLFW_KEY_KP_ADD:return VKEY_NUMADD;
	case GLFW_KEY_KP_DECIMAL:return VKEY_NUMDECIMAL;
    	
	case GLFW_KEY_F1:return VKEY_F1;
	case GLFW_KEY_F2:return VKEY_F2;
	case GLFW_KEY_F3:return VKEY_F3;
	case GLFW_KEY_F4:return VKEY_F4;
	case GLFW_KEY_F5:return VKEY_F5;
	case GLFW_KEY_F6:return VKEY_F6;
	case GLFW_KEY_F7:return VKEY_F7;
	case GLFW_KEY_F8:return VKEY_F8;
	case GLFW_KEY_F9:return VKEY_F9;
	case GLFW_KEY_F10:return VKEY_F10;
	case GLFW_KEY_F11:return VKEY_F11;
	case GLFW_KEY_F12:return VKEY_F12;
	}
	return 0;
}

//monkey key to special monkey char
int BBGlfwGame::KeyToChar( int key ){
	switch( key ){
	case VKEY_BACKSPACE:
	case VKEY_TAB:
	case VKEY_ENTER:
	case VKEY_ESC:
		return key;
	case VKEY_PAGEUP:
	case VKEY_PAGEDOWN:
	case VKEY_END:
	case VKEY_HOME:
	case VKEY_LEFT:
	case VKEY_UP:
	case VKEY_RIGHT:
	case VKEY_DOWN:
	case VKEY_INSERT:
		return key | 0x10000;
	case VKEY_DELETE:
		return 127;
	}
	return 0;
}

void BBGlfwGame::OnMouseButton( int button,int action ){
	switch( button ){
	case GLFW_MOUSE_BUTTON_LEFT:button=0;break;
	case GLFW_MOUSE_BUTTON_RIGHT:button=1;break;
	case GLFW_MOUSE_BUTTON_MIDDLE:button=2;break;
	default:return;
	}
	int x,y;
	glfwGetMousePos( &x,&y );
	switch( action ){
	case GLFW_PRESS:
		_glfwGame->MouseEvent( BBGameEvent::MouseDown,button,x,y );
		break;
	case GLFW_RELEASE:
		_glfwGame->MouseEvent( BBGameEvent::MouseUp,button,x,y );
		break;
	}
}

void BBGlfwGame::OnMousePos( int x,int y ){
	_game->MouseEvent( BBGameEvent::MouseMove,-1,x,y );
}

int BBGlfwGame::OnWindowClose(){
	_game->KeyEvent( BBGameEvent::KeyDown,0x1b0 );
	_game->KeyEvent( BBGameEvent::KeyUp,0x1b0 );
	return GL_FALSE;
}

void BBGlfwGame::OnKey( int key,int action ){

	key=TransKey( key );
	if( !key ) return;
	
	switch( action ){
	case GLFW_PRESS:
		_glfwGame->KeyEvent( BBGameEvent::KeyDown,key );
		if( int chr=KeyToChar( key ) ) _game->KeyEvent( BBGameEvent::KeyChar,chr );
		break;
	case GLFW_RELEASE:
		_glfwGame->KeyEvent( BBGameEvent::KeyUp,key );
		break;
	}
}

void BBGlfwGame::OnChar( int chr,int action ){

	switch( action ){
	case GLFW_PRESS:
		_glfwGame->KeyEvent( BBGameEvent::KeyChar,chr );
		break;
	}
}

BBGlfwVideoMode *BBGlfwGame::GetGlfwDesktopMode(){

	GLFWvidmode mode;
	glfwGetDesktopMode( &mode );
	
	return new BBGlfwVideoMode( mode.Width,mode.Height,mode.RedBits,mode.GreenBits,mode.BlueBits );
}

Array<BBGlfwVideoMode*> BBGlfwGame::GetGlfwVideoModes(){
	GLFWvidmode modes[1024];
	int n=glfwGetVideoModes( modes,1024 );
	Array<BBGlfwVideoMode*> bbmodes( n );
	for( int i=0;i<n;++i ){
		bbmodes[i]=new BBGlfwVideoMode( modes[i].Width,modes[i].Height,modes[i].RedBits,modes[i].GreenBits,modes[i].BlueBits );
	}
	return bbmodes;
}

void BBGlfwGame::SetGlfwWindow( int width,int height,int red,int green,int blue,int alpha,int depth,int stencil,bool fullscreen ){

	for( int i=0;i<=GLFW_KEY_LAST;++i ){
		int key=TransKey( i );
		if( key && glfwGetKey( i )==GLFW_PRESS ) KeyEvent( BBGameEvent::KeyUp,key );
	}

	GLFWvidmode desktopMode;
	glfwGetDesktopMode( &desktopMode );

	glfwCloseWindow();
	
	glfwOpenWindowHint( GLFW_WINDOW_NO_RESIZE,CFG_GLFW_WINDOW_RESIZABLE ? GL_FALSE : GL_TRUE );

	glfwOpenWindow( width,height,red,green,blue,alpha,depth,stencil,fullscreen ? GLFW_FULLSCREEN : GLFW_WINDOW );

	++glfwGraphicsSeq;

	if( !fullscreen ){	
		glfwSetWindowPos( (desktopMode.Width-width)/2,(desktopMode.Height-height)/2 );
		glfwSetWindowTitle( _STRINGIZE(CFG_GLFW_WINDOW_TITLE) );
	}

#if CFG_OPENGL_INIT_EXTENSIONS
	Init_GL_Exts();
#endif

#if CFG_GLFW_SWAP_INTERVAL>=0
	glfwSwapInterval( CFG_GLFW_SWAP_INTERVAL );
#endif

	glfwEnable( GLFW_KEY_REPEAT );
	glfwDisable( GLFW_AUTO_POLL_EVENTS );
	glfwSetKeyCallback( OnKey );
	glfwSetCharCallback( OnChar );
	glfwSetMouseButtonCallback( OnMouseButton );
	glfwSetMousePosCallback( OnMousePos );
	glfwSetWindowCloseCallback(	OnWindowClose );
}

void BBGlfwGame::Run(){

#if	CFG_GLFW_WINDOW_WIDTH && CFG_GLFW_WINDOW_HEIGHT

	SetGlfwWindow( CFG_GLFW_WINDOW_WIDTH,CFG_GLFW_WINDOW_HEIGHT,8,8,8,0,CFG_OPENGL_DEPTH_BUFFER_ENABLED ? 32 : 0,0,CFG_GLFW_WINDOW_FULLSCREEN );

#endif

	StartGame();

	RenderGame();
	
	for( ;; ){
	
		glfwPollEvents();
		
		if( !glfwGetWindowParam( GLFW_OPENED ) ) break;
	
		if( glfwGetWindowParam( GLFW_ICONIFIED ) ){
			if( !_suspended ){
				_iconified=true;
				SuspendGame();
			}
		}else if( glfwGetWindowParam( GLFW_ACTIVE ) ){
			if( _suspended ){
				_iconified=false;
				ResumeGame();
				_nextUpdate=glfwGetTime();
			}
		}else if( CFG_MOJO_AUTO_SUSPEND_ENABLED ){
			if( !_suspended ){
				SuspendGame();
			}
		}
	
		if( !_updateRate || _suspended ){
			if( !_iconified ) RenderGame();
			glfwWaitEvents();
			continue;
		}
		
		double delay=_nextUpdate-glfwGetTime();
		if( delay>0 ){
			glfwSleep( delay );
			continue;
		}

		int updates;
		for( updates=0;updates<4;++updates ){
			_nextUpdate+=_updatePeriod;
			UpdateGame();
			if( !_updateRate ) break;
			delay=_nextUpdate-glfwGetTime();
			if( delay>0 ) break;
		}
		
		RenderGame();
		
		if( !_updateRate ) continue;
		
		if( updates==4 ) _nextUpdate=glfwGetTime();
	}
}



//***** monkeygame.h *****

class BBMonkeyGame : public BBGlfwGame{
public:

	static void Main( int args,const char *argv[] );
};

//***** monkeygame.cpp *****

#define _QUOTE(X) #X
#define _STRINGIZE(X) _QUOTE(X)

void BBMonkeyGame::Main( int argc,const char *argv[] ){

	if( !glfwInit() ){
		puts( "glfwInit failed" );
		exit(-1);
	}

	BBMonkeyGame *game=new BBMonkeyGame();
	
	try{
	
		bb_std_main( argc,argv );
		
	}catch( ThrowableObject *ex ){
	
		glfwTerminate();
		
		game->Die( ex );
		
		return;
	}

	if( game->Delegate() ) game->Run();
	
	glfwTerminate();
}


// GLFW mojo runtime.
//
// Copyright 2011 Mark Sibly, all rights reserved.
// No warranty implied; use at your own risk.

//***** gxtkGraphics.h *****

class gxtkSurface;

class gxtkGraphics : public Object{
public:

	enum{
		MAX_VERTS=1024,
		MAX_QUADS=(MAX_VERTS/4)
	};

	int width;
	int height;

	int colorARGB;
	float r,g,b,alpha;
	float ix,iy,jx,jy,tx,ty;
	bool tformed;

	float vertices[MAX_VERTS*5];
	unsigned short quadIndices[MAX_QUADS*6];

	int primType;
	int vertCount;
	gxtkSurface *primSurf;
	
	gxtkGraphics();
	
	void Flush();
	float *Begin( int type,int count,gxtkSurface *surf );
	
	//***** GXTK API *****
	virtual int Width();
	virtual int Height();
	
	virtual int  BeginRender();
	virtual void EndRender();
	virtual void DiscardGraphics();

	virtual gxtkSurface *LoadSurface( String path );
	virtual gxtkSurface *LoadSurface__UNSAFE__( gxtkSurface *surface,String path );
	virtual gxtkSurface *CreateSurface( int width,int height );
	
	virtual int Cls( float r,float g,float b );
	virtual int SetAlpha( float alpha );
	virtual int SetColor( float r,float g,float b );
	virtual int SetBlend( int blend );
	virtual int SetScissor( int x,int y,int w,int h );
	virtual int SetMatrix( float ix,float iy,float jx,float jy,float tx,float ty );
	
	virtual int DrawPoint( float x,float y );
	virtual int DrawRect( float x,float y,float w,float h );
	virtual int DrawLine( float x1,float y1,float x2,float y2 );
	virtual int DrawOval( float x1,float y1,float x2,float y2 );
	virtual int DrawPoly( Array<Float> verts );
	virtual int DrawPoly2( Array<Float> verts,gxtkSurface *surface,int srcx,int srcy );
	virtual int DrawSurface( gxtkSurface *surface,float x,float y );
	virtual int DrawSurface2( gxtkSurface *surface,float x,float y,int srcx,int srcy,int srcw,int srch );
	
	virtual int ReadPixels( Array<int> pixels,int x,int y,int width,int height,int offset,int pitch );
	virtual int WritePixels2( gxtkSurface *surface,Array<int> pixels,int x,int y,int width,int height,int offset,int pitch );
};

class gxtkSurface : public Object{
public:
	unsigned char *data;
	int width;
	int height;
	int depth;
	int format;
	int seq;
	
	GLuint texture;
	float uscale;
	float vscale;
	
	gxtkSurface();
	
	void SetData( unsigned char *data,int width,int height,int depth );
	void SetSubData( int x,int y,int w,int h,unsigned *src,int pitch );
	void Bind();
	
	~gxtkSurface();
	
	//***** GXTK API *****
	virtual int Discard();
	virtual int Width();
	virtual int Height();
	virtual int Loaded();
	virtual bool OnUnsafeLoadComplete();
};

//***** gxtkGraphics.cpp *****

#ifndef GL_BGRA
#define GL_BGRA  0x80e1
#endif

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812f
#endif

#ifndef GL_GENERATE_MIPMAP
#define GL_GENERATE_MIPMAP 0x8191
#endif

static int Pow2Size( int n ){
	int i=1;
	while( i<n ) i+=i;
	return i;
}

gxtkGraphics::gxtkGraphics(){

	width=height=0;
#ifdef _glfw3_h_
	glfwGetWindowSize( BBGlfwGame::GlfwGame()->GetGLFWwindow(),&width,&height );
#else
	glfwGetWindowSize( &width,&height );
#endif
	
	if( CFG_OPENGL_GLES20_ENABLED ) return;
	
	for( int i=0;i<MAX_QUADS;++i ){
		quadIndices[i*6  ]=(short)(i*4);
		quadIndices[i*6+1]=(short)(i*4+1);
		quadIndices[i*6+2]=(short)(i*4+2);
		quadIndices[i*6+3]=(short)(i*4);
		quadIndices[i*6+4]=(short)(i*4+2);
		quadIndices[i*6+5]=(short)(i*4+3);
	}
}

void gxtkGraphics::Flush(){
	if( !vertCount ) return;

	if( primSurf ){
		glEnable( GL_TEXTURE_2D );
		primSurf->Bind();
	}
		
	switch( primType ){
	case 1:
		glDrawArrays( GL_POINTS,0,vertCount );
		break;
	case 2:
		glDrawArrays( GL_LINES,0,vertCount );
		break;
	case 3:
		glDrawArrays( GL_TRIANGLES,0,vertCount );
		break;
	case 4:
		glDrawElements( GL_TRIANGLES,vertCount/4*6,GL_UNSIGNED_SHORT,quadIndices );
		break;
	default:
		for( int j=0;j<vertCount;j+=primType ){
			glDrawArrays( GL_TRIANGLE_FAN,j,primType );
		}
		break;
	}

	if( primSurf ){
		glDisable( GL_TEXTURE_2D );
	}

	vertCount=0;
}

float *gxtkGraphics::Begin( int type,int count,gxtkSurface *surf ){
	if( primType!=type || primSurf!=surf || vertCount+count>MAX_VERTS ){
		Flush();
		primType=type;
		primSurf=surf;
	}
	float *vp=vertices+vertCount*5;
	vertCount+=count;
	return vp;
}

//***** GXTK API *****

int gxtkGraphics::Width(){
	return width;
}

int gxtkGraphics::Height(){
	return height;
}

int gxtkGraphics::BeginRender(){

	width=height=0;
#ifdef _glfw3_h_
	glfwGetWindowSize( BBGlfwGame::GlfwGame()->GetGLFWwindow(),&width,&height );
#else
	glfwGetWindowSize( &width,&height );
#endif
	
	if( CFG_OPENGL_GLES20_ENABLED ) return 0;
	
	glViewport( 0,0,width,height );

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho( 0,width,height,0,-1,1 );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	
	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( 2,GL_FLOAT,20,&vertices[0] );	
	
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	glTexCoordPointer( 2,GL_FLOAT,20,&vertices[2] );
	
	glEnableClientState( GL_COLOR_ARRAY );
	glColorPointer( 4,GL_UNSIGNED_BYTE,20,&vertices[4] );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_ONE,GL_ONE_MINUS_SRC_ALPHA );
	
	glDisable( GL_TEXTURE_2D );
	
	vertCount=0;
	
	return 1;
}

void gxtkGraphics::EndRender(){
	if( !CFG_OPENGL_GLES20_ENABLED ) Flush();
#ifdef _glfw3_h_
	glfwSwapBuffers( BBGlfwGame::GlfwGame()->GetGLFWwindow() );
#else
	glfwSwapBuffers();
#endif

}

void gxtkGraphics::DiscardGraphics(){
}

int gxtkGraphics::Cls( float r,float g,float b ){
	vertCount=0;

	glClearColor( r/255.0f,g/255.0f,b/255.0f,1 );
	glClear( GL_COLOR_BUFFER_BIT );

	return 0;
}

int gxtkGraphics::SetAlpha( float alpha ){
	this->alpha=alpha;
	
	int a=int(alpha*255);
	
	colorARGB=(a<<24) | (int(b*alpha)<<16) | (int(g*alpha)<<8) | int(r*alpha);
	
	return 0;
}

int gxtkGraphics::SetColor( float r,float g,float b ){
	this->r=r;
	this->g=g;
	this->b=b;

	int a=int(alpha*255);
	
	colorARGB=(a<<24) | (int(b*alpha)<<16) | (int(g*alpha)<<8) | int(r*alpha);
	
	return 0;
}

int gxtkGraphics::SetBlend( int blend ){

	Flush();
	
	switch( blend ){
	case 1:
		glBlendFunc( GL_ONE,GL_ONE );
		break;
	default:
		glBlendFunc( GL_ONE,GL_ONE_MINUS_SRC_ALPHA );
	}

	return 0;
}

int gxtkGraphics::SetScissor( int x,int y,int w,int h ){

	Flush();
	
	if( x!=0 || y!=0 || w!=Width() || h!=Height() ){
		glEnable( GL_SCISSOR_TEST );
		y=Height()-y-h;
		glScissor( x,y,w,h );
	}else{
		glDisable( GL_SCISSOR_TEST );
	}
	return 0;
}

int gxtkGraphics::SetMatrix( float ix,float iy,float jx,float jy,float tx,float ty ){

	tformed=(ix!=1 || iy!=0 || jx!=0 || jy!=1 || tx!=0 || ty!=0);

	this->ix=ix;this->iy=iy;this->jx=jx;this->jy=jy;this->tx=tx;this->ty=ty;

	return 0;
}

int gxtkGraphics::DrawPoint( float x,float y ){

	if( tformed ){
		float px=x;
		x=px * ix + y * jx + tx;
		y=px * iy + y * jy + ty;
	}
	
	float *vp=Begin( 1,1,0 );
	
	vp[0]=x;vp[1]=y;(int&)vp[4]=colorARGB;

	return 0;	
}
	
int gxtkGraphics::DrawLine( float x0,float y0,float x1,float y1 ){

	if( tformed ){
		float tx0=x0,tx1=x1;
		x0=tx0 * ix + y0 * jx + tx;y0=tx0 * iy + y0 * jy + ty;
		x1=tx1 * ix + y1 * jx + tx;y1=tx1 * iy + y1 * jy + ty;
	}
	
	float *vp=Begin( 2,2,0 );

	vp[0]=x0;vp[1]=y0;(int&)vp[4]=colorARGB;
	vp[5]=x1;vp[6]=y1;(int&)vp[9]=colorARGB;
	
	return 0;
}

int gxtkGraphics::DrawRect( float x,float y,float w,float h ){

	float x0=x,x1=x+w,x2=x+w,x3=x;
	float y0=y,y1=y,y2=y+h,y3=y+h;

	if( tformed ){
		float tx0=x0,tx1=x1,tx2=x2,tx3=x3;
		x0=tx0 * ix + y0 * jx + tx;y0=tx0 * iy + y0 * jy + ty;
		x1=tx1 * ix + y1 * jx + tx;y1=tx1 * iy + y1 * jy + ty;
		x2=tx2 * ix + y2 * jx + tx;y2=tx2 * iy + y2 * jy + ty;
		x3=tx3 * ix + y3 * jx + tx;y3=tx3 * iy + y3 * jy + ty;
	}
	
	float *vp=Begin( 4,4,0 );

	vp[0 ]=x0;vp[1 ]=y0;(int&)vp[4 ]=colorARGB;
	vp[5 ]=x1;vp[6 ]=y1;(int&)vp[9 ]=colorARGB;
	vp[10]=x2;vp[11]=y2;(int&)vp[14]=colorARGB;
	vp[15]=x3;vp[16]=y3;(int&)vp[19]=colorARGB;

	return 0;
}

int gxtkGraphics::DrawOval( float x,float y,float w,float h ){
	
	float xr=w/2.0f;
	float yr=h/2.0f;

	int n;
	if( tformed ){
		float dx_x=xr * ix;
		float dx_y=xr * iy;
		float dx=sqrtf( dx_x*dx_x+dx_y*dx_y );
		float dy_x=yr * jx;
		float dy_y=yr * jy;
		float dy=sqrtf( dy_x*dy_x+dy_y*dy_y );
		n=(int)( dx+dy );
	}else{
		n=(int)( abs( xr )+abs( yr ) );
	}
	
	if( n<12 ){
		n=12;
	}else if( n>MAX_VERTS ){
		n=MAX_VERTS;
	}else{
		n&=~3;
	}

	float x0=x+xr,y0=y+yr;
	
	float *vp=Begin( n,n,0 );

	for( int i=0;i<n;++i ){
	
		float th=i * 6.28318531f / n;

		float px=x0+cosf( th ) * xr;
		float py=y0-sinf( th ) * yr;
		
		if( tformed ){
			float ppx=px;
			px=ppx * ix + py * jx + tx;
			py=ppx * iy + py * jy + ty;
		}
		
		vp[0]=px;vp[1]=py;(int&)vp[4]=colorARGB;
		vp+=5;
	}
	
	return 0;
}

int gxtkGraphics::DrawPoly( Array<Float> verts ){

	int n=verts.Length()/2;
	if( n<1 || n>MAX_VERTS ) return 0;
	
	float *vp=Begin( n,n,0 );
	
	for( int i=0;i<n;++i ){
		int j=i*2;
		if( tformed ){
			vp[0]=verts[j] * ix + verts[j+1] * jx + tx;
			vp[1]=verts[j] * iy + verts[j+1] * jy + ty;
		}else{
			vp[0]=verts[j];
			vp[1]=verts[j+1];
		}
		(int&)vp[4]=colorARGB;
		vp+=5;
	}

	return 0;
}

int gxtkGraphics::DrawPoly2( Array<Float> verts,gxtkSurface *surface,int srcx,int srcy ){

	int n=verts.Length()/4;
	if( n<1 || n>MAX_VERTS ) return 0;
		
	float *vp=Begin( n,n,surface );
	
	for( int i=0;i<n;++i ){
		int j=i*4;
		if( tformed ){
			vp[0]=verts[j] * ix + verts[j+1] * jx + tx;
			vp[1]=verts[j] * iy + verts[j+1] * jy + ty;
		}else{
			vp[0]=verts[j];
			vp[1]=verts[j+1];
		}
		vp[2]=(srcx+verts[j+2])*surface->uscale;
		vp[3]=(srcy+verts[j+3])*surface->vscale;
		(int&)vp[4]=colorARGB;
		vp+=5;
	}
	
	return 0;
}

int gxtkGraphics::DrawSurface( gxtkSurface *surf,float x,float y ){
	
	float w=surf->Width();
	float h=surf->Height();
	float x0=x,x1=x+w,x2=x+w,x3=x;
	float y0=y,y1=y,y2=y+h,y3=y+h;
	float u0=0,u1=w*surf->uscale;
	float v0=0,v1=h*surf->vscale;

	if( tformed ){
		float tx0=x0,tx1=x1,tx2=x2,tx3=x3;
		x0=tx0 * ix + y0 * jx + tx;y0=tx0 * iy + y0 * jy + ty;
		x1=tx1 * ix + y1 * jx + tx;y1=tx1 * iy + y1 * jy + ty;
		x2=tx2 * ix + y2 * jx + tx;y2=tx2 * iy + y2 * jy + ty;
		x3=tx3 * ix + y3 * jx + tx;y3=tx3 * iy + y3 * jy + ty;
	}
	
	float *vp=Begin( 4,4,surf );
	
	vp[0 ]=x0;vp[1 ]=y0;vp[2 ]=u0;vp[3 ]=v0;(int&)vp[4 ]=colorARGB;
	vp[5 ]=x1;vp[6 ]=y1;vp[7 ]=u1;vp[8 ]=v0;(int&)vp[9 ]=colorARGB;
	vp[10]=x2;vp[11]=y2;vp[12]=u1;vp[13]=v1;(int&)vp[14]=colorARGB;
	vp[15]=x3;vp[16]=y3;vp[17]=u0;vp[18]=v1;(int&)vp[19]=colorARGB;
	
	return 0;
}

int gxtkGraphics::DrawSurface2( gxtkSurface *surf,float x,float y,int srcx,int srcy,int srcw,int srch ){
	
	float w=srcw;
	float h=srch;
	float x0=x,x1=x+w,x2=x+w,x3=x;
	float y0=y,y1=y,y2=y+h,y3=y+h;
	float u0=srcx*surf->uscale,u1=(srcx+srcw)*surf->uscale;
	float v0=srcy*surf->vscale,v1=(srcy+srch)*surf->vscale;

	if( tformed ){
		float tx0=x0,tx1=x1,tx2=x2,tx3=x3;
		x0=tx0 * ix + y0 * jx + tx;y0=tx0 * iy + y0 * jy + ty;
		x1=tx1 * ix + y1 * jx + tx;y1=tx1 * iy + y1 * jy + ty;
		x2=tx2 * ix + y2 * jx + tx;y2=tx2 * iy + y2 * jy + ty;
		x3=tx3 * ix + y3 * jx + tx;y3=tx3 * iy + y3 * jy + ty;
	}
	
	float *vp=Begin( 4,4,surf );
	
	vp[0 ]=x0;vp[1 ]=y0;vp[2 ]=u0;vp[3 ]=v0;(int&)vp[4 ]=colorARGB;
	vp[5 ]=x1;vp[6 ]=y1;vp[7 ]=u1;vp[8 ]=v0;(int&)vp[9 ]=colorARGB;
	vp[10]=x2;vp[11]=y2;vp[12]=u1;vp[13]=v1;(int&)vp[14]=colorARGB;
	vp[15]=x3;vp[16]=y3;vp[17]=u0;vp[18]=v1;(int&)vp[19]=colorARGB;
	
	return 0;
}
	
int gxtkGraphics::ReadPixels( Array<int> pixels,int x,int y,int width,int height,int offset,int pitch ){

	Flush();

	unsigned *p=(unsigned*)malloc(width*height*4);

	glReadPixels( x,this->height-y-height,width,height,GL_BGRA,GL_UNSIGNED_BYTE,p );
	
	for( int py=0;py<height;++py ){
		memcpy( &pixels[offset+py*pitch],&p[(height-py-1)*width],width*4 );
	}
	
	free( p );
	
	return 0;
}

int gxtkGraphics::WritePixels2( gxtkSurface *surface,Array<int> pixels,int x,int y,int width,int height,int offset,int pitch ){

	Flush();
	
	surface->SetSubData( x,y,width,height,(unsigned*)&pixels[offset],pitch );
	
	return 0;
}

//***** gxtkSurface *****

gxtkSurface::gxtkSurface():data(0),width(0),height(0),depth(0),format(0),seq(-1),texture(0),uscale(0),vscale(0){
}

gxtkSurface::~gxtkSurface(){
	Discard();
}

int gxtkSurface::Discard(){
	if( seq==glfwGraphicsSeq ){
		glDeleteTextures( 1,&texture );
		seq=-1;
	}
	if( data ){
		free( data );
		data=0;
	}
	return 0;
}

int gxtkSurface::Width(){
	return width;
}

int gxtkSurface::Height(){
	return height;
}

int gxtkSurface::Loaded(){
	return 1;
}

//Careful! Can't call any GL here as it may be executing off-thread.
//
void gxtkSurface::SetData( unsigned char *data,int width,int height,int depth ){

	this->data=data;
	this->width=width;
	this->height=height;
	this->depth=depth;
	
	unsigned char *p=data;
	int n=width*height;
	
	switch( depth ){
	case 1:
		format=GL_LUMINANCE;
		break;
	case 2:
		format=GL_LUMINANCE_ALPHA;
		if( data ){
			while( n-- ){	//premultiply alpha
				p[0]=p[0]*p[1]/255;
				p+=2;
			}
		}
		break;
	case 3:
		format=GL_RGB;
		break;
	case 4:
		format=GL_RGBA;
		if( data ){
			while( n-- ){	//premultiply alpha
				p[0]=p[0]*p[3]/255;
				p[1]=p[1]*p[3]/255;
				p[2]=p[2]*p[3]/255;
				p+=4;
			}
		}
		break;
	}
}

void gxtkSurface::SetSubData( int x,int y,int w,int h,unsigned *src,int pitch ){
	if( format!=GL_RGBA ) return;
	
	if( !data ) data=(unsigned char*)malloc( width*height*4 );
	
	unsigned *dst=(unsigned*)data+y*width+x;
	
	for( int py=0;py<h;++py ){
		unsigned *d=dst+py*width;
		unsigned *s=src+py*pitch;
		for( int px=0;px<w;++px ){
			unsigned p=*s++;
			unsigned a=p>>24;
			*d++=(a<<24) | ((p>>0&0xff)*a/255<<16) | ((p>>8&0xff)*a/255<<8) | ((p>>16&0xff)*a/255);
		}
	}
	
	if( seq==glfwGraphicsSeq ){
		glBindTexture( GL_TEXTURE_2D,texture );
		glPixelStorei( GL_UNPACK_ALIGNMENT,1 );
		if( width==pitch ){
			glTexSubImage2D( GL_TEXTURE_2D,0,x,y,w,h,format,GL_UNSIGNED_BYTE,dst );
		}else{
			for( int py=0;py<h;++py ){
				glTexSubImage2D( GL_TEXTURE_2D,0,x,y+py,w,1,format,GL_UNSIGNED_BYTE,dst+py*width );
			}
		}
	}
}

void gxtkSurface::Bind(){

	if( !glfwGraphicsSeq ) return;
	
	if( seq==glfwGraphicsSeq ){
		glBindTexture( GL_TEXTURE_2D,texture );
		return;
	}
	
	seq=glfwGraphicsSeq;
	
	glGenTextures( 1,&texture );
	glBindTexture( GL_TEXTURE_2D,texture );
	
	if( CFG_MOJO_IMAGE_FILTERING_ENABLED ){
		glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR );
	}else{
		glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST );
	}

	glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE );

	int texwidth=width;
	int texheight=height;
	
	glTexImage2D( GL_TEXTURE_2D,0,format,texwidth,texheight,0,format,GL_UNSIGNED_BYTE,0 );
	if( glGetError()!=GL_NO_ERROR ){
		texwidth=Pow2Size( width );
		texheight=Pow2Size( height );
		glTexImage2D( GL_TEXTURE_2D,0,format,texwidth,texheight,0,format,GL_UNSIGNED_BYTE,0 );
	}
	
	uscale=1.0/texwidth;
	vscale=1.0/texheight;
	
	if( data ){
		glPixelStorei( GL_UNPACK_ALIGNMENT,1 );
		glTexSubImage2D( GL_TEXTURE_2D,0,0,0,width,height,format,GL_UNSIGNED_BYTE,data );
	}
}

bool gxtkSurface::OnUnsafeLoadComplete(){
	Bind();
	return true;
}

gxtkSurface *gxtkGraphics::LoadSurface__UNSAFE__( gxtkSurface *surface,String path ){
	int width,height,depth;
	unsigned char *data=BBGlfwGame::GlfwGame()->LoadImageData( path,&width,&height,&depth );
	if( !data ) return 0;
	surface->SetData( data,width,height,depth );
	return surface;
}

gxtkSurface *gxtkGraphics::LoadSurface( String path ){
	gxtkSurface *surf=LoadSurface__UNSAFE__( new gxtkSurface(),path );
	if( !surf ) return 0;
	surf->Bind();
	return surf;
}

gxtkSurface *gxtkGraphics::CreateSurface( int width,int height ){
	gxtkSurface *surf=new gxtkSurface();
	surf->SetData( 0,width,height,4 );
	surf->Bind();
	return surf;
}

//***** gxtkAudio.h *****

class gxtkSample;

class gxtkChannel{
public:
	ALuint source;
	gxtkSample *sample;
	int flags;
	int state;
	
	int AL_Source();
};

class gxtkAudio : public Object{
public:
	ALCdevice *alcDevice;
	ALCcontext *alcContext;
	gxtkChannel channels[33];

	gxtkAudio();

	virtual void mark();

	//***** GXTK API *****
	virtual int Suspend();
	virtual int Resume();

	virtual gxtkSample *LoadSample__UNSAFE__( gxtkSample *sample,String path );
	virtual gxtkSample *LoadSample( String path );
	virtual int PlaySample( gxtkSample *sample,int channel,int flags );

	virtual int StopChannel( int channel );
	virtual int PauseChannel( int channel );
	virtual int ResumeChannel( int channel );
	virtual int ChannelState( int channel );
	virtual int SetVolume( int channel,float volume );
	virtual int SetPan( int channel,float pan );
	virtual int SetRate( int channel,float rate );
	
	virtual int PlayMusic( String path,int flags );
	virtual int StopMusic();
	virtual int PauseMusic();
	virtual int ResumeMusic();
	virtual int MusicState();
	virtual int SetMusicVolume( float volume );
};

class gxtkSample : public Object{
public:
	ALuint al_buffer;

	gxtkSample();
	gxtkSample( ALuint buf );
	~gxtkSample();
	
	void SetBuffer( ALuint buf );
	
	//***** GXTK API *****
	virtual int Discard();
};

//***** gxtkAudio.cpp *****

static std::vector<ALuint> discarded;

static void FlushDiscarded( gxtkAudio *audio ){

	if( !discarded.size() ) return;
	
	for( int i=0;i<33;++i ){
		gxtkChannel *chan=&audio->channels[i];
		if( chan->state ){
			int state=0;
			alGetSourcei( chan->source,AL_SOURCE_STATE,&state );
			if( state==AL_STOPPED ) alSourcei( chan->source,AL_BUFFER,0 );
		}
	}
	
	std::vector<ALuint> out;
	
	for( int i=0;i<discarded.size();++i ){
		ALuint buf=discarded[i];
		alDeleteBuffers( 1,&buf );
		ALenum err=alGetError();
		if( err==AL_NO_ERROR ){
//			printf( "alDeleteBuffers OK!\n" );fflush( stdout );
		}else{
//			printf( "alDeleteBuffers failed...\n" );fflush( stdout );
			out.push_back( buf );
		}
	}
	discarded=out;
}

int gxtkChannel::AL_Source(){
	if( !source ) alGenSources( 1,&source );
	return source;
}

gxtkAudio::gxtkAudio(){

	if( alcDevice=alcOpenDevice( 0 ) ){
		if( alcContext=alcCreateContext( alcDevice,0 ) ){
			if( alcMakeContextCurrent( alcContext ) ){
				//alc all go!
			}else{
				bbPrint( "OpenAl error: alcMakeContextCurrent failed" );
			}
		}else{
			bbPrint( "OpenAl error: alcCreateContext failed" );
		}
	}else{
		bbPrint( "OpenAl error: alcOpenDevice failed" );
	}

	alDistanceModel( AL_NONE );
	
	memset( channels,0,sizeof(channels) );
}

void gxtkAudio::mark(){
	for( int i=0;i<33;++i ){
		gxtkChannel *chan=&channels[i];
		if( chan->state!=0 ){
			int state=0;
			alGetSourcei( chan->source,AL_SOURCE_STATE,&state );
			if( state!=AL_STOPPED ) gc_mark( chan->sample );
		}
	}
}

int gxtkAudio::Suspend(){
	for( int i=0;i<33;++i ){
		gxtkChannel *chan=&channels[i];
		if( chan->state==1 ){
			int state=0;
			alGetSourcei( chan->source,AL_SOURCE_STATE,&state );
			if( state==AL_PLAYING ) alSourcePause( chan->source );
		}
	}
	return 0;
}

int gxtkAudio::Resume(){
	for( int i=0;i<33;++i ){
		gxtkChannel *chan=&channels[i];
		if( chan->state==1 ){
			int state=0;
			alGetSourcei( chan->source,AL_SOURCE_STATE,&state );
			if( state==AL_PAUSED ) alSourcePlay( chan->source );
		}
	}
	return 0;
}

gxtkSample *gxtkAudio::LoadSample__UNSAFE__( gxtkSample *sample,String path ){

	int length=0;
	int channels=0;
	int format=0;
	int hertz=0;
	unsigned char *data=BBGlfwGame::GlfwGame()->LoadAudioData( path,&length,&channels,&format,&hertz );
	if( !data ) return 0;
	
	int al_format=0;
	if( format==1 && channels==1 ){
		al_format=AL_FORMAT_MONO8;
	}else if( format==1 && channels==2 ){
		al_format=AL_FORMAT_STEREO8;
	}else if( format==2 && channels==1 ){
		al_format=AL_FORMAT_MONO16;
	}else if( format==2 && channels==2 ){
		al_format=AL_FORMAT_STEREO16;
	}
	
	int size=length*channels*format;
	
	ALuint al_buffer;
	alGenBuffers( 1,&al_buffer );
	alBufferData( al_buffer,al_format,data,size,hertz );
	free( data );
	
	sample->SetBuffer( al_buffer );
	return sample;
}

gxtkSample *gxtkAudio::LoadSample( String path ){

	FlushDiscarded( this );

	return LoadSample__UNSAFE__( new gxtkSample(),path );
}

int gxtkAudio::PlaySample( gxtkSample *sample,int channel,int flags ){

	FlushDiscarded( this );
	
	gxtkChannel *chan=&channels[channel];
	
	chan->AL_Source();
	
	alSourceStop( chan->source );
	alSourcei( chan->source,AL_BUFFER,sample->al_buffer );
	alSourcei( chan->source,AL_LOOPING,flags ? 1 : 0 );
	alSourcePlay( chan->source );
	
	gc_assign( chan->sample,sample );

	chan->flags=flags;
	chan->state=1;

	return 0;
}

int gxtkAudio::StopChannel( int channel ){
	gxtkChannel *chan=&channels[channel];

	if( chan->state!=0 ){
		alSourceStop( chan->source );
		chan->state=0;
	}
	return 0;
}

int gxtkAudio::PauseChannel( int channel ){
	gxtkChannel *chan=&channels[channel];

	if( chan->state==1 ){
		int state=0;
		alGetSourcei( chan->source,AL_SOURCE_STATE,&state );
		if( state==AL_STOPPED ){
			chan->state=0;
		}else{
			alSourcePause( chan->source );
			chan->state=2;
		}
	}
	return 0;
}

int gxtkAudio::ResumeChannel( int channel ){
	gxtkChannel *chan=&channels[channel];

	if( chan->state==2 ){
		alSourcePlay( chan->source );
		chan->state=1;
	}
	return 0;
}

int gxtkAudio::ChannelState( int channel ){
	gxtkChannel *chan=&channels[channel];
	
	if( chan->state==1 ){
		int state=0;
		alGetSourcei( chan->source,AL_SOURCE_STATE,&state );
		if( state==AL_STOPPED ) chan->state=0;
	}
	return chan->state;
}

int gxtkAudio::SetVolume( int channel,float volume ){
	gxtkChannel *chan=&channels[channel];

	alSourcef( chan->AL_Source(),AL_GAIN,volume );
	return 0;
}

int gxtkAudio::SetPan( int channel,float pan ){
	gxtkChannel *chan=&channels[channel];
	
	float x=sinf( pan ),y=0,z=-cosf( pan );
	alSource3f( chan->AL_Source(),AL_POSITION,x,y,z );
	return 0;
}

int gxtkAudio::SetRate( int channel,float rate ){
	gxtkChannel *chan=&channels[channel];

	alSourcef( chan->AL_Source(),AL_PITCH,rate );
	return 0;
}

int gxtkAudio::PlayMusic( String path,int flags ){
	StopMusic();
	
	gxtkSample *music=LoadSample( path );
	if( !music ) return -1;
	
	PlaySample( music,32,flags );
	return 0;
}

int gxtkAudio::StopMusic(){
	StopChannel( 32 );
	return 0;
}

int gxtkAudio::PauseMusic(){
	PauseChannel( 32 );
	return 0;
}

int gxtkAudio::ResumeMusic(){
	ResumeChannel( 32 );
	return 0;
}

int gxtkAudio::MusicState(){
	return ChannelState( 32 );
}

int gxtkAudio::SetMusicVolume( float volume ){
	SetVolume( 32,volume );
	return 0;
}

gxtkSample::gxtkSample():
al_buffer(0){
}

gxtkSample::gxtkSample( ALuint buf ):
al_buffer(buf){
}

gxtkSample::~gxtkSample(){
	puts( "Discarding sample" );
	Discard();
}

void gxtkSample::SetBuffer( ALuint buf ){
	al_buffer=buf;
}

int gxtkSample::Discard(){
	if( al_buffer ){
		discarded.push_back( al_buffer );
		al_buffer=0;
	}
	return 0;
}


// ***** thread.h *****

#if __cplusplus_winrt

using namespace Windows::System::Threading;

#endif

class BBThread : public Object{
public:
	Object *result;
	
	BBThread();
	~BBThread();
	
	virtual void Start();
	virtual bool IsRunning();
	virtual Object *Result();
	virtual void SetResult( Object *result );
	
	virtual void Run__UNSAFE__();
	
	virtual void Wait();
	
private:

	enum{
		INIT=0,
		RUNNING=1,
		FINISHED=2
	};

	
	int _state;
	Object *_result;
	
#if __cplusplus_winrt

	friend class Launcher;

	class Launcher{
	
		friend class BBThread;
		BBThread *_thread;
		
		Launcher( BBThread *thread ):_thread(thread){
		}
		
		public:
		void operator()( IAsyncAction ^operation ){
			_thread->Run__UNSAFE__();
			_thread->_state=FINISHED;
		} 
	};

#elif _WIN32

	DWORD _id;
	HANDLE _handle;
	
	static DWORD WINAPI run( void *p );
	
#else

	pthread_t _handle;
	
	static void *run( void *p );
	
#endif

};

// ***** thread.cpp *****

BBThread::BBThread():_result( 0 ),_state( INIT ){
}

BBThread::~BBThread(){
	Wait();
}

bool BBThread::IsRunning(){
	return _state==RUNNING;
}

void BBThread::SetResult( Object *result ){
	_result=result;
}

Object *BBThread::Result(){
	return _result;
}

void BBThread::Run__UNSAFE__(){
}

#if __cplusplus_winrt

void BBThread::Start(){
	if( _state==RUNNING ) return;
	
	if( _state==FINISHED ) {}

	_result=0;
	
	_state=RUNNING;
	
	Launcher launcher( this );
	
	auto handler=ref new WorkItemHandler( launcher );
	
	ThreadPool::RunAsync( handler );
}

void BBThread::Wait(){
//	exit( -1 );
}

#elif _WIN32

void BBThread::Start(){
	if( _state==RUNNING ) return;
	
	if( _state==FINISHED ) CloseHandle( _handle );

	_state=RUNNING;

	_handle=CreateThread( 0,0,run,this,0,&_id );
	
//	_handle=CreateThread( 0,0,run,this,CREATE_SUSPENDED,&_id );
//	SetThreadPriority( _handle,THREAD_PRIORITY_ABOVE_NORMAL );
//	ResumeThread( _handle );
}

void BBThread::Wait(){
	if( _state==INIT ) return;

	WaitForSingleObject( _handle,INFINITE );
	CloseHandle( _handle );

	_state=INIT;
}

DWORD WINAPI BBThread::run( void *p ){
	BBThread *thread=(BBThread*)p;

	thread->Run__UNSAFE__();
	
	thread->_state=FINISHED;
	return 0;
}

#else

void BBThread::Start(){
	if( _state==RUNNING ) return;
	
	if( _state==FINISHED ) pthread_join( _handle,0 );

	_result=0;
		
	_state=RUNNING;
	
	pthread_create( &_handle,0,run,this );
}

void BBThread::Wait(){
	if( _state==INIT ) return;
	
	pthread_join( _handle,0 );
	
	_state=INIT;
}

void *BBThread::run( void *p ){
	BBThread *thread=(BBThread*)p;

	thread->Run__UNSAFE__();

	thread->_state=FINISHED;
	return 0;
}

#endif

class c_App;
class c_CMyApp;
class c_GameDelegate;
class c_Image;
class c_GraphicsContext;
class c_Frame;
class c_InputDevice;
class c_JoyState;
class c_BBGameEvent;
class c_CAIManager;
class c_PathfindingMapItem;
class c_IVector;
class c_COpenListItem;
class c_CTileStatusData;
class c_CSpawnManager;
class c_Map;
class c_IntMap;
class c_Node;
class c_CComponentUpdater;
class c_CInputAreaUpdater;
class c_List;
class c_Node2;
class c_HeadNode;
class c_CMenuUpdater;
class c_CUnitUpdater;
class c_CAnimationManager;
class c_CAnimation;
class c_CImage;
class c_Stack;
class c_Map2;
class c_IntMap2;
class c_Node3;
class c_FVector;
class c_CGameObject;
class c_CComponent;
class c_CInputArea;
class c_List2;
class c_Node4;
class c_HeadNode2;
class c_CMapInterface;
class c_Enumerator;
class c_Enumerator2;
class c_List3;
class c_Node5;
class c_HeadNode3;
class c_CMenuComponent;
class c_CGlobalEventData;
class c_List4;
class c_Map3;
class c_IntMap3;
class c_Node6;
class c_Node7;
class c_HeadNode4;
class c_CCommandBuffer;
class c_CCommand;
class c_Deque;
class c_Enumerator3;
class c_CPath;
class c_Deque2;
class c_List5;
class c_COpenListItemList;
class c_Node8;
class c_HeadNode5;
class c_List6;
class c_Node9;
class c_HeadNode6;
class c_CSpawnRequestData;
class c_Deque3;
class c_CAnimationGraph;
class c_CAnimationVariable;
class c_Map4;
class c_IntMap4;
class c_Node10;
class c_CAnimationState;
class c_CAnimationNode;
class c_CPlayAnimationNode;
class c_CFloatMultiRangeCondition;
class c_CFloatRangeMiniCondition;
class c_Deque4;
class c_CStateTransition;
class c_CStateBoolTransition;
class c_Deque5;
class c_CMovement;
class c_CUnit;
class c_CRenderer;
class c_CRenderItem;
class c_CMap;
class c_Map5;
class c_IntMap5;
class c_Node11;
class c_List7;
class c_CRenderItemList;
class c_Node12;
class c_HeadNode7;
class c_CSprite;
class c_CSquareItem;
class c_CColor;
class c_Enumerator4;
class c_List8;
class c_Node13;
class c_HeadNode8;
class c_Enumerator5;
class c_Enumerator6;
class c_List9;
class c_Node14;
class c_HeadNode9;
class c_Enumerator7;
class c_List10;
class c_Node15;
class c_HeadNode10;
class c_Enumerator8;
class c_Enumerator9;
class c_Enumerator10;
class c_Enumerator11;
class c_App : public Object{
	public:
	c_App();
	c_App* m_new();
	virtual int p_OnCreate();
	int p_OnSuspend();
	int p_OnResume();
	virtual int p_OnUpdate();
	int p_OnLoading();
	virtual int p_OnRender();
	int p_OnClose();
	int p_OnBack();
	void mark();
	String debug();
};
String dbg_type(c_App**p){return "App";}
class c_CMyApp : public c_App{
	public:
	int m_FramesPerSecond;
	Float m_DeltaTime;
	c_Image* m_TerrainImage;
	c_List* m_ComponentUpdaterList;
	Array<int > m_TerrainSpriteIndexArray;
	c_List3* m_GameObjectList;
	c_CCommandBuffer* m_CommandBuffer;
	int m_PreviousKeyPDown;
	int m_PreviousKeyMinusDown;
	Float m_TimeMult;
	int m_PreviousKeyPlusDown;
	c_CRenderer* m_Renderer;
	c_CMyApp();
	c_CMyApp* m_new();
	int p_AddGameObject(c_CGameObject*);
	int p_SpawnMenu(c_FVector*,int,int,int,c_CMapInterface*);
	int p_OnCreate();
	int p_OnUpdate();
	int p_OnRender();
	void mark();
	String debug();
};
String dbg_type(c_CMyApp**p){return "CMyApp";}
extern c_App* bb_app__app;
class c_GameDelegate : public BBGameDelegate{
	public:
	gxtkGraphics* m__graphics;
	gxtkAudio* m__audio;
	c_InputDevice* m__input;
	c_GameDelegate();
	c_GameDelegate* m_new();
	void StartGame();
	void SuspendGame();
	void ResumeGame();
	void UpdateGame();
	void RenderGame();
	void KeyEvent(int,int);
	void MouseEvent(int,int,Float,Float);
	void TouchEvent(int,int,Float,Float);
	void MotionEvent(int,int,Float,Float,Float);
	void DiscardGraphics();
	void mark();
	String debug();
};
String dbg_type(c_GameDelegate**p){return "GameDelegate";}
extern c_GameDelegate* bb_app__delegate;
extern BBGame* bb_app__game;
int bbMain();
extern gxtkGraphics* bb_graphics_device;
int bb_graphics_SetGraphicsDevice(gxtkGraphics*);
class c_Image : public Object{
	public:
	gxtkSurface* m_surface;
	int m_width;
	int m_height;
	Array<c_Frame* > m_frames;
	int m_flags;
	Float m_tx;
	Float m_ty;
	c_Image* m_source;
	c_Image();
	static int m_DefaultFlags;
	c_Image* m_new();
	int p_SetHandle(Float,Float);
	int p_ApplyFlags(int);
	c_Image* p_Init(gxtkSurface*,int,int);
	c_Image* p_Init2(gxtkSurface*,int,int,int,int,int,int,c_Image*,int,int,int,int);
	int p_Width();
	int p_Height();
	int p_Frames();
	void mark();
	String debug();
};
String dbg_type(c_Image**p){return "Image";}
class c_GraphicsContext : public Object{
	public:
	c_Image* m_defaultFont;
	c_Image* m_font;
	int m_firstChar;
	int m_matrixSp;
	Float m_ix;
	Float m_iy;
	Float m_jx;
	Float m_jy;
	Float m_tx;
	Float m_ty;
	int m_tformed;
	int m_matDirty;
	Float m_color_r;
	Float m_color_g;
	Float m_color_b;
	Float m_alpha;
	int m_blend;
	Float m_scissor_x;
	Float m_scissor_y;
	Float m_scissor_width;
	Float m_scissor_height;
	Array<Float > m_matrixStack;
	c_GraphicsContext();
	c_GraphicsContext* m_new();
	int p_Validate();
	void mark();
	String debug();
};
String dbg_type(c_GraphicsContext**p){return "GraphicsContext";}
extern c_GraphicsContext* bb_graphics_context;
String bb_data_FixDataPath(String);
class c_Frame : public Object{
	public:
	int m_x;
	int m_y;
	c_Frame();
	c_Frame* m_new(int,int);
	c_Frame* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Frame**p){return "Frame";}
c_Image* bb_graphics_LoadImage(String,int,int);
c_Image* bb_graphics_LoadImage2(String,int,int,int,int);
int bb_graphics_SetFont(c_Image*,int);
extern gxtkAudio* bb_audio_device;
int bb_audio_SetAudioDevice(gxtkAudio*);
class c_InputDevice : public Object{
	public:
	Array<c_JoyState* > m__joyStates;
	Array<bool > m__keyDown;
	int m__keyHitPut;
	Array<int > m__keyHitQueue;
	Array<int > m__keyHit;
	int m__charGet;
	int m__charPut;
	Array<int > m__charQueue;
	Float m__mouseX;
	Float m__mouseY;
	Array<Float > m__touchX;
	Array<Float > m__touchY;
	Float m__accelX;
	Float m__accelY;
	Float m__accelZ;
	c_InputDevice();
	c_InputDevice* m_new();
	void p_PutKeyHit(int);
	void p_BeginUpdate();
	void p_EndUpdate();
	void p_KeyEvent(int,int);
	void p_MouseEvent(int,int,Float,Float);
	void p_TouchEvent(int,int,Float,Float);
	void p_MotionEvent(int,int,Float,Float,Float);
	bool p_KeyDown(int);
	Float p_MouseX();
	Float p_MouseY();
	void mark();
	String debug();
};
String dbg_type(c_InputDevice**p){return "InputDevice";}
class c_JoyState : public Object{
	public:
	Array<Float > m_joyx;
	Array<Float > m_joyy;
	Array<Float > m_joyz;
	Array<bool > m_buttons;
	c_JoyState();
	c_JoyState* m_new();
	void mark();
	String debug();
};
String dbg_type(c_JoyState**p){return "JoyState";}
extern c_InputDevice* bb_input_device;
int bb_input_SetInputDevice(c_InputDevice*);
extern gxtkGraphics* bb_graphics_renderDevice;
int bb_graphics_SetMatrix(Float,Float,Float,Float,Float,Float);
int bb_graphics_SetMatrix2(Array<Float >);
int bb_graphics_SetColor(Float,Float,Float);
int bb_graphics_SetAlpha(Float);
int bb_graphics_SetBlend(int);
int bb_graphics_DeviceWidth();
int bb_graphics_DeviceHeight();
int bb_graphics_SetScissor(Float,Float,Float,Float);
int bb_graphics_BeginRender();
int bb_graphics_EndRender();
class c_BBGameEvent : public Object{
	public:
	c_BBGameEvent();
	void mark();
	String debug();
};
String dbg_type(c_BBGameEvent**p){return "BBGameEvent";}
int bb_app_EndApp();
int bb_app_GetDate(Array<int >);
Array<int > bb_app_GetDate2();
extern int bb_random_Seed;
extern int bb_app__updateRate;
int bb_app_SetUpdateRate(int);
class c_CAIManager : public Object{
	public:
	c_CAIManager();
	static Array<int > m_TerrainDescriptionArray;
	static int m_TerrainWidth;
	static int m_TerrainHeight;
	static int m_TerrainTileCount;
	static Array<c_PathfindingMapItem* > m_PathfindingMap;
	static Array<c_IVector* > m_NeighbourArray;
	static Array<c_COpenListItem* > m_OpenListItemPool;
	static Array<c_CTileStatusData* > m_TileStatusArray;
	static int m_Init();
	static int m_TileSize;
	static Float m_MapScale;
	static c_Deque2* m_PathQueue;
	static c_COpenListItemList* m_OpenList;
	static int m_OpenListItemPoolCount;
	static c_COpenListItem* m_GetOpenListItemFromPool();
	static int m_PoolOpenListItem(c_COpenListItem*);
	static int m_ComputePath(c_CPath*);
	static int m_Update(Float);
	static bool m_DisplayDebug;
	static c_IVector* m_GetTilePositionFromIndex(int);
	static int m_RenderDebug();
	static int m_GetTileIndexFromPosition(c_IVector*);
	static c_CTileStatusData* m_GetTileStatusData(c_IVector*);
	static int m_SetTileStatus(c_IVector*,int,int);
	static int m_AddRequest(c_CPath*);
	void mark();
	String debug();
};
String dbg_type(c_CAIManager**p){return "CAIManager";}
class c_PathfindingMapItem : public Object{
	public:
	int m_Obstacle;
	bool m_Explored;
	Float m_CostSoFar;
	int m_CameFrom;
	c_PathfindingMapItem();
	c_PathfindingMapItem* m_new();
	void mark();
	String debug();
};
String dbg_type(c_PathfindingMapItem**p){return "PathfindingMapItem";}
class c_IVector : public Object{
	public:
	int m_X;
	int m_Y;
	c_IVector();
	c_IVector* m_new(int,int);
	void mark();
	String debug();
};
String dbg_type(c_IVector**p){return "IVector";}
class c_COpenListItem : public Object{
	public:
	c_IVector* m_Position;
	Float m_Estimate;
	c_COpenListItem();
	c_COpenListItem* m_new();
	int p_Init3(c_IVector*,Float);
	void mark();
	String debug();
};
String dbg_type(c_COpenListItem**p){return "COpenListItem";}
class c_CTileStatusData : public Object{
	public:
	int m_TileStatus;
	int m_UnitID;
	c_CTileStatusData();
	c_CTileStatusData* m_new();
	void mark();
	String debug();
};
String dbg_type(c_CTileStatusData**p){return "CTileStatusData";}
class c_CSpawnManager : public Object{
	public:
	c_CSpawnManager();
	static c_IntMap* m_HumanAnimationSet;
	static c_IntMap* m_OrcAnimationSet;
	static int m_Init();
	static c_Deque3* m_SpawnRequestQueue;
	static int m_CreateHumanFootman(c_CMyApp*,c_CCommandBuffer*,c_FVector*);
	static int m_CreateOrcFootman(c_CMyApp*,c_CCommandBuffer*,c_FVector*);
	static int m_Update(c_CMyApp*,c_CCommandBuffer*);
	static int m_RequestSpawn(int,c_FVector*);
	void mark();
	String debug();
};
String dbg_type(c_CSpawnManager**p){return "CSpawnManager";}
class c_Map : public Object{
	public:
	c_Node* m_root;
	c_Map();
	c_Map* m_new();
	virtual int p_Compare(int,int)=0;
	int p_RotateLeft(c_Node*);
	int p_RotateRight(c_Node*);
	int p_InsertFixup(c_Node*);
	bool p_Add(int,int);
	c_Node* p_FindNode(int);
	int p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Map**p){return "Map";}
class c_IntMap : public c_Map{
	public:
	c_IntMap();
	c_IntMap* m_new();
	int p_Compare(int,int);
	void mark();
	String debug();
};
String dbg_type(c_IntMap**p){return "IntMap";}
class c_Node : public Object{
	public:
	int m_key;
	c_Node* m_right;
	c_Node* m_left;
	int m_value;
	int m_color;
	c_Node* m_parent;
	c_Node();
	c_Node* m_new(int,int,int,c_Node*);
	c_Node* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node**p){return "Node";}
class c_CComponentUpdater : public Object{
	public:
	int m_ComponentType;
	c_CComponentUpdater();
	c_CComponentUpdater* m_new(int);
	c_CComponentUpdater* m_new2();
	virtual int p_AddComponent(c_CComponent*)=0;
	virtual int p_Update(c_CCommandBuffer*)=0;
	void mark();
	String debug();
};
String dbg_type(c_CComponentUpdater**p){return "CComponentUpdater";}
class c_CInputAreaUpdater : public c_CComponentUpdater{
	public:
	c_List8* m_ComponentList;
	c_CInputAreaUpdater();
	c_CInputAreaUpdater* m_new();
	int p_Update(c_CCommandBuffer*);
	int p_AddComponent(c_CComponent*);
	void mark();
	String debug();
};
String dbg_type(c_CInputAreaUpdater**p){return "CInputAreaUpdater";}
class c_List : public Object{
	public:
	c_Node2* m__head;
	c_List();
	c_List* m_new();
	c_Node2* p_AddLast(c_CComponentUpdater*);
	c_List* m_new2(Array<c_CComponentUpdater* >);
	c_Enumerator2* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List**p){return "List";}
class c_Node2 : public Object{
	public:
	c_Node2* m__succ;
	c_Node2* m__pred;
	c_CComponentUpdater* m__data;
	c_Node2();
	c_Node2* m_new(c_Node2*,c_Node2*,c_CComponentUpdater*);
	c_Node2* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node2**p){return "Node";}
class c_HeadNode : public c_Node2{
	public:
	c_HeadNode();
	c_HeadNode* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode**p){return "HeadNode";}
class c_CMenuUpdater : public c_CComponentUpdater{
	public:
	c_List9* m_ComponentList;
	c_CMenuUpdater();
	c_CMenuUpdater* m_new();
	int p_Update(c_CCommandBuffer*);
	int p_AddComponent(c_CComponent*);
	void mark();
	String debug();
};
String dbg_type(c_CMenuUpdater**p){return "CMenuUpdater";}
class c_CUnitUpdater : public c_CComponentUpdater{
	public:
	c_List10* m_UnitList;
	c_CUnitUpdater();
	c_CUnitUpdater* m_new();
	int p_Update(c_CCommandBuffer*);
	int p_AddComponent(c_CComponent*);
	void mark();
	String debug();
};
String dbg_type(c_CUnitUpdater**p){return "CUnitUpdater";}
Float bb_random_Rnd();
Float bb_random_Rnd2(Float,Float);
Float bb_random_Rnd3(Float);
class c_CAnimationManager : public Object{
	public:
	c_CAnimationManager();
	static c_IntMap2* m_AnimationMap;
	static int m_LoadUnitAnimations(String,int);
	static int m_Init();
	static c_CAnimation* m_GetAnimation(int);
	void mark();
	String debug();
};
String dbg_type(c_CAnimationManager**p){return "CAnimationManager";}
class c_CAnimation : public Object{
	public:
	Float m_Duration;
	c_Stack* m_KeyframeList;
	Float m_TimePerKeyframe;
	c_CAnimation();
	c_CAnimation* m_new(Float);
	c_CAnimation* m_new2();
	int p_AddKeyframe(c_Image*,int,bool);
	void mark();
	String debug();
};
String dbg_type(c_CAnimation**p){return "CAnimation";}
class c_CImage : public Object{
	public:
	c_Image* m_Img;
	int m_SpriteIndex;
	bool m_MirrorVertically;
	c_CImage();
	c_CImage* m_new(c_Image*,int,bool);
	c_CImage* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_CImage**p){return "CImage";}
class c_Stack : public Object{
	public:
	Array<c_CImage* > m_data;
	int m_length;
	c_Stack();
	c_Stack* m_new();
	c_Stack* m_new2(Array<c_CImage* >);
	void p_Push(c_CImage*);
	void p_Push2(Array<c_CImage* >,int,int);
	void p_Push3(Array<c_CImage* >,int);
	static c_CImage* m_NIL;
	void p_Length(int);
	int p_Length2();
	c_CImage* p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Stack**p){return "Stack";}
int bb_math_Max(int,int);
Float bb_math_Max2(Float,Float);
class c_Map2 : public Object{
	public:
	c_Node3* m_root;
	c_Map2();
	c_Map2* m_new();
	virtual int p_Compare(int,int)=0;
	int p_RotateLeft2(c_Node3*);
	int p_RotateRight2(c_Node3*);
	int p_InsertFixup2(c_Node3*);
	bool p_Set(int,c_CAnimation*);
	c_Node3* p_FindNode(int);
	c_CAnimation* p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Map2**p){return "Map";}
class c_IntMap2 : public c_Map2{
	public:
	c_IntMap2();
	c_IntMap2* m_new();
	int p_Compare(int,int);
	void mark();
	String debug();
};
String dbg_type(c_IntMap2**p){return "IntMap";}
class c_Node3 : public Object{
	public:
	int m_key;
	c_Node3* m_right;
	c_Node3* m_left;
	c_CAnimation* m_value;
	int m_color;
	c_Node3* m_parent;
	c_Node3();
	c_Node3* m_new(int,c_CAnimation*,int,c_Node3*);
	c_Node3* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node3**p){return "Node";}
class c_FVector : public Object{
	public:
	Float m_X;
	Float m_Y;
	c_FVector();
	c_FVector* m_new(Float,Float);
	Float p_GetOrientationFromNormed();
	void mark();
	String debug();
};
String dbg_type(c_FVector**p){return "FVector";}
class c_CGameObject : public Object{
	public:
	int m_ID;
	c_List2* m_ComponentList;
	c_CGameObject();
	static int m_GameObjectCount;
	c_CGameObject* m_new();
	int p_AddComponent(c_CComponent*);
	int p_Update2(Float,c_CCommandBuffer*);
	int p_RenderDebug();
	c_CComponent* p_GetComponent(int);
	void mark();
	String debug();
};
String dbg_type(c_CGameObject**p){return "CGameObject";}
class c_CComponent : public Object{
	public:
	int m_Type;
	c_CGameObject* m_GameObject;
	c_IntMap3* m_LocalToGlobalEventMap;
	c_CComponent();
	c_CComponent* m_new(int);
	c_CComponent* m_new2();
	int p_BindEvents(int,int,c_CComponent*);
	virtual int p_Update2(Float,c_CCommandBuffer*);
	virtual int p_RenderDebug();
	virtual int p_OnEvent(int);
	int p_RaiseEvent(int);
	void mark();
	String debug();
};
String dbg_type(c_CComponent**p){return "CComponent";}
class c_CInputArea : public c_CComponent{
	public:
	c_FVector* m_TopLeftCorner;
	c_FVector* m_BottomRightCorner;
	c_FVector* m_Size;
	int m_Priority;
	int m_State;
	c_FVector* m_LastInputGlobalPosition;
	c_CInputArea();
	c_CInputArea* m_new(c_FVector*,c_FVector*,int);
	c_CInputArea* m_new2();
	static bool m_DebugDisplay;
	int p_RenderDebug();
	void mark();
	String debug();
};
String dbg_type(c_CInputArea**p){return "CInputArea";}
class c_List2 : public Object{
	public:
	c_Node4* m__head;
	c_List2();
	c_List2* m_new();
	c_Node4* p_AddLast2(c_CComponent*);
	c_List2* m_new2(Array<c_CComponent* >);
	c_Enumerator* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List2**p){return "List";}
class c_Node4 : public Object{
	public:
	c_Node4* m__succ;
	c_Node4* m__pred;
	c_CComponent* m__data;
	c_Node4();
	c_Node4* m_new(c_Node4*,c_Node4*,c_CComponent*);
	c_Node4* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node4**p){return "Node";}
class c_HeadNode2 : public c_Node4{
	public:
	c_HeadNode2();
	c_HeadNode2* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode2**p){return "HeadNode";}
class c_CMapInterface : public c_CComponent{
	public:
	c_CInputArea* m_InputArea;
	int m_PreviousInputAreaState;
	bool m_HumanFootmanMenuActivated;
	bool m_OrcFootmanMenuActivated;
	c_CMapInterface();
	c_CMapInterface* m_new();
	int p_Update2(Float,c_CCommandBuffer*);
	int p_OnEvent(int);
	void mark();
	String debug();
};
String dbg_type(c_CMapInterface**p){return "CMapInterface";}
class c_Enumerator : public Object{
	public:
	c_List2* m__list;
	c_Node4* m__curr;
	c_Enumerator();
	c_Enumerator* m_new(c_List2*);
	c_Enumerator* m_new2();
	bool p_HasNext();
	c_CComponent* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator**p){return "Enumerator";}
class c_Enumerator2 : public Object{
	public:
	c_List* m__list;
	c_Node2* m__curr;
	c_Enumerator2();
	c_Enumerator2* m_new(c_List*);
	c_Enumerator2* m_new2();
	bool p_HasNext();
	c_CComponentUpdater* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator2**p){return "Enumerator";}
class c_List3 : public Object{
	public:
	c_Node5* m__head;
	c_List3();
	c_List3* m_new();
	c_Node5* p_AddLast3(c_CGameObject*);
	c_List3* m_new2(Array<c_CGameObject* >);
	c_Enumerator3* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List3**p){return "List";}
class c_Node5 : public Object{
	public:
	c_Node5* m__succ;
	c_Node5* m__pred;
	c_CGameObject* m__data;
	c_Node5();
	c_Node5* m_new(c_Node5*,c_Node5*,c_CGameObject*);
	c_Node5* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node5**p){return "Node";}
class c_HeadNode3 : public c_Node5{
	public:
	c_HeadNode3();
	c_HeadNode3* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode3**p){return "HeadNode";}
class c_CMenuComponent : public c_CComponent{
	public:
	c_FVector* m_MenuPosition;
	int m_MenuIdleAnimID;
	int m_MenuID;
	int m_RequestState;
	int m_State;
	int m_HighlightSquareID;
	c_CColor* m_NeutralColor;
	c_CColor* m_HighlightColor;
	c_CColor* m_SelectColor;
	bool m_Init;
	int m_SpriteID;
	Float m_HighlightWidth;
	Float m_HighlightHeight;
	c_CMenuComponent();
	c_CMenuComponent* m_new(c_FVector*,int,int);
	c_CMenuComponent* m_new2();
	int p_Update2(Float,c_CCommandBuffer*);
	int p_OnEvent(int);
	void mark();
	String debug();
};
String dbg_type(c_CMenuComponent**p){return "CMenuComponent";}
class c_CGlobalEventData : public Object{
	public:
	int m_GlobalEventID;
	c_CComponent* m_TargetComponent;
	c_CGlobalEventData();
	c_CGlobalEventData* m_new(int,c_CComponent*);
	c_CGlobalEventData* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_CGlobalEventData**p){return "CGlobalEventData";}
class c_List4 : public Object{
	public:
	c_Node7* m__head;
	c_List4();
	c_List4* m_new();
	c_Node7* p_AddLast4(c_CGlobalEventData*);
	c_List4* m_new2(Array<c_CGlobalEventData* >);
	c_Enumerator6* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List4**p){return "List";}
class c_Map3 : public Object{
	public:
	c_Node6* m_root;
	c_Map3();
	c_Map3* m_new();
	virtual int p_Compare(int,int)=0;
	c_Node6* p_FindNode(int);
	c_List4* p_Get(int);
	int p_RotateLeft3(c_Node6*);
	int p_RotateRight3(c_Node6*);
	int p_InsertFixup3(c_Node6*);
	bool p_Add2(int,c_List4*);
	void mark();
	String debug();
};
String dbg_type(c_Map3**p){return "Map";}
class c_IntMap3 : public c_Map3{
	public:
	c_IntMap3();
	c_IntMap3* m_new();
	int p_Compare(int,int);
	void mark();
	String debug();
};
String dbg_type(c_IntMap3**p){return "IntMap";}
class c_Node6 : public Object{
	public:
	int m_key;
	c_Node6* m_right;
	c_Node6* m_left;
	c_List4* m_value;
	int m_color;
	c_Node6* m_parent;
	c_Node6();
	c_Node6* m_new(int,c_List4*,int,c_Node6*);
	c_Node6* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node6**p){return "Node";}
class c_Node7 : public Object{
	public:
	c_Node7* m__succ;
	c_Node7* m__pred;
	c_CGlobalEventData* m__data;
	c_Node7();
	c_Node7* m_new(c_Node7*,c_Node7*,c_CGlobalEventData*);
	c_Node7* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node7**p){return "Node";}
class c_HeadNode4 : public c_Node7{
	public:
	c_HeadNode4();
	c_HeadNode4* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode4**p){return "HeadNode";}
class c_CCommandBuffer : public Object{
	public:
	Array<c_CCommand* > m_AvailableCommands;
	int m_AvailableCommandCount;
	int m_LastRenderItemID;
	c_Deque* m_CommandQueue;
	c_CCommandBuffer();
	c_CCommandBuffer* m_new();
	c_CCommand* p_GetNewCommand();
	int p_AddCommand_CreateRenderItem(int,Float);
	int p_AddCommand(int,int,Float,Float);
	int p_AddCommand2(int,int,int,int,int);
	bool p_IsEmpty();
	c_CCommand* p_PopCommand();
	int p_RecycleCommand(c_CCommand*);
	void mark();
	String debug();
};
String dbg_type(c_CCommandBuffer**p){return "CCommandBuffer";}
class c_CCommand : public Object{
	public:
	int m_Type;
	int m_ID;
	Float m_ParamFloatA;
	Float m_ParamFloatB;
	int m_ParamIntA;
	int m_ParamIntB;
	int m_ParamIntC;
	c_CCommand();
	c_CCommand* m_new();
	void mark();
	String debug();
};
String dbg_type(c_CCommand**p){return "CCommand";}
class c_Deque : public Object{
	public:
	Array<c_CCommand* > m__data;
	int m__capacity;
	int m__last;
	int m__first;
	c_Deque();
	c_Deque* m_new();
	c_Deque* m_new2(Array<c_CCommand* >);
	int p_Length2();
	void p_Grow();
	void p_PushLast(c_CCommand*);
	bool p_IsEmpty();
	static c_CCommand* m_NIL;
	c_CCommand* p_PopFirst();
	void mark();
	String debug();
};
String dbg_type(c_Deque**p){return "Deque";}
int bb_input_KeyDown(int);
class c_Enumerator3 : public Object{
	public:
	c_List3* m__list;
	c_Node5* m__curr;
	c_Enumerator3();
	c_Enumerator3* m_new(c_List3*);
	c_Enumerator3* m_new2();
	bool p_HasNext();
	c_CGameObject* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator3**p){return "Enumerator";}
class c_CPath : public Object{
	public:
	c_IVector* m_StartPoint;
	c_IVector* m_EndPoint;
	c_List6* m_NodeList;
	int m_PathStatus;
	c_CPath();
	c_CPath* m_new();
	int p_Request(c_IVector*,c_IVector*);
	void mark();
	String debug();
};
String dbg_type(c_CPath**p){return "CPath";}
class c_Deque2 : public Object{
	public:
	Array<c_CPath* > m__data;
	int m__capacity;
	int m__last;
	int m__first;
	c_Deque2();
	c_Deque2* m_new();
	c_Deque2* m_new2(Array<c_CPath* >);
	int p_Length2();
	bool p_IsEmpty();
	static c_CPath* m_NIL;
	c_CPath* p_PopFirst();
	void p_Grow();
	void p_PushLast2(c_CPath*);
	void mark();
	String debug();
};
String dbg_type(c_Deque2**p){return "Deque";}
class c_List5 : public Object{
	public:
	c_Node8* m__head;
	c_List5();
	c_List5* m_new();
	c_Node8* p_AddLast5(c_COpenListItem*);
	c_List5* m_new2(Array<c_COpenListItem* >);
	int p_Clear();
	bool p_IsEmpty();
	virtual int p_Compare2(c_COpenListItem*,c_COpenListItem*);
	int p_Sort(int);
	c_COpenListItem* p_First();
	c_COpenListItem* p_RemoveFirst();
	bool p_Equals(c_COpenListItem*,c_COpenListItem*);
	c_Node8* p_Find(c_COpenListItem*,c_Node8*);
	c_Node8* p_Find2(c_COpenListItem*);
	void p_RemoveFirst2(c_COpenListItem*);
	void mark();
	String debug();
};
String dbg_type(c_List5**p){return "List";}
class c_COpenListItemList : public c_List5{
	public:
	c_COpenListItemList();
	c_COpenListItemList* m_new();
	int p_Compare2(c_COpenListItem*,c_COpenListItem*);
	void mark();
	String debug();
};
String dbg_type(c_COpenListItemList**p){return "COpenListItemList";}
class c_Node8 : public Object{
	public:
	c_Node8* m__succ;
	c_Node8* m__pred;
	c_COpenListItem* m__data;
	c_Node8();
	c_Node8* m_new(c_Node8*,c_Node8*,c_COpenListItem*);
	c_Node8* m_new2();
	int p_Remove();
	void mark();
	String debug();
};
String dbg_type(c_Node8**p){return "Node";}
class c_HeadNode5 : public c_Node8{
	public:
	c_HeadNode5();
	c_HeadNode5* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode5**p){return "HeadNode";}
class c_List6 : public Object{
	public:
	c_Node9* m__head;
	c_List6();
	c_List6* m_new();
	c_Node9* p_AddLast6(c_IVector*);
	c_List6* m_new2(Array<c_IVector* >);
	int p_Clear();
	c_Node9* p_AddFirst(c_IVector*);
	c_Node9* p_FirstNode();
	c_Node9* p_LastNode();
	c_Enumerator11* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List6**p){return "List";}
class c_Node9 : public Object{
	public:
	c_Node9* m__succ;
	c_Node9* m__pred;
	c_IVector* m__data;
	c_Node9();
	c_Node9* m_new(c_Node9*,c_Node9*,c_IVector*);
	c_Node9* m_new2();
	c_IVector* p_Value();
	virtual c_Node9* p_GetNode();
	c_Node9* p_NextNode();
	c_Node9* p_PrevNode();
	void mark();
	String debug();
};
String dbg_type(c_Node9**p){return "Node";}
class c_HeadNode6 : public c_Node9{
	public:
	c_HeadNode6();
	c_HeadNode6* m_new();
	c_Node9* p_GetNode();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode6**p){return "HeadNode";}
class c_CSpawnRequestData : public Object{
	public:
	int m_TemplateID;
	c_FVector* m_Position;
	c_CSpawnRequestData();
	c_CSpawnRequestData* m_new(int,c_FVector*);
	c_CSpawnRequestData* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_CSpawnRequestData**p){return "CSpawnRequestData";}
class c_Deque3 : public Object{
	public:
	Array<c_CSpawnRequestData* > m__data;
	int m__capacity;
	int m__last;
	int m__first;
	c_Deque3();
	c_Deque3* m_new();
	c_Deque3* m_new2(Array<c_CSpawnRequestData* >);
	bool p_IsEmpty();
	static c_CSpawnRequestData* m_NIL;
	c_CSpawnRequestData* p_PopFirst();
	int p_Length2();
	void p_Grow();
	void p_PushLast3(c_CSpawnRequestData*);
	void mark();
	String debug();
};
String dbg_type(c_Deque3**p){return "Deque";}
class c_CAnimationGraph : public c_CComponent{
	public:
	int m_SpriteID;
	c_IntMap* m_AnimationSet;
	c_IntMap4* m_VariableMap;
	c_CAnimationState* m_DefaultState;
	c_CAnimationState* m_CurrentState;
	c_CAnimationGraph();
	int p_AddVariable(int,c_CAnimationVariable*);
	c_CAnimationGraph* m_new(int,c_IntMap*);
	c_CAnimationGraph* m_new2();
	int p_Update2(Float,c_CCommandBuffer*);
	int p_SetVariable(int,bool);
	int p_SetVariable2(int,Float);
	void mark();
	String debug();
};
String dbg_type(c_CAnimationGraph**p){return "CAnimationGraph";}
class c_CAnimationVariable : public Object{
	public:
	int m_VariableName;
	Float m_FloatValueA;
	bool m_BoolValue;
	c_CAnimationVariable();
	c_CAnimationVariable* m_new(int);
	c_CAnimationVariable* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_CAnimationVariable**p){return "CAnimationVariable";}
class c_Map4 : public Object{
	public:
	c_Node10* m_root;
	c_Map4();
	c_Map4* m_new();
	virtual int p_Compare(int,int)=0;
	int p_RotateLeft4(c_Node10*);
	int p_RotateRight4(c_Node10*);
	int p_InsertFixup4(c_Node10*);
	bool p_Add3(int,c_CAnimationVariable*);
	c_Node10* p_FindNode(int);
	c_CAnimationVariable* p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Map4**p){return "Map";}
class c_IntMap4 : public c_Map4{
	public:
	c_IntMap4();
	c_IntMap4* m_new();
	int p_Compare(int,int);
	void mark();
	String debug();
};
String dbg_type(c_IntMap4**p){return "IntMap";}
class c_Node10 : public Object{
	public:
	int m_key;
	c_Node10* m_right;
	c_Node10* m_left;
	c_CAnimationVariable* m_value;
	int m_color;
	c_Node10* m_parent;
	c_Node10();
	c_Node10* m_new(int,c_CAnimationVariable*,int,c_Node10*);
	c_Node10* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node10**p){return "Node";}
class c_CAnimationState : public Object{
	public:
	int m_ID;
	c_CAnimationNode* m_EntryPoint;
	c_Deque5* m_TransitionArray;
	c_CAnimationState();
	c_CAnimationState* m_new(int);
	c_CAnimationState* m_new2();
	int p_AddTransition(c_CStateTransition*);
	int p_SignalDead();
	int p_Signal(int,c_IntMap*,c_CCommandBuffer*);
	void mark();
	String debug();
};
String dbg_type(c_CAnimationState**p){return "CAnimationState";}
class c_CAnimationNode : public Object{
	public:
	c_CAnimationNode();
	c_CAnimationNode* m_new();
	virtual int p_SignalDead();
	virtual int p_Signal(int,c_IntMap*,c_CCommandBuffer*);
	void mark();
	String debug();
};
String dbg_type(c_CAnimationNode**p){return "CAnimationNode";}
class c_CPlayAnimationNode : public c_CAnimationNode{
	public:
	int m_AnimID;
	c_CPlayAnimationNode();
	c_CPlayAnimationNode* m_new(int);
	c_CPlayAnimationNode* m_new2();
	int p_Signal(int,c_IntMap*,c_CCommandBuffer*);
	void mark();
	String debug();
};
String dbg_type(c_CPlayAnimationNode**p){return "CPlayAnimationNode";}
class c_CFloatMultiRangeCondition : public c_CAnimationNode{
	public:
	c_CAnimationVariable* m_FloatVariable;
	c_Deque4* m_MiniConditionArray;
	c_CAnimationNode* m_ActiveNode;
	c_CFloatMultiRangeCondition();
	c_CFloatMultiRangeCondition* m_new(c_CAnimationVariable*);
	c_CFloatMultiRangeCondition* m_new2();
	int p_AddMiniCondition(c_CFloatRangeMiniCondition*);
	c_CAnimationNode* p_AssessCondition();
	int p_Signal(int,c_IntMap*,c_CCommandBuffer*);
	int p_SignalDead();
	void mark();
	String debug();
};
String dbg_type(c_CFloatMultiRangeCondition**p){return "CFloatMultiRangeCondition";}
class c_CFloatRangeMiniCondition : public Object{
	public:
	Float m_MinRange;
	Float m_MaxRange;
	c_CAnimationNode* m_Link;
	c_CFloatRangeMiniCondition();
	c_CFloatRangeMiniCondition* m_new(Float,Float,c_CAnimationNode*);
	c_CFloatRangeMiniCondition* m_new2();
	bool p_AssessCondition2(c_CAnimationVariable*);
	void mark();
	String debug();
};
String dbg_type(c_CFloatRangeMiniCondition**p){return "CFloatRangeMiniCondition";}
class c_Deque4 : public Object{
	public:
	Array<c_CFloatRangeMiniCondition* > m__data;
	int m__capacity;
	int m__last;
	int m__first;
	c_Deque4();
	c_Deque4* m_new();
	c_Deque4* m_new2(Array<c_CFloatRangeMiniCondition* >);
	int p_Length2();
	void p_Grow();
	void p_PushLast4(c_CFloatRangeMiniCondition*);
	c_Enumerator10* p_ObjectEnumerator();
	c_CFloatRangeMiniCondition* p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Deque4**p){return "Deque";}
class c_CStateTransition : public Object{
	public:
	c_CAnimationState* m_NextState;
	bool m_Inverted;
	c_CStateTransition();
	c_CStateTransition* m_new(c_CAnimationState*,bool);
	c_CStateTransition* m_new2();
	virtual bool p_Assess();
	void mark();
	String debug();
};
String dbg_type(c_CStateTransition**p){return "CStateTransition";}
class c_CStateBoolTransition : public c_CStateTransition{
	public:
	c_CAnimationVariable* m_BoolVariable;
	c_CStateBoolTransition();
	c_CStateBoolTransition* m_new(c_CAnimationVariable*,c_CAnimationState*,bool);
	c_CStateBoolTransition* m_new2();
	bool p_Assess();
	void mark();
	String debug();
};
String dbg_type(c_CStateBoolTransition**p){return "CStateBoolTransition";}
class c_Deque5 : public Object{
	public:
	Array<c_CStateTransition* > m__data;
	int m__capacity;
	int m__last;
	int m__first;
	c_Deque5();
	c_Deque5* m_new();
	c_Deque5* m_new2(Array<c_CStateTransition* >);
	int p_Length2();
	void p_Grow();
	void p_PushLast5(c_CStateTransition*);
	c_Enumerator9* p_ObjectEnumerator();
	c_CStateTransition* p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Deque5**p){return "Deque";}
class c_CMovement : public c_CComponent{
	public:
	c_FVector* m_CurrentPosition;
	c_IntMap* m_AnimationSet;
	c_CAnimationGraph* m_AnimationGraph;
	int m_SpriteID;
	bool m_Init;
	c_IVector* m_CurrentlyOccupiedTile;
	bool m_PathRequested;
	bool m_WaitingForSecondaryPath;
	c_CPath* m_SecondaryPath;
	c_IVector* m_DestinationPosition;
	c_Node9* m_TargetPathNode;
	int m_State;
	c_CPath* m_CurrentPath;
	bool m_HasAPath;
	bool m_MoveTargetSet;
	int m_LastPathUpdateResult;
	c_FVector* m_PreviousPosition;
	Float m_Orientation;
	c_CMovement();
	c_CMovement* m_new(int,c_FVector*,c_IntMap*,c_CAnimationGraph*);
	c_CMovement* m_new2();
	int p_UpdatePathfindRequest();
	bool p_CheckAndSwapPaths();
	bool p_MoveToNode(Float,c_Node9*);
	c_Node9* p_FindBestTargetNode(c_CPath*);
	int p_AssertCanOccupy(c_IVector*,String);
	int p_FollowPath(Float,c_CCommandBuffer*);
	int p_UpdateAnimation(c_CCommandBuffer*);
	int p_Update2(Float,c_CCommandBuffer*);
	static bool m_DebugPath;
	static bool m_DebugTargetPathNode;
	static bool m_DebugUnitID;
	static bool m_DebugPathStatus;
	static bool m_DebugState;
	int p_RenderDebug();
	int p_RequestMoveTo(c_IVector*);
	int p_CancelMoveTo();
	void mark();
	String debug();
};
String dbg_type(c_CMovement**p){return "CMovement";}
class c_CUnit : public c_CComponent{
	public:
	int m_AttitudeGroup;
	c_CMovement* m_Movement;
	int m_SpriteID;
	c_IntMap* m_AnimationSet;
	c_CAnimationGraph* m_AnimationGraph;
	c_FVector* m_Position;
	c_IVector* m_ClosestEnemyPosition;
	bool m_ClosestEnemyDirty;
	bool m_Init;
	int m_HealthBarBackgroundID;
	c_CColor* m_HealthBarBackgroundColor;
	Float m_HealthBarBackgroundWidth;
	Float m_HealthBarBackgroundHeight;
	int m_HealthBarID;
	c_CColor* m_HealthBarColor;
	Float m_HealthBarWidth;
	Float m_HealthBarHeight;
	bool m_EnemyOnAdjacentTile;
	int m_CurrentAnimID;
	bool m_Attack;
	c_FVector* m_PreviousPosition;
	c_FVector* m_HealthBarOffset;
	c_CUnit();
	c_CUnit* m_new(int,c_CMovement*,int,c_IntMap*,c_CAnimationGraph*);
	c_CUnit* m_new2();
	int p_SetClosestEnemy(c_FVector*);
	int p_Update2(Float,c_CCommandBuffer*);
	int p_RenderDebug();
	void mark();
	String debug();
};
String dbg_type(c_CUnit**p){return "CUnit";}
class c_CRenderer : public Object{
	public:
	c_IntMap5* m_RenderItemMap;
	c_CRenderItemList* m_RenderItemList;
	c_CRenderer();
	c_CRenderer* m_new();
	int p_AddRenderItem(c_CRenderItem*);
	int p_Update2(Float,c_CCommandBuffer*);
	int p_Render(c_List3*);
	void mark();
	String debug();
};
String dbg_type(c_CRenderer**p){return "CRenderer";}
class c_CRenderItem : public Object{
	public:
	int m_ID;
	Float m_Z;
	c_FVector* m_Position;
	c_FVector* m_Scale_;
	c_CColor* m_Color;
	c_CRenderItem();
	c_CRenderItem* m_new(int,Float);
	c_CRenderItem* m_new2();
	virtual int p_ProcessCommand(c_CCommand*);
	virtual int p_Update3(Float);
	virtual int p_Render2();
	void mark();
	String debug();
};
String dbg_type(c_CRenderItem**p){return "CRenderItem";}
class c_CMap : public c_CRenderItem{
	public:
	Array<int > m_SpriteIndexArray;
	int m_Width;
	int m_Height;
	int m_TileCount;
	int m_TileWidth;
	c_Image* m_Img;
	c_CMap();
	c_CMap* m_new(int,Float);
	c_CMap* m_new2();
	int p_Render2();
	void mark();
	String debug();
};
String dbg_type(c_CMap**p){return "CMap";}
class c_Map5 : public Object{
	public:
	c_Node11* m_root;
	c_Map5();
	c_Map5* m_new();
	virtual int p_Compare(int,int)=0;
	int p_RotateLeft5(c_Node11*);
	int p_RotateRight5(c_Node11*);
	int p_InsertFixup5(c_Node11*);
	bool p_Add4(int,c_CRenderItem*);
	c_Node11* p_FindNode(int);
	c_CRenderItem* p_Get(int);
	void mark();
	String debug();
};
String dbg_type(c_Map5**p){return "Map";}
class c_IntMap5 : public c_Map5{
	public:
	c_IntMap5();
	c_IntMap5* m_new();
	int p_Compare(int,int);
	void mark();
	String debug();
};
String dbg_type(c_IntMap5**p){return "IntMap";}
class c_Node11 : public Object{
	public:
	int m_key;
	c_Node11* m_right;
	c_Node11* m_left;
	c_CRenderItem* m_value;
	int m_color;
	c_Node11* m_parent;
	c_Node11();
	c_Node11* m_new(int,c_CRenderItem*,int,c_Node11*);
	c_Node11* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node11**p){return "Node";}
class c_List7 : public Object{
	public:
	c_Node12* m__head;
	c_List7();
	c_List7* m_new();
	c_Node12* p_AddLast7(c_CRenderItem*);
	c_List7* m_new2(Array<c_CRenderItem* >);
	c_Node12* p_FirstNode();
	c_Node12* p_AddFirst2(c_CRenderItem*);
	c_Enumerator4* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List7**p){return "List";}
class c_CRenderItemList : public c_List7{
	public:
	c_CRenderItemList();
	c_CRenderItemList* m_new();
	void mark();
	String debug();
};
String dbg_type(c_CRenderItemList**p){return "CRenderItemList";}
class c_Node12 : public Object{
	public:
	c_Node12* m__succ;
	c_Node12* m__pred;
	c_CRenderItem* m__data;
	c_Node12();
	c_Node12* m_new(c_Node12*,c_Node12*,c_CRenderItem*);
	c_Node12* m_new2();
	c_CRenderItem* p_Value();
	virtual c_Node12* p_GetNode();
	c_Node12* p_PrevNode();
	c_Node12* p_NextNode();
	void mark();
	String debug();
};
String dbg_type(c_Node12**p){return "Node";}
class c_HeadNode7 : public c_Node12{
	public:
	c_HeadNode7();
	c_HeadNode7* m_new();
	c_Node12* p_GetNode();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode7**p){return "HeadNode";}
class c_CSprite : public c_CRenderItem{
	public:
	c_CAnimation* m_CurrentAnimation;
	Float m_Time;
	c_CImage* m_ImageToDisplay;
	c_CSprite();
	c_CSprite* m_new(int,Float);
	c_CSprite* m_new2();
	int p_Update3(Float);
	int p_Render2();
	int p_PlayAnim(int);
	int p_ProcessCommand(c_CCommand*);
	void mark();
	String debug();
};
String dbg_type(c_CSprite**p){return "CSprite";}
class c_CSquareItem : public c_CRenderItem{
	public:
	Array<Float > m_Vertices;
	c_CSquareItem();
	c_CSquareItem* m_new(int,Float);
	c_CSquareItem* m_new2();
	int p_Render2();
	void mark();
	String debug();
};
String dbg_type(c_CSquareItem**p){return "CSquareItem";}
class c_CColor : public Object{
	public:
	int m_R;
	int m_G;
	int m_B;
	c_CColor();
	c_CColor* m_new(int,int,int);
	c_CColor* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_CColor**p){return "CColor";}
class c_Enumerator4 : public Object{
	public:
	c_List7* m__list;
	c_Node12* m__curr;
	c_Enumerator4();
	c_Enumerator4* m_new(c_List7*);
	c_Enumerator4* m_new2();
	bool p_HasNext();
	c_CRenderItem* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator4**p){return "Enumerator";}
int bb_graphics_DebugRenderDevice();
int bb_graphics_Cls(Float,Float,Float);
int bb_graphics_PushMatrix();
int bb_graphics_DrawImage(c_Image*,Float,Float,int);
int bb_graphics_Transform(Float,Float,Float,Float,Float,Float);
int bb_graphics_Transform2(Array<Float >);
int bb_graphics_Translate(Float,Float);
int bb_graphics_Rotate(Float);
int bb_graphics_Scale(Float,Float);
int bb_graphics_PopMatrix();
int bb_graphics_DrawImage2(c_Image*,Float,Float,Float,Float,Float,int);
int bb_graphics_DrawText(String,Float,Float,Float,Float);
Float bb_input_MouseX();
Float bb_input_MouseY();
class c_List8 : public Object{
	public:
	c_Node13* m__head;
	c_List8();
	c_List8* m_new();
	c_Node13* p_AddLast8(c_CInputArea*);
	c_List8* m_new2(Array<c_CInputArea* >);
	c_Enumerator5* p_ObjectEnumerator();
	c_Node13* p_FirstNode();
	c_Node13* p_AddFirst3(c_CInputArea*);
	void mark();
	String debug();
};
String dbg_type(c_List8**p){return "List";}
class c_Node13 : public Object{
	public:
	c_Node13* m__succ;
	c_Node13* m__pred;
	c_CInputArea* m__data;
	c_Node13();
	c_Node13* m_new(c_Node13*,c_Node13*,c_CInputArea*);
	c_Node13* m_new2();
	c_CInputArea* p_Value();
	virtual c_Node13* p_GetNode();
	c_Node13* p_PrevNode();
	c_Node13* p_NextNode();
	void mark();
	String debug();
};
String dbg_type(c_Node13**p){return "Node";}
class c_HeadNode8 : public c_Node13{
	public:
	c_HeadNode8();
	c_HeadNode8* m_new();
	c_Node13* p_GetNode();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode8**p){return "HeadNode";}
class c_Enumerator5 : public Object{
	public:
	c_List8* m__list;
	c_Node13* m__curr;
	c_Enumerator5();
	c_Enumerator5* m_new(c_List8*);
	c_Enumerator5* m_new2();
	bool p_HasNext();
	c_CInputArea* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator5**p){return "Enumerator";}
class c_Enumerator6 : public Object{
	public:
	c_List4* m__list;
	c_Node7* m__curr;
	c_Enumerator6();
	c_Enumerator6* m_new(c_List4*);
	c_Enumerator6* m_new2();
	bool p_HasNext();
	c_CGlobalEventData* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator6**p){return "Enumerator";}
int bb_input_MouseDown(int);
class c_List9 : public Object{
	public:
	c_Node14* m__head;
	c_List9();
	c_List9* m_new();
	c_Node14* p_AddLast9(c_CMenuComponent*);
	c_List9* m_new2(Array<c_CMenuComponent* >);
	c_Enumerator7* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List9**p){return "List";}
class c_Node14 : public Object{
	public:
	c_Node14* m__succ;
	c_Node14* m__pred;
	c_CMenuComponent* m__data;
	c_Node14();
	c_Node14* m_new(c_Node14*,c_Node14*,c_CMenuComponent*);
	c_Node14* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node14**p){return "Node";}
class c_HeadNode9 : public c_Node14{
	public:
	c_HeadNode9();
	c_HeadNode9* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode9**p){return "HeadNode";}
class c_Enumerator7 : public Object{
	public:
	c_List9* m__list;
	c_Node14* m__curr;
	c_Enumerator7();
	c_Enumerator7* m_new(c_List9*);
	c_Enumerator7* m_new2();
	bool p_HasNext();
	c_CMenuComponent* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator7**p){return "Enumerator";}
class c_List10 : public Object{
	public:
	c_Node15* m__head;
	c_List10();
	c_List10* m_new();
	c_Node15* p_AddLast10(c_CUnit*);
	c_List10* m_new2(Array<c_CUnit* >);
	c_Enumerator8* p_ObjectEnumerator();
	void mark();
	String debug();
};
String dbg_type(c_List10**p){return "List";}
class c_Node15 : public Object{
	public:
	c_Node15* m__succ;
	c_Node15* m__pred;
	c_CUnit* m__data;
	c_Node15();
	c_Node15* m_new(c_Node15*,c_Node15*,c_CUnit*);
	c_Node15* m_new2();
	void mark();
	String debug();
};
String dbg_type(c_Node15**p){return "Node";}
class c_HeadNode10 : public c_Node15{
	public:
	c_HeadNode10();
	c_HeadNode10* m_new();
	void mark();
	String debug();
};
String dbg_type(c_HeadNode10**p){return "HeadNode";}
class c_Enumerator8 : public Object{
	public:
	c_List10* m__list;
	c_Node15* m__curr;
	c_Enumerator8();
	c_Enumerator8* m_new(c_List10*);
	c_Enumerator8* m_new2();
	bool p_HasNext();
	c_CUnit* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator8**p){return "Enumerator";}
int bb_Misc_Round(Float);
int bb_graphics_DrawLine(Float,Float,Float,Float);
class c_Enumerator9 : public Object{
	public:
	c_Deque5* m__deque;
	int m__index;
	c_Enumerator9();
	c_Enumerator9* m_new(c_Deque5*);
	c_Enumerator9* m_new2();
	bool p_HasNext();
	c_CStateTransition* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator9**p){return "Enumerator";}
class c_Enumerator10 : public Object{
	public:
	c_Deque4* m__deque;
	int m__index;
	c_Enumerator10();
	c_Enumerator10* m_new(c_Deque4*);
	c_Enumerator10* m_new2();
	bool p_HasNext();
	c_CFloatRangeMiniCondition* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator10**p){return "Enumerator";}
int bb_math_Abs(int);
Float bb_math_Abs2(Float);
class c_Enumerator11 : public Object{
	public:
	c_List6* m__list;
	c_Node9* m__curr;
	c_Enumerator11();
	c_Enumerator11* m_new(c_List6*);
	c_Enumerator11* m_new2();
	bool p_HasNext();
	c_IVector* p_NextObject();
	void mark();
	String debug();
};
String dbg_type(c_Enumerator11**p){return "Enumerator";}
int bb_graphics_DrawCircle(Float,Float,Float);
int bb_graphics_DrawPoly(Array<Float >);
int bb_graphics_DrawPoly2(Array<Float >,c_Image*,int);
c_App::c_App(){
}
c_App* c_App::m_new(){
	DBG_ENTER("App.new")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<104>");
	if((bb_app__app)!=0){
		DBG_BLOCK();
		bbError(String(L"App has already been created",28));
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<105>");
	gc_assign(bb_app__app,this);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<106>");
	gc_assign(bb_app__delegate,(new c_GameDelegate)->m_new());
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<107>");
	bb_app__game->SetDelegate(bb_app__delegate);
	return this;
}
int c_App::p_OnCreate(){
	DBG_ENTER("App.OnCreate")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_App::p_OnSuspend(){
	DBG_ENTER("App.OnSuspend")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_App::p_OnResume(){
	DBG_ENTER("App.OnResume")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_App::p_OnUpdate(){
	DBG_ENTER("App.OnUpdate")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_App::p_OnLoading(){
	DBG_ENTER("App.OnLoading")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_App::p_OnRender(){
	DBG_ENTER("App.OnRender")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_App::p_OnClose(){
	DBG_ENTER("App.OnClose")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<129>");
	bb_app_EndApp();
	return 0;
}
int c_App::p_OnBack(){
	DBG_ENTER("App.OnBack")
	c_App *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<133>");
	p_OnClose();
	return 0;
}
void c_App::mark(){
	Object::mark();
}
String c_App::debug(){
	String t="(App)\n";
	return t;
}
c_CMyApp::c_CMyApp(){
	m_FramesPerSecond=30;
	m_DeltaTime=FLOAT(.0);
	m_TerrainImage=0;
	m_ComponentUpdaterList=(new c_List)->m_new();
	m_TerrainSpriteIndexArray=Array<int >();
	m_GameObjectList=(new c_List3)->m_new();
	m_CommandBuffer=(new c_CCommandBuffer)->m_new();
	m_PreviousKeyPDown=0;
	m_PreviousKeyMinusDown=0;
	m_TimeMult=FLOAT(1.0);
	m_PreviousKeyPlusDown=0;
	m_Renderer=(new c_CRenderer)->m_new();
}
c_CMyApp* c_CMyApp::m_new(){
	DBG_ENTER("CMyApp.new")
	c_CMyApp *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/main.monkey<19>");
	c_App::m_new();
	return this;
}
int c_CMyApp::p_AddGameObject(c_CGameObject* t_GameObject){
	DBG_ENTER("CMyApp.AddGameObject")
	c_CMyApp *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_GameObject,"GameObject")
	DBG_INFO("C:/MonkeyX77a/main.monkey<157>");
	c_Enumerator* t_=t_GameObject->m_ComponentList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CComponent* t_Component=t_->p_NextObject();
		DBG_LOCAL(t_Component,"Component")
		DBG_INFO("C:/MonkeyX77a/main.monkey<158>");
		c_Enumerator2* t_2=this->m_ComponentUpdaterList->p_ObjectEnumerator();
		while(t_2->p_HasNext()){
			DBG_BLOCK();
			c_CComponentUpdater* t_ComponentUpdater=t_2->p_NextObject();
			DBG_LOCAL(t_ComponentUpdater,"ComponentUpdater")
			DBG_INFO("C:/MonkeyX77a/main.monkey<159>");
			if(t_ComponentUpdater->m_ComponentType==t_Component->m_Type){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/main.monkey<160>");
				t_ComponentUpdater->p_AddComponent(t_Component);
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<164>");
	this->m_GameObjectList->p_AddLast3(t_GameObject);
	return 0;
}
int c_CMyApp::p_SpawnMenu(c_FVector* t_MenuCenter,int t_AnimID,int t_MenuID,int t_GlobalEventID,c_CMapInterface* t_MapInterface){
	DBG_ENTER("CMyApp.SpawnMenu")
	c_CMyApp *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_MenuCenter,"MenuCenter")
	DBG_LOCAL(t_AnimID,"AnimID")
	DBG_LOCAL(t_MenuID,"MenuID")
	DBG_LOCAL(t_GlobalEventID,"GlobalEventID")
	DBG_LOCAL(t_MapInterface,"MapInterface")
	DBG_INFO("C:/MonkeyX77a/main.monkey<94>");
	c_CGameObject* t_SpawnMenu=(new c_CGameObject)->m_new();
	DBG_LOCAL(t_SpawnMenu,"SpawnMenu")
	DBG_INFO("C:/MonkeyX77a/main.monkey<95>");
	c_CMenuComponent* t_MenuComponent=(new c_CMenuComponent)->m_new(t_MenuCenter,t_AnimID,t_MenuID);
	DBG_LOCAL(t_MenuComponent,"MenuComponent")
	DBG_INFO("C:/MonkeyX77a/main.monkey<96>");
	t_SpawnMenu->p_AddComponent(t_MenuComponent);
	DBG_INFO("C:/MonkeyX77a/main.monkey<97>");
	c_CInputArea* t_InputArea=(new c_CInputArea)->m_new(t_MenuCenter,(new c_FVector)->m_new(FLOAT(72.0),FLOAT(60.0)),1);
	DBG_LOCAL(t_InputArea,"InputArea")
	DBG_INFO("C:/MonkeyX77a/main.monkey<98>");
	t_InputArea->p_BindEvents(2,t_GlobalEventID,(t_MapInterface));
	DBG_INFO("C:/MonkeyX77a/main.monkey<99>");
	t_InputArea->p_BindEvents(0,1,(t_MenuComponent));
	DBG_INFO("C:/MonkeyX77a/main.monkey<100>");
	t_InputArea->p_BindEvents(1,0,(t_MenuComponent));
	DBG_INFO("C:/MonkeyX77a/main.monkey<101>");
	t_InputArea->p_BindEvents(2,2,(t_MenuComponent));
	DBG_INFO("C:/MonkeyX77a/main.monkey<102>");
	t_SpawnMenu->p_AddComponent(t_InputArea);
	DBG_INFO("C:/MonkeyX77a/main.monkey<103>");
	this->p_AddGameObject(t_SpawnMenu);
	return 0;
}
int c_CMyApp::p_OnCreate(){
	DBG_ENTER("CMyApp.OnCreate")
	c_CMyApp *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/main.monkey<47>");
	Array<int > t_date=bb_app_GetDate2();
	DBG_LOCAL(t_date,"date")
	DBG_INFO("C:/MonkeyX77a/main.monkey<48>");
	bb_random_Seed=t_date.At(5);
	DBG_INFO("C:/MonkeyX77a/main.monkey<51>");
	bb_app_SetUpdateRate(this->m_FramesPerSecond);
	DBG_INFO("C:/MonkeyX77a/main.monkey<52>");
	this->m_DeltaTime=FLOAT(1.0)/Float(this->m_FramesPerSecond);
	DBG_INFO("C:/MonkeyX77a/main.monkey<54>");
	gc_assign(this->m_TerrainImage,bb_graphics_LoadImage2(String(L"Terrain.png",11),32,32,16,c_Image::m_DefaultFlags));
	DBG_INFO("C:/MonkeyX77a/main.monkey<55>");
	c_CAIManager::m_Init();
	DBG_INFO("C:/MonkeyX77a/main.monkey<56>");
	c_CSpawnManager::m_Init();
	DBG_INFO("C:/MonkeyX77a/main.monkey<58>");
	this->m_ComponentUpdaterList->p_AddLast((new c_CInputAreaUpdater)->m_new());
	DBG_INFO("C:/MonkeyX77a/main.monkey<59>");
	this->m_ComponentUpdaterList->p_AddLast((new c_CMenuUpdater)->m_new());
	DBG_INFO("C:/MonkeyX77a/main.monkey<60>");
	this->m_ComponentUpdaterList->p_AddLast((new c_CUnitUpdater)->m_new());
	DBG_INFO("C:/MonkeyX77a/main.monkey<63>");
	gc_assign(this->m_TerrainSpriteIndexArray,Array<int >(c_CAIManager::m_TerrainTileCount));
	DBG_INFO("C:/MonkeyX77a/main.monkey<64>");
	for(int t_i=0;t_i<c_CAIManager::m_TerrainTileCount;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/main.monkey<65>");
		if(c_CAIManager::m_TerrainDescriptionArray.At(t_i)==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/main.monkey<66>");
			int t_FrameOffset=int(bb_random_Rnd2(FLOAT(0.0),FLOAT(7.0)));
			DBG_LOCAL(t_FrameOffset,"FrameOffset")
			DBG_INFO("C:/MonkeyX77a/main.monkey<67>");
			this->m_TerrainSpriteIndexArray.At(t_i)=t_FrameOffset;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/main.monkey<69>");
			this->m_TerrainSpriteIndexArray.At(t_i)=12;
		}
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<74>");
	c_CAnimationManager::m_Init();
	DBG_INFO("C:/MonkeyX77a/main.monkey<76>");
	c_FVector* t_MainInputAreaSize=(new c_FVector)->m_new(Float(c_CAIManager::m_TerrainWidth*c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,Float(c_CAIManager::m_TerrainHeight*c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale);
	DBG_LOCAL(t_MainInputAreaSize,"MainInputAreaSize")
	DBG_INFO("C:/MonkeyX77a/main.monkey<77>");
	c_FVector* t_MainInputAreaPosition=(new c_FVector)->m_new(t_MainInputAreaSize->m_X*FLOAT(0.5),t_MainInputAreaSize->m_Y*FLOAT(0.5));
	DBG_LOCAL(t_MainInputAreaPosition,"MainInputAreaPosition")
	DBG_INFO("C:/MonkeyX77a/main.monkey<78>");
	c_CGameObject* t_MapInterfaceGameObject=(new c_CGameObject)->m_new();
	DBG_LOCAL(t_MapInterfaceGameObject,"MapInterfaceGameObject")
	DBG_INFO("C:/MonkeyX77a/main.monkey<79>");
	t_MapInterfaceGameObject->p_AddComponent((new c_CInputArea)->m_new(t_MainInputAreaPosition,t_MainInputAreaSize,0));
	DBG_INFO("C:/MonkeyX77a/main.monkey<80>");
	c_CMapInterface* t_MapInterface=(new c_CMapInterface)->m_new();
	DBG_LOCAL(t_MapInterface,"MapInterface")
	DBG_INFO("C:/MonkeyX77a/main.monkey<81>");
	t_MapInterfaceGameObject->p_AddComponent(t_MapInterface);
	DBG_INFO("C:/MonkeyX77a/main.monkey<82>");
	this->p_AddGameObject(t_MapInterfaceGameObject);
	DBG_INFO("C:/MonkeyX77a/main.monkey<86>");
	p_SpawnMenu((new c_FVector)->m_new(FLOAT(50.0),FLOAT(45.0)),100,0,0,t_MapInterface);
	DBG_INFO("C:/MonkeyX77a/main.monkey<87>");
	p_SpawnMenu((new c_FVector)->m_new(FLOAT(50.0),FLOAT(125.0)),101,1,1,t_MapInterface);
	DBG_INFO("C:/MonkeyX77a/main.monkey<89>");
	int t_MapRenderItemId=this->m_CommandBuffer->p_AddCommand_CreateRenderItem(2,FLOAT(0.0));
	DBG_LOCAL(t_MapRenderItemId,"MapRenderItemId")
	DBG_INFO("C:/MonkeyX77a/main.monkey<90>");
	this->m_CommandBuffer->p_AddCommand(t_MapRenderItemId,7,c_CAIManager::m_MapScale,c_CAIManager::m_MapScale);
	return 0;
}
int c_CMyApp::p_OnUpdate(){
	DBG_ENTER("CMyApp.OnUpdate")
	c_CMyApp *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/main.monkey<107>");
	int t_KeyPDown=bb_input_KeyDown(80);
	DBG_LOCAL(t_KeyPDown,"KeyPDown")
	DBG_INFO("C:/MonkeyX77a/main.monkey<108>");
	if(((t_KeyPDown)!=0) && this->m_PreviousKeyPDown==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/main.monkey<109>");
		if(this->m_DeltaTime>FLOAT(0.0)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/main.monkey<110>");
			this->m_DeltaTime=FLOAT(0.0);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/main.monkey<112>");
			this->m_DeltaTime=FLOAT(1.0)/Float(this->m_FramesPerSecond);
		}
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<115>");
	this->m_PreviousKeyPDown=t_KeyPDown;
	DBG_INFO("C:/MonkeyX77a/main.monkey<117>");
	int t_KeyMinusDown=bb_input_KeyDown(109);
	DBG_LOCAL(t_KeyMinusDown,"KeyMinusDown")
	DBG_INFO("C:/MonkeyX77a/main.monkey<118>");
	if(((t_KeyMinusDown)!=0) && this->m_PreviousKeyMinusDown==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/main.monkey<119>");
		this->m_TimeMult*=FLOAT(0.5);
		DBG_INFO("C:/MonkeyX77a/main.monkey<120>");
		this->m_DeltaTime=FLOAT(1.0)/Float(this->m_FramesPerSecond)*this->m_TimeMult;
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<122>");
	this->m_PreviousKeyMinusDown=t_KeyMinusDown;
	DBG_INFO("C:/MonkeyX77a/main.monkey<124>");
	int t_KeyPlusDown=bb_input_KeyDown(107);
	DBG_LOCAL(t_KeyPlusDown,"KeyPlusDown")
	DBG_INFO("C:/MonkeyX77a/main.monkey<125>");
	if(((t_KeyPlusDown)!=0) && this->m_PreviousKeyPlusDown==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/main.monkey<126>");
		this->m_TimeMult*=FLOAT(2.0);
		DBG_INFO("C:/MonkeyX77a/main.monkey<127>");
		this->m_DeltaTime=FLOAT(1.0)/Float(this->m_FramesPerSecond)*this->m_TimeMult;
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<129>");
	this->m_PreviousKeyPlusDown=t_KeyPlusDown;
	DBG_INFO("C:/MonkeyX77a/main.monkey<131>");
	c_Enumerator3* t_=this->m_GameObjectList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CGameObject* t_GameObject=t_->p_NextObject();
		DBG_LOCAL(t_GameObject,"GameObject")
		DBG_INFO("C:/MonkeyX77a/main.monkey<132>");
		t_GameObject->p_Update2(this->m_DeltaTime,this->m_CommandBuffer);
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<135>");
	c_Enumerator2* t_2=this->m_ComponentUpdaterList->p_ObjectEnumerator();
	while(t_2->p_HasNext()){
		DBG_BLOCK();
		c_CComponentUpdater* t_ComponentUpdater=t_2->p_NextObject();
		DBG_LOCAL(t_ComponentUpdater,"ComponentUpdater")
		DBG_INFO("C:/MonkeyX77a/main.monkey<136>");
		t_ComponentUpdater->p_Update(this->m_CommandBuffer);
	}
	DBG_INFO("C:/MonkeyX77a/main.monkey<139>");
	c_CAIManager::m_Update(this->m_DeltaTime);
	DBG_INFO("C:/MonkeyX77a/main.monkey<141>");
	c_CSpawnManager::m_Update(this,this->m_CommandBuffer);
	DBG_INFO("C:/MonkeyX77a/main.monkey<143>");
	if((this->m_Renderer)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/main.monkey<144>");
		this->m_Renderer->p_Update2(this->m_DeltaTime,this->m_CommandBuffer);
	}
	return 0;
}
int c_CMyApp::p_OnRender(){
	DBG_ENTER("CMyApp.OnRender")
	c_CMyApp *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/main.monkey<150>");
	if((this->m_Renderer)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/main.monkey<151>");
		this->m_Renderer->p_Render(this->m_GameObjectList);
	}
	return 0;
}
void c_CMyApp::mark(){
	c_App::mark();
	gc_mark_q(m_TerrainImage);
	gc_mark_q(m_ComponentUpdaterList);
	gc_mark_q(m_TerrainSpriteIndexArray);
	gc_mark_q(m_GameObjectList);
	gc_mark_q(m_CommandBuffer);
	gc_mark_q(m_Renderer);
}
String c_CMyApp::debug(){
	String t="(CMyApp)\n";
	t=c_App::debug()+t;
	t+=dbg_decl("FramesPerSecond",&m_FramesPerSecond);
	t+=dbg_decl("DeltaTime",&m_DeltaTime);
	t+=dbg_decl("PreviousKeyPDown",&m_PreviousKeyPDown);
	t+=dbg_decl("PreviousKeyMinusDown",&m_PreviousKeyMinusDown);
	t+=dbg_decl("PreviousKeyPlusDown",&m_PreviousKeyPlusDown);
	t+=dbg_decl("TimeMult",&m_TimeMult);
	t+=dbg_decl("TerrainImage",&m_TerrainImage);
	t+=dbg_decl("TerrainSpriteIndexArray",&m_TerrainSpriteIndexArray);
	t+=dbg_decl("GameObjectList",&m_GameObjectList);
	t+=dbg_decl("CommandBuffer",&m_CommandBuffer);
	t+=dbg_decl("Renderer",&m_Renderer);
	t+=dbg_decl("ComponentUpdaterList",&m_ComponentUpdaterList);
	return t;
}
c_App* bb_app__app;
c_GameDelegate::c_GameDelegate(){
	m__graphics=0;
	m__audio=0;
	m__input=0;
}
c_GameDelegate* c_GameDelegate::m_new(){
	DBG_ENTER("GameDelegate.new")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<24>");
	return this;
}
void c_GameDelegate::StartGame(){
	DBG_ENTER("GameDelegate.StartGame")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<33>");
	gc_assign(m__graphics,(new gxtkGraphics));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<34>");
	bb_graphics_SetGraphicsDevice(m__graphics);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<35>");
	bb_graphics_SetFont(0,32);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<37>");
	gc_assign(m__audio,(new gxtkAudio));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<38>");
	bb_audio_SetAudioDevice(m__audio);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<40>");
	gc_assign(m__input,(new c_InputDevice)->m_new());
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<41>");
	bb_input_SetInputDevice(m__input);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<43>");
	bb_app__app->p_OnCreate();
}
void c_GameDelegate::SuspendGame(){
	DBG_ENTER("GameDelegate.SuspendGame")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<47>");
	bb_app__app->p_OnSuspend();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<48>");
	m__audio->Suspend();
}
void c_GameDelegate::ResumeGame(){
	DBG_ENTER("GameDelegate.ResumeGame")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<52>");
	m__audio->Resume();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<53>");
	bb_app__app->p_OnResume();
}
void c_GameDelegate::UpdateGame(){
	DBG_ENTER("GameDelegate.UpdateGame")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<57>");
	m__input->p_BeginUpdate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<58>");
	bb_app__app->p_OnUpdate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<59>");
	m__input->p_EndUpdate();
}
void c_GameDelegate::RenderGame(){
	DBG_ENTER("GameDelegate.RenderGame")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<63>");
	int t_mode=m__graphics->BeginRender();
	DBG_LOCAL(t_mode,"mode")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<64>");
	if((t_mode)!=0){
		DBG_BLOCK();
		bb_graphics_BeginRender();
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<65>");
	if(t_mode==2){
		DBG_BLOCK();
		bb_app__app->p_OnLoading();
	}else{
		DBG_BLOCK();
		bb_app__app->p_OnRender();
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<66>");
	if((t_mode)!=0){
		DBG_BLOCK();
		bb_graphics_EndRender();
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<67>");
	m__graphics->EndRender();
}
void c_GameDelegate::KeyEvent(int t_event,int t_data){
	DBG_ENTER("GameDelegate.KeyEvent")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<71>");
	m__input->p_KeyEvent(t_event,t_data);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<72>");
	if(t_event!=1){
		DBG_BLOCK();
		return;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<73>");
	int t_1=t_data;
	DBG_LOCAL(t_1,"1")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<74>");
	if(t_1==432){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<75>");
		bb_app__app->p_OnClose();
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<76>");
		if(t_1==416){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<77>");
			bb_app__app->p_OnBack();
		}
	}
}
void c_GameDelegate::MouseEvent(int t_event,int t_data,Float t_x,Float t_y){
	DBG_ENTER("GameDelegate.MouseEvent")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<82>");
	m__input->p_MouseEvent(t_event,t_data,t_x,t_y);
}
void c_GameDelegate::TouchEvent(int t_event,int t_data,Float t_x,Float t_y){
	DBG_ENTER("GameDelegate.TouchEvent")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<86>");
	m__input->p_TouchEvent(t_event,t_data,t_x,t_y);
}
void c_GameDelegate::MotionEvent(int t_event,int t_data,Float t_x,Float t_y,Float t_z){
	DBG_ENTER("GameDelegate.MotionEvent")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_z,"z")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<90>");
	m__input->p_MotionEvent(t_event,t_data,t_x,t_y,t_z);
}
void c_GameDelegate::DiscardGraphics(){
	DBG_ENTER("GameDelegate.DiscardGraphics")
	c_GameDelegate *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<94>");
	m__graphics->DiscardGraphics();
}
void c_GameDelegate::mark(){
	BBGameDelegate::mark();
	gc_mark_q(m__graphics);
	gc_mark_q(m__audio);
	gc_mark_q(m__input);
}
String c_GameDelegate::debug(){
	String t="(GameDelegate)\n";
	t+=dbg_decl("_graphics",&m__graphics);
	t+=dbg_decl("_audio",&m__audio);
	t+=dbg_decl("_input",&m__input);
	return t;
}
c_GameDelegate* bb_app__delegate;
BBGame* bb_app__game;
int bbMain(){
	DBG_ENTER("Main")
	DBG_INFO("C:/MonkeyX77a/main.monkey<169>");
	(new c_CMyApp)->m_new();
	return 0;
}
gxtkGraphics* bb_graphics_device;
int bb_graphics_SetGraphicsDevice(gxtkGraphics* t_dev){
	DBG_ENTER("SetGraphicsDevice")
	DBG_LOCAL(t_dev,"dev")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<59>");
	gc_assign(bb_graphics_device,t_dev);
	return 0;
}
c_Image::c_Image(){
	m_surface=0;
	m_width=0;
	m_height=0;
	m_frames=Array<c_Frame* >();
	m_flags=0;
	m_tx=FLOAT(.0);
	m_ty=FLOAT(.0);
	m_source=0;
}
int c_Image::m_DefaultFlags;
c_Image* c_Image::m_new(){
	DBG_ENTER("Image.new")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<66>");
	return this;
}
int c_Image::p_SetHandle(Float t_tx,Float t_ty){
	DBG_ENTER("Image.SetHandle")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_tx,"tx")
	DBG_LOCAL(t_ty,"ty")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<110>");
	this->m_tx=t_tx;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<111>");
	this->m_ty=t_ty;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<112>");
	this->m_flags=this->m_flags&-2;
	return 0;
}
int c_Image::p_ApplyFlags(int t_iflags){
	DBG_ENTER("Image.ApplyFlags")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_iflags,"iflags")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<188>");
	m_flags=t_iflags;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<190>");
	if((m_flags&2)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<191>");
		Array<c_Frame* > t_=m_frames;
		int t_2=0;
		while(t_2<t_.Length()){
			DBG_BLOCK();
			c_Frame* t_f=t_.At(t_2);
			t_2=t_2+1;
			DBG_LOCAL(t_f,"f")
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<192>");
			t_f->m_x+=1;
		}
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<194>");
		m_width-=2;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<197>");
	if((m_flags&4)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<198>");
		Array<c_Frame* > t_3=m_frames;
		int t_4=0;
		while(t_4<t_3.Length()){
			DBG_BLOCK();
			c_Frame* t_f2=t_3.At(t_4);
			t_4=t_4+1;
			DBG_LOCAL(t_f2,"f")
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<199>");
			t_f2->m_y+=1;
		}
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<201>");
		m_height-=2;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<204>");
	if((m_flags&1)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<205>");
		p_SetHandle(Float(m_width)/FLOAT(2.0),Float(m_height)/FLOAT(2.0));
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<208>");
	if(m_frames.Length()==1 && m_frames.At(0)->m_x==0 && m_frames.At(0)->m_y==0 && m_width==m_surface->Width() && m_height==m_surface->Height()){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<209>");
		m_flags|=65536;
	}
	return 0;
}
c_Image* c_Image::p_Init(gxtkSurface* t_surf,int t_nframes,int t_iflags){
	DBG_ENTER("Image.Init")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_surf,"surf")
	DBG_LOCAL(t_nframes,"nframes")
	DBG_LOCAL(t_iflags,"iflags")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<146>");
	gc_assign(m_surface,t_surf);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<148>");
	m_width=m_surface->Width()/t_nframes;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<149>");
	m_height=m_surface->Height();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<151>");
	gc_assign(m_frames,Array<c_Frame* >(t_nframes));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<152>");
	for(int t_i=0;t_i<t_nframes;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<153>");
		gc_assign(m_frames.At(t_i),(new c_Frame)->m_new(t_i*m_width,0));
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<156>");
	p_ApplyFlags(t_iflags);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<157>");
	return this;
}
c_Image* c_Image::p_Init2(gxtkSurface* t_surf,int t_x,int t_y,int t_iwidth,int t_iheight,int t_nframes,int t_iflags,c_Image* t_src,int t_srcx,int t_srcy,int t_srcw,int t_srch){
	DBG_ENTER("Image.Init")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_surf,"surf")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_iwidth,"iwidth")
	DBG_LOCAL(t_iheight,"iheight")
	DBG_LOCAL(t_nframes,"nframes")
	DBG_LOCAL(t_iflags,"iflags")
	DBG_LOCAL(t_src,"src")
	DBG_LOCAL(t_srcx,"srcx")
	DBG_LOCAL(t_srcy,"srcy")
	DBG_LOCAL(t_srcw,"srcw")
	DBG_LOCAL(t_srch,"srch")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<161>");
	gc_assign(m_surface,t_surf);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<162>");
	gc_assign(m_source,t_src);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<164>");
	m_width=t_iwidth;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<165>");
	m_height=t_iheight;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<167>");
	gc_assign(m_frames,Array<c_Frame* >(t_nframes));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<169>");
	int t_ix=t_x;
	int t_iy=t_y;
	DBG_LOCAL(t_ix,"ix")
	DBG_LOCAL(t_iy,"iy")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<171>");
	for(int t_i=0;t_i<t_nframes;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<172>");
		if(t_ix+m_width>t_srcw){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<173>");
			t_ix=0;
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<174>");
			t_iy+=m_height;
		}
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<176>");
		if(t_ix+m_width>t_srcw || t_iy+m_height>t_srch){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<177>");
			bbError(String(L"Image frame outside surface",27));
		}
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<179>");
		gc_assign(m_frames.At(t_i),(new c_Frame)->m_new(t_ix+t_srcx,t_iy+t_srcy));
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<180>");
		t_ix+=m_width;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<183>");
	p_ApplyFlags(t_iflags);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<184>");
	return this;
}
int c_Image::p_Width(){
	DBG_ENTER("Image.Width")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<77>");
	return m_width;
}
int c_Image::p_Height(){
	DBG_ENTER("Image.Height")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<81>");
	return m_height;
}
int c_Image::p_Frames(){
	DBG_ENTER("Image.Frames")
	c_Image *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<89>");
	int t_=m_frames.Length();
	return t_;
}
void c_Image::mark(){
	Object::mark();
	gc_mark_q(m_surface);
	gc_mark_q(m_frames);
	gc_mark_q(m_source);
}
String c_Image::debug(){
	String t="(Image)\n";
	t+=dbg_decl("DefaultFlags",&c_Image::m_DefaultFlags);
	t+=dbg_decl("source",&m_source);
	t+=dbg_decl("surface",&m_surface);
	t+=dbg_decl("width",&m_width);
	t+=dbg_decl("height",&m_height);
	t+=dbg_decl("flags",&m_flags);
	t+=dbg_decl("frames",&m_frames);
	t+=dbg_decl("tx",&m_tx);
	t+=dbg_decl("ty",&m_ty);
	return t;
}
c_GraphicsContext::c_GraphicsContext(){
	m_defaultFont=0;
	m_font=0;
	m_firstChar=0;
	m_matrixSp=0;
	m_ix=FLOAT(1.0);
	m_iy=FLOAT(.0);
	m_jx=FLOAT(.0);
	m_jy=FLOAT(1.0);
	m_tx=FLOAT(.0);
	m_ty=FLOAT(.0);
	m_tformed=0;
	m_matDirty=0;
	m_color_r=FLOAT(.0);
	m_color_g=FLOAT(.0);
	m_color_b=FLOAT(.0);
	m_alpha=FLOAT(.0);
	m_blend=0;
	m_scissor_x=FLOAT(.0);
	m_scissor_y=FLOAT(.0);
	m_scissor_width=FLOAT(.0);
	m_scissor_height=FLOAT(.0);
	m_matrixStack=Array<Float >(192);
}
c_GraphicsContext* c_GraphicsContext::m_new(){
	DBG_ENTER("GraphicsContext.new")
	c_GraphicsContext *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<25>");
	return this;
}
int c_GraphicsContext::p_Validate(){
	DBG_ENTER("GraphicsContext.Validate")
	c_GraphicsContext *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<36>");
	if((m_matDirty)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<37>");
		bb_graphics_renderDevice->SetMatrix(bb_graphics_context->m_ix,bb_graphics_context->m_iy,bb_graphics_context->m_jx,bb_graphics_context->m_jy,bb_graphics_context->m_tx,bb_graphics_context->m_ty);
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<38>");
		m_matDirty=0;
	}
	return 0;
}
void c_GraphicsContext::mark(){
	Object::mark();
	gc_mark_q(m_defaultFont);
	gc_mark_q(m_font);
	gc_mark_q(m_matrixStack);
}
String c_GraphicsContext::debug(){
	String t="(GraphicsContext)\n";
	t+=dbg_decl("color_r",&m_color_r);
	t+=dbg_decl("color_g",&m_color_g);
	t+=dbg_decl("color_b",&m_color_b);
	t+=dbg_decl("alpha",&m_alpha);
	t+=dbg_decl("blend",&m_blend);
	t+=dbg_decl("ix",&m_ix);
	t+=dbg_decl("iy",&m_iy);
	t+=dbg_decl("jx",&m_jx);
	t+=dbg_decl("jy",&m_jy);
	t+=dbg_decl("tx",&m_tx);
	t+=dbg_decl("ty",&m_ty);
	t+=dbg_decl("tformed",&m_tformed);
	t+=dbg_decl("matDirty",&m_matDirty);
	t+=dbg_decl("scissor_x",&m_scissor_x);
	t+=dbg_decl("scissor_y",&m_scissor_y);
	t+=dbg_decl("scissor_width",&m_scissor_width);
	t+=dbg_decl("scissor_height",&m_scissor_height);
	t+=dbg_decl("matrixStack",&m_matrixStack);
	t+=dbg_decl("matrixSp",&m_matrixSp);
	t+=dbg_decl("font",&m_font);
	t+=dbg_decl("firstChar",&m_firstChar);
	t+=dbg_decl("defaultFont",&m_defaultFont);
	return t;
}
c_GraphicsContext* bb_graphics_context;
String bb_data_FixDataPath(String t_path){
	DBG_ENTER("FixDataPath")
	DBG_LOCAL(t_path,"path")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/data.monkey<3>");
	int t_i=t_path.Find(String(L":/",2),0);
	DBG_LOCAL(t_i,"i")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/data.monkey<4>");
	if(t_i!=-1 && t_path.Find(String(L"/",1),0)==t_i+1){
		DBG_BLOCK();
		return t_path;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/data.monkey<5>");
	if(t_path.StartsWith(String(L"./",2)) || t_path.StartsWith(String(L"/",1))){
		DBG_BLOCK();
		return t_path;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/data.monkey<6>");
	String t_=String(L"monkey://data/",14)+t_path;
	return t_;
}
c_Frame::c_Frame(){
	m_x=0;
	m_y=0;
}
c_Frame* c_Frame::m_new(int t_x,int t_y){
	DBG_ENTER("Frame.new")
	c_Frame *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<19>");
	this->m_x=t_x;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<20>");
	this->m_y=t_y;
	return this;
}
c_Frame* c_Frame::m_new2(){
	DBG_ENTER("Frame.new")
	c_Frame *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<14>");
	return this;
}
void c_Frame::mark(){
	Object::mark();
}
String c_Frame::debug(){
	String t="(Frame)\n";
	t+=dbg_decl("x",&m_x);
	t+=dbg_decl("y",&m_y);
	return t;
}
c_Image* bb_graphics_LoadImage(String t_path,int t_frameCount,int t_flags){
	DBG_ENTER("LoadImage")
	DBG_LOCAL(t_path,"path")
	DBG_LOCAL(t_frameCount,"frameCount")
	DBG_LOCAL(t_flags,"flags")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<238>");
	gxtkSurface* t_surf=bb_graphics_device->LoadSurface(bb_data_FixDataPath(t_path));
	DBG_LOCAL(t_surf,"surf")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<239>");
	if((t_surf)!=0){
		DBG_BLOCK();
		c_Image* t_=((new c_Image)->m_new())->p_Init(t_surf,t_frameCount,t_flags);
		return t_;
	}
	return 0;
}
c_Image* bb_graphics_LoadImage2(String t_path,int t_frameWidth,int t_frameHeight,int t_frameCount,int t_flags){
	DBG_ENTER("LoadImage")
	DBG_LOCAL(t_path,"path")
	DBG_LOCAL(t_frameWidth,"frameWidth")
	DBG_LOCAL(t_frameHeight,"frameHeight")
	DBG_LOCAL(t_frameCount,"frameCount")
	DBG_LOCAL(t_flags,"flags")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<243>");
	gxtkSurface* t_surf=bb_graphics_device->LoadSurface(bb_data_FixDataPath(t_path));
	DBG_LOCAL(t_surf,"surf")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<244>");
	if((t_surf)!=0){
		DBG_BLOCK();
		c_Image* t_=((new c_Image)->m_new())->p_Init2(t_surf,0,0,t_frameWidth,t_frameHeight,t_frameCount,t_flags,0,0,0,t_surf->Width(),t_surf->Height());
		return t_;
	}
	return 0;
}
int bb_graphics_SetFont(c_Image* t_font,int t_firstChar){
	DBG_ENTER("SetFont")
	DBG_LOCAL(t_font,"font")
	DBG_LOCAL(t_firstChar,"firstChar")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<545>");
	if(!((t_font)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<546>");
		if(!((bb_graphics_context->m_defaultFont)!=0)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<547>");
			gc_assign(bb_graphics_context->m_defaultFont,bb_graphics_LoadImage(String(L"mojo_font.png",13),96,2));
		}
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<549>");
		t_font=bb_graphics_context->m_defaultFont;
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<550>");
		t_firstChar=32;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<552>");
	gc_assign(bb_graphics_context->m_font,t_font);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<553>");
	bb_graphics_context->m_firstChar=t_firstChar;
	return 0;
}
gxtkAudio* bb_audio_device;
int bb_audio_SetAudioDevice(gxtkAudio* t_dev){
	DBG_ENTER("SetAudioDevice")
	DBG_LOCAL(t_dev,"dev")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/audio.monkey<18>");
	gc_assign(bb_audio_device,t_dev);
	return 0;
}
c_InputDevice::c_InputDevice(){
	m__joyStates=Array<c_JoyState* >(4);
	m__keyDown=Array<bool >(512);
	m__keyHitPut=0;
	m__keyHitQueue=Array<int >(33);
	m__keyHit=Array<int >(512);
	m__charGet=0;
	m__charPut=0;
	m__charQueue=Array<int >(32);
	m__mouseX=FLOAT(.0);
	m__mouseY=FLOAT(.0);
	m__touchX=Array<Float >(32);
	m__touchY=Array<Float >(32);
	m__accelX=FLOAT(.0);
	m__accelY=FLOAT(.0);
	m__accelZ=FLOAT(.0);
}
c_InputDevice* c_InputDevice::m_new(){
	DBG_ENTER("InputDevice.new")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<22>");
	for(int t_i=0;t_i<4;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<23>");
		gc_assign(m__joyStates.At(t_i),(new c_JoyState)->m_new());
	}
	return this;
}
void c_InputDevice::p_PutKeyHit(int t_key){
	DBG_ENTER("InputDevice.PutKeyHit")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<233>");
	if(m__keyHitPut==m__keyHitQueue.Length()){
		DBG_BLOCK();
		return;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<234>");
	m__keyHit.At(t_key)+=1;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<235>");
	m__keyHitQueue.At(m__keyHitPut)=t_key;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<236>");
	m__keyHitPut+=1;
}
void c_InputDevice::p_BeginUpdate(){
	DBG_ENTER("InputDevice.BeginUpdate")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<185>");
	for(int t_i=0;t_i<4;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<186>");
		c_JoyState* t_state=m__joyStates.At(t_i);
		DBG_LOCAL(t_state,"state")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<187>");
		if(!BBGame::Game()->PollJoystick(t_i,t_state->m_joyx,t_state->m_joyy,t_state->m_joyz,t_state->m_buttons)){
			DBG_BLOCK();
			break;
		}
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<188>");
		for(int t_j=0;t_j<32;t_j=t_j+1){
			DBG_BLOCK();
			DBG_LOCAL(t_j,"j")
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<189>");
			int t_key=256+t_i*32+t_j;
			DBG_LOCAL(t_key,"key")
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<190>");
			if(t_state->m_buttons.At(t_j)){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<191>");
				if(!m__keyDown.At(t_key)){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<192>");
					m__keyDown.At(t_key)=true;
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<193>");
					p_PutKeyHit(t_key);
				}
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<196>");
				m__keyDown.At(t_key)=false;
			}
		}
	}
}
void c_InputDevice::p_EndUpdate(){
	DBG_ENTER("InputDevice.EndUpdate")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<203>");
	for(int t_i=0;t_i<m__keyHitPut;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<204>");
		m__keyHit.At(m__keyHitQueue.At(t_i))=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<206>");
	m__keyHitPut=0;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<207>");
	m__charGet=0;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<208>");
	m__charPut=0;
}
void c_InputDevice::p_KeyEvent(int t_event,int t_data){
	DBG_ENTER("InputDevice.KeyEvent")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<107>");
	int t_1=t_event;
	DBG_LOCAL(t_1,"1")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<108>");
	if(t_1==1){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<109>");
		if(!m__keyDown.At(t_data)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<110>");
			m__keyDown.At(t_data)=true;
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<111>");
			p_PutKeyHit(t_data);
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<112>");
			if(t_data==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<113>");
				m__keyDown.At(384)=true;
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<114>");
				p_PutKeyHit(384);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<115>");
				if(t_data==384){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<116>");
					m__keyDown.At(1)=true;
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<117>");
					p_PutKeyHit(1);
				}
			}
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<120>");
		if(t_1==2){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<121>");
			if(m__keyDown.At(t_data)){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<122>");
				m__keyDown.At(t_data)=false;
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<123>");
				if(t_data==1){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<124>");
					m__keyDown.At(384)=false;
				}else{
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<125>");
					if(t_data==384){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<126>");
						m__keyDown.At(1)=false;
					}
				}
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<129>");
			if(t_1==3){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<130>");
				if(m__charPut<m__charQueue.Length()){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<131>");
					m__charQueue.At(m__charPut)=t_data;
					DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<132>");
					m__charPut+=1;
				}
			}
		}
	}
}
void c_InputDevice::p_MouseEvent(int t_event,int t_data,Float t_x,Float t_y){
	DBG_ENTER("InputDevice.MouseEvent")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<138>");
	int t_2=t_event;
	DBG_LOCAL(t_2,"2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<139>");
	if(t_2==4){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<140>");
		p_KeyEvent(1,1+t_data);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<141>");
		if(t_2==5){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<142>");
			p_KeyEvent(2,1+t_data);
			return;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<144>");
			if(t_2==6){
				DBG_BLOCK();
			}else{
				DBG_BLOCK();
				return;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<148>");
	m__mouseX=t_x;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<149>");
	m__mouseY=t_y;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<150>");
	m__touchX.At(0)=t_x;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<151>");
	m__touchY.At(0)=t_y;
}
void c_InputDevice::p_TouchEvent(int t_event,int t_data,Float t_x,Float t_y){
	DBG_ENTER("InputDevice.TouchEvent")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<155>");
	int t_3=t_event;
	DBG_LOCAL(t_3,"3")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<156>");
	if(t_3==7){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<157>");
		p_KeyEvent(1,384+t_data);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<158>");
		if(t_3==8){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<159>");
			p_KeyEvent(2,384+t_data);
			return;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<161>");
			if(t_3==9){
				DBG_BLOCK();
			}else{
				DBG_BLOCK();
				return;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<165>");
	m__touchX.At(t_data)=t_x;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<166>");
	m__touchY.At(t_data)=t_y;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<167>");
	if(t_data==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<168>");
		m__mouseX=t_x;
		DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<169>");
		m__mouseY=t_y;
	}
}
void c_InputDevice::p_MotionEvent(int t_event,int t_data,Float t_x,Float t_y,Float t_z){
	DBG_ENTER("InputDevice.MotionEvent")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_event,"event")
	DBG_LOCAL(t_data,"data")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_z,"z")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<174>");
	int t_4=t_event;
	DBG_LOCAL(t_4,"4")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<175>");
	if(t_4==10){
		DBG_BLOCK();
	}else{
		DBG_BLOCK();
		return;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<179>");
	m__accelX=t_x;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<180>");
	m__accelY=t_y;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<181>");
	m__accelZ=t_z;
}
bool c_InputDevice::p_KeyDown(int t_key){
	DBG_ENTER("InputDevice.KeyDown")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<43>");
	if(t_key>0 && t_key<512){
		DBG_BLOCK();
		return m__keyDown.At(t_key);
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<44>");
	return false;
}
Float c_InputDevice::p_MouseX(){
	DBG_ENTER("InputDevice.MouseX")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<65>");
	return m__mouseX;
}
Float c_InputDevice::p_MouseY(){
	DBG_ENTER("InputDevice.MouseY")
	c_InputDevice *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<69>");
	return m__mouseY;
}
void c_InputDevice::mark(){
	Object::mark();
	gc_mark_q(m__joyStates);
	gc_mark_q(m__keyDown);
	gc_mark_q(m__keyHitQueue);
	gc_mark_q(m__keyHit);
	gc_mark_q(m__charQueue);
	gc_mark_q(m__touchX);
	gc_mark_q(m__touchY);
}
String c_InputDevice::debug(){
	String t="(InputDevice)\n";
	t+=dbg_decl("_keyDown",&m__keyDown);
	t+=dbg_decl("_keyHit",&m__keyHit);
	t+=dbg_decl("_keyHitQueue",&m__keyHitQueue);
	t+=dbg_decl("_keyHitPut",&m__keyHitPut);
	t+=dbg_decl("_charQueue",&m__charQueue);
	t+=dbg_decl("_charPut",&m__charPut);
	t+=dbg_decl("_charGet",&m__charGet);
	t+=dbg_decl("_mouseX",&m__mouseX);
	t+=dbg_decl("_mouseY",&m__mouseY);
	t+=dbg_decl("_touchX",&m__touchX);
	t+=dbg_decl("_touchY",&m__touchY);
	t+=dbg_decl("_accelX",&m__accelX);
	t+=dbg_decl("_accelY",&m__accelY);
	t+=dbg_decl("_accelZ",&m__accelZ);
	t+=dbg_decl("_joyStates",&m__joyStates);
	return t;
}
c_JoyState::c_JoyState(){
	m_joyx=Array<Float >(2);
	m_joyy=Array<Float >(2);
	m_joyz=Array<Float >(2);
	m_buttons=Array<bool >(32);
}
c_JoyState* c_JoyState::m_new(){
	DBG_ENTER("JoyState.new")
	c_JoyState *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/inputdevice.monkey<10>");
	return this;
}
void c_JoyState::mark(){
	Object::mark();
	gc_mark_q(m_joyx);
	gc_mark_q(m_joyy);
	gc_mark_q(m_joyz);
	gc_mark_q(m_buttons);
}
String c_JoyState::debug(){
	String t="(JoyState)\n";
	t+=dbg_decl("joyx",&m_joyx);
	t+=dbg_decl("joyy",&m_joyy);
	t+=dbg_decl("joyz",&m_joyz);
	t+=dbg_decl("buttons",&m_buttons);
	return t;
}
c_InputDevice* bb_input_device;
int bb_input_SetInputDevice(c_InputDevice* t_dev){
	DBG_ENTER("SetInputDevice")
	DBG_LOCAL(t_dev,"dev")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/input.monkey<18>");
	gc_assign(bb_input_device,t_dev);
	return 0;
}
gxtkGraphics* bb_graphics_renderDevice;
int bb_graphics_SetMatrix(Float t_ix,Float t_iy,Float t_jx,Float t_jy,Float t_tx,Float t_ty){
	DBG_ENTER("SetMatrix")
	DBG_LOCAL(t_ix,"ix")
	DBG_LOCAL(t_iy,"iy")
	DBG_LOCAL(t_jx,"jx")
	DBG_LOCAL(t_jy,"jy")
	DBG_LOCAL(t_tx,"tx")
	DBG_LOCAL(t_ty,"ty")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<311>");
	bb_graphics_context->m_ix=t_ix;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<312>");
	bb_graphics_context->m_iy=t_iy;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<313>");
	bb_graphics_context->m_jx=t_jx;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<314>");
	bb_graphics_context->m_jy=t_jy;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<315>");
	bb_graphics_context->m_tx=t_tx;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<316>");
	bb_graphics_context->m_ty=t_ty;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<317>");
	bb_graphics_context->m_tformed=((t_ix!=FLOAT(1.0) || t_iy!=FLOAT(0.0) || t_jx!=FLOAT(0.0) || t_jy!=FLOAT(1.0) || t_tx!=FLOAT(0.0) || t_ty!=FLOAT(0.0))?1:0);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<318>");
	bb_graphics_context->m_matDirty=1;
	return 0;
}
int bb_graphics_SetMatrix2(Array<Float > t_m){
	DBG_ENTER("SetMatrix")
	DBG_LOCAL(t_m,"m")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<307>");
	bb_graphics_SetMatrix(t_m.At(0),t_m.At(1),t_m.At(2),t_m.At(3),t_m.At(4),t_m.At(5));
	return 0;
}
int bb_graphics_SetColor(Float t_r,Float t_g,Float t_b){
	DBG_ENTER("SetColor")
	DBG_LOCAL(t_r,"r")
	DBG_LOCAL(t_g,"g")
	DBG_LOCAL(t_b,"b")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<253>");
	bb_graphics_context->m_color_r=t_r;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<254>");
	bb_graphics_context->m_color_g=t_g;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<255>");
	bb_graphics_context->m_color_b=t_b;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<256>");
	bb_graphics_renderDevice->SetColor(t_r,t_g,t_b);
	return 0;
}
int bb_graphics_SetAlpha(Float t_alpha){
	DBG_ENTER("SetAlpha")
	DBG_LOCAL(t_alpha,"alpha")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<270>");
	bb_graphics_context->m_alpha=t_alpha;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<271>");
	bb_graphics_renderDevice->SetAlpha(t_alpha);
	return 0;
}
int bb_graphics_SetBlend(int t_blend){
	DBG_ENTER("SetBlend")
	DBG_LOCAL(t_blend,"blend")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<279>");
	bb_graphics_context->m_blend=t_blend;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<280>");
	bb_graphics_renderDevice->SetBlend(t_blend);
	return 0;
}
int bb_graphics_DeviceWidth(){
	DBG_ENTER("DeviceWidth")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<230>");
	int t_=bb_graphics_device->Width();
	return t_;
}
int bb_graphics_DeviceHeight(){
	DBG_ENTER("DeviceHeight")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<234>");
	int t_=bb_graphics_device->Height();
	return t_;
}
int bb_graphics_SetScissor(Float t_x,Float t_y,Float t_width,Float t_height){
	DBG_ENTER("SetScissor")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_width,"width")
	DBG_LOCAL(t_height,"height")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<288>");
	bb_graphics_context->m_scissor_x=t_x;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<289>");
	bb_graphics_context->m_scissor_y=t_y;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<290>");
	bb_graphics_context->m_scissor_width=t_width;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<291>");
	bb_graphics_context->m_scissor_height=t_height;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<292>");
	bb_graphics_renderDevice->SetScissor(int(t_x),int(t_y),int(t_width),int(t_height));
	return 0;
}
int bb_graphics_BeginRender(){
	DBG_ENTER("BeginRender")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<216>");
	gc_assign(bb_graphics_renderDevice,bb_graphics_device);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<217>");
	bb_graphics_context->m_matrixSp=0;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<218>");
	bb_graphics_SetMatrix(FLOAT(1.0),FLOAT(0.0),FLOAT(0.0),FLOAT(1.0),FLOAT(0.0),FLOAT(0.0));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<219>");
	bb_graphics_SetColor(FLOAT(255.0),FLOAT(255.0),FLOAT(255.0));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<220>");
	bb_graphics_SetAlpha(FLOAT(1.0));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<221>");
	bb_graphics_SetBlend(0);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<222>");
	bb_graphics_SetScissor(FLOAT(0.0),FLOAT(0.0),Float(bb_graphics_DeviceWidth()),Float(bb_graphics_DeviceHeight()));
	return 0;
}
int bb_graphics_EndRender(){
	DBG_ENTER("EndRender")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<226>");
	bb_graphics_renderDevice=0;
	return 0;
}
c_BBGameEvent::c_BBGameEvent(){
}
void c_BBGameEvent::mark(){
	Object::mark();
}
String c_BBGameEvent::debug(){
	String t="(BBGameEvent)\n";
	return t;
}
int bb_app_EndApp(){
	DBG_ENTER("EndApp")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<186>");
	bbError(String());
	return 0;
}
int bb_app_GetDate(Array<int > t_date){
	DBG_ENTER("GetDate")
	DBG_LOCAL(t_date,"date")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<170>");
	bb_app__game->GetDate(t_date);
	return 0;
}
Array<int > bb_app_GetDate2(){
	DBG_ENTER("GetDate")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<164>");
	Array<int > t_date=Array<int >(7);
	DBG_LOCAL(t_date,"date")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<165>");
	bb_app_GetDate(t_date);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<166>");
	return t_date;
}
int bb_random_Seed;
int bb_app__updateRate;
int bb_app_SetUpdateRate(int t_hertz){
	DBG_ENTER("SetUpdateRate")
	DBG_LOCAL(t_hertz,"hertz")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<151>");
	bb_app__updateRate=t_hertz;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/app.monkey<152>");
	bb_app__game->SetUpdateRate(t_hertz);
	return 0;
}
c_CAIManager::c_CAIManager(){
}
Array<int > c_CAIManager::m_TerrainDescriptionArray;
int c_CAIManager::m_TerrainWidth;
int c_CAIManager::m_TerrainHeight;
int c_CAIManager::m_TerrainTileCount;
Array<c_PathfindingMapItem* > c_CAIManager::m_PathfindingMap;
Array<c_IVector* > c_CAIManager::m_NeighbourArray;
Array<c_COpenListItem* > c_CAIManager::m_OpenListItemPool;
Array<c_CTileStatusData* > c_CAIManager::m_TileStatusArray;
int c_CAIManager::m_Init(){
	DBG_ENTER("CAIManager.Init")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<108>");
	int t_[]={0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,1,1,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,1,1,0,1,0,1,1,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,1,1,0,1,0,0,1,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,1,0,0,1,1,1,0,0,0,0,0,1,0,0,1,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,0,0,0,0,1,0,0,1,1,0,1,0,1,0,1,0,0,0,0,0,0,1,1,1,0,1,0,0,0,0,0,0,0,0,1,0,0,1,1,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0};
	gc_assign(m_TerrainDescriptionArray,Array<int >(t_,600));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<109>");
	m_TerrainTileCount=m_TerrainWidth*m_TerrainHeight;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<111>");
	gc_assign(m_PathfindingMap,Array<c_PathfindingMapItem* >(m_TerrainTileCount));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<112>");
	for(int t_i=0;t_i<m_TerrainTileCount;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<113>");
		gc_assign(m_PathfindingMap.At(t_i),(new c_PathfindingMapItem)->m_new());
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<114>");
		m_PathfindingMap.At(t_i)->m_Obstacle=0;
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<115>");
		if(m_TerrainDescriptionArray.At(t_i)==1){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<116>");
			m_PathfindingMap.At(t_i)->m_Obstacle=1;
		}
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<121>");
	gc_assign(m_NeighbourArray.At(0),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<122>");
	m_NeighbourArray.At(0)->m_X=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<123>");
	m_NeighbourArray.At(0)->m_Y=0;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<126>");
	gc_assign(m_NeighbourArray.At(1),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<127>");
	m_NeighbourArray.At(1)->m_X=-1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<128>");
	m_NeighbourArray.At(1)->m_Y=0;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<131>");
	gc_assign(m_NeighbourArray.At(2),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<132>");
	m_NeighbourArray.At(2)->m_X=0;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<133>");
	m_NeighbourArray.At(2)->m_Y=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<136>");
	gc_assign(m_NeighbourArray.At(3),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<137>");
	m_NeighbourArray.At(3)->m_X=0;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<138>");
	m_NeighbourArray.At(3)->m_Y=-1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<141>");
	gc_assign(m_NeighbourArray.At(4),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<142>");
	m_NeighbourArray.At(4)->m_X=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<143>");
	m_NeighbourArray.At(4)->m_Y=-1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<146>");
	gc_assign(m_NeighbourArray.At(5),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<147>");
	m_NeighbourArray.At(5)->m_X=-1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<148>");
	m_NeighbourArray.At(5)->m_Y=-1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<151>");
	gc_assign(m_NeighbourArray.At(6),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<152>");
	m_NeighbourArray.At(6)->m_X=-1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<153>");
	m_NeighbourArray.At(6)->m_Y=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<156>");
	gc_assign(m_NeighbourArray.At(7),(new c_IVector)->m_new(0,0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<157>");
	m_NeighbourArray.At(7)->m_X=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<158>");
	m_NeighbourArray.At(7)->m_Y=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<160>");
	gc_assign(m_OpenListItemPool,Array<c_COpenListItem* >(m_TerrainTileCount));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<162>");
	gc_assign(m_TileStatusArray,Array<c_CTileStatusData* >(m_TerrainTileCount));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<163>");
	for(int t_i2=0;t_i2<m_TerrainTileCount;t_i2=t_i2+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i2,"i")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<164>");
		gc_assign(m_TileStatusArray.At(t_i2),(new c_CTileStatusData)->m_new());
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<165>");
		if(m_TerrainDescriptionArray.At(t_i2)!=0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<166>");
			m_TileStatusArray.At(t_i2)->m_TileStatus=2;
		}
	}
	return 0;
}
int c_CAIManager::m_TileSize;
Float c_CAIManager::m_MapScale;
c_Deque2* c_CAIManager::m_PathQueue;
c_COpenListItemList* c_CAIManager::m_OpenList;
int c_CAIManager::m_OpenListItemPoolCount;
c_COpenListItem* c_CAIManager::m_GetOpenListItemFromPool(){
	DBG_ENTER("CAIManager.GetOpenListItemFromPool")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<190>");
	if(m_OpenListItemPoolCount==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<191>");
		c_COpenListItem* t_=(new c_COpenListItem)->m_new();
		return t_;
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<193>");
	c_COpenListItem* t_OpenListItem=m_OpenListItemPool.At(m_OpenListItemPoolCount-1);
	DBG_LOCAL(t_OpenListItem,"OpenListItem")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<194>");
	m_OpenListItemPoolCount-=1;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<195>");
	return t_OpenListItem;
}
int c_CAIManager::m_PoolOpenListItem(c_COpenListItem* t_OpenListItem){
	DBG_ENTER("CAIManager.PoolOpenListItem")
	DBG_LOCAL(t_OpenListItem,"OpenListItem")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<183>");
	if(m_OpenListItemPoolCount<m_TerrainTileCount){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<184>");
		gc_assign(m_OpenListItemPool.At(m_OpenListItemPoolCount),t_OpenListItem);
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<185>");
		m_OpenListItemPoolCount+=1;
	}
	return 0;
}
int c_CAIManager::m_ComputePath(c_CPath* t_Path){
	DBG_ENTER("CAIManager.ComputePath")
	DBG_LOCAL(t_Path,"Path")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<201>");
	for(int t_i=0;t_i<m_TerrainTileCount;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<202>");
		m_PathfindingMap.At(t_i)->m_Explored=false;
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<203>");
		m_PathfindingMap.At(t_i)->m_CostSoFar=FLOAT(0.0);
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<204>");
		m_PathfindingMap.At(t_i)->m_CameFrom=-1;
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<207>");
	m_OpenList->p_Clear();
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<208>");
	c_COpenListItem* t_StartPointOpenListItem=m_GetOpenListItemFromPool();
	DBG_LOCAL(t_StartPointOpenListItem,"StartPointOpenListItem")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<209>");
	t_StartPointOpenListItem->p_Init3(t_Path->m_StartPoint,FLOAT(0.0));
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<210>");
	m_OpenList->p_AddLast5(t_StartPointOpenListItem);
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<213>");
	while(m_OpenList->p_IsEmpty()==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<214>");
		m_OpenList->p_Sort(1);
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<215>");
		c_COpenListItem* t_CurrentOpenListItem=m_OpenList->p_First();
		DBG_LOCAL(t_CurrentOpenListItem,"CurrentOpenListItem")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<216>");
		m_OpenList->p_RemoveFirst();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<217>");
		if(t_CurrentOpenListItem->m_Position->m_X==t_Path->m_EndPoint->m_X && t_CurrentOpenListItem->m_Position->m_Y==t_Path->m_EndPoint->m_Y){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<218>");
			break;
		}
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<220>");
		int t_CurrentNodeIndex=t_CurrentOpenListItem->m_Position->m_X+t_CurrentOpenListItem->m_Position->m_Y*m_TerrainWidth;
		DBG_LOCAL(t_CurrentNodeIndex,"CurrentNodeIndex")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<221>");
		for(int t_i2=0;t_i2<8;t_i2=t_i2+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i2,"i")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<222>");
			c_IVector* t_NeighbourPosition=(new c_IVector)->m_new(0,0);
			DBG_LOCAL(t_NeighbourPosition,"NeighbourPosition")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<223>");
			t_NeighbourPosition->m_X=t_CurrentOpenListItem->m_Position->m_X+m_NeighbourArray.At(t_i2)->m_X;
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<224>");
			t_NeighbourPosition->m_Y=t_CurrentOpenListItem->m_Position->m_Y+m_NeighbourArray.At(t_i2)->m_Y;
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<225>");
			int t_NeighbourIndex=t_NeighbourPosition->m_X+t_NeighbourPosition->m_Y*m_TerrainWidth;
			DBG_LOCAL(t_NeighbourIndex,"NeighbourIndex")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<226>");
			if(Float(t_NeighbourPosition->m_X)>=FLOAT(0.0) && t_NeighbourPosition->m_X<m_TerrainWidth && t_NeighbourPosition->m_Y>=0 && t_NeighbourPosition->m_Y<m_TerrainHeight && m_PathfindingMap.At(t_NeighbourIndex)->m_Obstacle==0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/AIManager.monkey<227>");
				Float t_Cost=FLOAT(1.0);
				DBG_LOCAL(t_Cost,"Cost")
				DBG_INFO("C:/MonkeyX77a/AIManager.monkey<228>");
				if(Float(m_NeighbourArray.At(t_i2)->m_X)!=FLOAT(0.0) && Float(m_NeighbourArray.At(t_i2)->m_Y)!=FLOAT(0.0)){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<229>");
					t_Cost=FLOAT(1.414213562);
				}
				DBG_INFO("C:/MonkeyX77a/AIManager.monkey<231>");
				Float t_NeighbourCost=m_PathfindingMap.At(t_CurrentNodeIndex)->m_CostSoFar+t_Cost;
				DBG_LOCAL(t_NeighbourCost,"NeighbourCost")
				DBG_INFO("C:/MonkeyX77a/AIManager.monkey<232>");
				if(m_PathfindingMap.At(t_NeighbourIndex)->m_Explored==false || t_NeighbourCost<m_PathfindingMap.At(t_NeighbourIndex)->m_CostSoFar){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<233>");
					m_PathfindingMap.At(t_NeighbourIndex)->m_Explored=true;
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<234>");
					m_PathfindingMap.At(t_NeighbourIndex)->m_CostSoFar=t_NeighbourCost;
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<235>");
					Float t_Heuristic=(Float)sqrt(Float((t_NeighbourPosition->m_X-t_Path->m_EndPoint->m_X)*(t_NeighbourPosition->m_X-t_Path->m_EndPoint->m_X)+(t_NeighbourPosition->m_Y-t_Path->m_EndPoint->m_Y)*(t_NeighbourPosition->m_Y-t_Path->m_EndPoint->m_Y)));
					DBG_LOCAL(t_Heuristic,"Heuristic")
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<236>");
					Float t_NeighbourEstimate=t_NeighbourCost+t_Heuristic;
					DBG_LOCAL(t_NeighbourEstimate,"NeighbourEstimate")
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<238>");
					c_COpenListItem* t_NeighbourOpenListItem=m_GetOpenListItemFromPool();
					DBG_LOCAL(t_NeighbourOpenListItem,"NeighbourOpenListItem")
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<239>");
					t_NeighbourOpenListItem->p_Init3(t_NeighbourPosition,t_NeighbourEstimate);
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<241>");
					m_OpenList->p_AddLast5(t_NeighbourOpenListItem);
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<242>");
					m_PathfindingMap.At(t_NeighbourIndex)->m_CameFrom=t_CurrentNodeIndex;
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<246>");
		m_PoolOpenListItem(t_CurrentOpenListItem);
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<249>");
	t_Path->m_NodeList->p_Clear();
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<250>");
	int t_NodeIndex=t_Path->m_EndPoint->m_X+t_Path->m_EndPoint->m_Y*m_TerrainWidth;
	DBG_LOCAL(t_NodeIndex,"NodeIndex")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<251>");
	t_Path->m_NodeList->p_AddFirst(t_Path->m_EndPoint);
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<252>");
	while(true){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<253>");
		int t_PreviousNodeIndex=m_PathfindingMap.At(t_NodeIndex)->m_CameFrom;
		DBG_LOCAL(t_PreviousNodeIndex,"PreviousNodeIndex")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<254>");
		if(t_PreviousNodeIndex==-1){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<255>");
			t_Path->m_PathStatus=2;
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<256>");
			return 0;
		}
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<258>");
		c_IVector* t_NodePosition=(new c_IVector)->m_new(0,0);
		DBG_LOCAL(t_NodePosition,"NodePosition")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<259>");
		t_NodePosition->m_X=t_PreviousNodeIndex % m_TerrainWidth;
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<260>");
		t_NodePosition->m_Y=t_PreviousNodeIndex/m_TerrainWidth;
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<261>");
		t_Path->m_NodeList->p_AddFirst(t_NodePosition);
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<262>");
		if(t_NodePosition->m_X==t_Path->m_StartPoint->m_X && t_NodePosition->m_Y==t_Path->m_StartPoint->m_Y){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<263>");
			break;
		}
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<265>");
		t_NodeIndex=t_PreviousNodeIndex;
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<267>");
	t_Path->m_PathStatus=1;
	return 0;
}
int c_CAIManager::m_Update(Float t_DeltaTime){
	DBG_ENTER("CAIManager.Update")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<176>");
	if(t_DeltaTime!=FLOAT(0.0) && m_PathQueue->p_Length2()!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<177>");
		c_CPath* t_Path=m_PathQueue->p_PopFirst();
		DBG_LOCAL(t_Path,"Path")
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<178>");
		m_ComputePath(t_Path);
	}
	return 0;
}
bool c_CAIManager::m_DisplayDebug;
c_IVector* c_CAIManager::m_GetTilePositionFromIndex(int t_Index){
	DBG_ENTER("CAIManager.GetTilePositionFromIndex")
	DBG_LOCAL(t_Index,"Index")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<283>");
	c_IVector* t_=(new c_IVector)->m_new(t_Index % m_TerrainWidth,t_Index/m_TerrainWidth);
	return t_;
}
int c_CAIManager::m_RenderDebug(){
	DBG_ENTER("CAIManager.RenderDebug")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<298>");
	if(m_DisplayDebug){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AIManager.monkey<299>");
		for(int t_i=0;t_i<m_TerrainTileCount;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<300>");
			String t_Text=String();
			DBG_LOCAL(t_Text,"Text")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<301>");
			int t_1=m_TileStatusArray.At(t_i)->m_TileStatus;
			DBG_LOCAL(t_1,"1")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<302>");
			if(t_1==0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/AIManager.monkey<303>");
				t_Text=String(L"Free",4);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/AIManager.monkey<304>");
				if(t_1==1){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/AIManager.monkey<305>");
					t_Text=String(L"Occ(",4)+String(m_TileStatusArray.At(t_i)->m_UnitID)+String(L")",1);
				}
			}
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<307>");
			c_IVector* t_TextPosition=m_GetTilePositionFromIndex(t_i);
			DBG_LOCAL(t_TextPosition,"TextPosition")
			DBG_INFO("C:/MonkeyX77a/AIManager.monkey<308>");
			bb_graphics_DrawText(t_Text,(Float(t_TextPosition->m_X)+FLOAT(0.5))*Float(m_TileSize)*m_MapScale,(Float(t_TextPosition->m_Y)+FLOAT(0.5))*Float(m_TileSize)*m_MapScale,FLOAT(0.5),FLOAT(0.5));
		}
	}
	return 0;
}
int c_CAIManager::m_GetTileIndexFromPosition(c_IVector* t_Position){
	DBG_ENTER("CAIManager.GetTileIndexFromPosition")
	DBG_LOCAL(t_Position,"Position")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<279>");
	int t_=t_Position->m_X+t_Position->m_Y*m_TerrainWidth;
	return t_;
}
c_CTileStatusData* c_CAIManager::m_GetTileStatusData(c_IVector* t_Position){
	DBG_ENTER("CAIManager.GetTileStatusData")
	DBG_LOCAL(t_Position,"Position")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<287>");
	int t_Index=m_GetTileIndexFromPosition(t_Position);
	DBG_LOCAL(t_Index,"Index")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<288>");
	return m_TileStatusArray.At(t_Index);
}
int c_CAIManager::m_SetTileStatus(c_IVector* t_Position,int t_TileStatus,int t_UnitID){
	DBG_ENTER("CAIManager.SetTileStatus")
	DBG_LOCAL(t_Position,"Position")
	DBG_LOCAL(t_TileStatus,"TileStatus")
	DBG_LOCAL(t_UnitID,"UnitID")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<292>");
	int t_Index=m_GetTileIndexFromPosition(t_Position);
	DBG_LOCAL(t_Index,"Index")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<293>");
	m_TileStatusArray.At(t_Index)->m_TileStatus=t_TileStatus;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<294>");
	m_TileStatusArray.At(t_Index)->m_UnitID=t_UnitID;
	return 0;
}
int c_CAIManager::m_AddRequest(c_CPath* t_Path){
	DBG_ENTER("CAIManager.AddRequest")
	DBG_LOCAL(t_Path,"Path")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<172>");
	m_PathQueue->p_PushLast2(t_Path);
	return 0;
}
void c_CAIManager::mark(){
	Object::mark();
}
String c_CAIManager::debug(){
	String t="(CAIManager)\n";
	t+=dbg_decl("TerrainDescriptionArray",&c_CAIManager::m_TerrainDescriptionArray);
	t+=dbg_decl("TileSize",&c_CAIManager::m_TileSize);
	t+=dbg_decl("TerrainHeight",&c_CAIManager::m_TerrainHeight);
	t+=dbg_decl("TerrainWidth",&c_CAIManager::m_TerrainWidth);
	t+=dbg_decl("MapScale",&c_CAIManager::m_MapScale);
	t+=dbg_decl("TerrainTileCount",&c_CAIManager::m_TerrainTileCount);
	t+=dbg_decl("PathQueue",&c_CAIManager::m_PathQueue);
	t+=dbg_decl("PathfindingMap",&c_CAIManager::m_PathfindingMap);
	t+=dbg_decl("OpenList",&c_CAIManager::m_OpenList);
	t+=dbg_decl("NeighbourArray",&c_CAIManager::m_NeighbourArray);
	t+=dbg_decl("OpenListItemPool",&c_CAIManager::m_OpenListItemPool);
	t+=dbg_decl("OpenListItemPoolCount",&c_CAIManager::m_OpenListItemPoolCount);
	t+=dbg_decl("TileStatusArray",&c_CAIManager::m_TileStatusArray);
	t+=dbg_decl("DisplayDebug",&c_CAIManager::m_DisplayDebug);
	return t;
}
c_PathfindingMapItem::c_PathfindingMapItem(){
	m_Obstacle=0;
	m_Explored=false;
	m_CostSoFar=FLOAT(0.0);
	m_CameFrom=-1;
}
c_PathfindingMapItem* c_PathfindingMapItem::m_new(){
	DBG_ENTER("PathfindingMapItem.new")
	c_PathfindingMapItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<47>");
	return this;
}
void c_PathfindingMapItem::mark(){
	Object::mark();
}
String c_PathfindingMapItem::debug(){
	String t="(PathfindingMapItem)\n";
	t+=dbg_decl("Obstacle",&m_Obstacle);
	t+=dbg_decl("Explored",&m_Explored);
	t+=dbg_decl("CostSoFar",&m_CostSoFar);
	t+=dbg_decl("CameFrom",&m_CameFrom);
	return t;
}
c_IVector::c_IVector(){
	m_X=0;
	m_Y=0;
}
c_IVector* c_IVector::m_new(int t_X,int t_Y){
	DBG_ENTER("IVector.new")
	c_IVector *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_X,"X")
	DBG_LOCAL(t_Y,"Y")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<28>");
	this->m_X=t_X;
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<29>");
	this->m_Y=t_Y;
	return this;
}
void c_IVector::mark(){
	Object::mark();
}
String c_IVector::debug(){
	String t="(IVector)\n";
	t+=dbg_decl("X",&m_X);
	t+=dbg_decl("Y",&m_Y);
	return t;
}
c_COpenListItem::c_COpenListItem(){
	m_Position=0;
	m_Estimate=FLOAT(.0);
}
c_COpenListItem* c_COpenListItem::m_new(){
	DBG_ENTER("COpenListItem.new")
	c_COpenListItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<29>");
	return this;
}
int c_COpenListItem::p_Init3(c_IVector* t_Position,Float t_Estimate){
	DBG_ENTER("COpenListItem.Init")
	c_COpenListItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Position,"Position")
	DBG_LOCAL(t_Estimate,"Estimate")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<34>");
	gc_assign(this->m_Position,t_Position);
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<35>");
	this->m_Estimate=t_Estimate;
	return 0;
}
void c_COpenListItem::mark(){
	Object::mark();
	gc_mark_q(m_Position);
}
String c_COpenListItem::debug(){
	String t="(COpenListItem)\n";
	t+=dbg_decl("Position",&m_Position);
	t+=dbg_decl("Estimate",&m_Estimate);
	return t;
}
c_CTileStatusData::c_CTileStatusData(){
	m_TileStatus=0;
	m_UnitID=-1;
}
c_CTileStatusData* c_CTileStatusData::m_new(){
	DBG_ENTER("CTileStatusData.new")
	c_CTileStatusData *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<54>");
	return this;
}
void c_CTileStatusData::mark(){
	Object::mark();
}
String c_CTileStatusData::debug(){
	String t="(CTileStatusData)\n";
	t+=dbg_decl("TileStatus",&m_TileStatus);
	t+=dbg_decl("UnitID",&m_UnitID);
	return t;
}
c_CSpawnManager::c_CSpawnManager(){
}
c_IntMap* c_CSpawnManager::m_HumanAnimationSet;
c_IntMap* c_CSpawnManager::m_OrcAnimationSet;
int c_CSpawnManager::m_Init(){
	DBG_ENTER("CSpawnManager.Init")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<25>");
	m_HumanAnimationSet->p_Add(0,0);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<26>");
	m_HumanAnimationSet->p_Add(1,1);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<27>");
	m_HumanAnimationSet->p_Add(2,2);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<28>");
	m_HumanAnimationSet->p_Add(3,3);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<29>");
	m_HumanAnimationSet->p_Add(4,4);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<30>");
	m_HumanAnimationSet->p_Add(5,5);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<31>");
	m_HumanAnimationSet->p_Add(6,6);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<32>");
	m_HumanAnimationSet->p_Add(7,7);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<33>");
	m_HumanAnimationSet->p_Add(9,9);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<34>");
	m_HumanAnimationSet->p_Add(10,10);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<35>");
	m_HumanAnimationSet->p_Add(11,11);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<36>");
	m_HumanAnimationSet->p_Add(12,12);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<37>");
	m_HumanAnimationSet->p_Add(13,13);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<38>");
	m_HumanAnimationSet->p_Add(14,14);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<39>");
	m_HumanAnimationSet->p_Add(15,15);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<40>");
	m_HumanAnimationSet->p_Add(16,16);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<41>");
	m_HumanAnimationSet->p_Add(18,18);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<42>");
	m_HumanAnimationSet->p_Add(20,20);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<43>");
	m_HumanAnimationSet->p_Add(17,17);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<44>");
	m_HumanAnimationSet->p_Add(19,19);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<45>");
	m_HumanAnimationSet->p_Add(21,21);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<46>");
	m_HumanAnimationSet->p_Add(22,22);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<47>");
	m_HumanAnimationSet->p_Add(24,24);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<48>");
	m_HumanAnimationSet->p_Add(23,23);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<51>");
	m_OrcAnimationSet->p_Add(0,200);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<52>");
	m_OrcAnimationSet->p_Add(1,201);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<53>");
	m_OrcAnimationSet->p_Add(2,202);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<54>");
	m_OrcAnimationSet->p_Add(3,203);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<55>");
	m_OrcAnimationSet->p_Add(4,204);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<56>");
	m_OrcAnimationSet->p_Add(5,205);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<57>");
	m_OrcAnimationSet->p_Add(6,206);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<58>");
	m_OrcAnimationSet->p_Add(7,207);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<59>");
	m_OrcAnimationSet->p_Add(9,209);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<60>");
	m_OrcAnimationSet->p_Add(10,210);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<61>");
	m_OrcAnimationSet->p_Add(11,211);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<62>");
	m_OrcAnimationSet->p_Add(12,212);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<63>");
	m_OrcAnimationSet->p_Add(13,213);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<64>");
	m_OrcAnimationSet->p_Add(14,214);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<65>");
	m_OrcAnimationSet->p_Add(15,215);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<66>");
	m_OrcAnimationSet->p_Add(16,216);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<67>");
	m_OrcAnimationSet->p_Add(18,218);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<68>");
	m_OrcAnimationSet->p_Add(20,220);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<69>");
	m_OrcAnimationSet->p_Add(17,217);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<70>");
	m_OrcAnimationSet->p_Add(19,219);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<71>");
	m_OrcAnimationSet->p_Add(21,221);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<72>");
	m_OrcAnimationSet->p_Add(22,222);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<73>");
	m_OrcAnimationSet->p_Add(24,224);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<74>");
	m_OrcAnimationSet->p_Add(23,223);
	return 0;
}
c_Deque3* c_CSpawnManager::m_SpawnRequestQueue;
int c_CSpawnManager::m_CreateHumanFootman(c_CMyApp* t_MyApp,c_CCommandBuffer* t_CommandBuffer,c_FVector* t_SpawnPosition){
	DBG_ENTER("CSpawnManager.CreateHumanFootman")
	DBG_LOCAL(t_MyApp,"MyApp")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_LOCAL(t_SpawnPosition,"SpawnPosition")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<93>");
	int t_SpriteID=t_CommandBuffer->p_AddCommand_CreateRenderItem(1,FLOAT(0.0));
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<94>");
	t_CommandBuffer->p_AddCommand(t_SpriteID,7,c_CAIManager::m_MapScale,c_CAIManager::m_MapScale);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<96>");
	c_CGameObject* t_Footman=(new c_CGameObject)->m_new();
	DBG_LOCAL(t_Footman,"Footman")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<97>");
	c_CAnimationGraph* t_AnimationGraph=(new c_CAnimationGraph)->m_new(t_SpriteID,m_HumanAnimationSet);
	DBG_LOCAL(t_AnimationGraph,"AnimationGraph")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<98>");
	c_CMovement* t_Movement=(new c_CMovement)->m_new(t_SpriteID,t_SpawnPosition,m_HumanAnimationSet,t_AnimationGraph);
	DBG_LOCAL(t_Movement,"Movement")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<99>");
	c_CUnit* t_Unit=(new c_CUnit)->m_new(t_SpriteID,t_Movement,0,m_HumanAnimationSet,t_AnimationGraph);
	DBG_LOCAL(t_Unit,"Unit")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<100>");
	t_Footman->p_AddComponent(t_Movement);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<101>");
	t_Footman->p_AddComponent(t_Unit);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<102>");
	t_Footman->p_AddComponent(t_AnimationGraph);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<103>");
	t_MyApp->p_AddGameObject(t_Footman);
	return 0;
}
int c_CSpawnManager::m_CreateOrcFootman(c_CMyApp* t_MyApp,c_CCommandBuffer* t_CommandBuffer,c_FVector* t_SpawnPosition){
	DBG_ENTER("CSpawnManager.CreateOrcFootman")
	DBG_LOCAL(t_MyApp,"MyApp")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_LOCAL(t_SpawnPosition,"SpawnPosition")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<107>");
	int t_SpriteID=t_CommandBuffer->p_AddCommand_CreateRenderItem(1,FLOAT(0.0));
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<108>");
	t_CommandBuffer->p_AddCommand(t_SpriteID,7,c_CAIManager::m_MapScale,c_CAIManager::m_MapScale);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<110>");
	c_CGameObject* t_Footman=(new c_CGameObject)->m_new();
	DBG_LOCAL(t_Footman,"Footman")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<111>");
	c_CAnimationGraph* t_AnimationGraph=(new c_CAnimationGraph)->m_new(t_SpriteID,m_OrcAnimationSet);
	DBG_LOCAL(t_AnimationGraph,"AnimationGraph")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<112>");
	c_CMovement* t_Movement=(new c_CMovement)->m_new(t_SpriteID,t_SpawnPosition,m_OrcAnimationSet,t_AnimationGraph);
	DBG_LOCAL(t_Movement,"Movement")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<113>");
	c_CUnit* t_Unit=(new c_CUnit)->m_new(t_SpriteID,t_Movement,1,m_OrcAnimationSet,t_AnimationGraph);
	DBG_LOCAL(t_Unit,"Unit")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<115>");
	t_Footman->p_AddComponent(t_Movement);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<116>");
	t_Footman->p_AddComponent(t_Unit);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<117>");
	t_Footman->p_AddComponent(t_AnimationGraph);
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<118>");
	t_MyApp->p_AddGameObject(t_Footman);
	return 0;
}
int c_CSpawnManager::m_Update(c_CMyApp* t_MyApp,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CSpawnManager.Update")
	DBG_LOCAL(t_MyApp,"MyApp")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<81>");
	if(m_SpawnRequestQueue->p_IsEmpty()==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<82>");
		c_CSpawnRequestData* t_SpawnRequestData=m_SpawnRequestQueue->p_PopFirst();
		DBG_LOCAL(t_SpawnRequestData,"SpawnRequestData")
		DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<83>");
		int t_1=t_SpawnRequestData->m_TemplateID;
		DBG_LOCAL(t_1,"1")
		DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<84>");
		if(t_1==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<85>");
			m_CreateHumanFootman(t_MyApp,t_CommandBuffer,t_SpawnRequestData->m_Position);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<86>");
			if(t_1==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<87>");
				m_CreateOrcFootman(t_MyApp,t_CommandBuffer,t_SpawnRequestData->m_Position);
			}
		}
	}
	return 0;
}
int c_CSpawnManager::m_RequestSpawn(int t_TemplateID,c_FVector* t_Position){
	DBG_ENTER("CSpawnManager.RequestSpawn")
	DBG_LOCAL(t_TemplateID,"TemplateID")
	DBG_LOCAL(t_Position,"Position")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<78>");
	m_SpawnRequestQueue->p_PushLast3((new c_CSpawnRequestData)->m_new(t_TemplateID,t_Position));
	return 0;
}
void c_CSpawnManager::mark(){
	Object::mark();
}
String c_CSpawnManager::debug(){
	String t="(CSpawnManager)\n";
	t+=dbg_decl("SpawnRequestQueue",&c_CSpawnManager::m_SpawnRequestQueue);
	t+=dbg_decl("OrcAnimationSet",&c_CSpawnManager::m_OrcAnimationSet);
	t+=dbg_decl("HumanAnimationSet",&c_CSpawnManager::m_HumanAnimationSet);
	return t;
}
c_Map::c_Map(){
	m_root=0;
}
c_Map* c_Map::m_new(){
	DBG_ENTER("Map.new")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<7>");
	return this;
}
int c_Map::p_RotateLeft(c_Node* t_node){
	DBG_ENTER("Map.RotateLeft")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<251>");
	c_Node* t_child=t_node->m_right;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<252>");
	gc_assign(t_node->m_right,t_child->m_left);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<253>");
	if((t_child->m_left)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<254>");
		gc_assign(t_child->m_left->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<256>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<257>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<258>");
		if(t_node==t_node->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<259>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<261>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<264>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<266>");
	gc_assign(t_child->m_left,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<267>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map::p_RotateRight(c_Node* t_node){
	DBG_ENTER("Map.RotateRight")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<271>");
	c_Node* t_child=t_node->m_left;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<272>");
	gc_assign(t_node->m_left,t_child->m_right);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<273>");
	if((t_child->m_right)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<274>");
		gc_assign(t_child->m_right->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<276>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<277>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<278>");
		if(t_node==t_node->m_parent->m_right){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<279>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<281>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<284>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<286>");
	gc_assign(t_child->m_right,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<287>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map::p_InsertFixup(c_Node* t_node){
	DBG_ENTER("Map.InsertFixup")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<212>");
	while(((t_node->m_parent)!=0) && t_node->m_parent->m_color==-1 && ((t_node->m_parent->m_parent)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<213>");
		if(t_node->m_parent==t_node->m_parent->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<214>");
			c_Node* t_uncle=t_node->m_parent->m_parent->m_right;
			DBG_LOCAL(t_uncle,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<215>");
			if(((t_uncle)!=0) && t_uncle->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<216>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<217>");
				t_uncle->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<218>");
				t_uncle->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<219>");
				t_node=t_uncle->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<221>");
				if(t_node==t_node->m_parent->m_right){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<222>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<223>");
					p_RotateLeft(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<225>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<226>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<227>");
				p_RotateRight(t_node->m_parent->m_parent);
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<230>");
			c_Node* t_uncle2=t_node->m_parent->m_parent->m_left;
			DBG_LOCAL(t_uncle2,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<231>");
			if(((t_uncle2)!=0) && t_uncle2->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<232>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<233>");
				t_uncle2->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<234>");
				t_uncle2->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<235>");
				t_node=t_uncle2->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<237>");
				if(t_node==t_node->m_parent->m_left){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<238>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<239>");
					p_RotateRight(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<241>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<242>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<243>");
				p_RotateLeft(t_node->m_parent->m_parent);
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<247>");
	m_root->m_color=1;
	return 0;
}
bool c_Map::p_Add(int t_key,int t_value){
	DBG_ENTER("Map.Add")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<61>");
	c_Node* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<62>");
	c_Node* t_parent=0;
	int t_cmp=0;
	DBG_LOCAL(t_parent,"parent")
	DBG_LOCAL(t_cmp,"cmp")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<64>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<65>");
		t_parent=t_node;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<66>");
		t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<67>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<68>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<69>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<70>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<72>");
				return false;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<76>");
	t_node=(new c_Node)->m_new(t_key,t_value,-1,t_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<78>");
	if((t_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<79>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<80>");
			gc_assign(t_parent->m_right,t_node);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<82>");
			gc_assign(t_parent->m_left,t_node);
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<84>");
		p_InsertFixup(t_node);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<86>");
		gc_assign(m_root,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<88>");
	return true;
}
c_Node* c_Map::p_FindNode(int t_key){
	DBG_ENTER("Map.FindNode")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<157>");
	c_Node* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<159>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<160>");
		int t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_LOCAL(t_cmp,"cmp")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<161>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<162>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<163>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<164>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<166>");
				return t_node;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<169>");
	return t_node;
}
int c_Map::p_Get(int t_key){
	DBG_ENTER("Map.Get")
	c_Map *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<101>");
	c_Node* t_node=p_FindNode(t_key);
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<102>");
	if((t_node)!=0){
		DBG_BLOCK();
		return t_node->m_value;
	}
	return 0;
}
void c_Map::mark(){
	Object::mark();
	gc_mark_q(m_root);
}
String c_Map::debug(){
	String t="(Map)\n";
	t+=dbg_decl("root",&m_root);
	return t;
}
c_IntMap::c_IntMap(){
}
c_IntMap* c_IntMap::m_new(){
	DBG_ENTER("IntMap.new")
	c_IntMap *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<534>");
	c_Map::m_new();
	return this;
}
int c_IntMap::p_Compare(int t_lhs,int t_rhs){
	DBG_ENTER("IntMap.Compare")
	c_IntMap *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<537>");
	int t_=t_lhs-t_rhs;
	return t_;
}
void c_IntMap::mark(){
	c_Map::mark();
}
String c_IntMap::debug(){
	String t="(IntMap)\n";
	t=c_Map::debug()+t;
	return t;
}
c_Node::c_Node(){
	m_key=0;
	m_right=0;
	m_left=0;
	m_value=0;
	m_color=0;
	m_parent=0;
}
c_Node* c_Node::m_new(int t_key,int t_value,int t_color,c_Node* t_parent){
	DBG_ENTER("Node.new")
	c_Node *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_LOCAL(t_color,"color")
	DBG_LOCAL(t_parent,"parent")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<364>");
	this->m_key=t_key;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<365>");
	this->m_value=t_value;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<366>");
	this->m_color=t_color;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<367>");
	gc_assign(this->m_parent,t_parent);
	return this;
}
c_Node* c_Node::m_new2(){
	DBG_ENTER("Node.new")
	c_Node *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<361>");
	return this;
}
void c_Node::mark(){
	Object::mark();
	gc_mark_q(m_right);
	gc_mark_q(m_left);
	gc_mark_q(m_parent);
}
String c_Node::debug(){
	String t="(Node)\n";
	t+=dbg_decl("key",&m_key);
	t+=dbg_decl("value",&m_value);
	t+=dbg_decl("color",&m_color);
	t+=dbg_decl("parent",&m_parent);
	t+=dbg_decl("left",&m_left);
	t+=dbg_decl("right",&m_right);
	return t;
}
c_CComponentUpdater::c_CComponentUpdater(){
	m_ComponentType=0;
}
c_CComponentUpdater* c_CComponentUpdater::m_new(int t_ComponentType){
	DBG_ENTER("CComponentUpdater.new")
	c_CComponentUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ComponentType,"ComponentType")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<28>");
	this->m_ComponentType=t_ComponentType;
	return this;
}
c_CComponentUpdater* c_CComponentUpdater::m_new2(){
	DBG_ENTER("CComponentUpdater.new")
	c_CComponentUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<24>");
	return this;
}
void c_CComponentUpdater::mark(){
	Object::mark();
}
String c_CComponentUpdater::debug(){
	String t="(CComponentUpdater)\n";
	t+=dbg_decl("ComponentType",&m_ComponentType);
	return t;
}
c_CInputAreaUpdater::c_CInputAreaUpdater(){
	m_ComponentList=(new c_List8)->m_new();
}
c_CInputAreaUpdater* c_CInputAreaUpdater::m_new(){
	DBG_ENTER("CInputAreaUpdater.new")
	c_CInputAreaUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<10>");
	c_CComponentUpdater::m_new(2);
	return this;
}
int c_CInputAreaUpdater::p_Update(c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CInputAreaUpdater.Update")
	c_CInputAreaUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<14>");
	Float t_MouseX=bb_input_MouseX();
	DBG_LOCAL(t_MouseX,"MouseX")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<15>");
	Float t_MouseY=bb_input_MouseY();
	DBG_LOCAL(t_MouseY,"MouseY")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<17>");
	c_CInputArea* t_PrioritaryInputArea=0;
	DBG_LOCAL(t_PrioritaryInputArea,"PrioritaryInputArea")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<18>");
	c_Enumerator5* t_=this->m_ComponentList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CInputArea* t_InputArea=t_->p_NextObject();
		DBG_LOCAL(t_InputArea,"InputArea")
		DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<19>");
		if(t_PrioritaryInputArea==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<20>");
			if(t_InputArea->m_TopLeftCorner->m_X<=t_MouseX && t_MouseX<=t_InputArea->m_BottomRightCorner->m_X && t_InputArea->m_TopLeftCorner->m_Y<=t_MouseY && t_MouseY<=t_InputArea->m_BottomRightCorner->m_Y){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<21>");
				t_PrioritaryInputArea=t_InputArea;
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<22>");
				break;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<27>");
	c_Enumerator5* t_2=this->m_ComponentList->p_ObjectEnumerator();
	while(t_2->p_HasNext()){
		DBG_BLOCK();
		c_CInputArea* t_InputArea2=t_2->p_NextObject();
		DBG_LOCAL(t_InputArea2,"InputArea")
		DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<28>");
		if(t_InputArea2!=t_PrioritaryInputArea){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<29>");
			if(t_InputArea2->m_State==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<31>");
				t_InputArea2->p_RaiseEvent(1);
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<32>");
				t_InputArea2->m_State=0;
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<36>");
			t_InputArea2->m_LastInputGlobalPosition->m_X=t_MouseX;
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<37>");
			t_InputArea2->m_LastInputGlobalPosition->m_Y=t_MouseY;
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<38>");
			if((bb_input_MouseDown(0))!=0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<39>");
				if(t_InputArea2->m_State!=2){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<41>");
					t_InputArea2->m_State=2;
					DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<42>");
					t_InputArea2->p_RaiseEvent(2);
				}
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<44>");
				if(t_InputArea2->m_State!=1){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<46>");
					t_InputArea2->p_RaiseEvent(0);
					DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<47>");
					t_InputArea2->m_State=1;
				}
			}
		}
	}
	return 0;
}
int c_CInputAreaUpdater::p_AddComponent(c_CComponent* t_Component){
	DBG_ENTER("CInputAreaUpdater.AddComponent")
	c_CInputAreaUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Component,"Component")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<57>");
	c_CInputArea* t_InputArea=dynamic_cast<c_CInputArea*>(t_Component);
	DBG_LOCAL(t_InputArea,"InputArea")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<59>");
	c_Node13* t_ComponentNode=this->m_ComponentList->p_FirstNode();
	DBG_LOCAL(t_ComponentNode,"ComponentNode")
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<60>");
	if(t_ComponentNode==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<62>");
		this->m_ComponentList->p_AddFirst3(t_InputArea);
		DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<63>");
		return 0;
	}
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<67>");
	while((t_ComponentNode)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<68>");
		if(t_InputArea->m_Priority>t_ComponentNode->p_Value()->m_Priority){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<69>");
			if(t_ComponentNode->p_PrevNode()==0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<70>");
				this->m_ComponentList->p_AddFirst3(t_InputArea);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<72>");
				(new c_Node13)->m_new(t_ComponentNode,t_ComponentNode->p_PrevNode(),t_InputArea);
			}
			DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<74>");
			return 0;
		}
		DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<76>");
		t_ComponentNode=t_ComponentNode->p_NextNode();
	}
	DBG_INFO("C:/MonkeyX77a/InputAreaUpdater.monkey<80>");
	this->m_ComponentList->p_AddLast8(t_InputArea);
	return 0;
}
void c_CInputAreaUpdater::mark(){
	c_CComponentUpdater::mark();
	gc_mark_q(m_ComponentList);
}
String c_CInputAreaUpdater::debug(){
	String t="(CInputAreaUpdater)\n";
	t=c_CComponentUpdater::debug()+t;
	t+=dbg_decl("ComponentList",&m_ComponentList);
	return t;
}
c_List::c_List(){
	m__head=((new c_HeadNode)->m_new());
}
c_List* c_List::m_new(){
	DBG_ENTER("List.new")
	c_List *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node2* c_List::p_AddLast(c_CComponentUpdater* t_data){
	DBG_ENTER("List.AddLast")
	c_List *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node2* t_=(new c_Node2)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List* c_List::m_new2(Array<c_CComponentUpdater* > t_data){
	DBG_ENTER("List.new")
	c_List *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CComponentUpdater* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CComponentUpdater* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast(t_t);
	}
	return this;
}
c_Enumerator2* c_List::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator2* t_=(new c_Enumerator2)->m_new(this);
	return t_;
}
void c_List::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node2::c_Node2(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node2* c_Node2::m_new(c_Node2* t_succ,c_Node2* t_pred,c_CComponentUpdater* t_data){
	DBG_ENTER("Node.new")
	c_Node2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node2* c_Node2::m_new2(){
	DBG_ENTER("Node.new")
	c_Node2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
void c_Node2::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node2::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode::c_HeadNode(){
}
c_HeadNode* c_HeadNode::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node2::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode::mark(){
	c_Node2::mark();
}
String c_HeadNode::debug(){
	String t="(HeadNode)\n";
	t=c_Node2::debug()+t;
	return t;
}
c_CMenuUpdater::c_CMenuUpdater(){
	m_ComponentList=(new c_List9)->m_new();
}
c_CMenuUpdater* c_CMenuUpdater::m_new(){
	DBG_ENTER("CMenuUpdater.new")
	c_CMenuUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<8>");
	c_CComponentUpdater::m_new(1);
	return this;
}
int c_CMenuUpdater::p_Update(c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CMenuUpdater.Update")
	c_CMenuUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<12>");
	c_CMenuComponent* t_SelectedMenu=0;
	DBG_LOCAL(t_SelectedMenu,"SelectedMenu")
	DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<13>");
	c_Enumerator7* t_=this->m_ComponentList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CMenuComponent* t_MenuComponent=t_->p_NextObject();
		DBG_LOCAL(t_MenuComponent,"MenuComponent")
		DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<14>");
		int t_1=t_MenuComponent->m_RequestState;
		DBG_LOCAL(t_1,"1")
		DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<15>");
		if(t_1==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<16>");
			if(t_MenuComponent->m_State!=2){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<17>");
				t_CommandBuffer->p_AddCommand2(t_MenuComponent->m_HighlightSquareID,6,t_MenuComponent->m_NeutralColor->m_R,t_MenuComponent->m_NeutralColor->m_G,t_MenuComponent->m_NeutralColor->m_B);
				DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<18>");
				t_MenuComponent->m_State=t_MenuComponent->m_RequestState;
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<20>");
			if(t_1==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<21>");
				if(t_MenuComponent->m_State!=2){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<22>");
					t_CommandBuffer->p_AddCommand2(t_MenuComponent->m_HighlightSquareID,6,t_MenuComponent->m_HighlightColor->m_R,t_MenuComponent->m_HighlightColor->m_G,t_MenuComponent->m_HighlightColor->m_B);
					DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<23>");
					t_MenuComponent->m_State=t_MenuComponent->m_RequestState;
				}
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<25>");
				if(t_1==2){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<26>");
					t_SelectedMenu=t_MenuComponent;
					DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<27>");
					t_CommandBuffer->p_AddCommand2(t_MenuComponent->m_HighlightSquareID,6,t_MenuComponent->m_SelectColor->m_R,t_MenuComponent->m_SelectColor->m_G,t_MenuComponent->m_SelectColor->m_B);
					DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<28>");
					t_MenuComponent->m_State=t_MenuComponent->m_RequestState;
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<30>");
		t_MenuComponent->m_RequestState=-1;
	}
	DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<33>");
	if((t_SelectedMenu)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<34>");
		c_Enumerator7* t_2=this->m_ComponentList->p_ObjectEnumerator();
		while(t_2->p_HasNext()){
			DBG_BLOCK();
			c_CMenuComponent* t_MenuComponent2=t_2->p_NextObject();
			DBG_LOCAL(t_MenuComponent2,"MenuComponent")
			DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<35>");
			if(t_MenuComponent2!=t_SelectedMenu){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<36>");
				t_CommandBuffer->p_AddCommand2(t_MenuComponent2->m_HighlightSquareID,6,t_MenuComponent2->m_NeutralColor->m_R,t_MenuComponent2->m_NeutralColor->m_G,t_MenuComponent2->m_NeutralColor->m_B);
				DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<37>");
				t_MenuComponent2->m_State=0;
			}
		}
	}
	return 0;
}
int c_CMenuUpdater::p_AddComponent(c_CComponent* t_Component){
	DBG_ENTER("CMenuUpdater.AddComponent")
	c_CMenuUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Component,"Component")
	DBG_INFO("C:/MonkeyX77a/MenuUpdater.monkey<44>");
	this->m_ComponentList->p_AddLast9(dynamic_cast<c_CMenuComponent*>(t_Component));
	return 0;
}
void c_CMenuUpdater::mark(){
	c_CComponentUpdater::mark();
	gc_mark_q(m_ComponentList);
}
String c_CMenuUpdater::debug(){
	String t="(CMenuUpdater)\n";
	t=c_CComponentUpdater::debug()+t;
	t+=dbg_decl("ComponentList",&m_ComponentList);
	return t;
}
c_CUnitUpdater::c_CUnitUpdater(){
	m_UnitList=(new c_List10)->m_new();
}
c_CUnitUpdater* c_CUnitUpdater::m_new(){
	DBG_ENTER("CUnitUpdater.new")
	c_CUnitUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<7>");
	c_CComponentUpdater::m_new(4);
	return this;
}
int c_CUnitUpdater::p_Update(c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CUnitUpdater.Update")
	c_CUnitUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<11>");
	c_FVector* t_ClosestUnitPosition=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	DBG_LOCAL(t_ClosestUnitPosition,"ClosestUnitPosition")
	DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<12>");
	c_Enumerator8* t_=this->m_UnitList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CUnit* t_UnitA=t_->p_NextObject();
		DBG_LOCAL(t_UnitA,"UnitA")
		DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<14>");
		Float t_ClosestUnitDistanceSq=FLOAT(1000000000000.0);
		DBG_LOCAL(t_ClosestUnitDistanceSq,"ClosestUnitDistanceSq")
		DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<15>");
		c_Enumerator8* t_2=this->m_UnitList->p_ObjectEnumerator();
		while(t_2->p_HasNext()){
			DBG_BLOCK();
			c_CUnit* t_UnitB=t_2->p_NextObject();
			DBG_LOCAL(t_UnitB,"UnitB")
			DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<16>");
			if(t_UnitA!=t_UnitB && t_UnitA->m_AttitudeGroup!=t_UnitB->m_AttitudeGroup){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<17>");
				Float t_UnitDistanceSq=(t_UnitA->m_Position->m_X-t_UnitB->m_Position->m_X)*(t_UnitA->m_Position->m_X-t_UnitB->m_Position->m_X)+(t_UnitA->m_Position->m_Y-t_UnitB->m_Position->m_Y)*(t_UnitA->m_Position->m_Y-t_UnitB->m_Position->m_Y);
				DBG_LOCAL(t_UnitDistanceSq,"UnitDistanceSq")
				DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<18>");
				if(t_UnitDistanceSq<t_ClosestUnitDistanceSq){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<19>");
					t_ClosestUnitDistanceSq=t_UnitDistanceSq;
					DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<20>");
					t_ClosestUnitPosition->m_X=t_UnitB->m_Position->m_X;
					DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<21>");
					t_ClosestUnitPosition->m_Y=t_UnitB->m_Position->m_Y;
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<25>");
		t_UnitA->p_SetClosestEnemy(t_ClosestUnitPosition);
	}
	return 0;
}
int c_CUnitUpdater::p_AddComponent(c_CComponent* t_Component){
	DBG_ENTER("CUnitUpdater.AddComponent")
	c_CUnitUpdater *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Component,"Component")
	DBG_INFO("C:/MonkeyX77a/UnitUpdater.monkey<30>");
	m_UnitList->p_AddLast10(dynamic_cast<c_CUnit*>(t_Component));
	return 0;
}
void c_CUnitUpdater::mark(){
	c_CComponentUpdater::mark();
	gc_mark_q(m_UnitList);
}
String c_CUnitUpdater::debug(){
	String t="(CUnitUpdater)\n";
	t=c_CComponentUpdater::debug()+t;
	t+=dbg_decl("UnitList",&m_UnitList);
	return t;
}
Float bb_random_Rnd(){
	DBG_ENTER("Rnd")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/random.monkey<21>");
	bb_random_Seed=bb_random_Seed*1664525+1013904223|0;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/random.monkey<22>");
	Float t_=Float(bb_random_Seed>>8&16777215)/FLOAT(16777216.0);
	return t_;
}
Float bb_random_Rnd2(Float t_low,Float t_high){
	DBG_ENTER("Rnd")
	DBG_LOCAL(t_low,"low")
	DBG_LOCAL(t_high,"high")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/random.monkey<30>");
	Float t_=bb_random_Rnd3(t_high-t_low)+t_low;
	return t_;
}
Float bb_random_Rnd3(Float t_range){
	DBG_ENTER("Rnd")
	DBG_LOCAL(t_range,"range")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/random.monkey<26>");
	Float t_=bb_random_Rnd()*t_range;
	return t_;
}
c_CAnimationManager::c_CAnimationManager(){
}
c_IntMap2* c_CAnimationManager::m_AnimationMap;
int c_CAnimationManager::m_LoadUnitAnimations(String t_ImageName,int t_FirstAnimID){
	DBG_ENTER("CAnimationManager.LoadUnitAnimations")
	DBG_LOCAL(t_ImageName,"ImageName")
	DBG_LOCAL(t_FirstAnimID,"FirstAnimID")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<142>");
	c_Image* t_Image=bb_graphics_LoadImage2(t_ImageName,64,64,128,1);
	DBG_LOCAL(t_Image,"Image")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<144>");
	Float t_WalkAnimDuration=FLOAT(0.75);
	DBG_LOCAL(t_WalkAnimDuration,"WalkAnimDuration")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<145>");
	Float t_AttackAnimDuration=FLOAT(0.5);
	DBG_LOCAL(t_AttackAnimDuration,"AttackAnimDuration")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<147>");
	c_CAnimation* t_HumanFootmanWalkUp=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkUp,"HumanFootmanWalkUp")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<148>");
	t_HumanFootmanWalkUp->p_AddKeyframe(t_Image,32,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<149>");
	t_HumanFootmanWalkUp->p_AddKeyframe(t_Image,24,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<150>");
	t_HumanFootmanWalkUp->p_AddKeyframe(t_Image,16,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<151>");
	t_HumanFootmanWalkUp->p_AddKeyframe(t_Image,8,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<152>");
	t_HumanFootmanWalkUp->p_AddKeyframe(t_Image,0,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<153>");
	m_AnimationMap->p_Set(0+t_FirstAnimID,t_HumanFootmanWalkUp);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<155>");
	c_CAnimation* t_HumanFootmanWalkDown=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkDown,"HumanFootmanWalkDown")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<156>");
	t_HumanFootmanWalkDown->p_AddKeyframe(t_Image,4,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<157>");
	t_HumanFootmanWalkDown->p_AddKeyframe(t_Image,12,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<158>");
	t_HumanFootmanWalkDown->p_AddKeyframe(t_Image,20,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<159>");
	t_HumanFootmanWalkDown->p_AddKeyframe(t_Image,28,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<160>");
	t_HumanFootmanWalkDown->p_AddKeyframe(t_Image,36,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<161>");
	m_AnimationMap->p_Set(1+t_FirstAnimID,t_HumanFootmanWalkDown);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<163>");
	c_CAnimation* t_HumanFootmanWalkRight=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkRight,"HumanFootmanWalkRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<164>");
	t_HumanFootmanWalkRight->p_AddKeyframe(t_Image,2,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<165>");
	t_HumanFootmanWalkRight->p_AddKeyframe(t_Image,10,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<166>");
	t_HumanFootmanWalkRight->p_AddKeyframe(t_Image,18,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<167>");
	t_HumanFootmanWalkRight->p_AddKeyframe(t_Image,26,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<168>");
	t_HumanFootmanWalkRight->p_AddKeyframe(t_Image,34,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<169>");
	m_AnimationMap->p_Set(2+t_FirstAnimID,t_HumanFootmanWalkRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<171>");
	c_CAnimation* t_HumanFootmanWalkLeft=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkLeft,"HumanFootmanWalkLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<172>");
	t_HumanFootmanWalkLeft->p_AddKeyframe(t_Image,2,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<173>");
	t_HumanFootmanWalkLeft->p_AddKeyframe(t_Image,10,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<174>");
	t_HumanFootmanWalkLeft->p_AddKeyframe(t_Image,18,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<175>");
	t_HumanFootmanWalkLeft->p_AddKeyframe(t_Image,26,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<176>");
	t_HumanFootmanWalkLeft->p_AddKeyframe(t_Image,34,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<177>");
	m_AnimationMap->p_Set(3+t_FirstAnimID,t_HumanFootmanWalkLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<179>");
	c_CAnimation* t_HumanFootmanAttackUp=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackUp,"HumanFootmanAttackUp")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<180>");
	t_HumanFootmanAttackUp->p_AddKeyframe(t_Image,40,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<181>");
	t_HumanFootmanAttackUp->p_AddKeyframe(t_Image,48,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<182>");
	t_HumanFootmanAttackUp->p_AddKeyframe(t_Image,56,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<183>");
	t_HumanFootmanAttackUp->p_AddKeyframe(t_Image,64,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<184>");
	m_AnimationMap->p_Set(4+t_FirstAnimID,t_HumanFootmanAttackUp);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<186>");
	c_CAnimation* t_HumanFootmanAttackDown=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackDown,"HumanFootmanAttackDown")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<187>");
	t_HumanFootmanAttackDown->p_AddKeyframe(t_Image,44,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<188>");
	t_HumanFootmanAttackDown->p_AddKeyframe(t_Image,52,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<189>");
	t_HumanFootmanAttackDown->p_AddKeyframe(t_Image,60,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<190>");
	t_HumanFootmanAttackDown->p_AddKeyframe(t_Image,68,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<191>");
	m_AnimationMap->p_Set(5+t_FirstAnimID,t_HumanFootmanAttackDown);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<193>");
	c_CAnimation* t_HumanFootmanAttackRight=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackRight,"HumanFootmanAttackRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<194>");
	t_HumanFootmanAttackRight->p_AddKeyframe(t_Image,42,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<195>");
	t_HumanFootmanAttackRight->p_AddKeyframe(t_Image,50,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<196>");
	t_HumanFootmanAttackRight->p_AddKeyframe(t_Image,58,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<197>");
	t_HumanFootmanAttackRight->p_AddKeyframe(t_Image,66,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<198>");
	m_AnimationMap->p_Set(6+t_FirstAnimID,t_HumanFootmanAttackRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<200>");
	c_CAnimation* t_HumanFootmanAttackLeft=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackLeft,"HumanFootmanAttackLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<201>");
	t_HumanFootmanAttackLeft->p_AddKeyframe(t_Image,42,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<202>");
	t_HumanFootmanAttackLeft->p_AddKeyframe(t_Image,50,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<203>");
	t_HumanFootmanAttackLeft->p_AddKeyframe(t_Image,58,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<204>");
	t_HumanFootmanAttackLeft->p_AddKeyframe(t_Image,66,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<205>");
	m_AnimationMap->p_Set(7+t_FirstAnimID,t_HumanFootmanAttackLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<208>");
	c_CAnimation* t_HumanFootmanIdleRight=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleRight,"HumanFootmanIdleRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<209>");
	t_HumanFootmanIdleRight->p_AddKeyframe(t_Image,2,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<210>");
	m_AnimationMap->p_Set(17+t_FirstAnimID,t_HumanFootmanIdleRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<212>");
	c_CAnimation* t_HumanFootmanIdleUp=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleUp,"HumanFootmanIdleUp")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<213>");
	t_HumanFootmanIdleUp->p_AddKeyframe(t_Image,0,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<214>");
	m_AnimationMap->p_Set(18+t_FirstAnimID,t_HumanFootmanIdleUp);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<216>");
	c_CAnimation* t_HumanFootmanIdleLeft=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleLeft,"HumanFootmanIdleLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<217>");
	t_HumanFootmanIdleLeft->p_AddKeyframe(t_Image,2,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<218>");
	m_AnimationMap->p_Set(19+t_FirstAnimID,t_HumanFootmanIdleLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<220>");
	c_CAnimation* t_HumanFootmanIdleDown=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleDown,"HumanFootmanIdleDown")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<221>");
	t_HumanFootmanIdleDown->p_AddKeyframe(t_Image,4,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<222>");
	m_AnimationMap->p_Set(20+t_FirstAnimID,t_HumanFootmanIdleDown);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<224>");
	c_CAnimation* t_HumanFootmanIdleUpRight=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleUpRight,"HumanFootmanIdleUpRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<225>");
	t_HumanFootmanIdleUpRight->p_AddKeyframe(t_Image,2,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<226>");
	m_AnimationMap->p_Set(21+t_FirstAnimID,t_HumanFootmanIdleUpRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<228>");
	c_CAnimation* t_HumanFootmanIdleUpLeft=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleUpLeft,"HumanFootmanIdleUpLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<229>");
	t_HumanFootmanIdleUpLeft->p_AddKeyframe(t_Image,0,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<230>");
	m_AnimationMap->p_Set(22+t_FirstAnimID,t_HumanFootmanIdleUpLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<232>");
	c_CAnimation* t_HumanFootmanIdleDownLeft=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleDownLeft,"HumanFootmanIdleDownLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<233>");
	t_HumanFootmanIdleDownLeft->p_AddKeyframe(t_Image,2,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<234>");
	m_AnimationMap->p_Set(23+t_FirstAnimID,t_HumanFootmanIdleDownLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<236>");
	c_CAnimation* t_HumanFootmanIdleDownRight=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanIdleDownRight,"HumanFootmanIdleDownRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<237>");
	t_HumanFootmanIdleDownRight->p_AddKeyframe(t_Image,4,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<238>");
	m_AnimationMap->p_Set(24+t_FirstAnimID,t_HumanFootmanIdleDownRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<241>");
	c_CAnimation* t_HumanFootmanWalkUpRight=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkUpRight,"HumanFootmanWalkUpRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<242>");
	t_HumanFootmanWalkUpRight->p_AddKeyframe(t_Image,1,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<243>");
	t_HumanFootmanWalkUpRight->p_AddKeyframe(t_Image,9,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<244>");
	t_HumanFootmanWalkUpRight->p_AddKeyframe(t_Image,17,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<245>");
	t_HumanFootmanWalkUpRight->p_AddKeyframe(t_Image,25,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<246>");
	t_HumanFootmanWalkUpRight->p_AddKeyframe(t_Image,33,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<247>");
	m_AnimationMap->p_Set(9+t_FirstAnimID,t_HumanFootmanWalkUpRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<249>");
	c_CAnimation* t_HumanFootmanWalkUpLeft=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkUpLeft,"HumanFootmanWalkUpLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<250>");
	t_HumanFootmanWalkUpLeft->p_AddKeyframe(t_Image,1,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<251>");
	t_HumanFootmanWalkUpLeft->p_AddKeyframe(t_Image,9,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<252>");
	t_HumanFootmanWalkUpLeft->p_AddKeyframe(t_Image,17,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<253>");
	t_HumanFootmanWalkUpLeft->p_AddKeyframe(t_Image,25,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<254>");
	t_HumanFootmanWalkUpLeft->p_AddKeyframe(t_Image,33,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<255>");
	m_AnimationMap->p_Set(10+t_FirstAnimID,t_HumanFootmanWalkUpLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<257>");
	c_CAnimation* t_HumanFootmanWalkDownRight=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkDownRight,"HumanFootmanWalkDownRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<258>");
	t_HumanFootmanWalkDownRight->p_AddKeyframe(t_Image,3,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<259>");
	t_HumanFootmanWalkDownRight->p_AddKeyframe(t_Image,11,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<260>");
	t_HumanFootmanWalkDownRight->p_AddKeyframe(t_Image,19,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<261>");
	t_HumanFootmanWalkDownRight->p_AddKeyframe(t_Image,27,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<262>");
	t_HumanFootmanWalkDownRight->p_AddKeyframe(t_Image,35,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<263>");
	m_AnimationMap->p_Set(11+t_FirstAnimID,t_HumanFootmanWalkDownRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<265>");
	c_CAnimation* t_HumanFootmanWalkDownLeft=(new c_CAnimation)->m_new(t_WalkAnimDuration);
	DBG_LOCAL(t_HumanFootmanWalkDownLeft,"HumanFootmanWalkDownLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<266>");
	t_HumanFootmanWalkDownLeft->p_AddKeyframe(t_Image,3,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<267>");
	t_HumanFootmanWalkDownLeft->p_AddKeyframe(t_Image,11,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<268>");
	t_HumanFootmanWalkDownLeft->p_AddKeyframe(t_Image,19,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<269>");
	t_HumanFootmanWalkDownLeft->p_AddKeyframe(t_Image,27,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<270>");
	t_HumanFootmanWalkDownLeft->p_AddKeyframe(t_Image,35,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<271>");
	m_AnimationMap->p_Set(12+t_FirstAnimID,t_HumanFootmanWalkDownLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<274>");
	c_CAnimation* t_HumanFootmanAttackUpRight=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackUpRight,"HumanFootmanAttackUpRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<275>");
	t_HumanFootmanAttackUpRight->p_AddKeyframe(t_Image,41,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<276>");
	t_HumanFootmanAttackUpRight->p_AddKeyframe(t_Image,49,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<277>");
	t_HumanFootmanAttackUpRight->p_AddKeyframe(t_Image,57,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<278>");
	t_HumanFootmanAttackUpRight->p_AddKeyframe(t_Image,65,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<280>");
	m_AnimationMap->p_Set(13+t_FirstAnimID,t_HumanFootmanAttackUpRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<282>");
	c_CAnimation* t_HumanFootmanAttackUpLeft=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackUpLeft,"HumanFootmanAttackUpLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<283>");
	t_HumanFootmanAttackUpLeft->p_AddKeyframe(t_Image,41,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<284>");
	t_HumanFootmanAttackUpLeft->p_AddKeyframe(t_Image,49,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<285>");
	t_HumanFootmanAttackUpLeft->p_AddKeyframe(t_Image,57,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<286>");
	t_HumanFootmanAttackUpLeft->p_AddKeyframe(t_Image,65,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<288>");
	m_AnimationMap->p_Set(14+t_FirstAnimID,t_HumanFootmanAttackUpLeft);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<290>");
	c_CAnimation* t_HumanFootmanAttackDownRight=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackDownRight,"HumanFootmanAttackDownRight")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<291>");
	t_HumanFootmanAttackDownRight->p_AddKeyframe(t_Image,43,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<292>");
	t_HumanFootmanAttackDownRight->p_AddKeyframe(t_Image,51,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<293>");
	t_HumanFootmanAttackDownRight->p_AddKeyframe(t_Image,59,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<294>");
	t_HumanFootmanAttackDownRight->p_AddKeyframe(t_Image,67,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<296>");
	m_AnimationMap->p_Set(15+t_FirstAnimID,t_HumanFootmanAttackDownRight);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<298>");
	c_CAnimation* t_HumanFootmanAttackDownLeft=(new c_CAnimation)->m_new(t_AttackAnimDuration);
	DBG_LOCAL(t_HumanFootmanAttackDownLeft,"HumanFootmanAttackDownLeft")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<299>");
	t_HumanFootmanAttackDownLeft->p_AddKeyframe(t_Image,43,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<300>");
	t_HumanFootmanAttackDownLeft->p_AddKeyframe(t_Image,51,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<301>");
	t_HumanFootmanAttackDownLeft->p_AddKeyframe(t_Image,59,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<302>");
	t_HumanFootmanAttackDownLeft->p_AddKeyframe(t_Image,67,true);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<304>");
	m_AnimationMap->p_Set(16+t_FirstAnimID,t_HumanFootmanAttackDownLeft);
	return 0;
}
int c_CAnimationManager::m_Init(){
	DBG_ENTER("CAnimationManager.Init")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<119>");
	c_Image* t_PortraitsImage=bb_graphics_LoadImage2(String(L"Portraits.png",13),46,38,5,1);
	DBG_LOCAL(t_PortraitsImage,"PortraitsImage")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<121>");
	c_CAnimation* t_MenuSpawnHumanIdle=(new c_CAnimation)->m_new(FLOAT(0.5));
	DBG_LOCAL(t_MenuSpawnHumanIdle,"MenuSpawnHumanIdle")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<122>");
	t_MenuSpawnHumanIdle->p_AddKeyframe(t_PortraitsImage,0,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<123>");
	m_AnimationMap->p_Set(100,t_MenuSpawnHumanIdle);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<125>");
	c_CAnimation* t_MenuSpawnOrcIdle=(new c_CAnimation)->m_new(FLOAT(0.5));
	DBG_LOCAL(t_MenuSpawnOrcIdle,"MenuSpawnOrcIdle")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<126>");
	t_MenuSpawnOrcIdle->p_AddKeyframe(t_PortraitsImage,1,false);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<127>");
	m_AnimationMap->p_Set(101,t_MenuSpawnOrcIdle);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<129>");
	m_LoadUnitAnimations(String(L"HumanFootman.png",16),0);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<130>");
	m_LoadUnitAnimations(String(L"OrcFootman.png",14),200);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<132>");
	m_LoadUnitAnimations(String(L"HumanFootman.png",16),0);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<133>");
	m_LoadUnitAnimations(String(L"OrcFootman.png",14),200);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<134>");
	return 0;
}
c_CAnimation* c_CAnimationManager::m_GetAnimation(int t_AnimationName){
	DBG_ENTER("CAnimationManager.GetAnimation")
	DBG_LOCAL(t_AnimationName,"AnimationName")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<308>");
	c_CAnimation* t_=m_AnimationMap->p_Get(t_AnimationName);
	return t_;
}
void c_CAnimationManager::mark(){
	Object::mark();
}
String c_CAnimationManager::debug(){
	String t="(CAnimationManager)\n";
	t+=dbg_decl("AnimationMap",&c_CAnimationManager::m_AnimationMap);
	return t;
}
c_CAnimation::c_CAnimation(){
	m_Duration=FLOAT(.0);
	m_KeyframeList=(new c_Stack)->m_new();
	m_TimePerKeyframe=FLOAT(.0);
}
c_CAnimation* c_CAnimation::m_new(Float t_Duration){
	DBG_ENTER("CAnimation.new")
	c_CAnimation *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Duration,"Duration")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<102>");
	this->m_Duration=t_Duration;
	return this;
}
c_CAnimation* c_CAnimation::m_new2(){
	DBG_ENTER("CAnimation.new")
	c_CAnimation *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<97>");
	return this;
}
int c_CAnimation::p_AddKeyframe(c_Image* t_Img,int t_SpriteIndex,bool t_MirrorVertically){
	DBG_ENTER("CAnimation.AddKeyframe")
	c_CAnimation *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Img,"Img")
	DBG_LOCAL(t_SpriteIndex,"SpriteIndex")
	DBG_LOCAL(t_MirrorVertically,"MirrorVertically")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<107>");
	this->m_KeyframeList->p_Push((new c_CImage)->m_new(t_Img,t_SpriteIndex,t_MirrorVertically));
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<108>");
	this->m_TimePerKeyframe=this->m_Duration/Float(this->m_KeyframeList->p_Length2());
	return 0;
}
void c_CAnimation::mark(){
	Object::mark();
	gc_mark_q(m_KeyframeList);
}
String c_CAnimation::debug(){
	String t="(CAnimation)\n";
	t+=dbg_decl("KeyframeList",&m_KeyframeList);
	t+=dbg_decl("Duration",&m_Duration);
	t+=dbg_decl("TimePerKeyframe",&m_TimePerKeyframe);
	return t;
}
c_CImage::c_CImage(){
	m_Img=0;
	m_SpriteIndex=0;
	m_MirrorVertically=false;
}
c_CImage* c_CImage::m_new(c_Image* t_Img,int t_SpriteIndex,bool t_MirrorVertically){
	DBG_ENTER("CImage.new")
	c_CImage *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Img,"Img")
	DBG_LOCAL(t_SpriteIndex,"SpriteIndex")
	DBG_LOCAL(t_MirrorVertically,"MirrorVertically")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<91>");
	gc_assign(this->m_Img,t_Img);
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<92>");
	this->m_SpriteIndex=t_SpriteIndex;
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<93>");
	this->m_MirrorVertically=t_MirrorVertically;
	return this;
}
c_CImage* c_CImage::m_new2(){
	DBG_ENTER("CImage.new")
	c_CImage *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationManager.monkey<86>");
	return this;
}
void c_CImage::mark(){
	Object::mark();
	gc_mark_q(m_Img);
}
String c_CImage::debug(){
	String t="(CImage)\n";
	t+=dbg_decl("Img",&m_Img);
	t+=dbg_decl("SpriteIndex",&m_SpriteIndex);
	t+=dbg_decl("MirrorVertically",&m_MirrorVertically);
	return t;
}
c_Stack::c_Stack(){
	m_data=Array<c_CImage* >();
	m_length=0;
}
c_Stack* c_Stack::m_new(){
	DBG_ENTER("Stack.new")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Stack* c_Stack::m_new2(Array<c_CImage* > t_data){
	DBG_ENTER("Stack.new")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<13>");
	gc_assign(this->m_data,t_data.Slice(0));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<14>");
	this->m_length=t_data.Length();
	return this;
}
void c_Stack::p_Push(c_CImage* t_value){
	DBG_ENTER("Stack.Push")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<67>");
	if(m_length==m_data.Length()){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<68>");
		gc_assign(m_data,m_data.Resize(m_length*2+10));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<70>");
	gc_assign(m_data.At(m_length),t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<71>");
	m_length+=1;
}
void c_Stack::p_Push2(Array<c_CImage* > t_values,int t_offset,int t_count){
	DBG_ENTER("Stack.Push")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_values,"values")
	DBG_LOCAL(t_offset,"offset")
	DBG_LOCAL(t_count,"count")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<79>");
	for(int t_i=0;t_i<t_count;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<80>");
		p_Push(t_values.At(t_offset+t_i));
	}
}
void c_Stack::p_Push3(Array<c_CImage* > t_values,int t_offset){
	DBG_ENTER("Stack.Push")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_values,"values")
	DBG_LOCAL(t_offset,"offset")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<75>");
	p_Push2(t_values,t_offset,t_values.Length()-t_offset);
}
c_CImage* c_Stack::m_NIL;
void c_Stack::p_Length(int t_newlength){
	DBG_ENTER("Stack.Length")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_newlength,"newlength")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<41>");
	if(t_newlength<m_length){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<42>");
		for(int t_i=t_newlength;t_i<m_length;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<43>");
			gc_assign(m_data.At(t_i),m_NIL);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<45>");
		if(t_newlength>m_data.Length()){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<46>");
			gc_assign(m_data,m_data.Resize(bb_math_Max(m_length*2+10,t_newlength)));
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<48>");
	m_length=t_newlength;
}
int c_Stack::p_Length2(){
	DBG_ENTER("Stack.Length")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<52>");
	return m_length;
}
c_CImage* c_Stack::p_Get(int t_index){
	DBG_ENTER("Stack.Get")
	c_Stack *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_index,"index")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/stack.monkey<100>");
	return m_data.At(t_index);
}
void c_Stack::mark(){
	Object::mark();
	gc_mark_q(m_data);
}
String c_Stack::debug(){
	String t="(Stack)\n";
	t+=dbg_decl("NIL",&c_Stack::m_NIL);
	t+=dbg_decl("data",&m_data);
	t+=dbg_decl("length",&m_length);
	return t;
}
int bb_math_Max(int t_x,int t_y){
	DBG_ENTER("Max")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<56>");
	if(t_x>t_y){
		DBG_BLOCK();
		return t_x;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<57>");
	return t_y;
}
Float bb_math_Max2(Float t_x,Float t_y){
	DBG_ENTER("Max")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<83>");
	if(t_x>t_y){
		DBG_BLOCK();
		return t_x;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<84>");
	return t_y;
}
c_Map2::c_Map2(){
	m_root=0;
}
c_Map2* c_Map2::m_new(){
	DBG_ENTER("Map.new")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<7>");
	return this;
}
int c_Map2::p_RotateLeft2(c_Node3* t_node){
	DBG_ENTER("Map.RotateLeft")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<251>");
	c_Node3* t_child=t_node->m_right;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<252>");
	gc_assign(t_node->m_right,t_child->m_left);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<253>");
	if((t_child->m_left)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<254>");
		gc_assign(t_child->m_left->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<256>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<257>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<258>");
		if(t_node==t_node->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<259>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<261>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<264>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<266>");
	gc_assign(t_child->m_left,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<267>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map2::p_RotateRight2(c_Node3* t_node){
	DBG_ENTER("Map.RotateRight")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<271>");
	c_Node3* t_child=t_node->m_left;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<272>");
	gc_assign(t_node->m_left,t_child->m_right);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<273>");
	if((t_child->m_right)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<274>");
		gc_assign(t_child->m_right->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<276>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<277>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<278>");
		if(t_node==t_node->m_parent->m_right){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<279>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<281>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<284>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<286>");
	gc_assign(t_child->m_right,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<287>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map2::p_InsertFixup2(c_Node3* t_node){
	DBG_ENTER("Map.InsertFixup")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<212>");
	while(((t_node->m_parent)!=0) && t_node->m_parent->m_color==-1 && ((t_node->m_parent->m_parent)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<213>");
		if(t_node->m_parent==t_node->m_parent->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<214>");
			c_Node3* t_uncle=t_node->m_parent->m_parent->m_right;
			DBG_LOCAL(t_uncle,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<215>");
			if(((t_uncle)!=0) && t_uncle->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<216>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<217>");
				t_uncle->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<218>");
				t_uncle->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<219>");
				t_node=t_uncle->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<221>");
				if(t_node==t_node->m_parent->m_right){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<222>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<223>");
					p_RotateLeft2(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<225>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<226>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<227>");
				p_RotateRight2(t_node->m_parent->m_parent);
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<230>");
			c_Node3* t_uncle2=t_node->m_parent->m_parent->m_left;
			DBG_LOCAL(t_uncle2,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<231>");
			if(((t_uncle2)!=0) && t_uncle2->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<232>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<233>");
				t_uncle2->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<234>");
				t_uncle2->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<235>");
				t_node=t_uncle2->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<237>");
				if(t_node==t_node->m_parent->m_left){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<238>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<239>");
					p_RotateRight2(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<241>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<242>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<243>");
				p_RotateLeft2(t_node->m_parent->m_parent);
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<247>");
	m_root->m_color=1;
	return 0;
}
bool c_Map2::p_Set(int t_key,c_CAnimation* t_value){
	DBG_ENTER("Map.Set")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<29>");
	c_Node3* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<30>");
	c_Node3* t_parent=0;
	int t_cmp=0;
	DBG_LOCAL(t_parent,"parent")
	DBG_LOCAL(t_cmp,"cmp")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<32>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<33>");
		t_parent=t_node;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<34>");
		t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<35>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<36>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<37>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<38>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<40>");
				gc_assign(t_node->m_value,t_value);
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<41>");
				return false;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<45>");
	t_node=(new c_Node3)->m_new(t_key,t_value,-1,t_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<47>");
	if((t_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<48>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<49>");
			gc_assign(t_parent->m_right,t_node);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<51>");
			gc_assign(t_parent->m_left,t_node);
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<53>");
		p_InsertFixup2(t_node);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<55>");
		gc_assign(m_root,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<57>");
	return true;
}
c_Node3* c_Map2::p_FindNode(int t_key){
	DBG_ENTER("Map.FindNode")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<157>");
	c_Node3* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<159>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<160>");
		int t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_LOCAL(t_cmp,"cmp")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<161>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<162>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<163>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<164>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<166>");
				return t_node;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<169>");
	return t_node;
}
c_CAnimation* c_Map2::p_Get(int t_key){
	DBG_ENTER("Map.Get")
	c_Map2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<101>");
	c_Node3* t_node=p_FindNode(t_key);
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<102>");
	if((t_node)!=0){
		DBG_BLOCK();
		return t_node->m_value;
	}
	return 0;
}
void c_Map2::mark(){
	Object::mark();
	gc_mark_q(m_root);
}
String c_Map2::debug(){
	String t="(Map)\n";
	t+=dbg_decl("root",&m_root);
	return t;
}
c_IntMap2::c_IntMap2(){
}
c_IntMap2* c_IntMap2::m_new(){
	DBG_ENTER("IntMap.new")
	c_IntMap2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<534>");
	c_Map2::m_new();
	return this;
}
int c_IntMap2::p_Compare(int t_lhs,int t_rhs){
	DBG_ENTER("IntMap.Compare")
	c_IntMap2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<537>");
	int t_=t_lhs-t_rhs;
	return t_;
}
void c_IntMap2::mark(){
	c_Map2::mark();
}
String c_IntMap2::debug(){
	String t="(IntMap)\n";
	t=c_Map2::debug()+t;
	return t;
}
c_Node3::c_Node3(){
	m_key=0;
	m_right=0;
	m_left=0;
	m_value=0;
	m_color=0;
	m_parent=0;
}
c_Node3* c_Node3::m_new(int t_key,c_CAnimation* t_value,int t_color,c_Node3* t_parent){
	DBG_ENTER("Node.new")
	c_Node3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_LOCAL(t_color,"color")
	DBG_LOCAL(t_parent,"parent")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<364>");
	this->m_key=t_key;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<365>");
	gc_assign(this->m_value,t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<366>");
	this->m_color=t_color;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<367>");
	gc_assign(this->m_parent,t_parent);
	return this;
}
c_Node3* c_Node3::m_new2(){
	DBG_ENTER("Node.new")
	c_Node3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<361>");
	return this;
}
void c_Node3::mark(){
	Object::mark();
	gc_mark_q(m_right);
	gc_mark_q(m_left);
	gc_mark_q(m_value);
	gc_mark_q(m_parent);
}
String c_Node3::debug(){
	String t="(Node)\n";
	t+=dbg_decl("key",&m_key);
	t+=dbg_decl("value",&m_value);
	t+=dbg_decl("color",&m_color);
	t+=dbg_decl("parent",&m_parent);
	t+=dbg_decl("left",&m_left);
	t+=dbg_decl("right",&m_right);
	return t;
}
c_FVector::c_FVector(){
	m_X=FLOAT(.0);
	m_Y=FLOAT(.0);
}
c_FVector* c_FVector::m_new(Float t_X,Float t_Y){
	DBG_ENTER("FVector.new")
	c_FVector *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_X,"X")
	DBG_LOCAL(t_Y,"Y")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<8>");
	this->m_X=t_X;
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<9>");
	this->m_Y=t_Y;
	return this;
}
Float c_FVector::p_GetOrientationFromNormed(){
	DBG_ENTER("FVector.GetOrientationFromNormed")
	c_FVector *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<13>");
	Float t_AngleA=(Float)(acos(this->m_X)*R2D);
	DBG_LOCAL(t_AngleA,"AngleA")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<14>");
	Float t_AngleB=(Float)(asin(this->m_Y)*R2D);
	DBG_LOCAL(t_AngleB,"AngleB")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<15>");
	if(t_AngleB<FLOAT(0.0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Misc.monkey<16>");
		Float t_=-t_AngleA;
		return t_;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Misc.monkey<18>");
		return t_AngleA;
	}
}
void c_FVector::mark(){
	Object::mark();
}
String c_FVector::debug(){
	String t="(FVector)\n";
	t+=dbg_decl("X",&m_X);
	t+=dbg_decl("Y",&m_Y);
	return t;
}
c_CGameObject::c_CGameObject(){
	m_ID=-1;
	m_ComponentList=(new c_List2)->m_new();
}
int c_CGameObject::m_GameObjectCount;
c_CGameObject* c_CGameObject::m_new(){
	DBG_ENTER("CGameObject.new")
	c_CGameObject *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<83>");
	this->m_ID=m_GameObjectCount;
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<84>");
	m_GameObjectCount+=1;
	return this;
}
int c_CGameObject::p_AddComponent(c_CComponent* t_Component){
	DBG_ENTER("CGameObject.AddComponent")
	c_CGameObject *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Component,"Component")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<101>");
	this->m_ComponentList->p_AddLast2(t_Component);
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<102>");
	gc_assign(t_Component->m_GameObject,this);
	return 0;
}
int c_CGameObject::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CGameObject.Update")
	c_CGameObject *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<89>");
	c_Enumerator* t_=this->m_ComponentList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CComponent* t_Component=t_->p_NextObject();
		DBG_LOCAL(t_Component,"Component")
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<90>");
		t_Component->p_Update2(t_DeltaTime,t_CommandBuffer);
	}
	return 0;
}
int c_CGameObject::p_RenderDebug(){
	DBG_ENTER("CGameObject.RenderDebug")
	c_CGameObject *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<95>");
	c_Enumerator* t_=this->m_ComponentList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CComponent* t_Component=t_->p_NextObject();
		DBG_LOCAL(t_Component,"Component")
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<96>");
		t_Component->p_RenderDebug();
	}
	return 0;
}
c_CComponent* c_CGameObject::p_GetComponent(int t_ComponentType){
	DBG_ENTER("CGameObject.GetComponent")
	c_CGameObject *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ComponentType,"ComponentType")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<106>");
	c_Enumerator* t_=this->m_ComponentList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CComponent* t_Component=t_->p_NextObject();
		DBG_LOCAL(t_Component,"Component")
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<107>");
		if(t_Component->m_Type==t_ComponentType){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/GameObject.monkey<108>");
			return t_Component;
		}
	}
	return 0;
}
void c_CGameObject::mark(){
	Object::mark();
	gc_mark_q(m_ComponentList);
}
String c_CGameObject::debug(){
	String t="(CGameObject)\n";
	t+=dbg_decl("ID",&m_ID);
	t+=dbg_decl("GameObjectCount",&c_CGameObject::m_GameObjectCount);
	t+=dbg_decl("ComponentList",&m_ComponentList);
	return t;
}
c_CComponent::c_CComponent(){
	m_Type=-1;
	m_GameObject=0;
	m_LocalToGlobalEventMap=0;
}
c_CComponent* c_CComponent::m_new(int t_Type){
	DBG_ENTER("CComponent.new")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Type,"Type")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<41>");
	this->m_Type=t_Type;
	return this;
}
c_CComponent* c_CComponent::m_new2(){
	DBG_ENTER("CComponent.new")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<35>");
	return this;
}
int c_CComponent::p_BindEvents(int t_LocalEventID,int t_GlobalEventID,c_CComponent* t_TargetComponent){
	DBG_ENTER("CComponent.BindEvents")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_LocalEventID,"LocalEventID")
	DBG_LOCAL(t_GlobalEventID,"GlobalEventID")
	DBG_LOCAL(t_TargetComponent,"TargetComponent")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<53>");
	if(this->m_LocalToGlobalEventMap==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<54>");
		gc_assign(this->m_LocalToGlobalEventMap,(new c_IntMap3)->m_new());
	}
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<56>");
	c_List4* t_EventDataList=this->m_LocalToGlobalEventMap->p_Get(t_LocalEventID);
	DBG_LOCAL(t_EventDataList,"EventDataList")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<57>");
	c_CGlobalEventData* t_GlobalEventData=(new c_CGlobalEventData)->m_new(t_GlobalEventID,t_TargetComponent);
	DBG_LOCAL(t_GlobalEventData,"GlobalEventData")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<58>");
	if(t_EventDataList==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<59>");
		t_EventDataList=(new c_List4)->m_new();
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<60>");
		this->m_LocalToGlobalEventMap->p_Add2(t_LocalEventID,t_EventDataList);
	}
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<63>");
	t_EventDataList->p_AddLast4(t_GlobalEventData);
	return 0;
}
int c_CComponent::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CComponent.Update")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_CComponent::p_RenderDebug(){
	DBG_ENTER("CComponent.RenderDebug")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_CComponent::p_OnEvent(int t_EventID){
	DBG_ENTER("CComponent.OnEvent")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_CComponent::p_RaiseEvent(int t_LocalEventID){
	DBG_ENTER("CComponent.RaiseEvent")
	c_CComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_LocalEventID,"LocalEventID")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<66>");
	if((this->m_LocalToGlobalEventMap)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<67>");
		c_List4* t_EventDataList=this->m_LocalToGlobalEventMap->p_Get(t_LocalEventID);
		DBG_LOCAL(t_EventDataList,"EventDataList")
		DBG_INFO("C:/MonkeyX77a/GameObject.monkey<68>");
		if((t_EventDataList)!=0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/GameObject.monkey<69>");
			c_Enumerator6* t_=t_EventDataList->p_ObjectEnumerator();
			while(t_->p_HasNext()){
				DBG_BLOCK();
				c_CGlobalEventData* t_GlobalEventData=t_->p_NextObject();
				DBG_LOCAL(t_GlobalEventData,"GlobalEventData")
				DBG_INFO("C:/MonkeyX77a/GameObject.monkey<70>");
				t_GlobalEventData->m_TargetComponent->p_OnEvent(t_GlobalEventData->m_GlobalEventID);
			}
		}
	}
	return 0;
}
void c_CComponent::mark(){
	Object::mark();
	gc_mark_q(m_GameObject);
	gc_mark_q(m_LocalToGlobalEventMap);
}
String c_CComponent::debug(){
	String t="(CComponent)\n";
	t+=dbg_decl("Type",&m_Type);
	t+=dbg_decl("GameObject",&m_GameObject);
	t+=dbg_decl("LocalToGlobalEventMap",&m_LocalToGlobalEventMap);
	return t;
}
c_CInputArea::c_CInputArea(){
	m_TopLeftCorner=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_BottomRightCorner=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_Size=0;
	m_Priority=1;
	m_State=0;
	m_LastInputGlobalPosition=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
}
c_CInputArea* c_CInputArea::m_new(c_FVector* t_Center,c_FVector* t_Size,int t_Priority){
	DBG_ENTER("CInputArea.new")
	c_CInputArea *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Center,"Center")
	DBG_LOCAL(t_Size,"Size")
	DBG_LOCAL(t_Priority,"Priority")
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<28>");
	c_CComponent::m_new(2);
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<30>");
	this->m_TopLeftCorner->m_X=t_Center->m_X-t_Size->m_X*FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<31>");
	this->m_TopLeftCorner->m_Y=t_Center->m_Y-t_Size->m_Y*FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<32>");
	this->m_BottomRightCorner->m_X=t_Center->m_X+t_Size->m_X*FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<33>");
	this->m_BottomRightCorner->m_Y=t_Center->m_Y+t_Size->m_Y*FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<34>");
	gc_assign(this->m_Size,t_Size);
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<35>");
	this->m_Priority=t_Priority;
	return this;
}
c_CInputArea* c_CInputArea::m_new2(){
	DBG_ENTER("CInputArea.new")
	c_CInputArea *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<17>");
	c_CComponent::m_new2();
	return this;
}
bool c_CInputArea::m_DebugDisplay;
int c_CInputArea::p_RenderDebug(){
	DBG_ENTER("CInputArea.RenderDebug")
	c_CInputArea *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/InputArea.monkey<39>");
	if(m_DebugDisplay){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<40>");
		c_FVector* t_TopRightCorner=(new c_FVector)->m_new(this->m_TopLeftCorner->m_X+this->m_Size->m_X,this->m_TopLeftCorner->m_Y);
		DBG_LOCAL(t_TopRightCorner,"TopRightCorner")
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<41>");
		c_FVector* t_BottomLeftCorner=(new c_FVector)->m_new(this->m_TopLeftCorner->m_X,this->m_TopLeftCorner->m_Y+this->m_Size->m_Y);
		DBG_LOCAL(t_BottomLeftCorner,"BottomLeftCorner")
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<43>");
		c_CColor* t_ColorNeutral=(new c_CColor)->m_new(255,255,255);
		DBG_LOCAL(t_ColorNeutral,"ColorNeutral")
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<44>");
		c_CColor* t_ColorHovered=(new c_CColor)->m_new(255,0,0);
		DBG_LOCAL(t_ColorHovered,"ColorHovered")
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<45>");
		c_CColor* t_ColorClicked=(new c_CColor)->m_new(0,0,255);
		DBG_LOCAL(t_ColorClicked,"ColorClicked")
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<47>");
		int t_1=this->m_State;
		DBG_LOCAL(t_1,"1")
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<48>");
		if(t_1==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/InputArea.monkey<49>");
			bb_graphics_SetColor(Float(t_ColorNeutral->m_R),Float(t_ColorNeutral->m_G),Float(t_ColorNeutral->m_B));
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/InputArea.monkey<50>");
			if(t_1==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputArea.monkey<51>");
				bb_graphics_SetColor(Float(t_ColorHovered->m_R),Float(t_ColorHovered->m_G),Float(t_ColorHovered->m_B));
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/InputArea.monkey<52>");
				if(t_1==2){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/InputArea.monkey<53>");
					bb_graphics_SetColor(Float(t_ColorClicked->m_R),Float(t_ColorClicked->m_G),Float(t_ColorClicked->m_B));
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<55>");
		bb_graphics_DrawLine(this->m_TopLeftCorner->m_X,this->m_TopLeftCorner->m_Y,t_TopRightCorner->m_X,t_TopRightCorner->m_Y);
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<56>");
		bb_graphics_DrawLine(t_TopRightCorner->m_X,t_TopRightCorner->m_Y,m_BottomRightCorner->m_X,m_BottomRightCorner->m_Y);
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<57>");
		bb_graphics_DrawLine(m_BottomRightCorner->m_X,m_BottomRightCorner->m_Y,t_BottomLeftCorner->m_X,t_BottomLeftCorner->m_Y);
		DBG_INFO("C:/MonkeyX77a/InputArea.monkey<58>");
		bb_graphics_DrawLine(t_BottomLeftCorner->m_X,t_BottomLeftCorner->m_Y,this->m_TopLeftCorner->m_X,this->m_TopLeftCorner->m_Y);
	}
	return 0;
}
void c_CInputArea::mark(){
	c_CComponent::mark();
	gc_mark_q(m_TopLeftCorner);
	gc_mark_q(m_BottomRightCorner);
	gc_mark_q(m_Size);
	gc_mark_q(m_LastInputGlobalPosition);
}
String c_CInputArea::debug(){
	String t="(CInputArea)\n";
	t=c_CComponent::debug()+t;
	t+=dbg_decl("TopLeftCorner",&m_TopLeftCorner);
	t+=dbg_decl("BottomRightCorner",&m_BottomRightCorner);
	t+=dbg_decl("Size",&m_Size);
	t+=dbg_decl("State",&m_State);
	t+=dbg_decl("Priority",&m_Priority);
	t+=dbg_decl("LastInputGlobalPosition",&m_LastInputGlobalPosition);
	t+=dbg_decl("DebugDisplay",&c_CInputArea::m_DebugDisplay);
	return t;
}
c_List2::c_List2(){
	m__head=((new c_HeadNode2)->m_new());
}
c_List2* c_List2::m_new(){
	DBG_ENTER("List.new")
	c_List2 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node4* c_List2::p_AddLast2(c_CComponent* t_data){
	DBG_ENTER("List.AddLast")
	c_List2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node4* t_=(new c_Node4)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List2* c_List2::m_new2(Array<c_CComponent* > t_data){
	DBG_ENTER("List.new")
	c_List2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CComponent* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CComponent* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast2(t_t);
	}
	return this;
}
c_Enumerator* c_List2::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator* t_=(new c_Enumerator)->m_new(this);
	return t_;
}
void c_List2::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List2::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node4::c_Node4(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node4* c_Node4::m_new(c_Node4* t_succ,c_Node4* t_pred,c_CComponent* t_data){
	DBG_ENTER("Node.new")
	c_Node4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node4* c_Node4::m_new2(){
	DBG_ENTER("Node.new")
	c_Node4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
void c_Node4::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node4::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode2::c_HeadNode2(){
}
c_HeadNode2* c_HeadNode2::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node4::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode2::mark(){
	c_Node4::mark();
}
String c_HeadNode2::debug(){
	String t="(HeadNode)\n";
	t=c_Node4::debug()+t;
	return t;
}
c_CMapInterface::c_CMapInterface(){
	m_InputArea=0;
	m_PreviousInputAreaState=0;
	m_HumanFootmanMenuActivated=false;
	m_OrcFootmanMenuActivated=false;
}
c_CMapInterface* c_CMapInterface::m_new(){
	DBG_ENTER("CMapInterface.new")
	c_CMapInterface *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<15>");
	c_CComponent::m_new(3);
	return this;
}
int c_CMapInterface::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CMapInterface.Update")
	c_CMapInterface *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<18>");
	if(m_InputArea==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<19>");
		gc_assign(m_InputArea,dynamic_cast<c_CInputArea*>(this->m_GameObject->p_GetComponent(2)));
	}
	DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<23>");
	if(this->m_InputArea->m_State==2 && this->m_PreviousInputAreaState!=2){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<24>");
		c_FVector* t_SpawnPosition=(new c_FVector)->m_new(this->m_InputArea->m_LastInputGlobalPosition->m_X/Float(c_CAIManager::m_TileSize)/c_CAIManager::m_MapScale,this->m_InputArea->m_LastInputGlobalPosition->m_Y/Float(c_CAIManager::m_TileSize)/c_CAIManager::m_MapScale);
		DBG_LOCAL(t_SpawnPosition,"SpawnPosition")
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<25>");
		c_IVector* t_PositionI=(new c_IVector)->m_new(int(t_SpawnPosition->m_X),int(t_SpawnPosition->m_Y));
		DBG_LOCAL(t_PositionI,"PositionI")
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<26>");
		c_CTileStatusData* t_TileStatusData=c_CAIManager::m_GetTileStatusData(t_PositionI);
		DBG_LOCAL(t_TileStatusData,"TileStatusData")
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<28>");
		if(t_TileStatusData->m_TileStatus==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<29>");
			if(this->m_HumanFootmanMenuActivated){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<31>");
				c_CSpawnManager::m_RequestSpawn(0,t_SpawnPosition);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<32>");
				if(this->m_OrcFootmanMenuActivated){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<34>");
					c_CSpawnManager::m_RequestSpawn(1,t_SpawnPosition);
				}
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<38>");
	this->m_PreviousInputAreaState=this->m_InputArea->m_State;
	return 0;
}
int c_CMapInterface::p_OnEvent(int t_EventID){
	DBG_ENTER("CMapInterface.OnEvent")
	c_CMapInterface *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_EventID,"EventID")
	DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<42>");
	int t_1=t_EventID;
	DBG_LOCAL(t_1,"1")
	DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<43>");
	if(t_1==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<44>");
		this->m_HumanFootmanMenuActivated=true;
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<45>");
		this->m_OrcFootmanMenuActivated=false;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<46>");
		if(t_1==1){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<47>");
			this->m_OrcFootmanMenuActivated=true;
			DBG_INFO("C:/MonkeyX77a/MapInterface.monkey<48>");
			this->m_HumanFootmanMenuActivated=false;
		}
	}
	return 0;
}
void c_CMapInterface::mark(){
	c_CComponent::mark();
	gc_mark_q(m_InputArea);
}
String c_CMapInterface::debug(){
	String t="(CMapInterface)\n";
	t=c_CComponent::debug()+t;
	t+=dbg_decl("HumanFootmanMenuActivated",&m_HumanFootmanMenuActivated);
	t+=dbg_decl("OrcFootmanMenuActivated",&m_OrcFootmanMenuActivated);
	t+=dbg_decl("InputArea",&m_InputArea);
	t+=dbg_decl("PreviousInputAreaState",&m_PreviousInputAreaState);
	return t;
}
c_Enumerator::c_Enumerator(){
	m__list=0;
	m__curr=0;
}
c_Enumerator* c_Enumerator::m_new(c_List2* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator* c_Enumerator::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CComponent* c_Enumerator::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CComponent* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
c_Enumerator2::c_Enumerator2(){
	m__list=0;
	m__curr=0;
}
c_Enumerator2* c_Enumerator2::m_new(c_List* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator2* c_Enumerator2::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator2::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CComponentUpdater* c_Enumerator2::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CComponentUpdater* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator2::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator2::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
c_List3::c_List3(){
	m__head=((new c_HeadNode3)->m_new());
}
c_List3* c_List3::m_new(){
	DBG_ENTER("List.new")
	c_List3 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node5* c_List3::p_AddLast3(c_CGameObject* t_data){
	DBG_ENTER("List.AddLast")
	c_List3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node5* t_=(new c_Node5)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List3* c_List3::m_new2(Array<c_CGameObject* > t_data){
	DBG_ENTER("List.new")
	c_List3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CGameObject* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CGameObject* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast3(t_t);
	}
	return this;
}
c_Enumerator3* c_List3::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator3* t_=(new c_Enumerator3)->m_new(this);
	return t_;
}
void c_List3::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List3::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node5::c_Node5(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node5* c_Node5::m_new(c_Node5* t_succ,c_Node5* t_pred,c_CGameObject* t_data){
	DBG_ENTER("Node.new")
	c_Node5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node5* c_Node5::m_new2(){
	DBG_ENTER("Node.new")
	c_Node5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
void c_Node5::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node5::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode3::c_HeadNode3(){
}
c_HeadNode3* c_HeadNode3::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node5::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode3::mark(){
	c_Node5::mark();
}
String c_HeadNode3::debug(){
	String t="(HeadNode)\n";
	t=c_Node5::debug()+t;
	return t;
}
c_CMenuComponent::c_CMenuComponent(){
	m_MenuPosition=(new c_FVector)->m_new(FLOAT(1.0),FLOAT(1.0));
	m_MenuIdleAnimID=0;
	m_MenuID=0;
	m_RequestState=0;
	m_State=0;
	m_HighlightSquareID=-1;
	m_NeutralColor=(new c_CColor)->m_new(0,0,200);
	m_HighlightColor=(new c_CColor)->m_new(0,255,200);
	m_SelectColor=(new c_CColor)->m_new(255,200,0);
	m_Init=false;
	m_SpriteID=-1;
	m_HighlightWidth=FLOAT(48.0);
	m_HighlightHeight=FLOAT(40.0);
}
c_CMenuComponent* c_CMenuComponent::m_new(c_FVector* t_Position,int t_MenuIdleAnimID,int t_MenuID){
	DBG_ENTER("CMenuComponent.new")
	c_CMenuComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Position,"Position")
	DBG_LOCAL(t_MenuIdleAnimID,"MenuIdleAnimID")
	DBG_LOCAL(t_MenuID,"MenuID")
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<36>");
	c_CComponent::m_new(1);
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<38>");
	this->m_MenuPosition->m_X=t_Position->m_X;
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<39>");
	this->m_MenuPosition->m_Y=t_Position->m_Y;
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<41>");
	this->m_MenuIdleAnimID=t_MenuIdleAnimID;
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<43>");
	this->m_MenuID=t_MenuID;
	return this;
}
c_CMenuComponent* c_CMenuComponent::m_new2(){
	DBG_ENTER("CMenuComponent.new")
	c_CMenuComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<16>");
	c_CComponent::m_new2();
	return this;
}
int c_CMenuComponent::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CMenuComponent.Update")
	c_CMenuComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<47>");
	if(this->m_Init==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<48>");
		this->m_Init=true;
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<53>");
		this->m_SpriteID=t_CommandBuffer->p_AddCommand_CreateRenderItem(1,FLOAT(100.0));
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<54>");
		t_CommandBuffer->p_AddCommand2(this->m_SpriteID,4,this->m_MenuIdleAnimID,0,0);
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<55>");
		t_CommandBuffer->p_AddCommand(this->m_SpriteID,3,this->m_MenuPosition->m_X,this->m_MenuPosition->m_Y);
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<56>");
		t_CommandBuffer->p_AddCommand(this->m_SpriteID,7,c_CAIManager::m_MapScale,c_CAIManager::m_MapScale);
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<59>");
		this->m_HighlightSquareID=t_CommandBuffer->p_AddCommand_CreateRenderItem(5,FLOAT(99.0));
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<60>");
		t_CommandBuffer->p_AddCommand2(this->m_HighlightSquareID,6,this->m_NeutralColor->m_R,this->m_NeutralColor->m_G,this->m_NeutralColor->m_B);
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<61>");
		t_CommandBuffer->p_AddCommand(this->m_HighlightSquareID,3,this->m_MenuPosition->m_X,this->m_MenuPosition->m_Y);
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<62>");
		t_CommandBuffer->p_AddCommand(this->m_HighlightSquareID,7,m_HighlightWidth*c_CAIManager::m_MapScale,m_HighlightHeight*c_CAIManager::m_MapScale);
	}
	return 0;
}
int c_CMenuComponent::p_OnEvent(int t_EventID){
	DBG_ENTER("CMenuComponent.OnEvent")
	c_CMenuComponent *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_EventID,"EventID")
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<67>");
	int t_1=t_EventID;
	DBG_LOCAL(t_1,"1")
	DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<68>");
	if(t_1==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<70>");
		this->m_RequestState=0;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<71>");
		if(t_1==1){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<73>");
			this->m_RequestState=1;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<74>");
			if(t_1==2){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/MenuComponent.monkey<76>");
				this->m_RequestState=2;
			}
		}
	}
	return 0;
}
void c_CMenuComponent::mark(){
	c_CComponent::mark();
	gc_mark_q(m_MenuPosition);
	gc_mark_q(m_NeutralColor);
	gc_mark_q(m_HighlightColor);
	gc_mark_q(m_SelectColor);
}
String c_CMenuComponent::debug(){
	String t="(CMenuComponent)\n";
	t=c_CComponent::debug()+t;
	t+=dbg_decl("MenuID",&m_MenuID);
	t+=dbg_decl("State",&m_State);
	t+=dbg_decl("Init",&m_Init);
	t+=dbg_decl("SpriteID",&m_SpriteID);
	t+=dbg_decl("HighlightSquareID",&m_HighlightSquareID);
	t+=dbg_decl("NeutralColor",&m_NeutralColor);
	t+=dbg_decl("HighlightColor",&m_HighlightColor);
	t+=dbg_decl("SelectColor",&m_SelectColor);
	t+=dbg_decl("MenuPosition",&m_MenuPosition);
	t+=dbg_decl("HighlightWidth",&m_HighlightWidth);
	t+=dbg_decl("HighlightHeight",&m_HighlightHeight);
	t+=dbg_decl("MenuIdleAnimID",&m_MenuIdleAnimID);
	t+=dbg_decl("RequestState",&m_RequestState);
	return t;
}
c_CGlobalEventData::c_CGlobalEventData(){
	m_GlobalEventID=0;
	m_TargetComponent=0;
}
c_CGlobalEventData* c_CGlobalEventData::m_new(int t_GlobalEventID,c_CComponent* t_TargetComponent){
	DBG_ENTER("CGlobalEventData.new")
	c_CGlobalEventData *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_GlobalEventID,"GlobalEventID")
	DBG_LOCAL(t_TargetComponent,"TargetComponent")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<19>");
	this->m_GlobalEventID=t_GlobalEventID;
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<20>");
	gc_assign(this->m_TargetComponent,t_TargetComponent);
	return this;
}
c_CGlobalEventData* c_CGlobalEventData::m_new2(){
	DBG_ENTER("CGlobalEventData.new")
	c_CGlobalEventData *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/GameObject.monkey<14>");
	return this;
}
void c_CGlobalEventData::mark(){
	Object::mark();
	gc_mark_q(m_TargetComponent);
}
String c_CGlobalEventData::debug(){
	String t="(CGlobalEventData)\n";
	t+=dbg_decl("GlobalEventID",&m_GlobalEventID);
	t+=dbg_decl("TargetComponent",&m_TargetComponent);
	return t;
}
c_List4::c_List4(){
	m__head=((new c_HeadNode4)->m_new());
}
c_List4* c_List4::m_new(){
	DBG_ENTER("List.new")
	c_List4 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node7* c_List4::p_AddLast4(c_CGlobalEventData* t_data){
	DBG_ENTER("List.AddLast")
	c_List4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node7* t_=(new c_Node7)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List4* c_List4::m_new2(Array<c_CGlobalEventData* > t_data){
	DBG_ENTER("List.new")
	c_List4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CGlobalEventData* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CGlobalEventData* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast4(t_t);
	}
	return this;
}
c_Enumerator6* c_List4::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator6* t_=(new c_Enumerator6)->m_new(this);
	return t_;
}
void c_List4::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List4::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Map3::c_Map3(){
	m_root=0;
}
c_Map3* c_Map3::m_new(){
	DBG_ENTER("Map.new")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<7>");
	return this;
}
c_Node6* c_Map3::p_FindNode(int t_key){
	DBG_ENTER("Map.FindNode")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<157>");
	c_Node6* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<159>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<160>");
		int t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_LOCAL(t_cmp,"cmp")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<161>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<162>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<163>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<164>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<166>");
				return t_node;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<169>");
	return t_node;
}
c_List4* c_Map3::p_Get(int t_key){
	DBG_ENTER("Map.Get")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<101>");
	c_Node6* t_node=p_FindNode(t_key);
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<102>");
	if((t_node)!=0){
		DBG_BLOCK();
		return t_node->m_value;
	}
	return 0;
}
int c_Map3::p_RotateLeft3(c_Node6* t_node){
	DBG_ENTER("Map.RotateLeft")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<251>");
	c_Node6* t_child=t_node->m_right;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<252>");
	gc_assign(t_node->m_right,t_child->m_left);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<253>");
	if((t_child->m_left)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<254>");
		gc_assign(t_child->m_left->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<256>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<257>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<258>");
		if(t_node==t_node->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<259>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<261>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<264>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<266>");
	gc_assign(t_child->m_left,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<267>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map3::p_RotateRight3(c_Node6* t_node){
	DBG_ENTER("Map.RotateRight")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<271>");
	c_Node6* t_child=t_node->m_left;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<272>");
	gc_assign(t_node->m_left,t_child->m_right);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<273>");
	if((t_child->m_right)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<274>");
		gc_assign(t_child->m_right->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<276>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<277>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<278>");
		if(t_node==t_node->m_parent->m_right){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<279>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<281>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<284>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<286>");
	gc_assign(t_child->m_right,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<287>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map3::p_InsertFixup3(c_Node6* t_node){
	DBG_ENTER("Map.InsertFixup")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<212>");
	while(((t_node->m_parent)!=0) && t_node->m_parent->m_color==-1 && ((t_node->m_parent->m_parent)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<213>");
		if(t_node->m_parent==t_node->m_parent->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<214>");
			c_Node6* t_uncle=t_node->m_parent->m_parent->m_right;
			DBG_LOCAL(t_uncle,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<215>");
			if(((t_uncle)!=0) && t_uncle->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<216>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<217>");
				t_uncle->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<218>");
				t_uncle->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<219>");
				t_node=t_uncle->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<221>");
				if(t_node==t_node->m_parent->m_right){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<222>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<223>");
					p_RotateLeft3(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<225>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<226>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<227>");
				p_RotateRight3(t_node->m_parent->m_parent);
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<230>");
			c_Node6* t_uncle2=t_node->m_parent->m_parent->m_left;
			DBG_LOCAL(t_uncle2,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<231>");
			if(((t_uncle2)!=0) && t_uncle2->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<232>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<233>");
				t_uncle2->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<234>");
				t_uncle2->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<235>");
				t_node=t_uncle2->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<237>");
				if(t_node==t_node->m_parent->m_left){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<238>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<239>");
					p_RotateRight3(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<241>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<242>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<243>");
				p_RotateLeft3(t_node->m_parent->m_parent);
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<247>");
	m_root->m_color=1;
	return 0;
}
bool c_Map3::p_Add2(int t_key,c_List4* t_value){
	DBG_ENTER("Map.Add")
	c_Map3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<61>");
	c_Node6* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<62>");
	c_Node6* t_parent=0;
	int t_cmp=0;
	DBG_LOCAL(t_parent,"parent")
	DBG_LOCAL(t_cmp,"cmp")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<64>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<65>");
		t_parent=t_node;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<66>");
		t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<67>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<68>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<69>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<70>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<72>");
				return false;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<76>");
	t_node=(new c_Node6)->m_new(t_key,t_value,-1,t_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<78>");
	if((t_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<79>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<80>");
			gc_assign(t_parent->m_right,t_node);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<82>");
			gc_assign(t_parent->m_left,t_node);
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<84>");
		p_InsertFixup3(t_node);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<86>");
		gc_assign(m_root,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<88>");
	return true;
}
void c_Map3::mark(){
	Object::mark();
	gc_mark_q(m_root);
}
String c_Map3::debug(){
	String t="(Map)\n";
	t+=dbg_decl("root",&m_root);
	return t;
}
c_IntMap3::c_IntMap3(){
}
c_IntMap3* c_IntMap3::m_new(){
	DBG_ENTER("IntMap.new")
	c_IntMap3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<534>");
	c_Map3::m_new();
	return this;
}
int c_IntMap3::p_Compare(int t_lhs,int t_rhs){
	DBG_ENTER("IntMap.Compare")
	c_IntMap3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<537>");
	int t_=t_lhs-t_rhs;
	return t_;
}
void c_IntMap3::mark(){
	c_Map3::mark();
}
String c_IntMap3::debug(){
	String t="(IntMap)\n";
	t=c_Map3::debug()+t;
	return t;
}
c_Node6::c_Node6(){
	m_key=0;
	m_right=0;
	m_left=0;
	m_value=0;
	m_color=0;
	m_parent=0;
}
c_Node6* c_Node6::m_new(int t_key,c_List4* t_value,int t_color,c_Node6* t_parent){
	DBG_ENTER("Node.new")
	c_Node6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_LOCAL(t_color,"color")
	DBG_LOCAL(t_parent,"parent")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<364>");
	this->m_key=t_key;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<365>");
	gc_assign(this->m_value,t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<366>");
	this->m_color=t_color;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<367>");
	gc_assign(this->m_parent,t_parent);
	return this;
}
c_Node6* c_Node6::m_new2(){
	DBG_ENTER("Node.new")
	c_Node6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<361>");
	return this;
}
void c_Node6::mark(){
	Object::mark();
	gc_mark_q(m_right);
	gc_mark_q(m_left);
	gc_mark_q(m_value);
	gc_mark_q(m_parent);
}
String c_Node6::debug(){
	String t="(Node)\n";
	t+=dbg_decl("key",&m_key);
	t+=dbg_decl("value",&m_value);
	t+=dbg_decl("color",&m_color);
	t+=dbg_decl("parent",&m_parent);
	t+=dbg_decl("left",&m_left);
	t+=dbg_decl("right",&m_right);
	return t;
}
c_Node7::c_Node7(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node7* c_Node7::m_new(c_Node7* t_succ,c_Node7* t_pred,c_CGlobalEventData* t_data){
	DBG_ENTER("Node.new")
	c_Node7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node7* c_Node7::m_new2(){
	DBG_ENTER("Node.new")
	c_Node7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
void c_Node7::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node7::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode4::c_HeadNode4(){
}
c_HeadNode4* c_HeadNode4::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node7::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode4::mark(){
	c_Node7::mark();
}
String c_HeadNode4::debug(){
	String t="(HeadNode)\n";
	t=c_Node7::debug()+t;
	return t;
}
c_CCommandBuffer::c_CCommandBuffer(){
	m_AvailableCommands=Array<c_CCommand* >(100);
	m_AvailableCommandCount=100;
	m_LastRenderItemID=-1;
	m_CommandQueue=(new c_Deque)->m_new();
}
c_CCommandBuffer* c_CCommandBuffer::m_new(){
	DBG_ENTER("CCommandBuffer.new")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<36>");
	for(int t_i=0;t_i<100;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<37>");
		gc_assign(this->m_AvailableCommands.At(t_i),(new c_CCommand)->m_new());
	}
	return this;
}
c_CCommand* c_CCommandBuffer::p_GetNewCommand(){
	DBG_ENTER("CCommandBuffer.GetNewCommand")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<48>");
	if(this->m_AvailableCommandCount!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<49>");
		c_CCommand* t_Command=this->m_AvailableCommands.At(this->m_AvailableCommandCount-1);
		DBG_LOCAL(t_Command,"Command")
		DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<50>");
		this->m_AvailableCommandCount-=1;
		DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<51>");
		return t_Command;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<53>");
		return 0;
	}
}
int c_CCommandBuffer::p_AddCommand_CreateRenderItem(int t_CommandType,Float t_Z){
	DBG_ENTER("CCommandBuffer.AddCommand_CreateRenderItem")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_CommandType,"CommandType")
	DBG_LOCAL(t_Z,"Z")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<78>");
	c_CCommand* t_Command=this->p_GetNewCommand();
	DBG_LOCAL(t_Command,"Command")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<80>");
	this->m_LastRenderItemID+=1;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<81>");
	t_Command->m_Type=t_CommandType;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<82>");
	t_Command->m_ID=this->m_LastRenderItemID;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<83>");
	t_Command->m_ParamFloatA=t_Z;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<85>");
	this->m_CommandQueue->p_PushLast(t_Command);
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<87>");
	return t_Command->m_ID;
}
int c_CCommandBuffer::p_AddCommand(int t_ID,int t_CommandType,Float t_ParamFloatA,Float t_ParamFloatB){
	DBG_ENTER("CCommandBuffer.AddCommand")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_LOCAL(t_CommandType,"CommandType")
	DBG_LOCAL(t_ParamFloatA,"ParamFloatA")
	DBG_LOCAL(t_ParamFloatB,"ParamFloatB")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<58>");
	c_CCommand* t_Command=p_GetNewCommand();
	DBG_LOCAL(t_Command,"Command")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<59>");
	t_Command->m_Type=t_CommandType;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<60>");
	t_Command->m_ID=t_ID;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<61>");
	t_Command->m_ParamFloatA=t_ParamFloatA;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<62>");
	t_Command->m_ParamFloatB=t_ParamFloatB;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<63>");
	this->m_CommandQueue->p_PushLast(t_Command);
	return 0;
}
int c_CCommandBuffer::p_AddCommand2(int t_ID,int t_CommandType,int t_ParamIntA,int t_ParamIntB,int t_ParamIntC){
	DBG_ENTER("CCommandBuffer.AddCommand")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_LOCAL(t_CommandType,"CommandType")
	DBG_LOCAL(t_ParamIntA,"ParamIntA")
	DBG_LOCAL(t_ParamIntB,"ParamIntB")
	DBG_LOCAL(t_ParamIntC,"ParamIntC")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<67>");
	c_CCommand* t_Command=p_GetNewCommand();
	DBG_LOCAL(t_Command,"Command")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<68>");
	t_Command->m_Type=t_CommandType;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<69>");
	t_Command->m_ID=t_ID;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<70>");
	t_Command->m_ParamIntA=t_ParamIntA;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<71>");
	t_Command->m_ParamIntB=t_ParamIntB;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<72>");
	t_Command->m_ParamIntC=t_ParamIntC;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<73>");
	this->m_CommandQueue->p_PushLast(t_Command);
	return 0;
}
bool c_CCommandBuffer::p_IsEmpty(){
	DBG_ENTER("CCommandBuffer.IsEmpty")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<95>");
	bool t_=this->m_CommandQueue->p_IsEmpty();
	return t_;
}
c_CCommand* c_CCommandBuffer::p_PopCommand(){
	DBG_ENTER("CCommandBuffer.PopCommand")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<91>");
	c_CCommand* t_=this->m_CommandQueue->p_PopFirst();
	return t_;
}
int c_CCommandBuffer::p_RecycleCommand(c_CCommand* t_Command){
	DBG_ENTER("CCommandBuffer.RecycleCommand")
	c_CCommandBuffer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Command,"Command")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<42>");
	gc_assign(this->m_AvailableCommands.At(this->m_AvailableCommandCount),t_Command);
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<43>");
	t_Command->m_Type=0;
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<44>");
	this->m_AvailableCommandCount+=1;
	return 0;
}
void c_CCommandBuffer::mark(){
	Object::mark();
	gc_mark_q(m_AvailableCommands);
	gc_mark_q(m_CommandQueue);
}
String c_CCommandBuffer::debug(){
	String t="(CCommandBuffer)\n";
	t+=dbg_decl("CommandQueue",&m_CommandQueue);
	t+=dbg_decl("AvailableCommands",&m_AvailableCommands);
	t+=dbg_decl("AvailableCommandCount",&m_AvailableCommandCount);
	t+=dbg_decl("LastRenderItemID",&m_LastRenderItemID);
	return t;
}
c_CCommand::c_CCommand(){
	m_Type=0;
	m_ID=-1;
	m_ParamFloatA=FLOAT(0.0);
	m_ParamFloatB=FLOAT(0.0);
	m_ParamIntA=0;
	m_ParamIntB=0;
	m_ParamIntC=0;
}
c_CCommand* c_CCommand::m_new(){
	DBG_ENTER("CCommand.new")
	c_CCommand *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/CommandBuffer.monkey<19>");
	return this;
}
void c_CCommand::mark(){
	Object::mark();
}
String c_CCommand::debug(){
	String t="(CCommand)\n";
	t+=dbg_decl("Type",&m_Type);
	t+=dbg_decl("ID",&m_ID);
	t+=dbg_decl("ParamFloatA",&m_ParamFloatA);
	t+=dbg_decl("ParamFloatB",&m_ParamFloatB);
	t+=dbg_decl("ParamIntA",&m_ParamIntA);
	t+=dbg_decl("ParamIntB",&m_ParamIntB);
	t+=dbg_decl("ParamIntC",&m_ParamIntC);
	return t;
}
c_Deque::c_Deque(){
	m__data=Array<c_CCommand* >(4);
	m__capacity=0;
	m__last=0;
	m__first=0;
}
c_Deque* c_Deque::m_new(){
	DBG_ENTER("Deque.new")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Deque* c_Deque::m_new2(Array<c_CCommand* > t_arr){
	DBG_ENTER("Deque.new")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_arr,"arr")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<8>");
	gc_assign(m__data,t_arr.Slice(0));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<9>");
	m__capacity=m__data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<10>");
	m__last=m__capacity;
	return this;
}
int c_Deque::p_Length2(){
	DBG_ENTER("Deque.Length")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<31>");
	if(m__last>=m__first){
		DBG_BLOCK();
		int t_=m__last-m__first;
		return t_;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<32>");
	int t_2=m__capacity-m__first+m__last;
	return t_2;
}
void c_Deque::p_Grow(){
	DBG_ENTER("Deque.Grow")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<135>");
	Array<c_CCommand* > t_data=Array<c_CCommand* >(m__capacity*2+10);
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<136>");
	if(m__first<=m__last){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<137>");
		for(int t_i=m__first;t_i<m__last;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<138>");
			gc_assign(t_data.At(t_i-m__first),m__data.At(t_i));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<140>");
		m__last-=m__first;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<141>");
		m__first=0;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<143>");
		int t_n=m__capacity-m__first;
		DBG_LOCAL(t_n,"n")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<144>");
		for(int t_i2=0;t_i2<t_n;t_i2=t_i2+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i2,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<145>");
			gc_assign(t_data.At(t_i2),m__data.At(m__first+t_i2));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<147>");
		for(int t_i3=0;t_i3<m__last;t_i3=t_i3+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i3,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<148>");
			gc_assign(t_data.At(t_n+t_i3),m__data.At(t_i3));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<150>");
		m__last+=t_n;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<151>");
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<153>");
	m__capacity=t_data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<154>");
	gc_assign(m__data,t_data);
}
void c_Deque::p_PushLast(c_CCommand* t_value){
	DBG_ENTER("Deque.PushLast")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<83>");
	if(p_Length2()+1>=m__capacity){
		DBG_BLOCK();
		p_Grow();
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<84>");
	gc_assign(m__data.At(m__last),t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<85>");
	m__last+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<86>");
	if(m__last==m__capacity){
		DBG_BLOCK();
		m__last=0;
	}
}
bool c_Deque::p_IsEmpty(){
	DBG_ENTER("Deque.IsEmpty")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<36>");
	bool t_=m__first==m__last;
	return t_;
}
c_CCommand* c_Deque::m_NIL;
c_CCommand* c_Deque::p_PopFirst(){
	DBG_ENTER("Deque.PopFirst")
	c_Deque *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<91>");
	if(p_IsEmpty()){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on empty deque",32));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<93>");
	c_CCommand* t_v=m__data.At(m__first);
	DBG_LOCAL(t_v,"v")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<94>");
	gc_assign(m__data.At(m__first),m_NIL);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<95>");
	m__first+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<96>");
	if(m__first==m__capacity){
		DBG_BLOCK();
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<97>");
	return t_v;
}
void c_Deque::mark(){
	Object::mark();
	gc_mark_q(m__data);
}
String c_Deque::debug(){
	String t="(Deque)\n";
	t+=dbg_decl("NIL",&c_Deque::m_NIL);
	t+=dbg_decl("_data",&m__data);
	t+=dbg_decl("_capacity",&m__capacity);
	t+=dbg_decl("_first",&m__first);
	t+=dbg_decl("_last",&m__last);
	return t;
}
int bb_input_KeyDown(int t_key){
	DBG_ENTER("KeyDown")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/input.monkey<36>");
	int t_=((bb_input_device->p_KeyDown(t_key))?1:0);
	return t_;
}
c_Enumerator3::c_Enumerator3(){
	m__list=0;
	m__curr=0;
}
c_Enumerator3* c_Enumerator3::m_new(c_List3* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator3* c_Enumerator3::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator3::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CGameObject* c_Enumerator3::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CGameObject* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator3::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator3::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
c_CPath::c_CPath(){
	m_StartPoint=(new c_IVector)->m_new(0,0);
	m_EndPoint=(new c_IVector)->m_new(0,0);
	m_NodeList=(new c_List6)->m_new();
	m_PathStatus=2;
}
c_CPath* c_CPath::m_new(){
	DBG_ENTER("CPath.new")
	c_CPath *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<12>");
	return this;
}
int c_CPath::p_Request(c_IVector* t_StartPoint,c_IVector* t_EndPoint){
	DBG_ENTER("CPath.Request")
	c_CPath *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_StartPoint,"StartPoint")
	DBG_LOCAL(t_EndPoint,"EndPoint")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<19>");
	this->m_PathStatus=0;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<20>");
	this->m_StartPoint->m_X=t_StartPoint->m_X;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<21>");
	this->m_StartPoint->m_Y=t_StartPoint->m_Y;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<22>");
	this->m_EndPoint->m_X=t_EndPoint->m_X;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<23>");
	this->m_EndPoint->m_Y=t_EndPoint->m_Y;
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<25>");
	c_CAIManager::m_AddRequest(this);
	return 0;
}
void c_CPath::mark(){
	Object::mark();
	gc_mark_q(m_StartPoint);
	gc_mark_q(m_EndPoint);
	gc_mark_q(m_NodeList);
}
String c_CPath::debug(){
	String t="(CPath)\n";
	t+=dbg_decl("PathStatus",&m_PathStatus);
	t+=dbg_decl("StartPoint",&m_StartPoint);
	t+=dbg_decl("EndPoint",&m_EndPoint);
	t+=dbg_decl("NodeList",&m_NodeList);
	return t;
}
c_Deque2::c_Deque2(){
	m__data=Array<c_CPath* >(4);
	m__capacity=0;
	m__last=0;
	m__first=0;
}
c_Deque2* c_Deque2::m_new(){
	DBG_ENTER("Deque.new")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Deque2* c_Deque2::m_new2(Array<c_CPath* > t_arr){
	DBG_ENTER("Deque.new")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_arr,"arr")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<8>");
	gc_assign(m__data,t_arr.Slice(0));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<9>");
	m__capacity=m__data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<10>");
	m__last=m__capacity;
	return this;
}
int c_Deque2::p_Length2(){
	DBG_ENTER("Deque.Length")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<31>");
	if(m__last>=m__first){
		DBG_BLOCK();
		int t_=m__last-m__first;
		return t_;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<32>");
	int t_2=m__capacity-m__first+m__last;
	return t_2;
}
bool c_Deque2::p_IsEmpty(){
	DBG_ENTER("Deque.IsEmpty")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<36>");
	bool t_=m__first==m__last;
	return t_;
}
c_CPath* c_Deque2::m_NIL;
c_CPath* c_Deque2::p_PopFirst(){
	DBG_ENTER("Deque.PopFirst")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<91>");
	if(p_IsEmpty()){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on empty deque",32));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<93>");
	c_CPath* t_v=m__data.At(m__first);
	DBG_LOCAL(t_v,"v")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<94>");
	gc_assign(m__data.At(m__first),m_NIL);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<95>");
	m__first+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<96>");
	if(m__first==m__capacity){
		DBG_BLOCK();
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<97>");
	return t_v;
}
void c_Deque2::p_Grow(){
	DBG_ENTER("Deque.Grow")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<135>");
	Array<c_CPath* > t_data=Array<c_CPath* >(m__capacity*2+10);
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<136>");
	if(m__first<=m__last){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<137>");
		for(int t_i=m__first;t_i<m__last;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<138>");
			gc_assign(t_data.At(t_i-m__first),m__data.At(t_i));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<140>");
		m__last-=m__first;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<141>");
		m__first=0;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<143>");
		int t_n=m__capacity-m__first;
		DBG_LOCAL(t_n,"n")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<144>");
		for(int t_i2=0;t_i2<t_n;t_i2=t_i2+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i2,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<145>");
			gc_assign(t_data.At(t_i2),m__data.At(m__first+t_i2));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<147>");
		for(int t_i3=0;t_i3<m__last;t_i3=t_i3+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i3,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<148>");
			gc_assign(t_data.At(t_n+t_i3),m__data.At(t_i3));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<150>");
		m__last+=t_n;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<151>");
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<153>");
	m__capacity=t_data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<154>");
	gc_assign(m__data,t_data);
}
void c_Deque2::p_PushLast2(c_CPath* t_value){
	DBG_ENTER("Deque.PushLast")
	c_Deque2 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<83>");
	if(p_Length2()+1>=m__capacity){
		DBG_BLOCK();
		p_Grow();
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<84>");
	gc_assign(m__data.At(m__last),t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<85>");
	m__last+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<86>");
	if(m__last==m__capacity){
		DBG_BLOCK();
		m__last=0;
	}
}
void c_Deque2::mark(){
	Object::mark();
	gc_mark_q(m__data);
}
String c_Deque2::debug(){
	String t="(Deque)\n";
	t+=dbg_decl("NIL",&c_Deque2::m_NIL);
	t+=dbg_decl("_data",&m__data);
	t+=dbg_decl("_capacity",&m__capacity);
	t+=dbg_decl("_first",&m__first);
	t+=dbg_decl("_last",&m__last);
	return t;
}
c_List5::c_List5(){
	m__head=((new c_HeadNode5)->m_new());
}
c_List5* c_List5::m_new(){
	DBG_ENTER("List.new")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node8* c_List5::p_AddLast5(c_COpenListItem* t_data){
	DBG_ENTER("List.AddLast")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node8* t_=(new c_Node8)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List5* c_List5::m_new2(Array<c_COpenListItem* > t_data){
	DBG_ENTER("List.new")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_COpenListItem* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_COpenListItem* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast5(t_t);
	}
	return this;
}
int c_List5::p_Clear(){
	DBG_ENTER("List.Clear")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<36>");
	gc_assign(m__head->m__succ,m__head);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<37>");
	gc_assign(m__head->m__pred,m__head);
	return 0;
}
bool c_List5::p_IsEmpty(){
	DBG_ENTER("List.IsEmpty")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<50>");
	bool t_=m__head->m__succ==m__head;
	return t_;
}
int c_List5::p_Compare2(c_COpenListItem* t_lhs,c_COpenListItem* t_rhs){
	DBG_ENTER("List.Compare")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<32>");
	bbError(String(L"Unable to compare items",23));
	return 0;
}
int c_List5::p_Sort(int t_ascending){
	DBG_ENTER("List.Sort")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ascending,"ascending")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<194>");
	int t_ccsgn=-1;
	DBG_LOCAL(t_ccsgn,"ccsgn")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<195>");
	if((t_ascending)!=0){
		DBG_BLOCK();
		t_ccsgn=1;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<196>");
	int t_insize=1;
	DBG_LOCAL(t_insize,"insize")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<198>");
	do{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<199>");
		int t_merges=0;
		DBG_LOCAL(t_merges,"merges")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<200>");
		c_Node8* t_tail=m__head;
		DBG_LOCAL(t_tail,"tail")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<201>");
		c_Node8* t_p=m__head->m__succ;
		DBG_LOCAL(t_p,"p")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<203>");
		while(t_p!=m__head){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<204>");
			t_merges+=1;
			DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<205>");
			c_Node8* t_q=t_p->m__succ;
			int t_qsize=t_insize;
			int t_psize=1;
			DBG_LOCAL(t_q,"q")
			DBG_LOCAL(t_qsize,"qsize")
			DBG_LOCAL(t_psize,"psize")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<207>");
			while(t_psize<t_insize && t_q!=m__head){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<208>");
				t_psize+=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<209>");
				t_q=t_q->m__succ;
			}
			DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<212>");
			do{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<213>");
				c_Node8* t_t=0;
				DBG_LOCAL(t_t,"t")
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<214>");
				if(((t_psize)!=0) && ((t_qsize)!=0) && t_q!=m__head){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<215>");
					int t_cc=p_Compare2(t_p->m__data,t_q->m__data)*t_ccsgn;
					DBG_LOCAL(t_cc,"cc")
					DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<216>");
					if(t_cc<=0){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<217>");
						t_t=t_p;
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<218>");
						t_p=t_p->m__succ;
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<219>");
						t_psize-=1;
					}else{
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<221>");
						t_t=t_q;
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<222>");
						t_q=t_q->m__succ;
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<223>");
						t_qsize-=1;
					}
				}else{
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<225>");
					if((t_psize)!=0){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<226>");
						t_t=t_p;
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<227>");
						t_p=t_p->m__succ;
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<228>");
						t_psize-=1;
					}else{
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<229>");
						if(((t_qsize)!=0) && t_q!=m__head){
							DBG_BLOCK();
							DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<230>");
							t_t=t_q;
							DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<231>");
							t_q=t_q->m__succ;
							DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<232>");
							t_qsize-=1;
						}else{
							DBG_BLOCK();
							DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<234>");
							break;
						}
					}
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<236>");
				gc_assign(t_t->m__pred,t_tail);
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<237>");
				gc_assign(t_tail->m__succ,t_t);
				DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<238>");
				t_tail=t_t;
			}while(!(false));
			DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<240>");
			t_p=t_q;
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<242>");
		gc_assign(t_tail->m__succ,m__head);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<243>");
		gc_assign(m__head->m__pred,t_tail);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<245>");
		if(t_merges<=1){
			DBG_BLOCK();
			return 0;
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<247>");
		t_insize*=2;
	}while(!(false));
}
c_COpenListItem* c_List5::p_First(){
	DBG_ENTER("List.First")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<73>");
	if(p_IsEmpty()){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on empty list",31));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<75>");
	return m__head->m__succ->m__data;
}
c_COpenListItem* c_List5::p_RemoveFirst(){
	DBG_ENTER("List.RemoveFirst")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<87>");
	if(p_IsEmpty()){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on empty list",31));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<89>");
	c_COpenListItem* t_data=m__head->m__succ->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<90>");
	m__head->m__succ->p_Remove();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<91>");
	return t_data;
}
bool c_List5::p_Equals(c_COpenListItem* t_lhs,c_COpenListItem* t_rhs){
	DBG_ENTER("List.Equals")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<28>");
	bool t_=t_lhs==t_rhs;
	return t_;
}
c_Node8* c_List5::p_Find(c_COpenListItem* t_value,c_Node8* t_start){
	DBG_ENTER("List.Find")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_LOCAL(t_start,"start")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<116>");
	while(t_start!=m__head){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<117>");
		if(p_Equals(t_value,t_start->m__data)){
			DBG_BLOCK();
			return t_start;
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<118>");
		t_start=t_start->m__succ;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<120>");
	return 0;
}
c_Node8* c_List5::p_Find2(c_COpenListItem* t_value){
	DBG_ENTER("List.Find")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<112>");
	c_Node8* t_=p_Find(t_value,m__head->m__succ);
	return t_;
}
void c_List5::p_RemoveFirst2(c_COpenListItem* t_value){
	DBG_ENTER("List.RemoveFirst")
	c_List5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<141>");
	c_Node8* t_node=p_Find2(t_value);
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<142>");
	if((t_node)!=0){
		DBG_BLOCK();
		t_node->p_Remove();
	}
}
void c_List5::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List5::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_COpenListItemList::c_COpenListItemList(){
}
c_COpenListItemList* c_COpenListItemList::m_new(){
	DBG_ENTER("COpenListItemList.new")
	c_COpenListItemList *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<39>");
	c_List5::m_new();
	return this;
}
int c_COpenListItemList::p_Compare2(c_COpenListItem* t_a,c_COpenListItem* t_b){
	DBG_ENTER("COpenListItemList.Compare")
	c_COpenListItemList *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_a,"a")
	DBG_LOCAL(t_b,"b")
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<41>");
	if(t_a->m_Estimate>t_b->m_Estimate){
		DBG_BLOCK();
		return 1;
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<42>");
	if(t_a->m_Estimate==t_b->m_Estimate){
		DBG_BLOCK();
		return 0;
	}
	DBG_INFO("C:/MonkeyX77a/AIManager.monkey<43>");
	return -1;
}
void c_COpenListItemList::mark(){
	c_List5::mark();
}
String c_COpenListItemList::debug(){
	String t="(COpenListItemList)\n";
	t=c_List5::debug()+t;
	return t;
}
c_Node8::c_Node8(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node8* c_Node8::m_new(c_Node8* t_succ,c_Node8* t_pred,c_COpenListItem* t_data){
	DBG_ENTER("Node.new")
	c_Node8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node8* c_Node8::m_new2(){
	DBG_ENTER("Node.new")
	c_Node8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
int c_Node8::p_Remove(){
	DBG_ENTER("Node.Remove")
	c_Node8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<274>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<276>");
	gc_assign(m__succ->m__pred,m__pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<277>");
	gc_assign(m__pred->m__succ,m__succ);
	return 0;
}
void c_Node8::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node8::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode5::c_HeadNode5(){
}
c_HeadNode5* c_HeadNode5::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node8::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode5::mark(){
	c_Node8::mark();
}
String c_HeadNode5::debug(){
	String t="(HeadNode)\n";
	t=c_Node8::debug()+t;
	return t;
}
c_List6::c_List6(){
	m__head=((new c_HeadNode6)->m_new());
}
c_List6* c_List6::m_new(){
	DBG_ENTER("List.new")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node9* c_List6::p_AddLast6(c_IVector* t_data){
	DBG_ENTER("List.AddLast")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node9* t_=(new c_Node9)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List6* c_List6::m_new2(Array<c_IVector* > t_data){
	DBG_ENTER("List.new")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_IVector* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_IVector* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast6(t_t);
	}
	return this;
}
int c_List6::p_Clear(){
	DBG_ENTER("List.Clear")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<36>");
	gc_assign(m__head->m__succ,m__head);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<37>");
	gc_assign(m__head->m__pred,m__head);
	return 0;
}
c_Node9* c_List6::p_AddFirst(c_IVector* t_data){
	DBG_ENTER("List.AddFirst")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<104>");
	c_Node9* t_=(new c_Node9)->m_new(m__head->m__succ,m__head,t_data);
	return t_;
}
c_Node9* c_List6::p_FirstNode(){
	DBG_ENTER("List.FirstNode")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<62>");
	if(m__head->m__succ!=m__head){
		DBG_BLOCK();
		return m__head->m__succ;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<63>");
	return 0;
}
c_Node9* c_List6::p_LastNode(){
	DBG_ENTER("List.LastNode")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<67>");
	if(m__head->m__pred!=m__head){
		DBG_BLOCK();
		return m__head->m__pred;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<68>");
	return 0;
}
c_Enumerator11* c_List6::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator11* t_=(new c_Enumerator11)->m_new(this);
	return t_;
}
void c_List6::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List6::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node9::c_Node9(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node9* c_Node9::m_new(c_Node9* t_succ,c_Node9* t_pred,c_IVector* t_data){
	DBG_ENTER("Node.new")
	c_Node9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node9* c_Node9::m_new2(){
	DBG_ENTER("Node.new")
	c_Node9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
c_IVector* c_Node9::p_Value(){
	DBG_ENTER("Node.Value")
	c_Node9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<269>");
	return m__data;
}
c_Node9* c_Node9::p_GetNode(){
	DBG_ENTER("Node.GetNode")
	c_Node9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<301>");
	return this;
}
c_Node9* c_Node9::p_NextNode(){
	DBG_ENTER("Node.NextNode")
	c_Node9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<282>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<284>");
	c_Node9* t_=m__succ->p_GetNode();
	return t_;
}
c_Node9* c_Node9::p_PrevNode(){
	DBG_ENTER("Node.PrevNode")
	c_Node9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<289>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<291>");
	c_Node9* t_=m__pred->p_GetNode();
	return t_;
}
void c_Node9::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node9::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode6::c_HeadNode6(){
}
c_HeadNode6* c_HeadNode6::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node9::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
c_Node9* c_HeadNode6::p_GetNode(){
	DBG_ENTER("HeadNode.GetNode")
	c_HeadNode6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<316>");
	return 0;
}
void c_HeadNode6::mark(){
	c_Node9::mark();
}
String c_HeadNode6::debug(){
	String t="(HeadNode)\n";
	t=c_Node9::debug()+t;
	return t;
}
c_CSpawnRequestData::c_CSpawnRequestData(){
	m_TemplateID=0;
	m_Position=0;
}
c_CSpawnRequestData* c_CSpawnRequestData::m_new(int t_TemplateID,c_FVector* t_Position){
	DBG_ENTER("CSpawnRequestData.new")
	c_CSpawnRequestData *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_TemplateID,"TemplateID")
	DBG_LOCAL(t_Position,"Position")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<14>");
	this->m_TemplateID=t_TemplateID;
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<15>");
	gc_assign(this->m_Position,t_Position);
	return this;
}
c_CSpawnRequestData* c_CSpawnRequestData::m_new2(){
	DBG_ENTER("CSpawnRequestData.new")
	c_CSpawnRequestData *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/SpawnManager.monkey<9>");
	return this;
}
void c_CSpawnRequestData::mark(){
	Object::mark();
	gc_mark_q(m_Position);
}
String c_CSpawnRequestData::debug(){
	String t="(CSpawnRequestData)\n";
	t+=dbg_decl("TemplateID",&m_TemplateID);
	t+=dbg_decl("Position",&m_Position);
	return t;
}
c_Deque3::c_Deque3(){
	m__data=Array<c_CSpawnRequestData* >(4);
	m__capacity=0;
	m__last=0;
	m__first=0;
}
c_Deque3* c_Deque3::m_new(){
	DBG_ENTER("Deque.new")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Deque3* c_Deque3::m_new2(Array<c_CSpawnRequestData* > t_arr){
	DBG_ENTER("Deque.new")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_arr,"arr")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<8>");
	gc_assign(m__data,t_arr.Slice(0));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<9>");
	m__capacity=m__data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<10>");
	m__last=m__capacity;
	return this;
}
bool c_Deque3::p_IsEmpty(){
	DBG_ENTER("Deque.IsEmpty")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<36>");
	bool t_=m__first==m__last;
	return t_;
}
c_CSpawnRequestData* c_Deque3::m_NIL;
c_CSpawnRequestData* c_Deque3::p_PopFirst(){
	DBG_ENTER("Deque.PopFirst")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<91>");
	if(p_IsEmpty()){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on empty deque",32));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<93>");
	c_CSpawnRequestData* t_v=m__data.At(m__first);
	DBG_LOCAL(t_v,"v")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<94>");
	gc_assign(m__data.At(m__first),m_NIL);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<95>");
	m__first+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<96>");
	if(m__first==m__capacity){
		DBG_BLOCK();
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<97>");
	return t_v;
}
int c_Deque3::p_Length2(){
	DBG_ENTER("Deque.Length")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<31>");
	if(m__last>=m__first){
		DBG_BLOCK();
		int t_=m__last-m__first;
		return t_;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<32>");
	int t_2=m__capacity-m__first+m__last;
	return t_2;
}
void c_Deque3::p_Grow(){
	DBG_ENTER("Deque.Grow")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<135>");
	Array<c_CSpawnRequestData* > t_data=Array<c_CSpawnRequestData* >(m__capacity*2+10);
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<136>");
	if(m__first<=m__last){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<137>");
		for(int t_i=m__first;t_i<m__last;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<138>");
			gc_assign(t_data.At(t_i-m__first),m__data.At(t_i));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<140>");
		m__last-=m__first;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<141>");
		m__first=0;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<143>");
		int t_n=m__capacity-m__first;
		DBG_LOCAL(t_n,"n")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<144>");
		for(int t_i2=0;t_i2<t_n;t_i2=t_i2+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i2,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<145>");
			gc_assign(t_data.At(t_i2),m__data.At(m__first+t_i2));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<147>");
		for(int t_i3=0;t_i3<m__last;t_i3=t_i3+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i3,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<148>");
			gc_assign(t_data.At(t_n+t_i3),m__data.At(t_i3));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<150>");
		m__last+=t_n;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<151>");
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<153>");
	m__capacity=t_data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<154>");
	gc_assign(m__data,t_data);
}
void c_Deque3::p_PushLast3(c_CSpawnRequestData* t_value){
	DBG_ENTER("Deque.PushLast")
	c_Deque3 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<83>");
	if(p_Length2()+1>=m__capacity){
		DBG_BLOCK();
		p_Grow();
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<84>");
	gc_assign(m__data.At(m__last),t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<85>");
	m__last+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<86>");
	if(m__last==m__capacity){
		DBG_BLOCK();
		m__last=0;
	}
}
void c_Deque3::mark(){
	Object::mark();
	gc_mark_q(m__data);
}
String c_Deque3::debug(){
	String t="(Deque)\n";
	t+=dbg_decl("NIL",&c_Deque3::m_NIL);
	t+=dbg_decl("_data",&m__data);
	t+=dbg_decl("_capacity",&m__capacity);
	t+=dbg_decl("_first",&m__first);
	t+=dbg_decl("_last",&m__last);
	return t;
}
c_CAnimationGraph::c_CAnimationGraph(){
	m_SpriteID=-1;
	m_AnimationSet=0;
	m_VariableMap=(new c_IntMap4)->m_new();
	m_DefaultState=0;
	m_CurrentState=0;
}
int c_CAnimationGraph::p_AddVariable(int t_VariableID,c_CAnimationVariable* t_Variable){
	DBG_ENTER("CAnimationGraph.AddVariable")
	c_CAnimationGraph *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_VariableID,"VariableID")
	DBG_LOCAL(t_Variable,"Variable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<133>");
	this->m_VariableMap->p_Add3(t_VariableID,t_Variable);
	return 0;
}
c_CAnimationGraph* c_CAnimationGraph::m_new(int t_SpriteID,c_IntMap* t_AnimationSet){
	DBG_ENTER("CAnimationGraph.new")
	c_CAnimationGraph *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_LOCAL(t_AnimationSet,"AnimationSet")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<28>");
	c_CComponent::m_new(5);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<29>");
	this->m_SpriteID=t_SpriteID;
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<30>");
	gc_assign(this->m_AnimationSet,t_AnimationSet);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<32>");
	c_CAnimationVariable* t_AttackVariable=(new c_CAnimationVariable)->m_new(0);
	DBG_LOCAL(t_AttackVariable,"AttackVariable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<33>");
	p_AddVariable(0,t_AttackVariable);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<34>");
	c_CAnimationVariable* t_EnemyAngleVariable=(new c_CAnimationVariable)->m_new(1);
	DBG_LOCAL(t_EnemyAngleVariable,"EnemyAngleVariable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<35>");
	p_AddVariable(1,t_EnemyAngleVariable);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<36>");
	c_CAnimationVariable* t_UnitAngleVariable=(new c_CAnimationVariable)->m_new(2);
	DBG_LOCAL(t_UnitAngleVariable,"UnitAngleVariable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<37>");
	p_AddVariable(2,t_UnitAngleVariable);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<38>");
	c_CAnimationVariable* t_MoveVariable=(new c_CAnimationVariable)->m_new(3);
	DBG_LOCAL(t_MoveVariable,"MoveVariable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<39>");
	p_AddVariable(3,t_MoveVariable);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<42>");
	Float t_Slice=FLOAT(22.5);
	DBG_LOCAL(t_Slice,"Slice")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<45>");
	c_CAnimationState* t_AttackState=(new c_CAnimationState)->m_new(0);
	DBG_LOCAL(t_AttackState,"AttackState")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<46>");
	c_CPlayAnimationNode* t_AttackRightNode=(new c_CPlayAnimationNode)->m_new(6);
	DBG_LOCAL(t_AttackRightNode,"AttackRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<47>");
	c_CPlayAnimationNode* t_AttackUpNode=(new c_CPlayAnimationNode)->m_new(4);
	DBG_LOCAL(t_AttackUpNode,"AttackUpNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<48>");
	c_CPlayAnimationNode* t_AttackLeftNode=(new c_CPlayAnimationNode)->m_new(7);
	DBG_LOCAL(t_AttackLeftNode,"AttackLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<49>");
	c_CPlayAnimationNode* t_AttackDownNode=(new c_CPlayAnimationNode)->m_new(5);
	DBG_LOCAL(t_AttackDownNode,"AttackDownNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<50>");
	c_CPlayAnimationNode* t_AttackUpRightNode=(new c_CPlayAnimationNode)->m_new(13);
	DBG_LOCAL(t_AttackUpRightNode,"AttackUpRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<51>");
	c_CPlayAnimationNode* t_AttackUpLeftNode=(new c_CPlayAnimationNode)->m_new(14);
	DBG_LOCAL(t_AttackUpLeftNode,"AttackUpLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<52>");
	c_CPlayAnimationNode* t_AttackDownRightNode=(new c_CPlayAnimationNode)->m_new(15);
	DBG_LOCAL(t_AttackDownRightNode,"AttackDownRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<53>");
	c_CPlayAnimationNode* t_AttackDownLeftNode=(new c_CPlayAnimationNode)->m_new(16);
	DBG_LOCAL(t_AttackDownLeftNode,"AttackDownLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<55>");
	c_CFloatMultiRangeCondition* t_AttackMultiRangeCondition=(new c_CFloatMultiRangeCondition)->m_new(t_EnemyAngleVariable);
	DBG_LOCAL(t_AttackMultiRangeCondition,"AttackMultiRangeCondition")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<56>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-8.0)*t_Slice,FLOAT(-7.0)*t_Slice,(t_AttackLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<57>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-7.0)*t_Slice,FLOAT(-5.0)*t_Slice,(t_AttackUpLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<58>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-5.0)*t_Slice,FLOAT(-3.0)*t_Slice,(t_AttackUpNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<59>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-3.0)*t_Slice,FLOAT(-1.0)*t_Slice,(t_AttackUpRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<60>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-1.0)*t_Slice,FLOAT(1.0)*t_Slice,(t_AttackRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<61>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(1.0)*t_Slice,FLOAT(3.0)*t_Slice,(t_AttackDownRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<62>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(3.0)*t_Slice,FLOAT(5.0)*t_Slice,(t_AttackDownNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<63>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(5.0)*t_Slice,FLOAT(7.0)*t_Slice,(t_AttackDownLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<64>");
	t_AttackMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(7.0)*t_Slice,FLOAT(8.0)*t_Slice,(t_AttackLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<66>");
	gc_assign(t_AttackState->m_EntryPoint,(t_AttackMultiRangeCondition));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<69>");
	c_CAnimationState* t_IdleState=(new c_CAnimationState)->m_new(1);
	DBG_LOCAL(t_IdleState,"IdleState")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<70>");
	c_CPlayAnimationNode* t_IdleRightNode=(new c_CPlayAnimationNode)->m_new(17);
	DBG_LOCAL(t_IdleRightNode,"IdleRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<71>");
	c_CPlayAnimationNode* t_IdleUpNode=(new c_CPlayAnimationNode)->m_new(18);
	DBG_LOCAL(t_IdleUpNode,"IdleUpNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<72>");
	c_CPlayAnimationNode* t_IdleLeftNode=(new c_CPlayAnimationNode)->m_new(19);
	DBG_LOCAL(t_IdleLeftNode,"IdleLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<73>");
	c_CPlayAnimationNode* t_IdleDownNode=(new c_CPlayAnimationNode)->m_new(20);
	DBG_LOCAL(t_IdleDownNode,"IdleDownNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<74>");
	c_CPlayAnimationNode* t_IdleUpRightNode=(new c_CPlayAnimationNode)->m_new(21);
	DBG_LOCAL(t_IdleUpRightNode,"IdleUpRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<75>");
	c_CPlayAnimationNode* t_IdleUpLeftNode=(new c_CPlayAnimationNode)->m_new(22);
	DBG_LOCAL(t_IdleUpLeftNode,"IdleUpLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<76>");
	c_CPlayAnimationNode* t_IdleDownRightNode=(new c_CPlayAnimationNode)->m_new(24);
	DBG_LOCAL(t_IdleDownRightNode,"IdleDownRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<77>");
	c_CPlayAnimationNode* t_IdleDownLeftNode=(new c_CPlayAnimationNode)->m_new(23);
	DBG_LOCAL(t_IdleDownLeftNode,"IdleDownLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<79>");
	c_CFloatMultiRangeCondition* t_IdleMultiRangeCondition=(new c_CFloatMultiRangeCondition)->m_new(t_UnitAngleVariable);
	DBG_LOCAL(t_IdleMultiRangeCondition,"IdleMultiRangeCondition")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<80>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-8.0)*t_Slice,FLOAT(-7.0)*t_Slice,(t_IdleLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<81>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-7.0)*t_Slice,FLOAT(-5.0)*t_Slice,(t_IdleUpLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<82>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-5.0)*t_Slice,FLOAT(-3.0)*t_Slice,(t_IdleUpNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<83>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-3.0)*t_Slice,FLOAT(-1.0)*t_Slice,(t_IdleUpRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<84>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-1.0)*t_Slice,FLOAT(1.0)*t_Slice,(t_IdleRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<85>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(1.0)*t_Slice,FLOAT(3.0)*t_Slice,(t_IdleDownRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<86>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(3.0)*t_Slice,FLOAT(5.0)*t_Slice,(t_IdleDownNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<87>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(5.0)*t_Slice,FLOAT(7.0)*t_Slice,(t_IdleDownLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<88>");
	t_IdleMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(7.0)*t_Slice,FLOAT(8.0)*t_Slice,(t_IdleLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<90>");
	gc_assign(t_IdleState->m_EntryPoint,(t_IdleMultiRangeCondition));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<93>");
	c_CAnimationState* t_MoveState=(new c_CAnimationState)->m_new(2);
	DBG_LOCAL(t_MoveState,"MoveState")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<94>");
	c_CPlayAnimationNode* t_MoveRightNode=(new c_CPlayAnimationNode)->m_new(2);
	DBG_LOCAL(t_MoveRightNode,"MoveRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<95>");
	c_CPlayAnimationNode* t_MoveUpNode=(new c_CPlayAnimationNode)->m_new(0);
	DBG_LOCAL(t_MoveUpNode,"MoveUpNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<96>");
	c_CPlayAnimationNode* t_MoveLeftNode=(new c_CPlayAnimationNode)->m_new(3);
	DBG_LOCAL(t_MoveLeftNode,"MoveLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<97>");
	c_CPlayAnimationNode* t_MoveDownNode=(new c_CPlayAnimationNode)->m_new(1);
	DBG_LOCAL(t_MoveDownNode,"MoveDownNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<98>");
	c_CPlayAnimationNode* t_MoveUpRightNode=(new c_CPlayAnimationNode)->m_new(9);
	DBG_LOCAL(t_MoveUpRightNode,"MoveUpRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<99>");
	c_CPlayAnimationNode* t_MoveUpLeftNode=(new c_CPlayAnimationNode)->m_new(10);
	DBG_LOCAL(t_MoveUpLeftNode,"MoveUpLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<100>");
	c_CPlayAnimationNode* t_MoveDownRightNode=(new c_CPlayAnimationNode)->m_new(11);
	DBG_LOCAL(t_MoveDownRightNode,"MoveDownRightNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<101>");
	c_CPlayAnimationNode* t_MoveDownLeftNode=(new c_CPlayAnimationNode)->m_new(12);
	DBG_LOCAL(t_MoveDownLeftNode,"MoveDownLeftNode")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<103>");
	c_CFloatMultiRangeCondition* t_MoveMultiRangeCondition=(new c_CFloatMultiRangeCondition)->m_new(t_UnitAngleVariable);
	DBG_LOCAL(t_MoveMultiRangeCondition,"MoveMultiRangeCondition")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<104>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-8.0)*t_Slice,FLOAT(-7.0)*t_Slice,(t_MoveLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<105>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-7.0)*t_Slice,FLOAT(-5.0)*t_Slice,(t_MoveUpLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<106>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-5.0)*t_Slice,FLOAT(-3.0)*t_Slice,(t_MoveUpNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<107>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-3.0)*t_Slice,FLOAT(-1.0)*t_Slice,(t_MoveUpRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<108>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(-1.0)*t_Slice,FLOAT(1.0)*t_Slice,(t_MoveRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<109>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(1.0)*t_Slice,FLOAT(3.0)*t_Slice,(t_MoveDownRightNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<110>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(3.0)*t_Slice,FLOAT(5.0)*t_Slice,(t_MoveDownNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<111>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(5.0)*t_Slice,FLOAT(7.0)*t_Slice,(t_MoveDownLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<112>");
	t_MoveMultiRangeCondition->p_AddMiniCondition((new c_CFloatRangeMiniCondition)->m_new(FLOAT(7.0)*t_Slice,FLOAT(8.0)*t_Slice,(t_MoveLeftNode)));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<114>");
	gc_assign(t_MoveState->m_EntryPoint,(t_MoveMultiRangeCondition));
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<116>");
	c_CStateBoolTransition* t_TransitionIdleCombat=(new c_CStateBoolTransition)->m_new(t_AttackVariable,t_AttackState,false);
	DBG_LOCAL(t_TransitionIdleCombat,"TransitionIdleCombat")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<117>");
	t_IdleState->p_AddTransition(t_TransitionIdleCombat);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<119>");
	c_CStateBoolTransition* t_TransitionCombatIdle=(new c_CStateBoolTransition)->m_new(t_AttackVariable,t_IdleState,true);
	DBG_LOCAL(t_TransitionCombatIdle,"TransitionCombatIdle")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<120>");
	t_AttackState->p_AddTransition(t_TransitionCombatIdle);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<122>");
	c_CStateBoolTransition* t_TransitionIdleMove=(new c_CStateBoolTransition)->m_new(t_MoveVariable,t_MoveState,false);
	DBG_LOCAL(t_TransitionIdleMove,"TransitionIdleMove")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<123>");
	t_IdleState->p_AddTransition(t_TransitionIdleMove);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<125>");
	c_CStateBoolTransition* t_TransitionMoveIdle=(new c_CStateBoolTransition)->m_new(t_MoveVariable,t_IdleState,true);
	DBG_LOCAL(t_TransitionMoveIdle,"TransitionMoveIdle")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<126>");
	t_MoveState->p_AddTransition(t_TransitionMoveIdle);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<128>");
	gc_assign(this->m_DefaultState,t_IdleState);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<129>");
	gc_assign(this->m_CurrentState,this->m_DefaultState);
	return this;
}
c_CAnimationGraph* c_CAnimationGraph::m_new2(){
	DBG_ENTER("CAnimationGraph.new")
	c_CAnimationGraph *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<19>");
	c_CComponent::m_new2();
	return this;
}
int c_CAnimationGraph::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CAnimationGraph.Update")
	c_CAnimationGraph *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<158>");
	c_Enumerator9* t_=this->m_CurrentState->m_TransitionArray->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CStateTransition* t_StateTransition=t_->p_NextObject();
		DBG_LOCAL(t_StateTransition,"StateTransition")
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<159>");
		if(t_StateTransition->p_Assess()){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<160>");
			if((this->m_CurrentState)!=0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<161>");
				this->m_CurrentState->p_SignalDead();
			}
			DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<163>");
			gc_assign(this->m_CurrentState,t_StateTransition->m_NextState);
		}
	}
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<167>");
	this->m_CurrentState->p_Signal(this->m_SpriteID,this->m_AnimationSet,t_CommandBuffer);
	return 0;
}
int c_CAnimationGraph::p_SetVariable(int t_VariableID,bool t_BoolValue){
	DBG_ENTER("CAnimationGraph.SetVariable")
	c_CAnimationGraph *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_VariableID,"VariableID")
	DBG_LOCAL(t_BoolValue,"BoolValue")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<137>");
	c_CAnimationVariable* t_Variable=this->m_VariableMap->p_Get(t_VariableID);
	DBG_LOCAL(t_Variable,"Variable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<138>");
	t_Variable->m_BoolValue=t_BoolValue;
	return 0;
}
int c_CAnimationGraph::p_SetVariable2(int t_VariableID,Float t_FloatValue){
	DBG_ENTER("CAnimationGraph.SetVariable")
	c_CAnimationGraph *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_VariableID,"VariableID")
	DBG_LOCAL(t_FloatValue,"FloatValue")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<142>");
	c_CAnimationVariable* t_Variable=this->m_VariableMap->p_Get(t_VariableID);
	DBG_LOCAL(t_Variable,"Variable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<144>");
	t_Variable->m_FloatValueA=t_FloatValue;
	return 0;
}
void c_CAnimationGraph::mark(){
	c_CComponent::mark();
	gc_mark_q(m_AnimationSet);
	gc_mark_q(m_VariableMap);
	gc_mark_q(m_DefaultState);
	gc_mark_q(m_CurrentState);
}
String c_CAnimationGraph::debug(){
	String t="(CAnimationGraph)\n";
	t=c_CComponent::debug()+t;
	t+=dbg_decl("VariableMap",&m_VariableMap);
	t+=dbg_decl("SpriteID",&m_SpriteID);
	t+=dbg_decl("AnimationSet",&m_AnimationSet);
	t+=dbg_decl("DefaultState",&m_DefaultState);
	t+=dbg_decl("CurrentState",&m_CurrentState);
	return t;
}
c_CAnimationVariable::c_CAnimationVariable(){
	m_VariableName=-1;
	m_FloatValueA=FLOAT(0.0);
	m_BoolValue=false;
}
c_CAnimationVariable* c_CAnimationVariable::m_new(int t_VariableName){
	DBG_ENTER("CAnimationVariable.new")
	c_CAnimationVariable *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_VariableName,"VariableName")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<10>");
	this->m_VariableName=t_VariableName;
	return this;
}
c_CAnimationVariable* c_CAnimationVariable::m_new2(){
	DBG_ENTER("CAnimationVariable.new")
	c_CAnimationVariable *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<8>");
	return this;
}
void c_CAnimationVariable::mark(){
	Object::mark();
}
String c_CAnimationVariable::debug(){
	String t="(CAnimationVariable)\n";
	t+=dbg_decl("VariableName",&m_VariableName);
	t+=dbg_decl("BoolValue",&m_BoolValue);
	t+=dbg_decl("FloatValueA",&m_FloatValueA);
	return t;
}
c_Map4::c_Map4(){
	m_root=0;
}
c_Map4* c_Map4::m_new(){
	DBG_ENTER("Map.new")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<7>");
	return this;
}
int c_Map4::p_RotateLeft4(c_Node10* t_node){
	DBG_ENTER("Map.RotateLeft")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<251>");
	c_Node10* t_child=t_node->m_right;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<252>");
	gc_assign(t_node->m_right,t_child->m_left);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<253>");
	if((t_child->m_left)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<254>");
		gc_assign(t_child->m_left->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<256>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<257>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<258>");
		if(t_node==t_node->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<259>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<261>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<264>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<266>");
	gc_assign(t_child->m_left,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<267>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map4::p_RotateRight4(c_Node10* t_node){
	DBG_ENTER("Map.RotateRight")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<271>");
	c_Node10* t_child=t_node->m_left;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<272>");
	gc_assign(t_node->m_left,t_child->m_right);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<273>");
	if((t_child->m_right)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<274>");
		gc_assign(t_child->m_right->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<276>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<277>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<278>");
		if(t_node==t_node->m_parent->m_right){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<279>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<281>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<284>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<286>");
	gc_assign(t_child->m_right,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<287>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map4::p_InsertFixup4(c_Node10* t_node){
	DBG_ENTER("Map.InsertFixup")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<212>");
	while(((t_node->m_parent)!=0) && t_node->m_parent->m_color==-1 && ((t_node->m_parent->m_parent)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<213>");
		if(t_node->m_parent==t_node->m_parent->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<214>");
			c_Node10* t_uncle=t_node->m_parent->m_parent->m_right;
			DBG_LOCAL(t_uncle,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<215>");
			if(((t_uncle)!=0) && t_uncle->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<216>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<217>");
				t_uncle->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<218>");
				t_uncle->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<219>");
				t_node=t_uncle->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<221>");
				if(t_node==t_node->m_parent->m_right){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<222>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<223>");
					p_RotateLeft4(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<225>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<226>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<227>");
				p_RotateRight4(t_node->m_parent->m_parent);
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<230>");
			c_Node10* t_uncle2=t_node->m_parent->m_parent->m_left;
			DBG_LOCAL(t_uncle2,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<231>");
			if(((t_uncle2)!=0) && t_uncle2->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<232>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<233>");
				t_uncle2->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<234>");
				t_uncle2->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<235>");
				t_node=t_uncle2->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<237>");
				if(t_node==t_node->m_parent->m_left){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<238>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<239>");
					p_RotateRight4(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<241>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<242>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<243>");
				p_RotateLeft4(t_node->m_parent->m_parent);
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<247>");
	m_root->m_color=1;
	return 0;
}
bool c_Map4::p_Add3(int t_key,c_CAnimationVariable* t_value){
	DBG_ENTER("Map.Add")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<61>");
	c_Node10* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<62>");
	c_Node10* t_parent=0;
	int t_cmp=0;
	DBG_LOCAL(t_parent,"parent")
	DBG_LOCAL(t_cmp,"cmp")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<64>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<65>");
		t_parent=t_node;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<66>");
		t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<67>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<68>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<69>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<70>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<72>");
				return false;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<76>");
	t_node=(new c_Node10)->m_new(t_key,t_value,-1,t_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<78>");
	if((t_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<79>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<80>");
			gc_assign(t_parent->m_right,t_node);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<82>");
			gc_assign(t_parent->m_left,t_node);
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<84>");
		p_InsertFixup4(t_node);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<86>");
		gc_assign(m_root,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<88>");
	return true;
}
c_Node10* c_Map4::p_FindNode(int t_key){
	DBG_ENTER("Map.FindNode")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<157>");
	c_Node10* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<159>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<160>");
		int t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_LOCAL(t_cmp,"cmp")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<161>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<162>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<163>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<164>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<166>");
				return t_node;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<169>");
	return t_node;
}
c_CAnimationVariable* c_Map4::p_Get(int t_key){
	DBG_ENTER("Map.Get")
	c_Map4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<101>");
	c_Node10* t_node=p_FindNode(t_key);
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<102>");
	if((t_node)!=0){
		DBG_BLOCK();
		return t_node->m_value;
	}
	return 0;
}
void c_Map4::mark(){
	Object::mark();
	gc_mark_q(m_root);
}
String c_Map4::debug(){
	String t="(Map)\n";
	t+=dbg_decl("root",&m_root);
	return t;
}
c_IntMap4::c_IntMap4(){
}
c_IntMap4* c_IntMap4::m_new(){
	DBG_ENTER("IntMap.new")
	c_IntMap4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<534>");
	c_Map4::m_new();
	return this;
}
int c_IntMap4::p_Compare(int t_lhs,int t_rhs){
	DBG_ENTER("IntMap.Compare")
	c_IntMap4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<537>");
	int t_=t_lhs-t_rhs;
	return t_;
}
void c_IntMap4::mark(){
	c_Map4::mark();
}
String c_IntMap4::debug(){
	String t="(IntMap)\n";
	t=c_Map4::debug()+t;
	return t;
}
c_Node10::c_Node10(){
	m_key=0;
	m_right=0;
	m_left=0;
	m_value=0;
	m_color=0;
	m_parent=0;
}
c_Node10* c_Node10::m_new(int t_key,c_CAnimationVariable* t_value,int t_color,c_Node10* t_parent){
	DBG_ENTER("Node.new")
	c_Node10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_LOCAL(t_color,"color")
	DBG_LOCAL(t_parent,"parent")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<364>");
	this->m_key=t_key;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<365>");
	gc_assign(this->m_value,t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<366>");
	this->m_color=t_color;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<367>");
	gc_assign(this->m_parent,t_parent);
	return this;
}
c_Node10* c_Node10::m_new2(){
	DBG_ENTER("Node.new")
	c_Node10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<361>");
	return this;
}
void c_Node10::mark(){
	Object::mark();
	gc_mark_q(m_right);
	gc_mark_q(m_left);
	gc_mark_q(m_value);
	gc_mark_q(m_parent);
}
String c_Node10::debug(){
	String t="(Node)\n";
	t+=dbg_decl("key",&m_key);
	t+=dbg_decl("value",&m_value);
	t+=dbg_decl("color",&m_color);
	t+=dbg_decl("parent",&m_parent);
	t+=dbg_decl("left",&m_left);
	t+=dbg_decl("right",&m_right);
	return t;
}
c_CAnimationState::c_CAnimationState(){
	m_ID=0;
	m_EntryPoint=0;
	m_TransitionArray=(new c_Deque5)->m_new();
}
c_CAnimationState* c_CAnimationState::m_new(int t_ID){
	DBG_ENTER("CAnimationState.new")
	c_CAnimationState *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<242>");
	this->m_ID=t_ID;
	return this;
}
c_CAnimationState* c_CAnimationState::m_new2(){
	DBG_ENTER("CAnimationState.new")
	c_CAnimationState *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<237>");
	return this;
}
int c_CAnimationState::p_AddTransition(c_CStateTransition* t_StateTransition){
	DBG_ENTER("CAnimationState.AddTransition")
	c_CAnimationState *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_StateTransition,"StateTransition")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<252>");
	this->m_TransitionArray->p_PushLast5(t_StateTransition);
	return 0;
}
int c_CAnimationState::p_SignalDead(){
	DBG_ENTER("CAnimationState.SignalDead")
	c_CAnimationState *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<248>");
	this->m_EntryPoint->p_SignalDead();
	return 0;
}
int c_CAnimationState::p_Signal(int t_SpriteID,c_IntMap* t_AnimationSet,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CAnimationState.Signal")
	c_CAnimationState *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_LOCAL(t_AnimationSet,"AnimationSet")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<245>");
	this->m_EntryPoint->p_Signal(t_SpriteID,t_AnimationSet,t_CommandBuffer);
	return 0;
}
void c_CAnimationState::mark(){
	Object::mark();
	gc_mark_q(m_EntryPoint);
	gc_mark_q(m_TransitionArray);
}
String c_CAnimationState::debug(){
	String t="(CAnimationState)\n";
	t+=dbg_decl("EntryPoint",&m_EntryPoint);
	t+=dbg_decl("TransitionArray",&m_TransitionArray);
	t+=dbg_decl("ID",&m_ID);
	return t;
}
c_CAnimationNode::c_CAnimationNode(){
}
c_CAnimationNode* c_CAnimationNode::m_new(){
	DBG_ENTER("CAnimationNode.new")
	c_CAnimationNode *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<200>");
	return this;
}
int c_CAnimationNode::p_SignalDead(){
	DBG_ENTER("CAnimationNode.SignalDead")
	c_CAnimationNode *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_CAnimationNode::p_Signal(int t_SpriteID,c_IntMap* t_AnimationSet,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CAnimationNode.Signal")
	c_CAnimationNode *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
void c_CAnimationNode::mark(){
	Object::mark();
}
String c_CAnimationNode::debug(){
	String t="(CAnimationNode)\n";
	return t;
}
c_CPlayAnimationNode::c_CPlayAnimationNode(){
	m_AnimID=-1;
}
c_CPlayAnimationNode* c_CPlayAnimationNode::m_new(int t_AnimID){
	DBG_ENTER("CPlayAnimationNode.new")
	c_CPlayAnimationNode *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_AnimID,"AnimID")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<209>");
	c_CAnimationNode::m_new();
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<210>");
	this->m_AnimID=t_AnimID;
	return this;
}
c_CPlayAnimationNode* c_CPlayAnimationNode::m_new2(){
	DBG_ENTER("CPlayAnimationNode.new")
	c_CPlayAnimationNode *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<207>");
	c_CAnimationNode::m_new();
	return this;
}
int c_CPlayAnimationNode::p_Signal(int t_SpriteID,c_IntMap* t_AnimationSet,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CPlayAnimationNode.Signal")
	c_CPlayAnimationNode *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_LOCAL(t_AnimationSet,"AnimationSet")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<213>");
	int t_AnimID=t_AnimationSet->p_Get(this->m_AnimID);
	DBG_LOCAL(t_AnimID,"AnimID")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<215>");
	t_CommandBuffer->p_AddCommand2(t_SpriteID,4,t_AnimID,0,0);
	return 0;
}
void c_CPlayAnimationNode::mark(){
	c_CAnimationNode::mark();
}
String c_CPlayAnimationNode::debug(){
	String t="(CPlayAnimationNode)\n";
	t=c_CAnimationNode::debug()+t;
	t+=dbg_decl("AnimID",&m_AnimID);
	return t;
}
c_CFloatMultiRangeCondition::c_CFloatMultiRangeCondition(){
	m_FloatVariable=0;
	m_MiniConditionArray=(new c_Deque4)->m_new();
	m_ActiveNode=0;
}
c_CFloatMultiRangeCondition* c_CFloatMultiRangeCondition::m_new(c_CAnimationVariable* t_FloatVariable){
	DBG_ENTER("CFloatMultiRangeCondition.new")
	c_CFloatMultiRangeCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_FloatVariable,"FloatVariable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<260>");
	c_CAnimationNode::m_new();
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<261>");
	gc_assign(this->m_FloatVariable,t_FloatVariable);
	return this;
}
c_CFloatMultiRangeCondition* c_CFloatMultiRangeCondition::m_new2(){
	DBG_ENTER("CFloatMultiRangeCondition.new")
	c_CFloatMultiRangeCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<256>");
	c_CAnimationNode::m_new();
	return this;
}
int c_CFloatMultiRangeCondition::p_AddMiniCondition(c_CFloatRangeMiniCondition* t_MiniCondition){
	DBG_ENTER("CFloatMultiRangeCondition.AddMiniCondition")
	c_CFloatMultiRangeCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_MiniCondition,"MiniCondition")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<287>");
	this->m_MiniConditionArray->p_PushLast4(t_MiniCondition);
	return 0;
}
c_CAnimationNode* c_CFloatMultiRangeCondition::p_AssessCondition(){
	DBG_ENTER("CFloatMultiRangeCondition.AssessCondition")
	c_CFloatMultiRangeCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<278>");
	c_Enumerator10* t_=this->m_MiniConditionArray->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CFloatRangeMiniCondition* t_MiniCondition=t_->p_NextObject();
		DBG_LOCAL(t_MiniCondition,"MiniCondition")
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<279>");
		if(t_MiniCondition->p_AssessCondition2(this->m_FloatVariable)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<281>");
			return t_MiniCondition->m_Link;
		}
	}
	return 0;
}
int c_CFloatMultiRangeCondition::p_Signal(int t_SpriteID,c_IntMap* t_AnimationSet,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CFloatMultiRangeCondition.Signal")
	c_CFloatMultiRangeCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_LOCAL(t_AnimationSet,"AnimationSet")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<265>");
	c_CAnimationNode* t_Node=p_AssessCondition();
	DBG_LOCAL(t_Node,"Node")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<266>");
	if(t_Node!=this->m_ActiveNode){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<267>");
		if((this->m_ActiveNode)!=0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<268>");
			this->m_ActiveNode->p_SignalDead();
		}
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<270>");
		gc_assign(this->m_ActiveNode,t_Node);
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<271>");
		t_Node->p_Signal(t_SpriteID,t_AnimationSet,t_CommandBuffer);
	}
	return 0;
}
int c_CFloatMultiRangeCondition::p_SignalDead(){
	DBG_ENTER("CFloatMultiRangeCondition.SignalDead")
	c_CFloatMultiRangeCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<291>");
	this->m_ActiveNode->p_SignalDead();
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<292>");
	this->m_ActiveNode=0;
	return 0;
}
void c_CFloatMultiRangeCondition::mark(){
	c_CAnimationNode::mark();
	gc_mark_q(m_FloatVariable);
	gc_mark_q(m_MiniConditionArray);
	gc_mark_q(m_ActiveNode);
}
String c_CFloatMultiRangeCondition::debug(){
	String t="(CFloatMultiRangeCondition)\n";
	t=c_CAnimationNode::debug()+t;
	t+=dbg_decl("FloatVariable",&m_FloatVariable);
	t+=dbg_decl("ActiveNode",&m_ActiveNode);
	t+=dbg_decl("MiniConditionArray",&m_MiniConditionArray);
	return t;
}
c_CFloatRangeMiniCondition::c_CFloatRangeMiniCondition(){
	m_MinRange=FLOAT(0.0);
	m_MaxRange=FLOAT(0.0);
	m_Link=0;
}
c_CFloatRangeMiniCondition* c_CFloatRangeMiniCondition::m_new(Float t_MinRange,Float t_MaxRange,c_CAnimationNode* t_Link){
	DBG_ENTER("CFloatRangeMiniCondition.new")
	c_CFloatRangeMiniCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_MinRange,"MinRange")
	DBG_LOCAL(t_MaxRange,"MaxRange")
	DBG_LOCAL(t_Link,"Link")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<224>");
	this->m_MinRange=t_MinRange;
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<225>");
	this->m_MaxRange=t_MaxRange;
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<226>");
	gc_assign(this->m_Link,t_Link);
	return this;
}
c_CFloatRangeMiniCondition* c_CFloatRangeMiniCondition::m_new2(){
	DBG_ENTER("CFloatRangeMiniCondition.new")
	c_CFloatRangeMiniCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<219>");
	return this;
}
bool c_CFloatRangeMiniCondition::p_AssessCondition2(c_CAnimationVariable* t_FloatVariable){
	DBG_ENTER("CFloatRangeMiniCondition.AssessCondition")
	c_CFloatRangeMiniCondition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_FloatVariable,"FloatVariable")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<230>");
	if(this->m_MinRange<=t_FloatVariable->m_FloatValueA && t_FloatVariable->m_FloatValueA<=this->m_MaxRange){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<231>");
		return true;
	}
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<233>");
	return false;
}
void c_CFloatRangeMiniCondition::mark(){
	Object::mark();
	gc_mark_q(m_Link);
}
String c_CFloatRangeMiniCondition::debug(){
	String t="(CFloatRangeMiniCondition)\n";
	t+=dbg_decl("MinRange",&m_MinRange);
	t+=dbg_decl("MaxRange",&m_MaxRange);
	t+=dbg_decl("Link",&m_Link);
	return t;
}
c_Deque4::c_Deque4(){
	m__data=Array<c_CFloatRangeMiniCondition* >(4);
	m__capacity=0;
	m__last=0;
	m__first=0;
}
c_Deque4* c_Deque4::m_new(){
	DBG_ENTER("Deque.new")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Deque4* c_Deque4::m_new2(Array<c_CFloatRangeMiniCondition* > t_arr){
	DBG_ENTER("Deque.new")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_arr,"arr")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<8>");
	gc_assign(m__data,t_arr.Slice(0));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<9>");
	m__capacity=m__data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<10>");
	m__last=m__capacity;
	return this;
}
int c_Deque4::p_Length2(){
	DBG_ENTER("Deque.Length")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<31>");
	if(m__last>=m__first){
		DBG_BLOCK();
		int t_=m__last-m__first;
		return t_;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<32>");
	int t_2=m__capacity-m__first+m__last;
	return t_2;
}
void c_Deque4::p_Grow(){
	DBG_ENTER("Deque.Grow")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<135>");
	Array<c_CFloatRangeMiniCondition* > t_data=Array<c_CFloatRangeMiniCondition* >(m__capacity*2+10);
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<136>");
	if(m__first<=m__last){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<137>");
		for(int t_i=m__first;t_i<m__last;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<138>");
			gc_assign(t_data.At(t_i-m__first),m__data.At(t_i));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<140>");
		m__last-=m__first;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<141>");
		m__first=0;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<143>");
		int t_n=m__capacity-m__first;
		DBG_LOCAL(t_n,"n")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<144>");
		for(int t_i2=0;t_i2<t_n;t_i2=t_i2+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i2,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<145>");
			gc_assign(t_data.At(t_i2),m__data.At(m__first+t_i2));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<147>");
		for(int t_i3=0;t_i3<m__last;t_i3=t_i3+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i3,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<148>");
			gc_assign(t_data.At(t_n+t_i3),m__data.At(t_i3));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<150>");
		m__last+=t_n;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<151>");
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<153>");
	m__capacity=t_data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<154>");
	gc_assign(m__data,t_data);
}
void c_Deque4::p_PushLast4(c_CFloatRangeMiniCondition* t_value){
	DBG_ENTER("Deque.PushLast")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<83>");
	if(p_Length2()+1>=m__capacity){
		DBG_BLOCK();
		p_Grow();
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<84>");
	gc_assign(m__data.At(m__last),t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<85>");
	m__last+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<86>");
	if(m__last==m__capacity){
		DBG_BLOCK();
		m__last=0;
	}
}
c_Enumerator10* c_Deque4::p_ObjectEnumerator(){
	DBG_ENTER("Deque.ObjectEnumerator")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<58>");
	c_Enumerator10* t_=(new c_Enumerator10)->m_new(this);
	return t_;
}
c_CFloatRangeMiniCondition* c_Deque4::p_Get(int t_index){
	DBG_ENTER("Deque.Get")
	c_Deque4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_index,"index")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<63>");
	if(t_index<0 || t_index>=p_Length2()){
		DBG_BLOCK();
		bbError(String(L"Illegal deque index",19));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<65>");
	c_CFloatRangeMiniCondition* t_=m__data.At((t_index+m__first) % m__capacity);
	return t_;
}
void c_Deque4::mark(){
	Object::mark();
	gc_mark_q(m__data);
}
String c_Deque4::debug(){
	String t="(Deque)\n";
	t+=dbg_decl("_data",&m__data);
	t+=dbg_decl("_capacity",&m__capacity);
	t+=dbg_decl("_first",&m__first);
	t+=dbg_decl("_last",&m__last);
	return t;
}
c_CStateTransition::c_CStateTransition(){
	m_NextState=0;
	m_Inverted=false;
}
c_CStateTransition* c_CStateTransition::m_new(c_CAnimationState* t_NextState,bool t_Inverted){
	DBG_ENTER("CStateTransition.new")
	c_CStateTransition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_NextState,"NextState")
	DBG_LOCAL(t_Inverted,"Inverted")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<175>");
	gc_assign(this->m_NextState,t_NextState);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<176>");
	this->m_Inverted=t_Inverted;
	return this;
}
c_CStateTransition* c_CStateTransition::m_new2(){
	DBG_ENTER("CStateTransition.new")
	c_CStateTransition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<170>");
	return this;
}
bool c_CStateTransition::p_Assess(){
	DBG_ENTER("CStateTransition.Assess")
	c_CStateTransition *self=this;
	DBG_LOCAL(self,"Self")
	return false;
}
void c_CStateTransition::mark(){
	Object::mark();
	gc_mark_q(m_NextState);
}
String c_CStateTransition::debug(){
	String t="(CStateTransition)\n";
	t+=dbg_decl("NextState",&m_NextState);
	t+=dbg_decl("Inverted",&m_Inverted);
	return t;
}
c_CStateBoolTransition::c_CStateBoolTransition(){
	m_BoolVariable=0;
}
c_CStateBoolTransition* c_CStateBoolTransition::m_new(c_CAnimationVariable* t_BoolVariable,c_CAnimationState* t_NextState,bool t_Inverted){
	DBG_ENTER("CStateBoolTransition.new")
	c_CStateBoolTransition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_BoolVariable,"BoolVariable")
	DBG_LOCAL(t_NextState,"NextState")
	DBG_LOCAL(t_Inverted,"Inverted")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<187>");
	c_CStateTransition::m_new(t_NextState,t_Inverted);
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<188>");
	gc_assign(this->m_BoolVariable,t_BoolVariable);
	return this;
}
c_CStateBoolTransition* c_CStateBoolTransition::m_new2(){
	DBG_ENTER("CStateBoolTransition.new")
	c_CStateBoolTransition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<183>");
	c_CStateTransition::m_new2();
	return this;
}
bool c_CStateBoolTransition::p_Assess(){
	DBG_ENTER("CStateBoolTransition.Assess")
	c_CStateBoolTransition *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<192>");
	if(this->m_Inverted==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<193>");
		return m_BoolVariable->m_BoolValue;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/AnimationGraph.monkey<195>");
		bool t_=m_BoolVariable->m_BoolValue==false;
		return t_;
	}
}
void c_CStateBoolTransition::mark(){
	c_CStateTransition::mark();
	gc_mark_q(m_BoolVariable);
}
String c_CStateBoolTransition::debug(){
	String t="(CStateBoolTransition)\n";
	t=c_CStateTransition::debug()+t;
	t+=dbg_decl("BoolVariable",&m_BoolVariable);
	return t;
}
c_Deque5::c_Deque5(){
	m__data=Array<c_CStateTransition* >(4);
	m__capacity=0;
	m__last=0;
	m__first=0;
}
c_Deque5* c_Deque5::m_new(){
	DBG_ENTER("Deque.new")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Deque5* c_Deque5::m_new2(Array<c_CStateTransition* > t_arr){
	DBG_ENTER("Deque.new")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_arr,"arr")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<8>");
	gc_assign(m__data,t_arr.Slice(0));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<9>");
	m__capacity=m__data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<10>");
	m__last=m__capacity;
	return this;
}
int c_Deque5::p_Length2(){
	DBG_ENTER("Deque.Length")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<31>");
	if(m__last>=m__first){
		DBG_BLOCK();
		int t_=m__last-m__first;
		return t_;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<32>");
	int t_2=m__capacity-m__first+m__last;
	return t_2;
}
void c_Deque5::p_Grow(){
	DBG_ENTER("Deque.Grow")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<135>");
	Array<c_CStateTransition* > t_data=Array<c_CStateTransition* >(m__capacity*2+10);
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<136>");
	if(m__first<=m__last){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<137>");
		for(int t_i=m__first;t_i<m__last;t_i=t_i+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<138>");
			gc_assign(t_data.At(t_i-m__first),m__data.At(t_i));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<140>");
		m__last-=m__first;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<141>");
		m__first=0;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<143>");
		int t_n=m__capacity-m__first;
		DBG_LOCAL(t_n,"n")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<144>");
		for(int t_i2=0;t_i2<t_n;t_i2=t_i2+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i2,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<145>");
			gc_assign(t_data.At(t_i2),m__data.At(m__first+t_i2));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<147>");
		for(int t_i3=0;t_i3<m__last;t_i3=t_i3+1){
			DBG_BLOCK();
			DBG_LOCAL(t_i3,"i")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<148>");
			gc_assign(t_data.At(t_n+t_i3),m__data.At(t_i3));
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<150>");
		m__last+=t_n;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<151>");
		m__first=0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<153>");
	m__capacity=t_data.Length();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<154>");
	gc_assign(m__data,t_data);
}
void c_Deque5::p_PushLast5(c_CStateTransition* t_value){
	DBG_ENTER("Deque.PushLast")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<83>");
	if(p_Length2()+1>=m__capacity){
		DBG_BLOCK();
		p_Grow();
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<84>");
	gc_assign(m__data.At(m__last),t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<85>");
	m__last+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<86>");
	if(m__last==m__capacity){
		DBG_BLOCK();
		m__last=0;
	}
}
c_Enumerator9* c_Deque5::p_ObjectEnumerator(){
	DBG_ENTER("Deque.ObjectEnumerator")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<58>");
	c_Enumerator9* t_=(new c_Enumerator9)->m_new(this);
	return t_;
}
c_CStateTransition* c_Deque5::p_Get(int t_index){
	DBG_ENTER("Deque.Get")
	c_Deque5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_index,"index")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<63>");
	if(t_index<0 || t_index>=p_Length2()){
		DBG_BLOCK();
		bbError(String(L"Illegal deque index",19));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<65>");
	c_CStateTransition* t_=m__data.At((t_index+m__first) % m__capacity);
	return t_;
}
void c_Deque5::mark(){
	Object::mark();
	gc_mark_q(m__data);
}
String c_Deque5::debug(){
	String t="(Deque)\n";
	t+=dbg_decl("_data",&m__data);
	t+=dbg_decl("_capacity",&m__capacity);
	t+=dbg_decl("_first",&m__first);
	t+=dbg_decl("_last",&m__last);
	return t;
}
c_CMovement::c_CMovement(){
	m_CurrentPosition=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_AnimationSet=0;
	m_AnimationGraph=0;
	m_SpriteID=-1;
	m_Init=false;
	m_CurrentlyOccupiedTile=(new c_IVector)->m_new(0,0);
	m_PathRequested=false;
	m_WaitingForSecondaryPath=false;
	m_SecondaryPath=(new c_CPath)->m_new();
	m_DestinationPosition=(new c_IVector)->m_new(0,0);
	m_TargetPathNode=0;
	m_State=0;
	m_CurrentPath=(new c_CPath)->m_new();
	m_HasAPath=false;
	m_MoveTargetSet=false;
	m_LastPathUpdateResult=0;
	m_PreviousPosition=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_Orientation=FLOAT(0.0);
}
c_CMovement* c_CMovement::m_new(int t_SpriteID,c_FVector* t_Position,c_IntMap* t_AnimationSet,c_CAnimationGraph* t_AnimationGraph){
	DBG_ENTER("CMovement.new")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_LOCAL(t_Position,"Position")
	DBG_LOCAL(t_AnimationSet,"AnimationSet")
	DBG_LOCAL(t_AnimationGraph,"AnimationGraph")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<50>");
	c_CComponent::m_new(0);
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<51>");
	m_CurrentPosition->m_X=t_Position->m_X;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<52>");
	m_CurrentPosition->m_Y=t_Position->m_Y;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<53>");
	gc_assign(this->m_AnimationSet,t_AnimationSet);
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<54>");
	gc_assign(this->m_AnimationGraph,t_AnimationGraph);
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<56>");
	this->m_SpriteID=t_SpriteID;
	return this;
}
c_CMovement* c_CMovement::m_new2(){
	DBG_ENTER("CMovement.new")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<16>");
	c_CComponent::m_new2();
	return this;
}
int c_CMovement::p_UpdatePathfindRequest(){
	DBG_ENTER("CMovement.UpdatePathfindRequest")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<87>");
	if(this->m_PathRequested){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<89>");
		if(this->m_WaitingForSecondaryPath==false && this->m_SecondaryPath->m_PathStatus!=0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<90>");
			c_IVector* t_RoundedCurrentPosition=(new c_IVector)->m_new(bb_Misc_Round(this->m_CurrentPosition->m_X),bb_Misc_Round(this->m_CurrentPosition->m_Y));
			DBG_LOCAL(t_RoundedCurrentPosition,"RoundedCurrentPosition")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<92>");
			c_IVector* t_DestinationPosition=(new c_IVector)->m_new(this->m_DestinationPosition->m_X,this->m_DestinationPosition->m_Y);
			DBG_LOCAL(t_DestinationPosition,"DestinationPosition")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<93>");
			this->m_SecondaryPath->p_Request(t_RoundedCurrentPosition,t_DestinationPosition);
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<94>");
			this->m_WaitingForSecondaryPath=true;
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<95>");
			this->m_PathRequested=false;
		}
	}
	return 0;
}
bool c_CMovement::p_CheckAndSwapPaths(){
	DBG_ENTER("CMovement.CheckAndSwapPaths")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<101>");
	if(this->m_State==1 && this->m_WaitingForSecondaryPath==true && this->m_SecondaryPath->m_PathStatus==1){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<102>");
		this->m_WaitingForSecondaryPath=false;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<104>");
		c_CPath* t_Path=this->m_SecondaryPath;
		DBG_LOCAL(t_Path,"Path")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<105>");
		gc_assign(this->m_SecondaryPath,this->m_CurrentPath);
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<106>");
		gc_assign(this->m_CurrentPath,t_Path);
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<107>");
		this->m_HasAPath=true;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<108>");
		return true;
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<110>");
	return false;
}
bool c_CMovement::p_MoveToNode(Float t_DeltaTime,c_Node9* t_Node){
	DBG_ENTER("CMovement.MoveToNode")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_Node,"Node")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<122>");
	c_IVector* t_NodePosition=t_Node->p_Value();
	DBG_LOCAL(t_NodePosition,"NodePosition")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<123>");
	c_FVector* t_DirectionToNode=(new c_FVector)->m_new(Float(t_NodePosition->m_X)-this->m_CurrentPosition->m_X,Float(t_NodePosition->m_Y)-this->m_CurrentPosition->m_Y);
	DBG_LOCAL(t_DirectionToNode,"DirectionToNode")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<124>");
	Float t_Length=(Float)sqrt(t_DirectionToNode->m_X*t_DirectionToNode->m_X+t_DirectionToNode->m_Y*t_DirectionToNode->m_Y);
	DBG_LOCAL(t_Length,"Length")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<125>");
	bool t_NodeReached=false;
	DBG_LOCAL(t_NodeReached,"NodeReached")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<126>");
	if(t_Length<FLOAT(0.001)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<127>");
		t_NodeReached=true;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<129>");
		t_DirectionToNode->m_X=t_DirectionToNode->m_X/t_Length;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<130>");
		t_DirectionToNode->m_Y=t_DirectionToNode->m_Y/t_Length;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<132>");
		Float t_Speed=FLOAT(2.5);
		DBG_LOCAL(t_Speed,"Speed")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<133>");
		this->m_CurrentPosition->m_X+=t_DirectionToNode->m_X*t_DeltaTime*t_Speed;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<134>");
		this->m_CurrentPosition->m_Y+=t_DirectionToNode->m_Y*t_DeltaTime*t_Speed;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<136>");
		c_FVector* t_NewDirectionToNode=(new c_FVector)->m_new(Float(t_NodePosition->m_X)-this->m_CurrentPosition->m_X,Float(t_NodePosition->m_Y)-this->m_CurrentPosition->m_Y);
		DBG_LOCAL(t_NewDirectionToNode,"NewDirectionToNode")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<137>");
		Float t_Dot=t_DirectionToNode->m_X*t_NewDirectionToNode->m_X+t_DirectionToNode->m_Y*t_NewDirectionToNode->m_Y;
		DBG_LOCAL(t_Dot,"Dot")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<138>");
		if(t_Dot<=FLOAT(0.0)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<139>");
			t_NodeReached=true;
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<142>");
	if(t_NodeReached){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<144>");
		c_Node9* t_NextNode=t_Node->p_NextNode();
		DBG_LOCAL(t_NextNode,"NextNode")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<145>");
		if(t_NextNode==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<146>");
			this->m_CurrentPosition->m_X=Float(t_NodePosition->m_X);
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<147>");
			this->m_CurrentPosition->m_Y=Float(t_NodePosition->m_Y);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<149>");
			c_IVector* t_NextNodePosition=t_NextNode->p_Value();
			DBG_LOCAL(t_NextNodePosition,"NextNodePosition")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<150>");
			int t_NextNodeDirX=t_NextNodePosition->m_X-t_NodePosition->m_X;
			DBG_LOCAL(t_NextNodeDirX,"NextNodeDirX")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<151>");
			int t_NextNodeDirY=t_NextNodePosition->m_Y-t_NodePosition->m_Y;
			DBG_LOCAL(t_NextNodeDirY,"NextNodeDirY")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<152>");
			Float t_ExtraDistanceX=this->m_CurrentPosition->m_X-Float(t_NodePosition->m_X);
			DBG_LOCAL(t_ExtraDistanceX,"ExtraDistanceX")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<153>");
			Float t_ExtraDistanceY=this->m_CurrentPosition->m_Y-Float(t_NodePosition->m_Y);
			DBG_LOCAL(t_ExtraDistanceY,"ExtraDistanceY")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<154>");
			Float t_Length2=(Float)sqrt(t_ExtraDistanceX*t_ExtraDistanceX+t_ExtraDistanceY*t_ExtraDistanceY);
			DBG_LOCAL(t_Length2,"Length")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<156>");
			this->m_CurrentPosition->m_X=Float(t_NodePosition->m_X)+Float(t_NextNodeDirX)*t_Length2;
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<157>");
			this->m_CurrentPosition->m_Y=Float(t_NodePosition->m_Y)+Float(t_NextNodeDirY)*t_Length2;
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<160>");
	return t_NodeReached;
}
c_Node9* c_CMovement::p_FindBestTargetNode(c_CPath* t_Path){
	DBG_ENTER("CMovement.FindBestTargetNode")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Path,"Path")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<165>");
	c_Node9* t_TargetPathNode2=0;
	DBG_LOCAL(t_TargetPathNode2,"TargetPathNode2")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<167>");
	int t_Index=1;
	DBG_LOCAL(t_Index,"Index")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<168>");
	int t_SelectedIndex=-1;
	DBG_LOCAL(t_SelectedIndex,"SelectedIndex")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<169>");
	Float t_BestSqDist=FLOAT(1000000000000.0);
	DBG_LOCAL(t_BestSqDist,"BestSqDist")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<170>");
	c_Node9* t_PathNode=this->m_CurrentPath->m_NodeList->p_FirstNode()->p_NextNode();
	DBG_LOCAL(t_PathNode,"PathNode")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<174>");
	while((t_PathNode)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<176>");
		c_Node9* t_SegmentNodeA=t_PathNode->p_PrevNode();
		DBG_LOCAL(t_SegmentNodeA,"SegmentNodeA")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<177>");
		c_Node9* t_SegmentNodeB=t_PathNode;
		DBG_LOCAL(t_SegmentNodeB,"SegmentNodeB")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<181>");
		c_FVector* t_AB=(new c_FVector)->m_new(Float(t_SegmentNodeB->p_Value()->m_X-t_SegmentNodeA->p_Value()->m_X),Float(t_SegmentNodeB->p_Value()->m_Y-t_SegmentNodeA->p_Value()->m_Y));
		DBG_LOCAL(t_AB,"AB")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<182>");
		c_FVector* t_AC=(new c_FVector)->m_new(this->m_CurrentPosition->m_X-Float(t_SegmentNodeA->p_Value()->m_X),this->m_CurrentPosition->m_Y-Float(t_SegmentNodeA->p_Value()->m_Y));
		DBG_LOCAL(t_AC,"AC")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<183>");
		c_FVector* t_BC=(new c_FVector)->m_new(this->m_CurrentPosition->m_X-Float(t_SegmentNodeB->p_Value()->m_X),this->m_CurrentPosition->m_Y-Float(t_SegmentNodeB->p_Value()->m_Y));
		DBG_LOCAL(t_BC,"BC")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<184>");
		Float t_e=t_AC->m_X*t_AB->m_X+t_AC->m_Y*t_AB->m_Y;
		DBG_LOCAL(t_e,"e")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<186>");
		if(t_e<=FLOAT(0.0)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<189>");
			if(t_SegmentNodeA==this->m_CurrentPath->m_NodeList->p_FirstNode() && bb_math_Abs2(t_e)<=FLOAT(1.0)){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<190>");
				t_BestSqDist=t_AC->m_X*t_AC->m_X+t_AC->m_Y*t_AC->m_Y;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<191>");
				t_TargetPathNode2=t_SegmentNodeA;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<192>");
				t_SelectedIndex=t_Index;
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<196>");
			Float t_f=t_AB->m_X*t_AB->m_X+t_AB->m_Y*t_AB->m_Y;
			DBG_LOCAL(t_f,"f")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<198>");
			if(t_e<=t_f){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<200>");
				Float t_SqDist=t_AC->m_X*t_AC->m_X+t_AC->m_Y*t_AC->m_Y-t_e*t_e/t_f;
				DBG_LOCAL(t_SqDist,"SqDist")
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<202>");
				if(t_SqDist<t_BestSqDist){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<203>");
					t_BestSqDist=t_SqDist;
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<204>");
					t_TargetPathNode2=t_SegmentNodeB;
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<205>");
					t_SelectedIndex=t_Index;
				}
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<210>");
				Float t_SqDist2=t_BC->m_X*t_BC->m_X+t_BC->m_Y*t_BC->m_Y;
				DBG_LOCAL(t_SqDist2,"SqDist")
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<211>");
				if(t_SegmentNodeB==this->m_CurrentPath->m_NodeList->p_LastNode() && t_SqDist2<t_BestSqDist){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<212>");
					t_BestSqDist=t_SqDist2;
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<213>");
					t_TargetPathNode2=t_SegmentNodeB;
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<214>");
					t_SelectedIndex=t_Index;
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<219>");
		t_PathNode=t_PathNode->p_NextNode();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<220>");
		t_Index+=1;
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<223>");
	return t_TargetPathNode2;
}
int c_CMovement::p_AssertCanOccupy(c_IVector* t_TilePosition,String t_ErrorMessage){
	DBG_ENTER("CMovement.AssertCanOccupy")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_TilePosition,"TilePosition")
	DBG_LOCAL(t_ErrorMessage,"ErrorMessage")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<114>");
	int t_Index=c_CAIManager::m_GetTileIndexFromPosition(t_TilePosition);
	DBG_LOCAL(t_Index,"Index")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<115>");
	if(c_CAIManager::m_TileStatusArray.At(t_Index)->m_TileStatus==1 && c_CAIManager::m_TileStatusArray.At(t_Index)->m_UnitID!=this->m_GameObject->m_ID){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<116>");
		bbPrint(t_ErrorMessage);
	}
	return 0;
}
int c_CMovement::p_FollowPath(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CMovement.FollowPath")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<227>");
	p_UpdatePathfindRequest();
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<229>");
	bool t_PathRefreshed=false;
	DBG_LOCAL(t_PathRefreshed,"PathRefreshed")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<233>");
	if(this->m_TargetPathNode==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<234>");
		t_PathRefreshed=p_CheckAndSwapPaths();
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<239>");
	if(this->m_MoveTargetSet){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<240>");
		if(p_MoveToNode(t_DeltaTime,this->m_TargetPathNode)){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<241>");
			this->m_MoveTargetSet=false;
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<243>");
			t_PathRefreshed=p_CheckAndSwapPaths();
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<247>");
	if(this->m_State==2){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<248>");
		t_PathRefreshed=p_CheckAndSwapPaths();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<249>");
		if(t_PathRefreshed){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<250>");
			this->m_MoveTargetSet=false;
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<255>");
	if(this->m_HasAPath){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<256>");
		c_Node9* t_PotentialNextPathNode=0;
		DBG_LOCAL(t_PotentialNextPathNode,"PotentialNextPathNode")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<258>");
		if(this->m_MoveTargetSet==false){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<261>");
			if(t_PathRefreshed){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<264>");
				t_PotentialNextPathNode=p_FindBestTargetNode(this->m_CurrentPath);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<265>");
				this->m_LastPathUpdateResult=0;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<268>");
				if(t_PotentialNextPathNode==0){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<269>");
					this->m_TargetPathNode=0;
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<270>");
					this->m_PathRequested=true;
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<271>");
					this->m_HasAPath=false;
				}
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<275>");
				int t_1=this->m_State;
				DBG_LOCAL(t_1,"1")
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<276>");
				if(t_1==1){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<277>");
					t_PotentialNextPathNode=this->m_TargetPathNode->p_NextNode();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<278>");
					if(t_PotentialNextPathNode==0){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<280>");
						this->m_HasAPath=false;
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<281>");
						this->m_TargetPathNode=0;
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<282>");
						this->m_State=0;
					}
				}else{
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<284>");
					if(t_1==3){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<286>");
						this->m_HasAPath=false;
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<287>");
						this->m_TargetPathNode=0;
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<288>");
						this->m_State=0;
					}else{
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<289>");
						if(t_1==2){
							DBG_BLOCK();
							DBG_INFO("C:/MonkeyX77a/Movement.monkey<290>");
							t_PotentialNextPathNode=this->m_TargetPathNode;
						}
					}
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<296>");
		if((t_PotentialNextPathNode)!=0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<297>");
			c_IVector* t_PotentialNextNodePosition=t_PotentialNextPathNode->p_Value();
			DBG_LOCAL(t_PotentialNextNodePosition,"PotentialNextNodePosition")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<298>");
			c_CTileStatusData* t_TileStatusData=c_CAIManager::m_GetTileStatusData(t_PotentialNextNodePosition);
			DBG_LOCAL(t_TileStatusData,"TileStatusData")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<303>");
			if(t_TileStatusData->m_TileStatus==1 && t_TileStatusData->m_UnitID!=this->m_GameObject->m_ID){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<306>");
				c_IVector* t_RoundedCurrentPosition=(new c_IVector)->m_new(bb_Misc_Round(this->m_CurrentPosition->m_X),bb_Misc_Round(this->m_CurrentPosition->m_Y));
				DBG_LOCAL(t_RoundedCurrentPosition,"RoundedCurrentPosition")
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<307>");
				p_AssertCanOccupy(t_RoundedCurrentPosition,String(L"Error: Waiting",14));
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<308>");
				c_CAIManager::m_SetTileStatus(this->m_CurrentlyOccupiedTile,0,-1);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<309>");
				c_CAIManager::m_SetTileStatus(t_RoundedCurrentPosition,1,this->m_GameObject->m_ID);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<310>");
				this->m_CurrentlyOccupiedTile->m_X=t_RoundedCurrentPosition->m_X;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<311>");
				this->m_CurrentlyOccupiedTile->m_Y=t_RoundedCurrentPosition->m_Y;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<317>");
				this->m_CurrentPosition->m_X=Float(t_RoundedCurrentPosition->m_X);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<318>");
				this->m_CurrentPosition->m_Y=Float(t_RoundedCurrentPosition->m_Y);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<320>");
				gc_assign(this->m_TargetPathNode,t_PotentialNextPathNode);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<321>");
				this->m_State=2;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<324>");
				p_AssertCanOccupy(t_PotentialNextNodePosition,String(L"Error: Next Node",16));
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<325>");
				c_CAIManager::m_SetTileStatus(this->m_CurrentlyOccupiedTile,0,-1);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<326>");
				c_CAIManager::m_SetTileStatus(t_PotentialNextNodePosition,1,this->m_GameObject->m_ID);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<327>");
				this->m_CurrentlyOccupiedTile->m_X=t_PotentialNextNodePosition->m_X;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<328>");
				this->m_CurrentlyOccupiedTile->m_Y=t_PotentialNextNodePosition->m_Y;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<330>");
				gc_assign(this->m_TargetPathNode,t_PotentialNextPathNode);
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<331>");
				this->m_MoveTargetSet=true;
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<332>");
				this->m_State=1;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<337>");
	if(this->m_State==1){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<338>");
		this->m_AnimationGraph->p_SetVariable(3,true);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<340>");
		this->m_AnimationGraph->p_SetVariable(3,false);
	}
	return 0;
}
int c_CMovement::p_UpdateAnimation(c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CMovement.UpdateAnimation")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<374>");
	if(this->m_State!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<375>");
		int t_AnimID=-1;
		DBG_LOCAL(t_AnimID,"AnimID")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<376>");
		c_FVector* t_Direction=(new c_FVector)->m_new(this->m_CurrentPosition->m_X-this->m_PreviousPosition->m_X,this->m_CurrentPosition->m_Y-this->m_PreviousPosition->m_Y);
		DBG_LOCAL(t_Direction,"Direction")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<377>");
		Float t_Length=(Float)sqrt(t_Direction->m_X*t_Direction->m_X+t_Direction->m_Y*t_Direction->m_Y);
		DBG_LOCAL(t_Length,"Length")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<378>");
		if(bb_math_Abs2(t_Length)<FLOAT(2e-23)){
			DBG_BLOCK();
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<381>");
			t_Direction->m_X/=t_Length;
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<382>");
			t_Direction->m_Y/=t_Length;
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<384>");
			Float t_Angle=t_Direction->p_GetOrientationFromNormed();
			DBG_LOCAL(t_Angle,"Angle")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<385>");
			Float t_Slice=FLOAT(22.5);
			DBG_LOCAL(t_Slice,"Slice")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<386>");
			if(FLOAT(1.0)*t_Slice<t_Angle && t_Angle<=FLOAT(3.0)*t_Slice){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<387>");
				t_AnimID=this->m_AnimationSet->p_Get(11);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<388>");
				if(FLOAT(3.0)*t_Slice<t_Angle && t_Angle<=FLOAT(5.0)*t_Slice){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<389>");
					t_AnimID=this->m_AnimationSet->p_Get(1);
				}else{
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<390>");
					if(FLOAT(5.0)*t_Slice<t_Angle && t_Angle<=FLOAT(7.0)*t_Slice){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<391>");
						t_AnimID=this->m_AnimationSet->p_Get(12);
					}else{
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<392>");
						if(FLOAT(7.0)*t_Slice<t_Angle || t_Angle<=FLOAT(-7.0)*t_Slice){
							DBG_BLOCK();
							DBG_INFO("C:/MonkeyX77a/Movement.monkey<393>");
							t_AnimID=this->m_AnimationSet->p_Get(3);
						}else{
							DBG_BLOCK();
							DBG_INFO("C:/MonkeyX77a/Movement.monkey<394>");
							if(FLOAT(-7.0)*t_Slice<t_Angle && t_Angle<=FLOAT(-5.0)*t_Slice){
								DBG_BLOCK();
								DBG_INFO("C:/MonkeyX77a/Movement.monkey<395>");
								t_AnimID=this->m_AnimationSet->p_Get(10);
							}else{
								DBG_BLOCK();
								DBG_INFO("C:/MonkeyX77a/Movement.monkey<396>");
								if(FLOAT(-5.0)*t_Slice<t_Angle && t_Angle<=FLOAT(-3.0)*t_Slice){
									DBG_BLOCK();
									DBG_INFO("C:/MonkeyX77a/Movement.monkey<397>");
									t_AnimID=this->m_AnimationSet->p_Get(0);
								}else{
									DBG_BLOCK();
									DBG_INFO("C:/MonkeyX77a/Movement.monkey<398>");
									if(FLOAT(-3.0)*t_Slice<t_Angle && t_Angle<=FLOAT(-1.0)*t_Slice){
										DBG_BLOCK();
										DBG_INFO("C:/MonkeyX77a/Movement.monkey<399>");
										t_AnimID=this->m_AnimationSet->p_Get(9);
									}else{
										DBG_BLOCK();
										DBG_INFO("C:/MonkeyX77a/Movement.monkey<400>");
										if(FLOAT(-1.0)*t_Slice<t_Angle || t_Angle<=FLOAT(1.0)*t_Slice){
											DBG_BLOCK();
											DBG_INFO("C:/MonkeyX77a/Movement.monkey<401>");
											t_AnimID=this->m_AnimationSet->p_Get(2);
										}
									}
								}
							}
						}
					}
				}
			}
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<404>");
			this->m_Orientation=t_Angle;
		}
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<412>");
		this->m_AnimationGraph->p_SetVariable2(2,this->m_Orientation);
	}
	return 0;
}
int c_CMovement::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CMovement.Update")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<345>");
	if(this->m_Init==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<346>");
		this->m_Init=true;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<349>");
		c_IVector* t_RoundedCurrentPosition=(new c_IVector)->m_new(int(this->m_CurrentPosition->m_X),int(this->m_CurrentPosition->m_Y));
		DBG_LOCAL(t_RoundedCurrentPosition,"RoundedCurrentPosition")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<350>");
		c_CAIManager::m_SetTileStatus(t_RoundedCurrentPosition,1,this->m_GameObject->m_ID);
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<351>");
		this->m_CurrentlyOccupiedTile->m_X=t_RoundedCurrentPosition->m_X;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<352>");
		this->m_CurrentlyOccupiedTile->m_Y=t_RoundedCurrentPosition->m_Y;
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<354>");
		this->m_CurrentPosition->m_X=Float(t_RoundedCurrentPosition->m_X);
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<355>");
		this->m_CurrentPosition->m_Y=Float(t_RoundedCurrentPosition->m_Y);
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<359>");
	p_FollowPath(t_DeltaTime,t_CommandBuffer);
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<360>");
	p_UpdateAnimation(t_CommandBuffer);
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<362>");
	if(this->m_CurrentPosition->m_X!=this->m_PreviousPosition->m_X || this->m_CurrentPosition->m_Y!=this->m_PreviousPosition->m_Y){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<363>");
		t_CommandBuffer->p_AddCommand(this->m_SpriteID,3,(this->m_CurrentPosition->m_X+FLOAT(0.5))*c_CAIManager::m_MapScale*Float(c_CAIManager::m_TileSize),(this->m_CurrentPosition->m_Y+FLOAT(0.5))*c_CAIManager::m_MapScale*Float(c_CAIManager::m_TileSize));
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<365>");
	this->m_PreviousPosition->m_X=this->m_CurrentPosition->m_X;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<366>");
	this->m_PreviousPosition->m_Y=this->m_CurrentPosition->m_Y;
	return 0;
}
bool c_CMovement::m_DebugPath;
bool c_CMovement::m_DebugTargetPathNode;
bool c_CMovement::m_DebugUnitID;
bool c_CMovement::m_DebugPathStatus;
bool c_CMovement::m_DebugState;
int c_CMovement::p_RenderDebug(){
	DBG_ENTER("CMovement.RenderDebug")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<417>");
	if(m_DebugPath){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<418>");
		bb_graphics_SetColor(FLOAT(255.0),FLOAT(255.0),FLOAT(255.0));
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<419>");
		if(this->m_HasAPath){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<420>");
			c_IVector* t_PreviousNode=0;
			DBG_LOCAL(t_PreviousNode,"PreviousNode")
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<421>");
			c_Enumerator11* t_=this->m_CurrentPath->m_NodeList->p_ObjectEnumerator();
			while(t_->p_HasNext()){
				DBG_BLOCK();
				c_IVector* t_Node=t_->p_NextObject();
				DBG_LOCAL(t_Node,"Node")
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<422>");
				if((t_PreviousNode)!=0){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<423>");
					c_FVector* t_PointA=(new c_FVector)->m_new((Float(t_PreviousNode->m_X*c_CAIManager::m_TileSize)+FLOAT(0.5)*Float(c_CAIManager::m_TileSize))*c_CAIManager::m_MapScale,(Float(t_PreviousNode->m_Y*c_CAIManager::m_TileSize)+FLOAT(0.5)*Float(c_CAIManager::m_TileSize))*c_CAIManager::m_MapScale);
					DBG_LOCAL(t_PointA,"PointA")
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<424>");
					c_FVector* t_PointB=(new c_FVector)->m_new((Float(t_Node->m_X*c_CAIManager::m_TileSize)+FLOAT(0.5)*Float(c_CAIManager::m_TileSize))*c_CAIManager::m_MapScale,(Float(t_Node->m_Y*c_CAIManager::m_TileSize)+FLOAT(0.5)*Float(c_CAIManager::m_TileSize))*c_CAIManager::m_MapScale);
					DBG_LOCAL(t_PointB,"PointB")
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<425>");
					bb_graphics_DrawLine(t_PointA->m_X,t_PointA->m_Y,t_PointB->m_X,t_PointB->m_Y);
				}
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<427>");
				t_PreviousNode=t_Node;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<432>");
	if(m_DebugTargetPathNode && ((this->m_TargetPathNode)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<433>");
		bb_graphics_DrawCircle((Float(this->m_TargetPathNode->p_Value()->m_X)+FLOAT(0.5))*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,(Float(this->m_TargetPathNode->p_Value()->m_Y)+FLOAT(0.5))*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(10.0));
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<435>");
	if(m_DebugUnitID){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<436>");
		bb_graphics_DrawText(String(this->m_GameObject->m_ID),this->m_CurrentPosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,this->m_CurrentPosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(0.0),FLOAT(0.0));
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<438>");
	if(m_DebugPathStatus){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<439>");
		c_FVector* t_PathStatusPosition=(new c_FVector)->m_new(this->m_CurrentPosition->m_X+FLOAT(0.5),this->m_CurrentPosition->m_Y);
		DBG_LOCAL(t_PathStatusPosition,"PathStatusPosition")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<440>");
		bb_graphics_SetColor(FLOAT(0.0),FLOAT(255.0),FLOAT(0.0));
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<441>");
		if(this->m_SecondaryPath->m_PathStatus==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<443>");
			bb_graphics_SetColor(FLOAT(255.0),FLOAT(0.0),FLOAT(0.0));
		}
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<445>");
		bb_graphics_DrawCircle(t_PathStatusPosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,t_PathStatusPosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(3.0));
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<447>");
	if(m_DebugState){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<448>");
		c_FVector* t_StatusPosition=(new c_FVector)->m_new(this->m_CurrentPosition->m_X,this->m_CurrentPosition->m_Y+FLOAT(0.25));
		DBG_LOCAL(t_StatusPosition,"StatusPosition")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<449>");
		bb_graphics_SetColor(FLOAT(255.0),FLOAT(255.0),FLOAT(255.0));
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<450>");
		int t_2=this->m_State;
		DBG_LOCAL(t_2,"2")
		DBG_INFO("C:/MonkeyX77a/Movement.monkey<451>");
		if(t_2==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<452>");
			bb_graphics_DrawText(String(L"I",1),t_StatusPosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,t_StatusPosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(0.0),FLOAT(0.0));
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Movement.monkey<453>");
			if(t_2==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<454>");
				bb_graphics_DrawText(String(L"M",1),t_StatusPosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,t_StatusPosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(0.0),FLOAT(0.0));
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Movement.monkey<455>");
				if(t_2==2){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<456>");
					bb_graphics_DrawText(String(L"W",1),t_StatusPosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,t_StatusPosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(0.0),FLOAT(0.0));
				}else{
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Movement.monkey<457>");
					if(t_2==3){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Movement.monkey<458>");
						bb_graphics_DrawText(String(L"C",1),t_StatusPosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,t_StatusPosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(0.0),FLOAT(0.0));
					}
				}
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<462>");
	c_FVector* t_ThePosition=(new c_FVector)->m_new(this->m_CurrentPosition->m_X,this->m_CurrentPosition->m_Y+FLOAT(0.5));
	DBG_LOCAL(t_ThePosition,"ThePosition")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<463>");
	bb_graphics_DrawText(String(this->m_AnimationGraph->m_CurrentState->m_ID),t_ThePosition->m_X*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,t_ThePosition->m_Y*Float(c_CAIManager::m_TileSize)*c_CAIManager::m_MapScale,FLOAT(0.0),FLOAT(0.0));
	return 0;
}
int c_CMovement::p_RequestMoveTo(c_IVector* t_DestinationPosition){
	DBG_ENTER("CMovement.RequestMoveTo")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DestinationPosition,"DestinationPosition")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<62>");
	this->m_DestinationPosition->m_X=t_DestinationPosition->m_X;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<63>");
	this->m_DestinationPosition->m_Y=t_DestinationPosition->m_Y;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<64>");
	this->m_PathRequested=true;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<65>");
	this->m_State=1;
	return 0;
}
int c_CMovement::p_CancelMoveTo(){
	DBG_ENTER("CMovement.CancelMoveTo")
	c_CMovement *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<70>");
	this->m_State=3;
	DBG_INFO("C:/MonkeyX77a/Movement.monkey<71>");
	this->m_PathRequested=false;
	return 0;
}
void c_CMovement::mark(){
	c_CComponent::mark();
	gc_mark_q(m_CurrentPosition);
	gc_mark_q(m_AnimationSet);
	gc_mark_q(m_AnimationGraph);
	gc_mark_q(m_CurrentlyOccupiedTile);
	gc_mark_q(m_SecondaryPath);
	gc_mark_q(m_DestinationPosition);
	gc_mark_q(m_TargetPathNode);
	gc_mark_q(m_CurrentPath);
	gc_mark_q(m_PreviousPosition);
}
String c_CMovement::debug(){
	String t="(CMovement)\n";
	t=c_CComponent::debug()+t;
	t+=dbg_decl("State",&m_State);
	t+=dbg_decl("CurrentPosition",&m_CurrentPosition);
	t+=dbg_decl("Init",&m_Init);
	t+=dbg_decl("LastPathUpdateResult",&m_LastPathUpdateResult);
	t+=dbg_decl("MoveTargetSet",&m_MoveTargetSet);
	t+=dbg_decl("HasAPath",&m_HasAPath);
	t+=dbg_decl("DestinationPosition",&m_DestinationPosition);
	t+=dbg_decl("PathRequested",&m_PathRequested);
	t+=dbg_decl("CurrentPath",&m_CurrentPath);
	t+=dbg_decl("SecondaryPath",&m_SecondaryPath);
	t+=dbg_decl("WaitingForSecondaryPath",&m_WaitingForSecondaryPath);
	t+=dbg_decl("CurrentlyOccupiedTile",&m_CurrentlyOccupiedTile);
	t+=dbg_decl("TargetPathNode",&m_TargetPathNode);
	t+=dbg_decl("SpriteID",&m_SpriteID);
	t+=dbg_decl("PreviousPosition",&m_PreviousPosition);
	t+=dbg_decl("AnimationSet",&m_AnimationSet);
	t+=dbg_decl("Orientation",&m_Orientation);
	t+=dbg_decl("AnimationGraph",&m_AnimationGraph);
	t+=dbg_decl("DebugPath",&c_CMovement::m_DebugPath);
	t+=dbg_decl("DebugTargetPathNode",&c_CMovement::m_DebugTargetPathNode);
	t+=dbg_decl("DebugUnitID",&c_CMovement::m_DebugUnitID);
	t+=dbg_decl("DebugPathStatus",&c_CMovement::m_DebugPathStatus);
	t+=dbg_decl("DebugState",&c_CMovement::m_DebugState);
	return t;
}
c_CUnit::c_CUnit(){
	m_AttitudeGroup=0;
	m_Movement=0;
	m_SpriteID=-1;
	m_AnimationSet=0;
	m_AnimationGraph=0;
	m_Position=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_ClosestEnemyPosition=(new c_IVector)->m_new(0,0);
	m_ClosestEnemyDirty=false;
	m_Init=false;
	m_HealthBarBackgroundID=-1;
	m_HealthBarBackgroundColor=(new c_CColor)->m_new(255,255,255);
	m_HealthBarBackgroundWidth=FLOAT(28.0);
	m_HealthBarBackgroundHeight=FLOAT(4.0);
	m_HealthBarID=-1;
	m_HealthBarColor=(new c_CColor)->m_new(255,0,50);
	m_HealthBarWidth=FLOAT(26.0);
	m_HealthBarHeight=FLOAT(2.0);
	m_EnemyOnAdjacentTile=false;
	m_CurrentAnimID=-1;
	m_Attack=false;
	m_PreviousPosition=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_HealthBarOffset=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.5));
}
c_CUnit* c_CUnit::m_new(int t_SpriteID,c_CMovement* t_Movement,int t_AttitudeGroup,c_IntMap* t_AnimationSet,c_CAnimationGraph* t_AnimationGraph){
	DBG_ENTER("CUnit.new")
	c_CUnit *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_SpriteID,"SpriteID")
	DBG_LOCAL(t_Movement,"Movement")
	DBG_LOCAL(t_AttitudeGroup,"AttitudeGroup")
	DBG_LOCAL(t_AnimationSet,"AnimationSet")
	DBG_LOCAL(t_AnimationGraph,"AnimationGraph")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<39>");
	c_CComponent::m_new(4);
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<40>");
	this->m_AttitudeGroup=t_AttitudeGroup;
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<41>");
	gc_assign(this->m_Movement,t_Movement);
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<42>");
	this->m_SpriteID=t_SpriteID;
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<43>");
	gc_assign(this->m_AnimationSet,t_AnimationSet);
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<44>");
	gc_assign(this->m_AnimationGraph,t_AnimationGraph);
	return this;
}
c_CUnit* c_CUnit::m_new2(){
	DBG_ENTER("CUnit.new")
	c_CUnit *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<9>");
	c_CComponent::m_new2();
	return this;
}
int c_CUnit::p_SetClosestEnemy(c_FVector* t_ClosestEnemyPosition){
	DBG_ENTER("CUnit.SetClosestEnemy")
	c_CUnit *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ClosestEnemyPosition,"ClosestEnemyPosition")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<118>");
	int t_EnemyX=bb_Misc_Round(t_ClosestEnemyPosition->m_X);
	DBG_LOCAL(t_EnemyX,"EnemyX")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<119>");
	int t_EnemyY=bb_Misc_Round(t_ClosestEnemyPosition->m_Y);
	DBG_LOCAL(t_EnemyY,"EnemyY")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<120>");
	if(t_EnemyX!=this->m_ClosestEnemyPosition->m_X || t_EnemyY!=this->m_ClosestEnemyPosition->m_Y){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<121>");
		this->m_ClosestEnemyPosition->m_X=t_EnemyX;
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<122>");
		this->m_ClosestEnemyPosition->m_Y=t_EnemyY;
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<123>");
		this->m_ClosestEnemyDirty=true;
	}
	return 0;
}
int c_CUnit::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CUnit.Update")
	c_CUnit *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<48>");
	if(this->m_Init==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<49>");
		this->m_Init=true;
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<51>");
		this->m_HealthBarBackgroundID=t_CommandBuffer->p_AddCommand_CreateRenderItem(5,FLOAT(1.0));
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<52>");
		t_CommandBuffer->p_AddCommand2(this->m_HealthBarBackgroundID,6,this->m_HealthBarBackgroundColor->m_R,this->m_HealthBarBackgroundColor->m_G,this->m_HealthBarBackgroundColor->m_B);
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<53>");
		t_CommandBuffer->p_AddCommand(this->m_HealthBarBackgroundID,7,this->m_HealthBarBackgroundWidth*c_CAIManager::m_MapScale,this->m_HealthBarBackgroundHeight*c_CAIManager::m_MapScale);
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<55>");
		this->m_HealthBarID=t_CommandBuffer->p_AddCommand_CreateRenderItem(5,FLOAT(2.0));
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<56>");
		t_CommandBuffer->p_AddCommand2(this->m_HealthBarID,6,this->m_HealthBarColor->m_R,this->m_HealthBarColor->m_G,this->m_HealthBarColor->m_B);
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<57>");
		t_CommandBuffer->p_AddCommand(this->m_HealthBarID,7,this->m_HealthBarWidth*c_CAIManager::m_MapScale,this->m_HealthBarHeight*c_CAIManager::m_MapScale);
	}
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<61>");
	this->m_Position->m_X=this->m_Movement->m_CurrentPosition->m_X;
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<62>");
	this->m_Position->m_Y=this->m_Movement->m_CurrentPosition->m_Y;
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<64>");
	int t_RoundedPositionX=bb_Misc_Round(this->m_Position->m_X);
	DBG_LOCAL(t_RoundedPositionX,"RoundedPositionX")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<65>");
	int t_RoundedPositionY=bb_Misc_Round(this->m_Position->m_Y);
	DBG_LOCAL(t_RoundedPositionY,"RoundedPositionY")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<67>");
	int t_DiffX=this->m_ClosestEnemyPosition->m_X-t_RoundedPositionX;
	DBG_LOCAL(t_DiffX,"DiffX")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<68>");
	int t_DiffY=this->m_ClosestEnemyPosition->m_Y-t_RoundedPositionY;
	DBG_LOCAL(t_DiffY,"DiffY")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<69>");
	int t_AbsDiffX=bb_math_Abs(t_DiffX);
	DBG_LOCAL(t_AbsDiffX,"AbsDiffX")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<70>");
	int t_AbsDiffY=bb_math_Abs(t_DiffY);
	DBG_LOCAL(t_AbsDiffY,"AbsDiffY")
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<72>");
	if(t_AbsDiffX==1 && t_AbsDiffY==0 || t_AbsDiffX==0 && t_AbsDiffY==1 || t_AbsDiffX==1 && t_AbsDiffY==1){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<73>");
		this->m_EnemyOnAdjacentTile=true;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<75>");
		this->m_EnemyOnAdjacentTile=false;
	}
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<78>");
	if(this->m_ClosestEnemyDirty){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<79>");
		this->m_ClosestEnemyDirty=false;
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<81>");
		if(this->m_EnemyOnAdjacentTile==false){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Unit.monkey<82>");
			this->m_Movement->p_RequestMoveTo(this->m_ClosestEnemyPosition);
			DBG_INFO("C:/MonkeyX77a/Unit.monkey<83>");
			this->m_CurrentAnimID=-1;
		}
	}
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<86>");
	if(this->m_EnemyOnAdjacentTile==true){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<87>");
		if(this->m_Movement->m_State!=0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Unit.monkey<88>");
			this->m_Movement->p_CancelMoveTo();
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Unit.monkey<90>");
			if(this->m_Attack==false){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<91>");
				this->m_Attack=true;
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<93>");
				c_FVector* t_Direction=(new c_FVector)->m_new(Float(t_DiffX),Float(t_DiffY));
				DBG_LOCAL(t_Direction,"Direction")
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<94>");
				Float t_Length=(Float)sqrt(t_Direction->m_X*t_Direction->m_X+t_Direction->m_Y*t_Direction->m_Y);
				DBG_LOCAL(t_Length,"Length")
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<95>");
				t_Direction->m_X/=t_Length;
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<96>");
				t_Direction->m_Y/=t_Length;
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<97>");
				Float t_Angle=t_Direction->p_GetOrientationFromNormed();
				DBG_LOCAL(t_Angle,"Angle")
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<98>");
				this->m_AnimationGraph->p_SetVariable(0,true);
				DBG_INFO("C:/MonkeyX77a/Unit.monkey<99>");
				this->m_AnimationGraph->p_SetVariable2(1,t_Angle);
			}
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<103>");
		if(this->m_Attack==true){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Unit.monkey<105>");
			this->m_Attack=false;
			DBG_INFO("C:/MonkeyX77a/Unit.monkey<106>");
			this->m_AnimationGraph->p_SetVariable(0,false);
		}
	}
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<109>");
	if(this->m_Position->m_X!=this->m_PreviousPosition->m_X || this->m_Position->m_Y!=this->m_PreviousPosition->m_Y){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<110>");
		t_CommandBuffer->p_AddCommand(this->m_HealthBarBackgroundID,3,(this->m_Position->m_X+this->m_HealthBarOffset->m_X+FLOAT(0.5))*c_CAIManager::m_MapScale*Float(c_CAIManager::m_TileSize),(this->m_Position->m_Y+this->m_HealthBarOffset->m_Y+FLOAT(0.5))*c_CAIManager::m_MapScale*Float(c_CAIManager::m_TileSize));
		DBG_INFO("C:/MonkeyX77a/Unit.monkey<111>");
		t_CommandBuffer->p_AddCommand(this->m_HealthBarID,3,(this->m_Position->m_X+this->m_HealthBarOffset->m_X+FLOAT(0.5))*c_CAIManager::m_MapScale*Float(c_CAIManager::m_TileSize),(this->m_Position->m_Y+this->m_HealthBarOffset->m_Y+FLOAT(0.5))*c_CAIManager::m_MapScale*Float(c_CAIManager::m_TileSize));
	}
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<113>");
	this->m_PreviousPosition->m_X=this->m_Position->m_X;
	DBG_INFO("C:/MonkeyX77a/Unit.monkey<114>");
	this->m_PreviousPosition->m_Y=this->m_Position->m_Y;
	return 0;
}
int c_CUnit::p_RenderDebug(){
	DBG_ENTER("CUnit.RenderDebug")
	c_CUnit *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
void c_CUnit::mark(){
	c_CComponent::mark();
	gc_mark_q(m_Movement);
	gc_mark_q(m_AnimationSet);
	gc_mark_q(m_AnimationGraph);
	gc_mark_q(m_Position);
	gc_mark_q(m_ClosestEnemyPosition);
	gc_mark_q(m_HealthBarBackgroundColor);
	gc_mark_q(m_HealthBarColor);
	gc_mark_q(m_PreviousPosition);
	gc_mark_q(m_HealthBarOffset);
}
String c_CUnit::debug(){
	String t="(CUnit)\n";
	t=c_CComponent::debug()+t;
	t+=dbg_decl("Init",&m_Init);
	t+=dbg_decl("AttitudeGroup",&m_AttitudeGroup);
	t+=dbg_decl("ClosestEnemyPosition",&m_ClosestEnemyPosition);
	t+=dbg_decl("Movement",&m_Movement);
	t+=dbg_decl("Position",&m_Position);
	t+=dbg_decl("PreviousPosition",&m_PreviousPosition);
	t+=dbg_decl("ClosestEnemyDirty",&m_ClosestEnemyDirty);
	t+=dbg_decl("CurrentAnimID",&m_CurrentAnimID);
	t+=dbg_decl("SpriteID",&m_SpriteID);
	t+=dbg_decl("EnemyOnAdjacentTile",&m_EnemyOnAdjacentTile);
	t+=dbg_decl("AnimationSet",&m_AnimationSet);
	t+=dbg_decl("Attack",&m_Attack);
	t+=dbg_decl("AnimationGraph",&m_AnimationGraph);
	t+=dbg_decl("HealthBarBackgroundWidth",&m_HealthBarBackgroundWidth);
	t+=dbg_decl("HealthBarBackgroundHeight",&m_HealthBarBackgroundHeight);
	t+=dbg_decl("HealthBarBackgroundID",&m_HealthBarBackgroundID);
	t+=dbg_decl("HealthBarBackgroundColor",&m_HealthBarBackgroundColor);
	t+=dbg_decl("HealthBarOffset",&m_HealthBarOffset);
	t+=dbg_decl("HealthBarWidth",&m_HealthBarWidth);
	t+=dbg_decl("HealthBarHeight",&m_HealthBarHeight);
	t+=dbg_decl("HealthBarID",&m_HealthBarID);
	t+=dbg_decl("HealthBarColor",&m_HealthBarColor);
	return t;
}
c_CRenderer::c_CRenderer(){
	m_RenderItemMap=(new c_IntMap5)->m_new();
	m_RenderItemList=(new c_CRenderItemList)->m_new();
}
c_CRenderer* c_CRenderer::m_new(){
	DBG_ENTER("CRenderer.new")
	c_CRenderer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<56>");
	return this;
}
int c_CRenderer::p_AddRenderItem(c_CRenderItem* t_RenderItem){
	DBG_ENTER("CRenderer.AddRenderItem")
	c_CRenderer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_RenderItem,"RenderItem")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<105>");
	this->m_RenderItemMap->p_Add4(t_RenderItem->m_ID,t_RenderItem);
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<108>");
	c_Node12* t_RenderItemNode=this->m_RenderItemList->p_FirstNode();
	DBG_LOCAL(t_RenderItemNode,"RenderItemNode")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<109>");
	if(t_RenderItemNode==0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<111>");
		this->m_RenderItemList->p_AddFirst2(t_RenderItem);
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<112>");
		return 0;
	}
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<116>");
	while((t_RenderItemNode)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<117>");
		if(t_RenderItem->m_Z<t_RenderItemNode->p_Value()->m_Z){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<118>");
			if(t_RenderItemNode->p_PrevNode()==0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<119>");
				this->m_RenderItemList->p_AddFirst2(t_RenderItem);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<121>");
				c_Node12* t_NewNode=(new c_Node12)->m_new(t_RenderItemNode,t_RenderItemNode->p_PrevNode(),t_RenderItem);
			}
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<123>");
			return 0;
		}
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<125>");
		t_RenderItemNode=t_RenderItemNode->p_NextNode();
	}
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<129>");
	this->m_RenderItemList->p_AddLast7(t_RenderItem);
	return 0;
}
int c_CRenderer::p_Update2(Float t_DeltaTime,c_CCommandBuffer* t_CommandBuffer){
	DBG_ENTER("CRenderer.Update")
	c_CRenderer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_LOCAL(t_CommandBuffer,"CommandBuffer")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<61>");
	while(t_CommandBuffer->p_IsEmpty()==false){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<62>");
		c_CCommand* t_Command=t_CommandBuffer->p_PopCommand();
		DBG_LOCAL(t_Command,"Command")
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<63>");
		int t_2=t_Command->m_Type;
		DBG_LOCAL(t_2,"2")
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<64>");
		if(t_2==2){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<65>");
			c_CMap* t_Map=(new c_CMap)->m_new(t_Command->m_ID,t_Command->m_ParamFloatA);
			DBG_LOCAL(t_Map,"Map")
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<66>");
			p_AddRenderItem(t_Map);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<67>");
			if(t_2==1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<68>");
				c_CSprite* t_Sprite=(new c_CSprite)->m_new(t_Command->m_ID,t_Command->m_ParamFloatA);
				DBG_LOCAL(t_Sprite,"Sprite")
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<69>");
				p_AddRenderItem(t_Sprite);
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<70>");
				if(t_2==5){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Renderer.monkey<71>");
					c_CSquareItem* t_SquareItem=(new c_CSquareItem)->m_new(t_Command->m_ID,t_Command->m_ParamFloatA);
					DBG_LOCAL(t_SquareItem,"SquareItem")
					DBG_INFO("C:/MonkeyX77a/Renderer.monkey<72>");
					p_AddRenderItem(t_SquareItem);
				}else{
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/Renderer.monkey<74>");
					c_CRenderItem* t_RenderItem=this->m_RenderItemMap->p_Get(t_Command->m_ID);
					DBG_LOCAL(t_RenderItem,"RenderItem")
					DBG_INFO("C:/MonkeyX77a/Renderer.monkey<75>");
					if((t_RenderItem)!=0){
						DBG_BLOCK();
						DBG_INFO("C:/MonkeyX77a/Renderer.monkey<76>");
						t_RenderItem->p_ProcessCommand(t_Command);
					}
				}
			}
		}
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<79>");
		t_CommandBuffer->p_RecycleCommand(t_Command);
	}
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<82>");
	c_Enumerator4* t_=m_RenderItemList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CRenderItem* t_RenderItem2=t_->p_NextObject();
		DBG_LOCAL(t_RenderItem2,"RenderItem")
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<83>");
		t_RenderItem2->p_Update3(t_DeltaTime);
	}
	return 0;
}
int c_CRenderer::p_Render(c_List3* t_GameObjectList){
	DBG_ENTER("CRenderer.Render")
	c_CRenderer *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_GameObjectList,"GameObjectList")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<88>");
	bb_graphics_Cls(FLOAT(0.0),FLOAT(0.0),FLOAT(0.0));
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<90>");
	bb_graphics_PushMatrix();
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<92>");
	c_Enumerator4* t_=m_RenderItemList->p_ObjectEnumerator();
	while(t_->p_HasNext()){
		DBG_BLOCK();
		c_CRenderItem* t_RenderItem=t_->p_NextObject();
		DBG_LOCAL(t_RenderItem,"RenderItem")
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<93>");
		t_RenderItem->p_Render2();
	}
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<95>");
	c_CAIManager::m_RenderDebug();
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<96>");
	c_Enumerator3* t_2=t_GameObjectList->p_ObjectEnumerator();
	while(t_2->p_HasNext()){
		DBG_BLOCK();
		c_CGameObject* t_GameObject=t_2->p_NextObject();
		DBG_LOCAL(t_GameObject,"GameObject")
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<97>");
		t_GameObject->p_RenderDebug();
	}
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<99>");
	bb_graphics_PopMatrix();
	return 0;
}
void c_CRenderer::mark(){
	Object::mark();
	gc_mark_q(m_RenderItemMap);
	gc_mark_q(m_RenderItemList);
}
String c_CRenderer::debug(){
	String t="(CRenderer)\n";
	t+=dbg_decl("RenderItemList",&m_RenderItemList);
	t+=dbg_decl("RenderItemMap",&m_RenderItemMap);
	return t;
}
c_CRenderItem::c_CRenderItem(){
	m_ID=0;
	m_Z=FLOAT(0.0);
	m_Position=(new c_FVector)->m_new(FLOAT(0.0),FLOAT(0.0));
	m_Scale_=(new c_FVector)->m_new(FLOAT(1.0),FLOAT(1.0));
	m_Color=(new c_CColor)->m_new(255,255,255);
}
c_CRenderItem* c_CRenderItem::m_new(int t_ID,Float t_Z){
	DBG_ENTER("CRenderItem.new")
	c_CRenderItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_LOCAL(t_Z,"Z")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<15>");
	this->m_ID=t_ID;
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<16>");
	this->m_Z=t_Z;
	return this;
}
c_CRenderItem* c_CRenderItem::m_new2(){
	DBG_ENTER("CRenderItem.new")
	c_CRenderItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<7>");
	return this;
}
int c_CRenderItem::p_ProcessCommand(c_CCommand* t_Command){
	DBG_ENTER("CRenderItem.ProcessCommand")
	c_CRenderItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Command,"Command")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<27>");
	int t_1=t_Command->m_Type;
	DBG_LOCAL(t_1,"1")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<28>");
	if(t_1==3){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<29>");
		this->m_Position->m_X=t_Command->m_ParamFloatA;
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<30>");
		this->m_Position->m_Y=t_Command->m_ParamFloatB;
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Renderer.monkey<32>");
		if(t_1==7){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<34>");
			this->m_Scale_->m_X=t_Command->m_ParamFloatA;
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<35>");
			this->m_Scale_->m_Y=t_Command->m_ParamFloatB;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Renderer.monkey<36>");
			if(t_1==6){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<37>");
				this->m_Color->m_R=t_Command->m_ParamIntA;
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<38>");
				this->m_Color->m_G=t_Command->m_ParamIntB;
				DBG_INFO("C:/MonkeyX77a/Renderer.monkey<39>");
				this->m_Color->m_B=t_Command->m_ParamIntC;
			}
		}
	}
	return 0;
}
int c_CRenderItem::p_Update3(Float t_DeltaTime){
	DBG_ENTER("CRenderItem.Update")
	c_CRenderItem *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
int c_CRenderItem::p_Render2(){
	DBG_ENTER("CRenderItem.Render")
	c_CRenderItem *self=this;
	DBG_LOCAL(self,"Self")
	return 0;
}
void c_CRenderItem::mark(){
	Object::mark();
	gc_mark_q(m_Position);
	gc_mark_q(m_Scale_);
	gc_mark_q(m_Color);
}
String c_CRenderItem::debug(){
	String t="(CRenderItem)\n";
	t+=dbg_decl("Z",&m_Z);
	t+=dbg_decl("ID",&m_ID);
	t+=dbg_decl("Position",&m_Position);
	t+=dbg_decl("Scale_",&m_Scale_);
	t+=dbg_decl("Color",&m_Color);
	return t;
}
c_CMap::c_CMap(){
	m_SpriteIndexArray=Array<int >();
	m_Width=0;
	m_Height=0;
	m_TileCount=0;
	m_TileWidth=0;
	m_Img=0;
}
c_CMap* c_CMap::m_new(int t_ID,Float t_Z){
	DBG_ENTER("CMap.new")
	c_CMap *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_LOCAL(t_Z,"Z")
	DBG_INFO("C:/MonkeyX77a/Map.monkey<14>");
	c_CRenderItem::m_new(t_ID,t_Z);
	DBG_INFO("C:/MonkeyX77a/Map.monkey<16>");
	gc_assign(this->m_SpriteIndexArray,Array<int >(c_CAIManager::m_TerrainTileCount));
	DBG_INFO("C:/MonkeyX77a/Map.monkey<17>");
	for(int t_i=0;t_i<c_CAIManager::m_TerrainTileCount;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/Map.monkey<18>");
		if(c_CAIManager::m_TerrainDescriptionArray.At(t_i)==0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Map.monkey<19>");
			int t_FrameOffset=int(bb_random_Rnd2(FLOAT(0.0),FLOAT(7.0)));
			DBG_LOCAL(t_FrameOffset,"FrameOffset")
			DBG_INFO("C:/MonkeyX77a/Map.monkey<20>");
			this->m_SpriteIndexArray.At(t_i)=t_FrameOffset;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Map.monkey<22>");
			this->m_SpriteIndexArray.At(t_i)=12;
		}
	}
	DBG_INFO("C:/MonkeyX77a/Map.monkey<26>");
	this->m_Width=c_CAIManager::m_TerrainWidth;
	DBG_INFO("C:/MonkeyX77a/Map.monkey<27>");
	this->m_Height=c_CAIManager::m_TerrainHeight;
	DBG_INFO("C:/MonkeyX77a/Map.monkey<28>");
	this->m_TileCount=this->m_Width*this->m_Height;
	DBG_INFO("C:/MonkeyX77a/Map.monkey<29>");
	this->m_TileWidth=c_CAIManager::m_TileSize;
	DBG_INFO("C:/MonkeyX77a/Map.monkey<31>");
	gc_assign(this->m_Img,bb_graphics_LoadImage2(String(L"Terrain.png",11),32,32,16,c_Image::m_DefaultFlags));
	return this;
}
c_CMap* c_CMap::m_new2(){
	DBG_ENTER("CMap.new")
	c_CMap *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Map.monkey<5>");
	c_CRenderItem::m_new2();
	return this;
}
int c_CMap::p_Render2(){
	DBG_ENTER("CMap.Render")
	c_CMap *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Map.monkey<34>");
	bb_graphics_PushMatrix();
	DBG_INFO("C:/MonkeyX77a/Map.monkey<35>");
	bb_graphics_Scale(this->m_Scale_->m_X,this->m_Scale_->m_Y);
	DBG_INFO("C:/MonkeyX77a/Map.monkey<36>");
	for(int t_i=0;t_i<m_TileCount;t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/Map.monkey<37>");
		bb_graphics_DrawImage(this->m_Img,Float(t_i % this->m_Width*this->m_TileWidth),Float(t_i/this->m_Width*this->m_TileWidth),this->m_SpriteIndexArray.At(t_i));
	}
	DBG_INFO("C:/MonkeyX77a/Map.monkey<39>");
	bb_graphics_PopMatrix();
	return 0;
}
void c_CMap::mark(){
	c_CRenderItem::mark();
	gc_mark_q(m_SpriteIndexArray);
	gc_mark_q(m_Img);
}
String c_CMap::debug(){
	String t="(CMap)\n";
	t=c_CRenderItem::debug()+t;
	t+=dbg_decl("TileCount",&m_TileCount);
	t+=dbg_decl("Width",&m_Width);
	t+=dbg_decl("Height",&m_Height);
	t+=dbg_decl("TileWidth",&m_TileWidth);
	t+=dbg_decl("Img",&m_Img);
	t+=dbg_decl("SpriteIndexArray",&m_SpriteIndexArray);
	return t;
}
c_Map5::c_Map5(){
	m_root=0;
}
c_Map5* c_Map5::m_new(){
	DBG_ENTER("Map.new")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<7>");
	return this;
}
int c_Map5::p_RotateLeft5(c_Node11* t_node){
	DBG_ENTER("Map.RotateLeft")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<251>");
	c_Node11* t_child=t_node->m_right;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<252>");
	gc_assign(t_node->m_right,t_child->m_left);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<253>");
	if((t_child->m_left)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<254>");
		gc_assign(t_child->m_left->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<256>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<257>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<258>");
		if(t_node==t_node->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<259>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<261>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<264>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<266>");
	gc_assign(t_child->m_left,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<267>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map5::p_RotateRight5(c_Node11* t_node){
	DBG_ENTER("Map.RotateRight")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<271>");
	c_Node11* t_child=t_node->m_left;
	DBG_LOCAL(t_child,"child")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<272>");
	gc_assign(t_node->m_left,t_child->m_right);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<273>");
	if((t_child->m_right)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<274>");
		gc_assign(t_child->m_right->m_parent,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<276>");
	gc_assign(t_child->m_parent,t_node->m_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<277>");
	if((t_node->m_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<278>");
		if(t_node==t_node->m_parent->m_right){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<279>");
			gc_assign(t_node->m_parent->m_right,t_child);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<281>");
			gc_assign(t_node->m_parent->m_left,t_child);
		}
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<284>");
		gc_assign(m_root,t_child);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<286>");
	gc_assign(t_child->m_right,t_node);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<287>");
	gc_assign(t_node->m_parent,t_child);
	return 0;
}
int c_Map5::p_InsertFixup5(c_Node11* t_node){
	DBG_ENTER("Map.InsertFixup")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<212>");
	while(((t_node->m_parent)!=0) && t_node->m_parent->m_color==-1 && ((t_node->m_parent->m_parent)!=0)){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<213>");
		if(t_node->m_parent==t_node->m_parent->m_parent->m_left){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<214>");
			c_Node11* t_uncle=t_node->m_parent->m_parent->m_right;
			DBG_LOCAL(t_uncle,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<215>");
			if(((t_uncle)!=0) && t_uncle->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<216>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<217>");
				t_uncle->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<218>");
				t_uncle->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<219>");
				t_node=t_uncle->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<221>");
				if(t_node==t_node->m_parent->m_right){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<222>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<223>");
					p_RotateLeft5(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<225>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<226>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<227>");
				p_RotateRight5(t_node->m_parent->m_parent);
			}
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<230>");
			c_Node11* t_uncle2=t_node->m_parent->m_parent->m_left;
			DBG_LOCAL(t_uncle2,"uncle")
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<231>");
			if(((t_uncle2)!=0) && t_uncle2->m_color==-1){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<232>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<233>");
				t_uncle2->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<234>");
				t_uncle2->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<235>");
				t_node=t_uncle2->m_parent;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<237>");
				if(t_node==t_node->m_parent->m_left){
					DBG_BLOCK();
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<238>");
					t_node=t_node->m_parent;
					DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<239>");
					p_RotateRight5(t_node);
				}
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<241>");
				t_node->m_parent->m_color=1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<242>");
				t_node->m_parent->m_parent->m_color=-1;
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<243>");
				p_RotateLeft5(t_node->m_parent->m_parent);
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<247>");
	m_root->m_color=1;
	return 0;
}
bool c_Map5::p_Add4(int t_key,c_CRenderItem* t_value){
	DBG_ENTER("Map.Add")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<61>");
	c_Node11* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<62>");
	c_Node11* t_parent=0;
	int t_cmp=0;
	DBG_LOCAL(t_parent,"parent")
	DBG_LOCAL(t_cmp,"cmp")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<64>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<65>");
		t_parent=t_node;
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<66>");
		t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<67>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<68>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<69>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<70>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<72>");
				return false;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<76>");
	t_node=(new c_Node11)->m_new(t_key,t_value,-1,t_parent);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<78>");
	if((t_parent)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<79>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<80>");
			gc_assign(t_parent->m_right,t_node);
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<82>");
			gc_assign(t_parent->m_left,t_node);
		}
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<84>");
		p_InsertFixup5(t_node);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<86>");
		gc_assign(m_root,t_node);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<88>");
	return true;
}
c_Node11* c_Map5::p_FindNode(int t_key){
	DBG_ENTER("Map.FindNode")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<157>");
	c_Node11* t_node=m_root;
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<159>");
	while((t_node)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<160>");
		int t_cmp=p_Compare(t_key,t_node->m_key);
		DBG_LOCAL(t_cmp,"cmp")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<161>");
		if(t_cmp>0){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<162>");
			t_node=t_node->m_right;
		}else{
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<163>");
			if(t_cmp<0){
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<164>");
				t_node=t_node->m_left;
			}else{
				DBG_BLOCK();
				DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<166>");
				return t_node;
			}
		}
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<169>");
	return t_node;
}
c_CRenderItem* c_Map5::p_Get(int t_key){
	DBG_ENTER("Map.Get")
	c_Map5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<101>");
	c_Node11* t_node=p_FindNode(t_key);
	DBG_LOCAL(t_node,"node")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<102>");
	if((t_node)!=0){
		DBG_BLOCK();
		return t_node->m_value;
	}
	return 0;
}
void c_Map5::mark(){
	Object::mark();
	gc_mark_q(m_root);
}
String c_Map5::debug(){
	String t="(Map)\n";
	t+=dbg_decl("root",&m_root);
	return t;
}
c_IntMap5::c_IntMap5(){
}
c_IntMap5* c_IntMap5::m_new(){
	DBG_ENTER("IntMap.new")
	c_IntMap5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<534>");
	c_Map5::m_new();
	return this;
}
int c_IntMap5::p_Compare(int t_lhs,int t_rhs){
	DBG_ENTER("IntMap.Compare")
	c_IntMap5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_lhs,"lhs")
	DBG_LOCAL(t_rhs,"rhs")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<537>");
	int t_=t_lhs-t_rhs;
	return t_;
}
void c_IntMap5::mark(){
	c_Map5::mark();
}
String c_IntMap5::debug(){
	String t="(IntMap)\n";
	t=c_Map5::debug()+t;
	return t;
}
c_Node11::c_Node11(){
	m_key=0;
	m_right=0;
	m_left=0;
	m_value=0;
	m_color=0;
	m_parent=0;
}
c_Node11* c_Node11::m_new(int t_key,c_CRenderItem* t_value,int t_color,c_Node11* t_parent){
	DBG_ENTER("Node.new")
	c_Node11 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_key,"key")
	DBG_LOCAL(t_value,"value")
	DBG_LOCAL(t_color,"color")
	DBG_LOCAL(t_parent,"parent")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<364>");
	this->m_key=t_key;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<365>");
	gc_assign(this->m_value,t_value);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<366>");
	this->m_color=t_color;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<367>");
	gc_assign(this->m_parent,t_parent);
	return this;
}
c_Node11* c_Node11::m_new2(){
	DBG_ENTER("Node.new")
	c_Node11 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/map.monkey<361>");
	return this;
}
void c_Node11::mark(){
	Object::mark();
	gc_mark_q(m_right);
	gc_mark_q(m_left);
	gc_mark_q(m_value);
	gc_mark_q(m_parent);
}
String c_Node11::debug(){
	String t="(Node)\n";
	t+=dbg_decl("key",&m_key);
	t+=dbg_decl("value",&m_value);
	t+=dbg_decl("color",&m_color);
	t+=dbg_decl("parent",&m_parent);
	t+=dbg_decl("left",&m_left);
	t+=dbg_decl("right",&m_right);
	return t;
}
c_List7::c_List7(){
	m__head=((new c_HeadNode7)->m_new());
}
c_List7* c_List7::m_new(){
	DBG_ENTER("List.new")
	c_List7 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node12* c_List7::p_AddLast7(c_CRenderItem* t_data){
	DBG_ENTER("List.AddLast")
	c_List7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node12* t_=(new c_Node12)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List7* c_List7::m_new2(Array<c_CRenderItem* > t_data){
	DBG_ENTER("List.new")
	c_List7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CRenderItem* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CRenderItem* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast7(t_t);
	}
	return this;
}
c_Node12* c_List7::p_FirstNode(){
	DBG_ENTER("List.FirstNode")
	c_List7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<62>");
	if(m__head->m__succ!=m__head){
		DBG_BLOCK();
		return m__head->m__succ;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<63>");
	return 0;
}
c_Node12* c_List7::p_AddFirst2(c_CRenderItem* t_data){
	DBG_ENTER("List.AddFirst")
	c_List7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<104>");
	c_Node12* t_=(new c_Node12)->m_new(m__head->m__succ,m__head,t_data);
	return t_;
}
c_Enumerator4* c_List7::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator4* t_=(new c_Enumerator4)->m_new(this);
	return t_;
}
void c_List7::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List7::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_CRenderItemList::c_CRenderItemList(){
}
c_CRenderItemList* c_CRenderItemList::m_new(){
	DBG_ENTER("CRenderItemList.new")
	c_CRenderItemList *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Renderer.monkey<44>");
	c_List7::m_new();
	return this;
}
void c_CRenderItemList::mark(){
	c_List7::mark();
}
String c_CRenderItemList::debug(){
	String t="(CRenderItemList)\n";
	t=c_List7::debug()+t;
	return t;
}
c_Node12::c_Node12(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node12* c_Node12::m_new(c_Node12* t_succ,c_Node12* t_pred,c_CRenderItem* t_data){
	DBG_ENTER("Node.new")
	c_Node12 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node12* c_Node12::m_new2(){
	DBG_ENTER("Node.new")
	c_Node12 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
c_CRenderItem* c_Node12::p_Value(){
	DBG_ENTER("Node.Value")
	c_Node12 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<269>");
	return m__data;
}
c_Node12* c_Node12::p_GetNode(){
	DBG_ENTER("Node.GetNode")
	c_Node12 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<301>");
	return this;
}
c_Node12* c_Node12::p_PrevNode(){
	DBG_ENTER("Node.PrevNode")
	c_Node12 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<289>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<291>");
	c_Node12* t_=m__pred->p_GetNode();
	return t_;
}
c_Node12* c_Node12::p_NextNode(){
	DBG_ENTER("Node.NextNode")
	c_Node12 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<282>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<284>");
	c_Node12* t_=m__succ->p_GetNode();
	return t_;
}
void c_Node12::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node12::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode7::c_HeadNode7(){
}
c_HeadNode7* c_HeadNode7::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node12::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
c_Node12* c_HeadNode7::p_GetNode(){
	DBG_ENTER("HeadNode.GetNode")
	c_HeadNode7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<316>");
	return 0;
}
void c_HeadNode7::mark(){
	c_Node12::mark();
}
String c_HeadNode7::debug(){
	String t="(HeadNode)\n";
	t=c_Node12::debug()+t;
	return t;
}
c_CSprite::c_CSprite(){
	m_CurrentAnimation=0;
	m_Time=FLOAT(0.0);
	m_ImageToDisplay=0;
}
c_CSprite* c_CSprite::m_new(int t_ID,Float t_Z){
	DBG_ENTER("CSprite.new")
	c_CSprite *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_LOCAL(t_Z,"Z")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<12>");
	c_CRenderItem::m_new(t_ID,t_Z);
	return this;
}
c_CSprite* c_CSprite::m_new2(){
	DBG_ENTER("CSprite.new")
	c_CSprite *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<5>");
	c_CRenderItem::m_new2();
	return this;
}
int c_CSprite::p_Update3(Float t_DeltaTime){
	DBG_ENTER("CSprite.Update")
	c_CSprite *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_DeltaTime,"DeltaTime")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<23>");
	if((this->m_CurrentAnimation)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<24>");
		this->m_Time+=t_DeltaTime;
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<25>");
		if(this->m_Time>=this->m_CurrentAnimation->m_Duration){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Sprite.monkey<26>");
			this->m_Time=this->m_Time-this->m_CurrentAnimation->m_Duration;
		}
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<28>");
		int t_KeyframeIndex=int(this->m_Time/this->m_CurrentAnimation->m_TimePerKeyframe);
		DBG_LOCAL(t_KeyframeIndex,"KeyframeIndex")
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<29>");
		gc_assign(this->m_ImageToDisplay,m_CurrentAnimation->m_KeyframeList->p_Get(t_KeyframeIndex));
	}
	return 0;
}
int c_CSprite::p_Render2(){
	DBG_ENTER("CSprite.Render")
	c_CSprite *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<34>");
	if((this->m_ImageToDisplay)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<36>");
		bb_graphics_PushMatrix();
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<37>");
		bb_graphics_SetColor(Float(this->m_Color->m_R),Float(this->m_Color->m_G),Float(this->m_Color->m_B));
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<38>");
		bb_graphics_Translate(this->m_Position->m_X,this->m_Position->m_Y);
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<39>");
		c_FVector* t_LocalScale=(new c_FVector)->m_new(this->m_Scale_->m_X,this->m_Scale_->m_Y);
		DBG_LOCAL(t_LocalScale,"LocalScale")
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<40>");
		if(this->m_ImageToDisplay->m_MirrorVertically){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/Sprite.monkey<41>");
			t_LocalScale->m_X*=FLOAT(-1.0);
		}
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<43>");
		bb_graphics_Scale(t_LocalScale->m_X,t_LocalScale->m_Y);
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<44>");
		bb_graphics_DrawImage(this->m_ImageToDisplay->m_Img,FLOAT(0.0),FLOAT(0.0),this->m_ImageToDisplay->m_SpriteIndex);
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<45>");
		bb_graphics_PopMatrix();
	}
	return 0;
}
int c_CSprite::p_PlayAnim(int t_AnimName){
	DBG_ENTER("CSprite.PlayAnim")
	c_CSprite *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_AnimName,"AnimName")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<16>");
	if(t_AnimName!=-1){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<17>");
		gc_assign(this->m_CurrentAnimation,c_CAnimationManager::m_GetAnimation(t_AnimName));
	}
	return 0;
}
int c_CSprite::p_ProcessCommand(c_CCommand* t_Command){
	DBG_ENTER("CSprite.ProcessCommand")
	c_CSprite *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_Command,"Command")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<49>");
	c_CRenderItem::p_ProcessCommand(t_Command);
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<51>");
	int t_1=t_Command->m_Type;
	DBG_LOCAL(t_1,"1")
	DBG_INFO("C:/MonkeyX77a/Sprite.monkey<52>");
	if(t_1==4){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/Sprite.monkey<53>");
		p_PlayAnim(t_Command->m_ParamIntA);
	}
	return 0;
}
void c_CSprite::mark(){
	c_CRenderItem::mark();
	gc_mark_q(m_CurrentAnimation);
	gc_mark_q(m_ImageToDisplay);
}
String c_CSprite::debug(){
	String t="(CSprite)\n";
	t=c_CRenderItem::debug()+t;
	t+=dbg_decl("CurrentAnimation",&m_CurrentAnimation);
	t+=dbg_decl("Time",&m_Time);
	t+=dbg_decl("ImageToDisplay",&m_ImageToDisplay);
	return t;
}
c_CSquareItem::c_CSquareItem(){
	m_Vertices=Array<Float >(8);
}
c_CSquareItem* c_CSquareItem::m_new(int t_ID,Float t_Z){
	DBG_ENTER("CSquareItem.new")
	c_CSquareItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_ID,"ID")
	DBG_LOCAL(t_Z,"Z")
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<7>");
	c_CRenderItem::m_new(t_ID,t_Z);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<9>");
	this->m_Vertices.At(0)=FLOAT(-0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<10>");
	this->m_Vertices.At(1)=FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<11>");
	this->m_Vertices.At(2)=FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<12>");
	this->m_Vertices.At(3)=FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<13>");
	this->m_Vertices.At(4)=FLOAT(0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<14>");
	this->m_Vertices.At(5)=FLOAT(-0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<15>");
	this->m_Vertices.At(6)=FLOAT(-0.5);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<16>");
	this->m_Vertices.At(7)=FLOAT(-0.5);
	return this;
}
c_CSquareItem* c_CSquareItem::m_new2(){
	DBG_ENTER("CSquareItem.new")
	c_CSquareItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<3>");
	c_CRenderItem::m_new2();
	return this;
}
int c_CSquareItem::p_Render2(){
	DBG_ENTER("CSquareItem.Render")
	c_CSquareItem *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<20>");
	bb_graphics_PushMatrix();
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<21>");
	bb_graphics_SetColor(Float(this->m_Color->m_R),Float(this->m_Color->m_G),Float(this->m_Color->m_B));
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<22>");
	bb_graphics_Translate(this->m_Position->m_X,this->m_Position->m_Y);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<23>");
	bb_graphics_Scale(this->m_Scale_->m_X,this->m_Scale_->m_Y);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<24>");
	bb_graphics_DrawPoly(this->m_Vertices);
	DBG_INFO("C:/MonkeyX77a/SquareItem.monkey<25>");
	bb_graphics_PopMatrix();
	return 0;
}
void c_CSquareItem::mark(){
	c_CRenderItem::mark();
	gc_mark_q(m_Vertices);
}
String c_CSquareItem::debug(){
	String t="(CSquareItem)\n";
	t=c_CRenderItem::debug()+t;
	t+=dbg_decl("Vertices",&m_Vertices);
	return t;
}
c_CColor::c_CColor(){
	m_R=0;
	m_G=0;
	m_B=0;
}
c_CColor* c_CColor::m_new(int t_R,int t_G,int t_B){
	DBG_ENTER("CColor.new")
	c_CColor *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_R,"R")
	DBG_LOCAL(t_G,"G")
	DBG_LOCAL(t_B,"B")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<41>");
	this->m_R=t_R;
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<42>");
	this->m_G=t_G;
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<43>");
	this->m_B=t_B;
	return this;
}
c_CColor* c_CColor::m_new2(){
	DBG_ENTER("CColor.new")
	c_CColor *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<35>");
	return this;
}
void c_CColor::mark(){
	Object::mark();
}
String c_CColor::debug(){
	String t="(CColor)\n";
	t+=dbg_decl("R",&m_R);
	t+=dbg_decl("G",&m_G);
	t+=dbg_decl("B",&m_B);
	return t;
}
c_Enumerator4::c_Enumerator4(){
	m__list=0;
	m__curr=0;
}
c_Enumerator4* c_Enumerator4::m_new(c_List7* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator4* c_Enumerator4::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator4::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CRenderItem* c_Enumerator4::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator4 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CRenderItem* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator4::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator4::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
int bb_graphics_DebugRenderDevice(){
	DBG_ENTER("DebugRenderDevice")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<49>");
	if(!((bb_graphics_renderDevice)!=0)){
		DBG_BLOCK();
		bbError(String(L"Rendering operations can only be performed inside OnRender",58));
	}
	return 0;
}
int bb_graphics_Cls(Float t_r,Float t_g,Float t_b){
	DBG_ENTER("Cls")
	DBG_LOCAL(t_r,"r")
	DBG_LOCAL(t_g,"g")
	DBG_LOCAL(t_b,"b")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<376>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<378>");
	bb_graphics_renderDevice->Cls(t_r,t_g,t_b);
	return 0;
}
int bb_graphics_PushMatrix(){
	DBG_ENTER("PushMatrix")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<332>");
	int t_sp=bb_graphics_context->m_matrixSp;
	DBG_LOCAL(t_sp,"sp")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<333>");
	bb_graphics_context->m_matrixStack.At(t_sp+0)=bb_graphics_context->m_ix;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<334>");
	bb_graphics_context->m_matrixStack.At(t_sp+1)=bb_graphics_context->m_iy;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<335>");
	bb_graphics_context->m_matrixStack.At(t_sp+2)=bb_graphics_context->m_jx;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<336>");
	bb_graphics_context->m_matrixStack.At(t_sp+3)=bb_graphics_context->m_jy;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<337>");
	bb_graphics_context->m_matrixStack.At(t_sp+4)=bb_graphics_context->m_tx;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<338>");
	bb_graphics_context->m_matrixStack.At(t_sp+5)=bb_graphics_context->m_ty;
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<339>");
	bb_graphics_context->m_matrixSp=t_sp+6;
	return 0;
}
int bb_graphics_DrawImage(c_Image* t_image,Float t_x,Float t_y,int t_frame){
	DBG_ENTER("DrawImage")
	DBG_LOCAL(t_image,"image")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_frame,"frame")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<449>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<450>");
	if(t_frame<0 || t_frame>=t_image->m_frames.Length()){
		DBG_BLOCK();
		bbError(String(L"Invalid image frame",19));
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<453>");
	c_Frame* t_f=t_image->m_frames.At(t_frame);
	DBG_LOCAL(t_f,"f")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<455>");
	bb_graphics_context->p_Validate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<457>");
	if((t_image->m_flags&65536)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<458>");
		bb_graphics_renderDevice->DrawSurface(t_image->m_surface,t_x-t_image->m_tx,t_y-t_image->m_ty);
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<460>");
		bb_graphics_renderDevice->DrawSurface2(t_image->m_surface,t_x-t_image->m_tx,t_y-t_image->m_ty,t_f->m_x,t_f->m_y,t_image->m_width,t_image->m_height);
	}
	return 0;
}
int bb_graphics_Transform(Float t_ix,Float t_iy,Float t_jx,Float t_jy,Float t_tx,Float t_ty){
	DBG_ENTER("Transform")
	DBG_LOCAL(t_ix,"ix")
	DBG_LOCAL(t_iy,"iy")
	DBG_LOCAL(t_jx,"jx")
	DBG_LOCAL(t_jy,"jy")
	DBG_LOCAL(t_tx,"tx")
	DBG_LOCAL(t_ty,"ty")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<353>");
	Float t_ix2=t_ix*bb_graphics_context->m_ix+t_iy*bb_graphics_context->m_jx;
	DBG_LOCAL(t_ix2,"ix2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<354>");
	Float t_iy2=t_ix*bb_graphics_context->m_iy+t_iy*bb_graphics_context->m_jy;
	DBG_LOCAL(t_iy2,"iy2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<355>");
	Float t_jx2=t_jx*bb_graphics_context->m_ix+t_jy*bb_graphics_context->m_jx;
	DBG_LOCAL(t_jx2,"jx2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<356>");
	Float t_jy2=t_jx*bb_graphics_context->m_iy+t_jy*bb_graphics_context->m_jy;
	DBG_LOCAL(t_jy2,"jy2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<357>");
	Float t_tx2=t_tx*bb_graphics_context->m_ix+t_ty*bb_graphics_context->m_jx+bb_graphics_context->m_tx;
	DBG_LOCAL(t_tx2,"tx2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<358>");
	Float t_ty2=t_tx*bb_graphics_context->m_iy+t_ty*bb_graphics_context->m_jy+bb_graphics_context->m_ty;
	DBG_LOCAL(t_ty2,"ty2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<359>");
	bb_graphics_SetMatrix(t_ix2,t_iy2,t_jx2,t_jy2,t_tx2,t_ty2);
	return 0;
}
int bb_graphics_Transform2(Array<Float > t_m){
	DBG_ENTER("Transform")
	DBG_LOCAL(t_m,"m")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<349>");
	bb_graphics_Transform(t_m.At(0),t_m.At(1),t_m.At(2),t_m.At(3),t_m.At(4),t_m.At(5));
	return 0;
}
int bb_graphics_Translate(Float t_x,Float t_y){
	DBG_ENTER("Translate")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<363>");
	bb_graphics_Transform(FLOAT(1.0),FLOAT(0.0),FLOAT(0.0),FLOAT(1.0),t_x,t_y);
	return 0;
}
int bb_graphics_Rotate(Float t_angle){
	DBG_ENTER("Rotate")
	DBG_LOCAL(t_angle,"angle")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<371>");
	bb_graphics_Transform((Float)cos((t_angle)*D2R),-(Float)sin((t_angle)*D2R),(Float)sin((t_angle)*D2R),(Float)cos((t_angle)*D2R),FLOAT(0.0),FLOAT(0.0));
	return 0;
}
int bb_graphics_Scale(Float t_x,Float t_y){
	DBG_ENTER("Scale")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<367>");
	bb_graphics_Transform(t_x,FLOAT(0.0),FLOAT(0.0),t_y,FLOAT(0.0),FLOAT(0.0));
	return 0;
}
int bb_graphics_PopMatrix(){
	DBG_ENTER("PopMatrix")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<343>");
	int t_sp=bb_graphics_context->m_matrixSp-6;
	DBG_LOCAL(t_sp,"sp")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<344>");
	bb_graphics_SetMatrix(bb_graphics_context->m_matrixStack.At(t_sp+0),bb_graphics_context->m_matrixStack.At(t_sp+1),bb_graphics_context->m_matrixStack.At(t_sp+2),bb_graphics_context->m_matrixStack.At(t_sp+3),bb_graphics_context->m_matrixStack.At(t_sp+4),bb_graphics_context->m_matrixStack.At(t_sp+5));
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<345>");
	bb_graphics_context->m_matrixSp=t_sp;
	return 0;
}
int bb_graphics_DrawImage2(c_Image* t_image,Float t_x,Float t_y,Float t_rotation,Float t_scaleX,Float t_scaleY,int t_frame){
	DBG_ENTER("DrawImage")
	DBG_LOCAL(t_image,"image")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_rotation,"rotation")
	DBG_LOCAL(t_scaleX,"scaleX")
	DBG_LOCAL(t_scaleY,"scaleY")
	DBG_LOCAL(t_frame,"frame")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<467>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<468>");
	if(t_frame<0 || t_frame>=t_image->m_frames.Length()){
		DBG_BLOCK();
		bbError(String(L"Invalid image frame",19));
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<471>");
	c_Frame* t_f=t_image->m_frames.At(t_frame);
	DBG_LOCAL(t_f,"f")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<473>");
	bb_graphics_PushMatrix();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<475>");
	bb_graphics_Translate(t_x,t_y);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<476>");
	bb_graphics_Rotate(t_rotation);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<477>");
	bb_graphics_Scale(t_scaleX,t_scaleY);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<479>");
	bb_graphics_Translate(-t_image->m_tx,-t_image->m_ty);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<481>");
	bb_graphics_context->p_Validate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<483>");
	if((t_image->m_flags&65536)!=0){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<484>");
		bb_graphics_renderDevice->DrawSurface(t_image->m_surface,FLOAT(0.0),FLOAT(0.0));
	}else{
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<486>");
		bb_graphics_renderDevice->DrawSurface2(t_image->m_surface,FLOAT(0.0),FLOAT(0.0),t_f->m_x,t_f->m_y,t_image->m_width,t_image->m_height);
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<489>");
	bb_graphics_PopMatrix();
	return 0;
}
int bb_graphics_DrawText(String t_text,Float t_x,Float t_y,Float t_xalign,Float t_yalign){
	DBG_ENTER("DrawText")
	DBG_LOCAL(t_text,"text")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_xalign,"xalign")
	DBG_LOCAL(t_yalign,"yalign")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<574>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<576>");
	if(!((bb_graphics_context->m_font)!=0)){
		DBG_BLOCK();
		return 0;
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<578>");
	int t_w=bb_graphics_context->m_font->p_Width();
	DBG_LOCAL(t_w,"w")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<579>");
	int t_h=bb_graphics_context->m_font->p_Height();
	DBG_LOCAL(t_h,"h")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<581>");
	t_x-=(Float)floor(Float(t_w*t_text.Length())*t_xalign);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<582>");
	t_y-=(Float)floor(Float(t_h)*t_yalign);
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<584>");
	for(int t_i=0;t_i<t_text.Length();t_i=t_i+1){
		DBG_BLOCK();
		DBG_LOCAL(t_i,"i")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<585>");
		int t_ch=(int)t_text.At(t_i)-bb_graphics_context->m_firstChar;
		DBG_LOCAL(t_ch,"ch")
		DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<586>");
		if(t_ch>=0 && t_ch<bb_graphics_context->m_font->p_Frames()){
			DBG_BLOCK();
			DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<587>");
			bb_graphics_DrawImage(bb_graphics_context->m_font,t_x+Float(t_i*t_w),t_y,t_ch);
		}
	}
	return 0;
}
Float bb_input_MouseX(){
	DBG_ENTER("MouseX")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/input.monkey<54>");
	Float t_=bb_input_device->p_MouseX();
	return t_;
}
Float bb_input_MouseY(){
	DBG_ENTER("MouseY")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/input.monkey<58>");
	Float t_=bb_input_device->p_MouseY();
	return t_;
}
c_List8::c_List8(){
	m__head=((new c_HeadNode8)->m_new());
}
c_List8* c_List8::m_new(){
	DBG_ENTER("List.new")
	c_List8 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node13* c_List8::p_AddLast8(c_CInputArea* t_data){
	DBG_ENTER("List.AddLast")
	c_List8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node13* t_=(new c_Node13)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List8* c_List8::m_new2(Array<c_CInputArea* > t_data){
	DBG_ENTER("List.new")
	c_List8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CInputArea* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CInputArea* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast8(t_t);
	}
	return this;
}
c_Enumerator5* c_List8::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator5* t_=(new c_Enumerator5)->m_new(this);
	return t_;
}
c_Node13* c_List8::p_FirstNode(){
	DBG_ENTER("List.FirstNode")
	c_List8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<62>");
	if(m__head->m__succ!=m__head){
		DBG_BLOCK();
		return m__head->m__succ;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<63>");
	return 0;
}
c_Node13* c_List8::p_AddFirst3(c_CInputArea* t_data){
	DBG_ENTER("List.AddFirst")
	c_List8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<104>");
	c_Node13* t_=(new c_Node13)->m_new(m__head->m__succ,m__head,t_data);
	return t_;
}
void c_List8::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List8::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node13::c_Node13(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node13* c_Node13::m_new(c_Node13* t_succ,c_Node13* t_pred,c_CInputArea* t_data){
	DBG_ENTER("Node.new")
	c_Node13 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node13* c_Node13::m_new2(){
	DBG_ENTER("Node.new")
	c_Node13 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
c_CInputArea* c_Node13::p_Value(){
	DBG_ENTER("Node.Value")
	c_Node13 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<269>");
	return m__data;
}
c_Node13* c_Node13::p_GetNode(){
	DBG_ENTER("Node.GetNode")
	c_Node13 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<301>");
	return this;
}
c_Node13* c_Node13::p_PrevNode(){
	DBG_ENTER("Node.PrevNode")
	c_Node13 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<289>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<291>");
	c_Node13* t_=m__pred->p_GetNode();
	return t_;
}
c_Node13* c_Node13::p_NextNode(){
	DBG_ENTER("Node.NextNode")
	c_Node13 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<282>");
	if(m__succ->m__pred!=this){
		DBG_BLOCK();
		bbError(String(L"Illegal operation on removed node",33));
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<284>");
	c_Node13* t_=m__succ->p_GetNode();
	return t_;
}
void c_Node13::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node13::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode8::c_HeadNode8(){
}
c_HeadNode8* c_HeadNode8::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node13::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
c_Node13* c_HeadNode8::p_GetNode(){
	DBG_ENTER("HeadNode.GetNode")
	c_HeadNode8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<316>");
	return 0;
}
void c_HeadNode8::mark(){
	c_Node13::mark();
}
String c_HeadNode8::debug(){
	String t="(HeadNode)\n";
	t=c_Node13::debug()+t;
	return t;
}
c_Enumerator5::c_Enumerator5(){
	m__list=0;
	m__curr=0;
}
c_Enumerator5* c_Enumerator5::m_new(c_List8* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator5* c_Enumerator5::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator5::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CInputArea* c_Enumerator5::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator5 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CInputArea* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator5::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator5::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
c_Enumerator6::c_Enumerator6(){
	m__list=0;
	m__curr=0;
}
c_Enumerator6* c_Enumerator6::m_new(c_List4* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator6* c_Enumerator6::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator6::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CGlobalEventData* c_Enumerator6::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator6 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CGlobalEventData* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator6::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator6::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
int bb_input_MouseDown(int t_button){
	DBG_ENTER("MouseDown")
	DBG_LOCAL(t_button,"button")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/input.monkey<62>");
	int t_=((bb_input_device->p_KeyDown(1+t_button))?1:0);
	return t_;
}
c_List9::c_List9(){
	m__head=((new c_HeadNode9)->m_new());
}
c_List9* c_List9::m_new(){
	DBG_ENTER("List.new")
	c_List9 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node14* c_List9::p_AddLast9(c_CMenuComponent* t_data){
	DBG_ENTER("List.AddLast")
	c_List9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node14* t_=(new c_Node14)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List9* c_List9::m_new2(Array<c_CMenuComponent* > t_data){
	DBG_ENTER("List.new")
	c_List9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CMenuComponent* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CMenuComponent* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast9(t_t);
	}
	return this;
}
c_Enumerator7* c_List9::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator7* t_=(new c_Enumerator7)->m_new(this);
	return t_;
}
void c_List9::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List9::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node14::c_Node14(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node14* c_Node14::m_new(c_Node14* t_succ,c_Node14* t_pred,c_CMenuComponent* t_data){
	DBG_ENTER("Node.new")
	c_Node14 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node14* c_Node14::m_new2(){
	DBG_ENTER("Node.new")
	c_Node14 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
void c_Node14::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node14::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode9::c_HeadNode9(){
}
c_HeadNode9* c_HeadNode9::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node14::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode9::mark(){
	c_Node14::mark();
}
String c_HeadNode9::debug(){
	String t="(HeadNode)\n";
	t=c_Node14::debug()+t;
	return t;
}
c_Enumerator7::c_Enumerator7(){
	m__list=0;
	m__curr=0;
}
c_Enumerator7* c_Enumerator7::m_new(c_List9* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator7* c_Enumerator7::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator7::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CMenuComponent* c_Enumerator7::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator7 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CMenuComponent* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator7::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator7::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
c_List10::c_List10(){
	m__head=((new c_HeadNode10)->m_new());
}
c_List10* c_List10::m_new(){
	DBG_ENTER("List.new")
	c_List10 *self=this;
	DBG_LOCAL(self,"Self")
	return this;
}
c_Node15* c_List10::p_AddLast10(c_CUnit* t_data){
	DBG_ENTER("List.AddLast")
	c_List10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<108>");
	c_Node15* t_=(new c_Node15)->m_new(m__head,m__head->m__pred,t_data);
	return t_;
}
c_List10* c_List10::m_new2(Array<c_CUnit* > t_data){
	DBG_ENTER("List.new")
	c_List10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<13>");
	Array<c_CUnit* > t_=t_data;
	int t_2=0;
	while(t_2<t_.Length()){
		DBG_BLOCK();
		c_CUnit* t_t=t_.At(t_2);
		t_2=t_2+1;
		DBG_LOCAL(t_t,"t")
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<14>");
		p_AddLast10(t_t);
	}
	return this;
}
c_Enumerator8* c_List10::p_ObjectEnumerator(){
	DBG_ENTER("List.ObjectEnumerator")
	c_List10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<186>");
	c_Enumerator8* t_=(new c_Enumerator8)->m_new(this);
	return t_;
}
void c_List10::mark(){
	Object::mark();
	gc_mark_q(m__head);
}
String c_List10::debug(){
	String t="(List)\n";
	t+=dbg_decl("_head",&m__head);
	return t;
}
c_Node15::c_Node15(){
	m__succ=0;
	m__pred=0;
	m__data=0;
}
c_Node15* c_Node15::m_new(c_Node15* t_succ,c_Node15* t_pred,c_CUnit* t_data){
	DBG_ENTER("Node.new")
	c_Node15 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_succ,"succ")
	DBG_LOCAL(t_pred,"pred")
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<261>");
	gc_assign(m__succ,t_succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<262>");
	gc_assign(m__pred,t_pred);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<263>");
	gc_assign(m__succ->m__pred,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<264>");
	gc_assign(m__pred->m__succ,this);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<265>");
	gc_assign(m__data,t_data);
	return this;
}
c_Node15* c_Node15::m_new2(){
	DBG_ENTER("Node.new")
	c_Node15 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<258>");
	return this;
}
void c_Node15::mark(){
	Object::mark();
	gc_mark_q(m__succ);
	gc_mark_q(m__pred);
	gc_mark_q(m__data);
}
String c_Node15::debug(){
	String t="(Node)\n";
	t+=dbg_decl("_succ",&m__succ);
	t+=dbg_decl("_pred",&m__pred);
	t+=dbg_decl("_data",&m__data);
	return t;
}
c_HeadNode10::c_HeadNode10(){
}
c_HeadNode10* c_HeadNode10::m_new(){
	DBG_ENTER("HeadNode.new")
	c_HeadNode10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<310>");
	c_Node15::m_new2();
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<311>");
	gc_assign(m__succ,(this));
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<312>");
	gc_assign(m__pred,(this));
	return this;
}
void c_HeadNode10::mark(){
	c_Node15::mark();
}
String c_HeadNode10::debug(){
	String t="(HeadNode)\n";
	t=c_Node15::debug()+t;
	return t;
}
c_Enumerator8::c_Enumerator8(){
	m__list=0;
	m__curr=0;
}
c_Enumerator8* c_Enumerator8::m_new(c_List10* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator8* c_Enumerator8::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator8::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_CUnit* c_Enumerator8::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator8 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_CUnit* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator8::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator8::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
int bb_Misc_Round(Float t_Value){
	DBG_ENTER("Round")
	DBG_LOCAL(t_Value,"Value")
	DBG_INFO("C:/MonkeyX77a/Misc.monkey<49>");
	int t_=int(t_Value+FLOAT(0.49999997));
	return t_;
}
int bb_graphics_DrawLine(Float t_x1,Float t_y1,Float t_x2,Float t_y2){
	DBG_ENTER("DrawLine")
	DBG_LOCAL(t_x1,"x1")
	DBG_LOCAL(t_y1,"y1")
	DBG_LOCAL(t_x2,"x2")
	DBG_LOCAL(t_y2,"y2")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<399>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<401>");
	bb_graphics_context->p_Validate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<402>");
	bb_graphics_renderDevice->DrawLine(t_x1,t_y1,t_x2,t_y2);
	return 0;
}
c_Enumerator9::c_Enumerator9(){
	m__deque=0;
	m__index=-1;
}
c_Enumerator9* c_Enumerator9::m_new(c_Deque5* t_deque){
	DBG_ENTER("Enumerator.new")
	c_Enumerator9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_deque,"deque")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<162>");
	gc_assign(m__deque,t_deque);
	return this;
}
c_Enumerator9* c_Enumerator9::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<159>");
	return this;
}
bool c_Enumerator9::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<166>");
	bool t_=m__index<m__deque->p_Length2()-1;
	return t_;
}
c_CStateTransition* c_Enumerator9::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator9 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<170>");
	m__index+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<171>");
	c_CStateTransition* t_=m__deque->p_Get(m__index);
	return t_;
}
void c_Enumerator9::mark(){
	Object::mark();
	gc_mark_q(m__deque);
}
String c_Enumerator9::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_deque",&m__deque);
	t+=dbg_decl("_index",&m__index);
	return t;
}
c_Enumerator10::c_Enumerator10(){
	m__deque=0;
	m__index=-1;
}
c_Enumerator10* c_Enumerator10::m_new(c_Deque4* t_deque){
	DBG_ENTER("Enumerator.new")
	c_Enumerator10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_deque,"deque")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<162>");
	gc_assign(m__deque,t_deque);
	return this;
}
c_Enumerator10* c_Enumerator10::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<159>");
	return this;
}
bool c_Enumerator10::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<166>");
	bool t_=m__index<m__deque->p_Length2()-1;
	return t_;
}
c_CFloatRangeMiniCondition* c_Enumerator10::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator10 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<170>");
	m__index+=1;
	DBG_INFO("C:/MonkeyX77a/modules/monkey/deque.monkey<171>");
	c_CFloatRangeMiniCondition* t_=m__deque->p_Get(m__index);
	return t_;
}
void c_Enumerator10::mark(){
	Object::mark();
	gc_mark_q(m__deque);
}
String c_Enumerator10::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_deque",&m__deque);
	t+=dbg_decl("_index",&m__index);
	return t;
}
int bb_math_Abs(int t_x){
	DBG_ENTER("Abs")
	DBG_LOCAL(t_x,"x")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<46>");
	if(t_x>=0){
		DBG_BLOCK();
		return t_x;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<47>");
	int t_=-t_x;
	return t_;
}
Float bb_math_Abs2(Float t_x){
	DBG_ENTER("Abs")
	DBG_LOCAL(t_x,"x")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<73>");
	if(t_x>=FLOAT(0.0)){
		DBG_BLOCK();
		return t_x;
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/math.monkey<74>");
	Float t_=-t_x;
	return t_;
}
c_Enumerator11::c_Enumerator11(){
	m__list=0;
	m__curr=0;
}
c_Enumerator11* c_Enumerator11::m_new(c_List6* t_list){
	DBG_ENTER("Enumerator.new")
	c_Enumerator11 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_LOCAL(t_list,"list")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<326>");
	gc_assign(m__list,t_list);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<327>");
	gc_assign(m__curr,t_list->m__head->m__succ);
	return this;
}
c_Enumerator11* c_Enumerator11::m_new2(){
	DBG_ENTER("Enumerator.new")
	c_Enumerator11 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<323>");
	return this;
}
bool c_Enumerator11::p_HasNext(){
	DBG_ENTER("Enumerator.HasNext")
	c_Enumerator11 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<331>");
	while(m__curr->m__succ->m__pred!=m__curr){
		DBG_BLOCK();
		DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<332>");
		gc_assign(m__curr,m__curr->m__succ);
	}
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<334>");
	bool t_=m__curr!=m__list->m__head;
	return t_;
}
c_IVector* c_Enumerator11::p_NextObject(){
	DBG_ENTER("Enumerator.NextObject")
	c_Enumerator11 *self=this;
	DBG_LOCAL(self,"Self")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<338>");
	c_IVector* t_data=m__curr->m__data;
	DBG_LOCAL(t_data,"data")
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<339>");
	gc_assign(m__curr,m__curr->m__succ);
	DBG_INFO("C:/MonkeyX77a/modules/monkey/list.monkey<340>");
	return t_data;
}
void c_Enumerator11::mark(){
	Object::mark();
	gc_mark_q(m__list);
	gc_mark_q(m__curr);
}
String c_Enumerator11::debug(){
	String t="(Enumerator)\n";
	t+=dbg_decl("_list",&m__list);
	t+=dbg_decl("_curr",&m__curr);
	return t;
}
int bb_graphics_DrawCircle(Float t_x,Float t_y,Float t_r){
	DBG_ENTER("DrawCircle")
	DBG_LOCAL(t_x,"x")
	DBG_LOCAL(t_y,"y")
	DBG_LOCAL(t_r,"r")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<415>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<417>");
	bb_graphics_context->p_Validate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<418>");
	bb_graphics_renderDevice->DrawOval(t_x-t_r,t_y-t_r,t_r*FLOAT(2.0),t_r*FLOAT(2.0));
	return 0;
}
int bb_graphics_DrawPoly(Array<Float > t_verts){
	DBG_ENTER("DrawPoly")
	DBG_LOCAL(t_verts,"verts")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<431>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<433>");
	bb_graphics_context->p_Validate();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<434>");
	bb_graphics_renderDevice->DrawPoly(t_verts);
	return 0;
}
int bb_graphics_DrawPoly2(Array<Float > t_verts,c_Image* t_image,int t_frame){
	DBG_ENTER("DrawPoly")
	DBG_LOCAL(t_verts,"verts")
	DBG_LOCAL(t_image,"image")
	DBG_LOCAL(t_frame,"frame")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<439>");
	bb_graphics_DebugRenderDevice();
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<440>");
	if(t_frame<0 || t_frame>=t_image->m_frames.Length()){
		DBG_BLOCK();
		bbError(String(L"Invalid image frame",19));
	}
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<442>");
	c_Frame* t_f=t_image->m_frames.At(t_frame);
	DBG_LOCAL(t_f,"f")
	DBG_INFO("C:/MonkeyX77a/modules/mojo/graphics.monkey<443>");
	bb_graphics_renderDevice->DrawPoly2(t_verts,t_image->m_surface,t_f->m_x,t_f->m_y);
	return 0;
}
int bbInit(){
	GC_CTOR
	bb_app__app=0;
	DBG_GLOBAL("_app",&bb_app__app);
	bb_app__delegate=0;
	DBG_GLOBAL("_delegate",&bb_app__delegate);
	bb_app__game=BBGame::Game();
	DBG_GLOBAL("_game",&bb_app__game);
	bb_graphics_device=0;
	DBG_GLOBAL("device",&bb_graphics_device);
	bb_graphics_context=(new c_GraphicsContext)->m_new();
	DBG_GLOBAL("context",&bb_graphics_context);
	c_Image::m_DefaultFlags=0;
	DBG_GLOBAL("DefaultFlags",&c_Image::m_DefaultFlags);
	bb_audio_device=0;
	DBG_GLOBAL("device",&bb_audio_device);
	bb_input_device=0;
	DBG_GLOBAL("device",&bb_input_device);
	bb_graphics_renderDevice=0;
	DBG_GLOBAL("renderDevice",&bb_graphics_renderDevice);
	bb_random_Seed=1234;
	DBG_GLOBAL("Seed",&bb_random_Seed);
	bb_app__updateRate=0;
	DBG_GLOBAL("_updateRate",&bb_app__updateRate);
	c_CAIManager::m_TerrainDescriptionArray=Array<int >();
	DBG_GLOBAL("TerrainDescriptionArray",&c_CAIManager::m_TerrainDescriptionArray);
	c_CAIManager::m_TerrainWidth=30;
	DBG_GLOBAL("TerrainWidth",&c_CAIManager::m_TerrainWidth);
	c_CAIManager::m_TerrainHeight=18;
	DBG_GLOBAL("TerrainHeight",&c_CAIManager::m_TerrainHeight);
	c_CAIManager::m_TerrainTileCount=0;
	DBG_GLOBAL("TerrainTileCount",&c_CAIManager::m_TerrainTileCount);
	c_CAIManager::m_PathfindingMap=Array<c_PathfindingMapItem* >();
	DBG_GLOBAL("PathfindingMap",&c_CAIManager::m_PathfindingMap);
	c_CAIManager::m_NeighbourArray=Array<c_IVector* >(8);
	DBG_GLOBAL("NeighbourArray",&c_CAIManager::m_NeighbourArray);
	c_CAIManager::m_OpenListItemPool=Array<c_COpenListItem* >();
	DBG_GLOBAL("OpenListItemPool",&c_CAIManager::m_OpenListItemPool);
	c_CAIManager::m_TileStatusArray=Array<c_CTileStatusData* >();
	DBG_GLOBAL("TileStatusArray",&c_CAIManager::m_TileStatusArray);
	c_CSpawnManager::m_HumanAnimationSet=(new c_IntMap)->m_new();
	DBG_GLOBAL("HumanAnimationSet",&c_CSpawnManager::m_HumanAnimationSet);
	c_CSpawnManager::m_OrcAnimationSet=(new c_IntMap)->m_new();
	DBG_GLOBAL("OrcAnimationSet",&c_CSpawnManager::m_OrcAnimationSet);
	c_Stack::m_NIL=0;
	DBG_GLOBAL("NIL",&c_Stack::m_NIL);
	c_CAnimationManager::m_AnimationMap=(new c_IntMap2)->m_new();
	DBG_GLOBAL("AnimationMap",&c_CAnimationManager::m_AnimationMap);
	c_CAIManager::m_TileSize=32;
	DBG_GLOBAL("TileSize",&c_CAIManager::m_TileSize);
	c_CAIManager::m_MapScale=FLOAT(1.5);
	DBG_GLOBAL("MapScale",&c_CAIManager::m_MapScale);
	c_CGameObject::m_GameObjectCount=0;
	DBG_GLOBAL("GameObjectCount",&c_CGameObject::m_GameObjectCount);
	c_CAIManager::m_PathQueue=(new c_Deque2)->m_new();
	DBG_GLOBAL("PathQueue",&c_CAIManager::m_PathQueue);
	c_Deque2::m_NIL=0;
	DBG_GLOBAL("NIL",&c_Deque2::m_NIL);
	c_CAIManager::m_OpenList=(new c_COpenListItemList)->m_new();
	DBG_GLOBAL("OpenList",&c_CAIManager::m_OpenList);
	c_CAIManager::m_OpenListItemPoolCount=0;
	DBG_GLOBAL("OpenListItemPoolCount",&c_CAIManager::m_OpenListItemPoolCount);
	c_CSpawnManager::m_SpawnRequestQueue=(new c_Deque3)->m_new();
	DBG_GLOBAL("SpawnRequestQueue",&c_CSpawnManager::m_SpawnRequestQueue);
	c_Deque3::m_NIL=0;
	DBG_GLOBAL("NIL",&c_Deque3::m_NIL);
	c_Deque::m_NIL=0;
	DBG_GLOBAL("NIL",&c_Deque::m_NIL);
	c_CAIManager::m_DisplayDebug=false;
	DBG_GLOBAL("DisplayDebug",&c_CAIManager::m_DisplayDebug);
	c_CInputArea::m_DebugDisplay=true;
	DBG_GLOBAL("DebugDisplay",&c_CInputArea::m_DebugDisplay);
	c_CMovement::m_DebugPath=true;
	DBG_GLOBAL("DebugPath",&c_CMovement::m_DebugPath);
	c_CMovement::m_DebugTargetPathNode=true;
	DBG_GLOBAL("DebugTargetPathNode",&c_CMovement::m_DebugTargetPathNode);
	c_CMovement::m_DebugUnitID=true;
	DBG_GLOBAL("DebugUnitID",&c_CMovement::m_DebugUnitID);
	c_CMovement::m_DebugPathStatus=true;
	DBG_GLOBAL("DebugPathStatus",&c_CMovement::m_DebugPathStatus);
	c_CMovement::m_DebugState=true;
	DBG_GLOBAL("DebugState",&c_CMovement::m_DebugState);
	return 0;
}
void gc_mark(){
	gc_mark_q(bb_app__app);
	gc_mark_q(bb_app__delegate);
	gc_mark_q(bb_graphics_device);
	gc_mark_q(bb_graphics_context);
	gc_mark_q(bb_audio_device);
	gc_mark_q(bb_input_device);
	gc_mark_q(bb_graphics_renderDevice);
	gc_mark_q(c_CAIManager::m_TerrainDescriptionArray);
	gc_mark_q(c_CAIManager::m_PathfindingMap);
	gc_mark_q(c_CAIManager::m_NeighbourArray);
	gc_mark_q(c_CAIManager::m_OpenListItemPool);
	gc_mark_q(c_CAIManager::m_TileStatusArray);
	gc_mark_q(c_CSpawnManager::m_HumanAnimationSet);
	gc_mark_q(c_CSpawnManager::m_OrcAnimationSet);
	gc_mark_q(c_Stack::m_NIL);
	gc_mark_q(c_CAnimationManager::m_AnimationMap);
	gc_mark_q(c_CAIManager::m_PathQueue);
	gc_mark_q(c_Deque2::m_NIL);
	gc_mark_q(c_CAIManager::m_OpenList);
	gc_mark_q(c_CSpawnManager::m_SpawnRequestQueue);
	gc_mark_q(c_Deque3::m_NIL);
	gc_mark_q(c_Deque::m_NIL);
}
//${TRANSCODE_END}

int main( int argc,const char *argv[] ){

	BBMonkeyGame::Main( argc,argv );
}
