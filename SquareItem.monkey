Import Renderer

Class CSquareItem Extends CRenderItem	
	Field Vertices 		: Float[ 8 ]
	
	Method New( ID : Int, Z : Float )
		Super.New( ID, Z )
		
		Self.Vertices[ 0 ] = -0.5
		Self.Vertices[ 1 ] = 0.5
		Self.Vertices[ 2 ] = 0.5
		Self.Vertices[ 3 ] = 0.5
		Self.Vertices[ 4 ] = 0.5
		Self.Vertices[ 5 ] = -0.5
		Self.Vertices[ 6 ] = -0.5
		Self.Vertices[ 7 ] = -0.5
	End
	
	Method Render()
			PushMatrix()
			SetColor( Self.Color.R, Self.Color.G, Self.Color.B )
			Translate( Self.Position.X, Self.Position.Y )
			Scale( Self.Scale_.X, Self.Scale_.Y )
			DrawPoly( Self.Vertices )
			PopMatrix()
	End
End