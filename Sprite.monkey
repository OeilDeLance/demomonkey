Import mojo
Import Renderer
Import AnimationManager

Class CSprite Extends CRenderItem
	Field CurrentAnimation 	: CAnimation
	Field Time 				: Float = 0.0
	
	Field ImageToDisplay 	: CImage
	
	Method New( ID : Int, Z : Float )
		Super.New( ID, Z )
	End
	
	Method PlayAnim( AnimName : Int )
		If AnimName <> -1
			Self.CurrentAnimation = CAnimationManager.GetAnimation( AnimName )
			'Print( "Playing anim " + AnimName + " on sprite " + Self.ID )
		End
	End
	
	Method Update( DeltaTime: Float )
		If Self.CurrentAnimation
			Self.Time += DeltaTime
			If Self.Time >= Self.CurrentAnimation.Duration
				Self.Time = Self.Time - Self.CurrentAnimation.Duration
			End
			Local KeyframeIndex : Int = Self.Time / Self.CurrentAnimation.TimePerKeyframe
			Self.ImageToDisplay = CurrentAnimation.KeyframeList.Get( KeyframeIndex )
		End
	End
	
	Method Render()
		If Self.ImageToDisplay
			'Print( "Render " + Self.ID  )
			PushMatrix()
			SetColor( Self.Color.R, Self.Color.G, Self.Color.B )
			Translate( Self.Position.X, Self.Position.Y )
			Local LocalScale := New FVector( Self.Scale_.X, Self.Scale_.Y )
			If Self.ImageToDisplay.MirrorVertically
				LocalScale.X *= -1.0
			End
			Scale( LocalScale.X, LocalScale.Y )
			DrawImage( Self.ImageToDisplay.Img, 0.0 , 0.0, Self.ImageToDisplay.SpriteIndex )
			PopMatrix()
		End
	End
	Method ProcessCommand( Command : CCommand )
		Super.ProcessCommand( Command )
	
		Select Command.Type
			Case CommandType_PlayAnimation
				PlayAnim( Command.ParamIntA )
		End
	End
End