Import mojo
Import AnimationManager
Import GameObject
Import AIManager
Import CommandBuffer
Import Renderer
Import MenuComponent
Import MenuUpdater
Import Unit
Import InputArea
Import InputAreaUpdater
Import MapInterface
Import SpawnManager
Import UnitUpdater

'hack disable texture filters only works with win32
#MOJO_IMAGE_FILTERING_ENABLED = "false"

Class CMyApp Extends App

	Field FramesPerSecond : Int = 30
	Field DeltaTime : Float
	
	Field PreviousKeyPDown 		:= 0
	Field PreviousKeyMinusDown 	:= 0
	Field PreviousKeyPlusDown 	:= 0
	Field TimeMult				:= 1.0
	
	'Handle on the terrain resource
	Field TerrainImage : Image
	
	
	Field TerrainSpriteIndexArray 	: Int[]
	Field SpriteScale 				: Float = 1.5
	
	'Gameobject list to render and upate
	Field GameObjectList : List< CGameObject > = New List< CGameObject >() 
	
	Field CommandBuffer : CCommandBuffer 	= New CCommandBuffer()
	Field Renderer 		: CRenderer 		= New CRenderer()
	
	Field ComponentUpdaterList 	: List< CComponentUpdater > = New List< CComponentUpdater >()
	
	
	Method OnCreate()
		'Making sure the function Rnd returns different random number at every execution
		Local date := GetDate()
		Seed = date[ 5 ]
		
		
		SetUpdateRate( Self.FramesPerSecond )
		Self.DeltaTime = 1.0 / Self.FramesPerSecond
		
		Self.TerrainImage = LoadImage( "Terrain.png", 32, 32, 16 )
		CAIManager.Init()
		CSpawnManager.Init()
		
		Self.ComponentUpdaterList.AddLast( New CInputAreaUpdater() )
		Self.ComponentUpdaterList.AddLast( New CMenuUpdater() )
		Self.ComponentUpdaterList.AddLast( New CUnitUpdater() )
		
		'Initialising Sprite Index array
		Self.TerrainSpriteIndexArray = New Int[ CAIManager.TerrainTileCount ]
		For Local i : Int = 0 Until CAIManager.TerrainTileCount
			If CAIManager.TerrainDescriptionArray[ i ] = 0
				Local FrameOffset : Int = Rnd( 0, 7 )
				Self.TerrainSpriteIndexArray[ i ] = FrameOffset
			Else
				Self.TerrainSpriteIndexArray[ i ] = 12
			End
		End
		
		'Initialising AnimationManager
		CAnimationManager.Init()
		
		Local MainInputAreaSize 		:= New FVector( CAIManager.TerrainWidth * CAIManager.TileSize * CAIManager.MapScale, CAIManager.TerrainHeight * CAIManager.TileSize * CAIManager.MapScale )
		Local MainInputAreaPosition 	:= New FVector( MainInputAreaSize.X * 0.5, MainInputAreaSize.Y * 0.5 )
		Local MapInterfaceGameObject 	: CGameObject = New CGameObject()
		MapInterfaceGameObject.AddComponent( New CInputArea( MainInputAreaPosition, MainInputAreaSize, InputAreaPriority_Map ) )
		Local MapInterface := New CMapInterface(  )
		MapInterfaceGameObject.AddComponent( MapInterface )
		Self.AddGameObject( MapInterfaceGameObject )
		
		
		
		SpawnMenu( New FVector( 50.0, 45.0 ), Anim_MenuSpawnHumanIdle, MenuID_HumanFootman, MapInterfaceEvent_HumanFootmanMenuActivated, MapInterface )
		SpawnMenu( New FVector( 50.0, 125.0 ), Anim_MenuSpawnOrcIdle, MenuID_OrcFootman, MapInterfaceEvent_OrcFootmanMenuActivated, MapInterface )
		
		Local MapRenderItemId := Self.CommandBuffer.AddCommand_CreateRenderItem( CommandType_CreateMap, 0.0 )
		Self.CommandBuffer.AddCommand( MapRenderItemId, CommandType_UpdateScale, CAIManager.MapScale, CAIManager.MapScale )		
	End
	
	Method SpawnMenu( MenuCenter : FVector, AnimID : Int, MenuID : Int, GlobalEventID : Int, MapInterface : CMapInterface )
		Local SpawnMenu : CGameObject = New CGameObject()
		Local MenuComponent := New CMenuComponent( MenuCenter, AnimID, MenuID )
		SpawnMenu.AddComponent( MenuComponent )
		Local InputArea := New CInputArea( MenuCenter, New FVector( 48.0 * 1.5, 40.0 * 1.5 ), InputAreaPriority_InGameMenus )
		InputArea.BindEvents( InputAreaEvent_OnActivated, GlobalEventID, MapInterface )
		InputArea.BindEvents( InputAreaEvent_OnHoveredStart, MenuEvent_SetHighlight, MenuComponent )
		InputArea.BindEvents( InputAreaEvent_OnHoveredEnd, MenuEvent_SetNeutral, MenuComponent )
		InputArea.BindEvents( InputAreaEvent_OnActivated, MenuEvent_SetSelect, MenuComponent )
		SpawnMenu.AddComponent( InputArea )
		Self.AddGameObject( SpawnMenu )
	End
	
	Method OnUpdate()
		Local KeyPDown := KeyDown( KEY_P )
		If KeyPDown And Self.PreviousKeyPDown = False
			If Self.DeltaTime > 0.0
				Self.DeltaTime = 0.0
			Else
				Self.DeltaTime = 1.0 / Self.FramesPerSecond
			End
		End
		Self.PreviousKeyPDown = KeyPDown
		
		Local KeyMinusDown := KeyDown( KEY_NUMSUBTRACT )
		If KeyMinusDown And Self.PreviousKeyMinusDown = False
			Self.TimeMult *= 0.5
			Self.DeltaTime = ( 1.0 / Self.FramesPerSecond ) * Self.TimeMult 
		End
		Self.PreviousKeyMinusDown = KeyMinusDown
		
		Local KeyPlusDown := KeyDown( KEY_NUMADD )
		If KeyPlusDown And Self.PreviousKeyPlusDown = False
			Self.TimeMult *= 2.0
			Self.DeltaTime = ( 1.0 / Self.FramesPerSecond ) * Self.TimeMult 
		End
		Self.PreviousKeyPlusDown = KeyPlusDown
	
		For Local GameObject : CGameObject = Eachin Self.GameObjectList
			GameObject.Update( Self.DeltaTime, Self.CommandBuffer )
		End
		
		For Local ComponentUpdater := Eachin Self.ComponentUpdaterList
			ComponentUpdater.Update( Self.CommandBuffer )
		End
		
		CAIManager.Update( Self.DeltaTime )
		
		CSpawnManager.Update( Self, Self.CommandBuffer )
		
		If Self.Renderer
			Self.Renderer.Update( Self.DeltaTime, Self.CommandBuffer )
		End
	End
	
	Method OnRender()
		
		If Self.Renderer
			Self.Renderer.Render( Self.GameObjectList )
		End
		
	End
	
	Method AddGameObject( GameObject : CGameObject )
		For Local Component := Eachin GameObject.ComponentList 
			For Local ComponentUpdater := Eachin Self.ComponentUpdaterList 
				If ComponentUpdater.ComponentType = Component.Type
					ComponentUpdater.AddComponent( Component )
				End
			End
		End
		Self.GameObjectList.AddLast( GameObject )
	End
End

Function Main()
	New CMyApp()
End

