Import mojo
Import AIManager
Import GameObject
Import GameGlobals
Import Misc
Import AnimationGraph

Const MovementState_Idle 		: Int = 0
Const MovementState_Moving 		: Int = 1
Const MovementState_Waiting 	: Int = 2
Const MovementState_Canceling 	: Int = 3

Const PathUpdateResult_None 			: Int = 0
Const PathUpdateResult_PathRefreshed 	: Int = 1

Class CMovement Extends CComponent
	Field State							:= MovementState_Idle
	Field CurrentPosition				: FVector	= New FVector( )	
	Field Init							: Bool 		= False
	
	
	
	Field LastPathUpdateResult		:= PathUpdateResult_None
	Field MoveTargetSet 			:= False
	Field HasAPath 					: Bool 		= False
	Field DestinationPosition		: IVector 	= New IVector()
	Field PathRequested				: Bool 		= False
	Field CurrentPath				: CPath		= New CPath()
	Field SecondaryPath				: CPath		= New CPath()
	Field WaitingForSecondaryPath 	: Bool 		= False
	Field CurrentlyOccupiedTile		: IVector	= New IVector()
	
	Field TargetPathNode	: list.Node< IVector >
	
	Field SpriteID			: Int 		= -1
	Field CurrentAnimID		: Int 		= -1
	Field PreviousPosition	: FVector 	= New FVector()
	
	Field AnimationSet		: IntMap< Int >
	Field Orientation		:= 0.0
	Field AnimationGraph	: CAnimationGraph
	
	Global DebugPath			: Bool 		= True
	Global DebugTargetPathNode	: Bool 		= True
	Global DebugUnitID 			: Bool 		= True
	Global DebugPathStatus		: Bool 		= True
	Global DebugState			: Bool 		= True
	
	Method New( SpriteID : Int, Position : FVector, AnimationSet : IntMap< Int >, AnimationGraph : CAnimationGraph )
		Super.New( ComponentType_Movement )
		CurrentPosition.X 	= Position.X
		CurrentPosition.Y 	= Position.Y
		Self.AnimationSet 	= AnimationSet
		Self.AnimationGraph	= AnimationGraph
	
		Self.SpriteID		= SpriteID
		
	End
	
	Method RequestMoveTo( DestinationPosition : IVector )
		'Print( "Request" )
		Self.DestinationPosition.X 		= DestinationPosition.X
		Self.DestinationPosition.Y 		= DestinationPosition.Y
		Self.PathRequested 				= True
		Self.State						= MovementState_Moving
	End
	
	Method CancelMoveTo()
		'Print( "Cancel" )
		Self.State						= MovementState_Canceling
		Self.PathRequested 				= False
	End
	
	Method GetRandValidTerrainIndex : Int()
		Local TargetIsValid = False
		Local RandIndex : Int
		While TargetIsValid = False
			RandIndex = Rnd( 0, CAIManager.TerrainTileCount )
			If CAIManager.TerrainDescriptionArray[ RandIndex ] = 0
				TargetIsValid = True
			End
		End
		Return RandIndex
	End

	Method UpdatePathfindRequest ()
		If Self.PathRequested
			'It is possible that second path is pending if two requests are made in a quick succession
			If Self.WaitingForSecondaryPath = False And Self.SecondaryPath.PathStatus <> PathStatus_Pending
				Local RoundedCurrentPosition : IVector = New IVector( Round( Self.CurrentPosition.X ), Round( Self.CurrentPosition.Y ) )
				'Print( "Rounding " + Self.CurrentPosition.X + " to " + RoundedCurrentPosition.X + " and Rounding " + Self.CurrentPosition.Y + " to " + RoundedCurrentPosition.Y )
				Local DestinationPosition : IVector = New IVector( Self.DestinationPosition.X, Self.DestinationPosition.Y )
				Self.SecondaryPath.Request( RoundedCurrentPosition, DestinationPosition )
				Self.WaitingForSecondaryPath 	= True
				Self.PathRequested				= False
			End
		End
	End
	
	Method CheckAndSwapPaths : Bool ()
		If Self.State = MovementState_Moving And Self.WaitingForSecondaryPath = True And Self.SecondaryPath.PathStatus = PathStatus_Ready
			Self.WaitingForSecondaryPath = False			
			
			Local Path 				:= Self.SecondaryPath
			Self.SecondaryPath 		= Self.CurrentPath
			Self.CurrentPath 		= Path
			Self.HasAPath 			= True
			Return True
		End
		Return False
	End
	
	Method AssertCanOccupy( TilePosition : IVector, ErrorMessage : String )
		Local Index := CAIManager.GetTileIndexFromPosition( TilePosition )
		If CAIManager.TileStatusArray[ Index ].TileStatus = TileStatus_Occupied And CAIManager.TileStatusArray[ Index ].UnitID <> Self.GameObject.ID
			Print( ErrorMessage )
		End
	End
	
	Method MoveToNode : Bool( DeltaTime : Float, Node : list.Node< IVector > )
		
		Local NodePosition 		:= Node.Value
		Local DirectionToNode 	:= New FVector( NodePosition.X - Self.CurrentPosition.X, NodePosition.Y - Self.CurrentPosition.Y )
		Local Length 			:= Sqrt( DirectionToNode.X * DirectionToNode.X + DirectionToNode.Y * DirectionToNode.Y )
		Local NodeReached 		:= False
		If ( Length < 0.001 )
			NodeReached = True
		Else
			DirectionToNode.X = DirectionToNode.X / Length
			DirectionToNode.Y = DirectionToNode.Y / Length
			
			Local Speed : Float = 2.5
			Self.CurrentPosition.X += DirectionToNode.X * DeltaTime * Speed
			Self.CurrentPosition.Y += DirectionToNode.Y * DeltaTime * Speed
			
			Local NewDirectionToNode := New FVector( NodePosition.X - Self.CurrentPosition.X, NodePosition.Y - Self.CurrentPosition.Y )
			Local Dot := DirectionToNode.X * NewDirectionToNode.X + DirectionToNode.Y * NewDirectionToNode.Y
			If Dot <= 0
				NodeReached = True
			End
		End
		If NodeReached
			'Print( Self.UnitID + "Path Length: " + Self.CurrentPath.NodeList.Count() )
			Local NextNode := Node.NextNode()
			If NextNode = Null
				Self.CurrentPosition.X = NodePosition.X
				Self.CurrentPosition.Y = NodePosition.Y
			Else
				Local NextNodePosition 	:= NextNode.Value
				Local NextNodeDirX 		:= NextNodePosition.X - NodePosition.X
				Local NextNodeDirY 		:= NextNodePosition.Y - NodePosition.Y
				Local ExtraDistanceX	:= Self.CurrentPosition.X - NodePosition.X
				Local ExtraDistanceY	:= Self.CurrentPosition.Y - NodePosition.Y
				Local Length 			:= Sqrt( ExtraDistanceX * ExtraDistanceX + ExtraDistanceY * ExtraDistanceY )
				'Print( Length )
				Self.CurrentPosition.X = NodePosition.X + NextNodeDirX * Length
				Self.CurrentPosition.Y = NodePosition.Y + NextNodeDirY * Length
			End
		End
		Return NodeReached
	End
	
	Method FindBestTargetNode : list.Node< IVector > ( Path : CPath )
		' Find First node
		Local TargetPathNode2 : list.Node< IVector > = Null
		
		Local Index 				:= 1
		Local SelectedIndex			:= -1
		Local BestSqDist 			:= MAX_FLOAT
		Local PathNode 				:= Self.CurrentPath.NodeList.FirstNode().NextNode()
		
		'Print( "Self.CurrentPosition.X = " + Self.CurrentPosition.X + " Self.CurrentPosition.Y = " + Self.CurrentPosition.Y )
		
		While PathNode
			'Print( "----------------" )
			Local SegmentNodeA := PathNode.PrevNode()
			Local SegmentNodeB := PathNode
			
			'Print( "SegmentA.X = " + SegmentNodeA.Value.X + " SegmentA.Y = " + SegmentNodeA.Value.Y + " SegmentB.X = " + SegmentNodeB.Value.X + " SegmentB.Y = " + SegmentNodeB.Value.Y )
			
			Local AB 	:= New FVector( SegmentNodeB.Value.X - SegmentNodeA.Value.X, SegmentNodeB.Value.Y - SegmentNodeA.Value.Y )
			Local AC 	:= New FVector( Self.CurrentPosition.X - SegmentNodeA.Value.X, Self.CurrentPosition.Y - SegmentNodeA.Value.Y )
			Local BC 	:= New FVector( Self.CurrentPosition.X - SegmentNodeB.Value.X, Self.CurrentPosition.Y - SegmentNodeB.Value.Y )
			Local e 	:= AC.X * AB.X + AC.Y * AB.Y
			'Print( "Node index = " + Index )
			If e <= 0.0
				' Point is before segment 
				' Only valid if this is first segment
				If 	SegmentNodeA = Self.CurrentPath.NodeList.FirstNode() And Abs( e ) <= 1.0 	' Unit can stray away from path origin because path takes time to generate
					BestSqDist 		= AC.X * AC.X + AC.Y * AC.Y
					TargetPathNode2 = SegmentNodeA ' Unit is before the segment so selecting the start of segment
					SelectedIndex	= Index
					'Print( "Selected with dist = " + BestSqDist )
				End
			Else
				Local f := AB.X * AB.X + AB.Y * AB.Y
				
				If e <= f
					' Here C projects onto AB
					Local SqDist := ( AC.X * AC.X + AC.Y * AC.Y ) - e * e / f
					
					If SqDist < BestSqDist
						BestSqDist 		= SqDist
						TargetPathNode2 	= SegmentNodeB ' Unit is on the segment so selecting the end of segment
						SelectedIndex	= Index
						'Print( "Selected with dist = " + BestSqDist )
					End
				Else
					' Only valid if this is last segment
					Local SqDist := BC.X * BC.X + BC.Y * BC.Y
					If SegmentNodeB = Self.CurrentPath.NodeList.LastNode() And SqDist < BestSqDist
						BestSqDist 		= SqDist
						TargetPathNode2 	= SegmentNodeB ' Unit is before the segment so selecting the start of segment
						SelectedIndex	= Index
					End
				End
			End 
				
			PathNode = PathNode.NextNode()
			Index += 1
		End
		
		Return TargetPathNode2
	End
	
	Method FollowPath( DeltaTime : Float, CommandBuffer : CCommandBuffer )
		UpdatePathfindRequest()
		
		Local PathRefreshed := False
		
		'If suposed to move but no TargetPathNode that means unit is waiting 
		' for request
		If Self.TargetPathNode = Null
			PathRefreshed = CheckAndSwapPaths()
		End
		
		
		'Moving to target if one is selected
		If Self.MoveTargetSet
			If MoveToNode( DeltaTime, Self.TargetPathNode )
				Self.MoveTargetSet = False
				' Once unit has reached its move target checking if fresh path is waiting
				PathRefreshed = CheckAndSwapPaths()
			End
		End	
		
		If Self.State = MovementState_Waiting
			PathRefreshed = CheckAndSwapPaths()
			If PathRefreshed
				Self.MoveTargetSet = False
			End
		End	
		
		' Evaluating next target
		If Self.HasAPath
			Local PotentialNextPathNode : list.Node< IVector >
			' If we reached the move target
			If Self.MoveTargetSet = False
			
				' If we are just starting a new path 
				If PathRefreshed
					' If path was refreshed it is necessary to go through path nodes to find the closest one
					'Self.PathRefreshed = False
					PotentialNextPathNode = FindBestTargetNode( Self.CurrentPath )
					Self.LastPathUpdateResult = PathUpdateResult_None
					
					' If path does not go through unit position:
					If PotentialNextPathNode = Null
						Self.TargetPathNode 			= Null 
						Self.PathRequested 				= True ' Another path will be generated
						Self.HasAPath 					= False
					End
				Else '( Continue using existing path )	
					'If Self.TargetPathNode ' Check if we do have a node selected ( if not this is the first path and unit is waiting for result )
					Select Self.State
						Case MovementState_Moving
							PotentialNextPathNode		= Self.TargetPathNode.NextNode()
							If PotentialNextPathNode = Null
								' End of path reached
								Self.HasAPath 		= False
								Self.TargetPathNode = Null
								Self.State			= MovementState_Idle
							End
						Case MovementState_Canceling
							' End of path reached
							Self.HasAPath 		= False
							Self.TargetPathNode = Null
							Self.State			= MovementState_Idle
						Case MovementState_Waiting
							PotentialNextPathNode = Self.TargetPathNode
					End
					'End		
				End
			End
			
			If PotentialNextPathNode
				Local PotentialNextNodePosition 	: IVector = PotentialNextPathNode.Value
				Local TileStatusData 				:= CAIManager.GetTileStatusData( PotentialNextNodePosition )
				'Local CurrentNodePosition 			: IVector = Self.TargetPathNode.Value
				
				
				' Check if unit can move to the next node
				If TileStatusData.TileStatus = TileStatus_Occupied And TileStatusData.UnitID <> Self.GameObject.ID
					' If not: Mark Tile unit is at as occupied
					
					Local RoundedCurrentPosition : IVector = New IVector( Round( Self.CurrentPosition.X ), Round( Self.CurrentPosition.Y ) )
					AssertCanOccupy( RoundedCurrentPosition, "Error: Waiting" )
					CAIManager.SetTileStatus( Self.CurrentlyOccupiedTile, 	TileStatus_Free )
					CAIManager.SetTileStatus( RoundedCurrentPosition, TileStatus_Occupied, Self.GameObject.ID )
					Self.CurrentlyOccupiedTile.X = RoundedCurrentPosition.X
					Self.CurrentlyOccupiedTile.Y = RoundedCurrentPosition.Y
					
					'Print( "Rounding " + Self.CurrentPosition.X + " to " + RoundedCurrentPosition.X + " and rounding " + Self.CurrentPosition.Y + " to " + RoundedCurrentPosition.Y )
					
					' Force unit position to center the unit on the tile
					' But also enables MoveToNode to return true
					Self.CurrentPosition.X = RoundedCurrentPosition.X
					Self.CurrentPosition.Y = RoundedCurrentPosition.Y
					
					Self.TargetPathNode 		= PotentialNextPathNode
					Self.State					= MovementState_Waiting
				Else
					' Next tile is free					
					AssertCanOccupy( PotentialNextNodePosition, "Error: Next Node" )
					CAIManager.SetTileStatus( Self.CurrentlyOccupiedTile, 	TileStatus_Free )
					CAIManager.SetTileStatus( PotentialNextNodePosition, 	TileStatus_Occupied, Self.GameObject.ID )
					Self.CurrentlyOccupiedTile.X = PotentialNextNodePosition.X
					Self.CurrentlyOccupiedTile.Y = PotentialNextNodePosition.Y
					
					Self.TargetPathNode 		= PotentialNextPathNode
					Self.MoveTargetSet 			= True
					Self.State					= MovementState_Moving		
				End
			End
		End
		
		If Self.State = MovementState_Moving
			Self.AnimationGraph.SetVariable( AnimationVariable_Move, True )
		Else
			Self.AnimationGraph.SetVariable( AnimationVariable_Move, False )
		End
	End
	
	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )
		If Self.Init = False
			Self.Init 		= True
			
			
			Local RoundedCurrentPosition : IVector = New IVector( Self.CurrentPosition.X, Self.CurrentPosition.Y )
			CAIManager.SetTileStatus( RoundedCurrentPosition, TileStatus_Occupied, Self.GameObject.ID )
			Self.CurrentlyOccupiedTile.X = RoundedCurrentPosition.X
			Self.CurrentlyOccupiedTile.Y = RoundedCurrentPosition.Y
						
			Self.CurrentPosition.X = RoundedCurrentPosition.X
			Self.CurrentPosition.Y = RoundedCurrentPosition.Y
		End
		
		
		FollowPath( DeltaTime, CommandBuffer )
		UpdateAnimation( CommandBuffer )
		
		If Self.CurrentPosition.X <> Self.PreviousPosition.X Or Self.CurrentPosition.Y <> Self.PreviousPosition.Y
			CommandBuffer.AddCommand( Self.SpriteID, CommandType_UpdatePosition, ( Self.CurrentPosition.X + 0.5 ) * CAIManager.MapScale * CAIManager.TileSize, ( Self.CurrentPosition.Y + 0.5 ) * CAIManager.MapScale * CAIManager.TileSize )
		End
		Self.PreviousPosition.X = Self.CurrentPosition.X 
		Self.PreviousPosition.Y = Self.CurrentPosition.Y
		
		
	End
	
	Method UpdateAnimation( CommandBuffer : CCommandBuffer )
		
	
		If Self.State <> MovementState_Idle
			Local AnimID : Int = -1
			Local Direction := New FVector( Self.CurrentPosition.X - Self.PreviousPosition.X, Self.CurrentPosition.Y - Self.PreviousPosition.Y )
			Local Length := Sqrt( Direction.X * Direction.X + Direction.Y * Direction.Y )
			If Abs( Length ) < FLT_EPSILON
				'AnimID = Self.AnimationSet.Get( Anim_IdleUp )
			Else
				Direction.X /= Length
				Direction.Y /= Length
				
				Local Angle := Direction.GetOrientationFromNormed()
				Local Slice := 45.0 / 2.0
				If 		1 * Slice < Angle And Angle <= 3 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkDownRight )
				Else If 3 * Slice < Angle And Angle <= 5 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkDown )
				Else If 5 * Slice < Angle And Angle <= 7 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkDownLeft )
				Else If 7 * Slice < Angle Or Angle <= -7 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkLeft )
				Else If -7 * Slice < Angle And Angle <= -5 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkUpLeft )
				Else If -5 * Slice < Angle And Angle <= -3 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkUp )
				Else If -3 * Slice < Angle And Angle <= -1 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkUpRight )
				Else If -1 * Slice < Angle Or Angle <= 1 * Slice
					AnimID = Self.AnimationSet.Get( Anim_WalkRight )
				End
				
				Self.Orientation = Angle
				
			End
			
			'If AnimID <> -1 And Self.CurrentAnimID <> AnimID
			'	CommandBuffer.AddCommand( Self.SpriteID, CommandType_PlayAnimation, AnimID )
			'	Self.CurrentAnimID = AnimID
			'End
			Self.AnimationGraph.SetVariable( AnimationVariable_UnitAngle, Self.Orientation )
		End
	End
	
	Method RenderDebug()
		If DebugPath
			SetColor( 255, 255, 255 )
			If Self.HasAPath
				Local PreviousNode : IVector
				For Local Node : IVector =  Eachin Self.CurrentPath.NodeList
					If PreviousNode
						Local PointA := New FVector( ( PreviousNode.X * CAIManager.TileSize + 0.5 * CAIManager.TileSize ) * CAIManager.MapScale, ( PreviousNode.Y * CAIManager.TileSize + 0.5 * CAIManager.TileSize ) * CAIManager.MapScale )
						Local PointB := New FVector( ( Node.X * CAIManager.TileSize + 0.5 * CAIManager.TileSize ) * CAIManager.MapScale, ( Node.Y * CAIManager.TileSize + 0.5 * CAIManager.TileSize ) * CAIManager.MapScale )
						DrawLine( PointA.X , PointA.Y, PointB.X, PointB.Y )
					End
					PreviousNode = Node
				End
			End
			
		End
		If DebugTargetPathNode And Self.TargetPathNode
			DrawCircle( ( Self.TargetPathNode.Value.X + 0.5 ) * CAIManager.TileSize * CAIManager.MapScale, ( Self.TargetPathNode.Value.Y + 0.5 ) * CAIManager.TileSize * CAIManager.MapScale, 10.0 )
		End
		If DebugUnitID
			DrawText( Self.GameObject.ID, Self.CurrentPosition.X * CAIManager.TileSize * CAIManager.MapScale, Self.CurrentPosition.Y * CAIManager.TileSize * CAIManager.MapScale )
		End
		If DebugPathStatus
			Local PathStatusPosition := New FVector( Self.CurrentPosition.X + 0.5, Self.CurrentPosition.Y )
			SetColor( 0, 255, 0 )
			If Self.SecondaryPath.PathStatus = PathStatus_Pending
				
				SetColor( 255, 0, 0 )
			End
			DrawCircle( PathStatusPosition.X * CAIManager.TileSize * CAIManager.MapScale, PathStatusPosition.Y * CAIManager.TileSize * CAIManager.MapScale, 3.0 )
		End
		If DebugState
			Local StatusPosition := New FVector( Self.CurrentPosition.X, Self.CurrentPosition.Y + 0.25 )
			SetColor( 255, 255, 255 )
			Select Self.State
				Case MovementState_Idle
					DrawText( "I",  StatusPosition.X * CAIManager.TileSize * CAIManager.MapScale, StatusPosition.Y * CAIManager.TileSize * CAIManager.MapScale )
				Case MovementState_Moving
					DrawText( "M",  StatusPosition.X * CAIManager.TileSize * CAIManager.MapScale, StatusPosition.Y * CAIManager.TileSize * CAIManager.MapScale )
				Case MovementState_Waiting
					DrawText( "W",  StatusPosition.X * CAIManager.TileSize * CAIManager.MapScale, StatusPosition.Y * CAIManager.TileSize * CAIManager.MapScale )
				Case MovementState_Canceling
					DrawText( "C",  StatusPosition.X * CAIManager.TileSize * CAIManager.MapScale, StatusPosition.Y * CAIManager.TileSize * CAIManager.MapScale )
			End
		
		End
		Local ThePosition := New FVector( Self.CurrentPosition.X, Self.CurrentPosition.Y + 0.5 )
		DrawText( Self.AnimationGraph.CurrentState.ID,  ThePosition.X * CAIManager.TileSize * CAIManager.MapScale, ThePosition.Y * CAIManager.TileSize * CAIManager.MapScale )
	End
End