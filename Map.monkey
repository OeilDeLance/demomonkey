Import Renderer
Import AIManager
Import mojo

Class CMap Extends CRenderItem
	Field TileCount 		: Int
	Field Width				: Int
	Field Height			: Int
	Field TileWidth			: Int
	Field Img 				: Image
	Field SpriteIndexArray	: Int[]
	
	Method New( ID : Int, Z : Float )
		Super.New( ID, Z )
		'Initialising Sprite Index array
		Self.SpriteIndexArray = New Int[ CAIManager.TerrainTileCount ]
		For Local i : Int = 0 Until CAIManager.TerrainTileCount
			If CAIManager.TerrainDescriptionArray[ i ] = 0
				Local FrameOffset : Int = Rnd( 0, 7 )
				Self.SpriteIndexArray[ i ] = FrameOffset
			Else
				Self.SpriteIndexArray[ i ] = 12
			End
		End
		
		Self.Width 		= CAIManager.TerrainWidth
		Self.Height 	= CAIManager.TerrainHeight
		Self.TileCount 	= Self.Width * Self.Height
		Self.TileWidth	= CAIManager.TileSize
		
		Self.Img = LoadImage( "Terrain.png", 32, 32, 16 )
	End
	Method Render()
		PushMatrix()
		Scale( Self.Scale_.X, Self.Scale_.Y )
		For Local i := 0 Until TileCount 
			DrawImage( Self.Img, ( i Mod Self.Width ) * Self.TileWidth , ( i / Self.Width ) * Self.TileWidth, Self.SpriteIndexArray[ i ] )
		End
		PopMatrix()
	End
End