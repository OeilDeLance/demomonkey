
Const UnitZ 					: Float 	= 0.0
Const UnitHealthBarBackgroundZ 	: Float 	= 1.0
Const UnitHealthBarZ 			: Float 	= 2.0
Const MenuZ 					: Float 	= 100.0


Const MaxCommandCount : Int = 100

Const CommandType_Available 	: Int 	= 0
Const CommandType_CreateSprite 	: Int 	= 1
Const CommandType_CreateMap 	: Int 	= 2
Const CommandType_UpdatePosition: Int 	= 3
Const CommandType_PlayAnimation	: Int 	= 4
Const CommandType_SquareItem	: Int 	= 5
Const CommandType_ChangeColor	: Int 	= 6
Const CommandType_UpdateScale	: Int 	= 7

Class CCommand
	Field Type 			: Int = CommandType_Available
	Field ID 			: Int = -1
	Field ParamFloatA	: Float = 0.0
	Field ParamFloatB	: Float = 0.0
	Field ParamIntA		: Int 	= 0
	Field ParamIntB		: Int 	= 0
	Field ParamIntC		: Int 	= 0
End

Class CCommandBuffer
	Field CommandQueue			: Deque< CCommand > = New Deque< CCommand >()
	Field AvailableCommands 	: CCommand[ MaxCommandCount ]
	Field AvailableCommandCount : Int = MaxCommandCount
	Field LastRenderItemID		: Int = -1
	
	Method New( )
		For Local i : Int = 0 Until MaxCommandCount
			Self.AvailableCommands[ i ] = New CCommand()
		End
	End
	
	Method RecycleCommand( Command : CCommand )
		Self.AvailableCommands[ Self.AvailableCommandCount ] = Command
		Command.Type = CommandType_Available
		Self.AvailableCommandCount += 1
	End
	
	Method GetNewCommand : CCommand()
		If Self.AvailableCommandCount <> 0
			Local Command := Self.AvailableCommands[ Self.AvailableCommandCount - 1 ]
			Self.AvailableCommandCount -= 1
			Return Command
		Else
			Return Null
		End
	End
	
	Method AddCommand( ID : Int, CommandType : Int, ParamFloatA : Float = 0.0, ParamFloatB : Float = 0.0 )
		Local Command := GetNewCommand()
		Command.Type 		= CommandType
		Command.ID	 		= ID
		Command.ParamFloatA	= ParamFloatA
		Command.ParamFloatB	= ParamFloatB
		Self.CommandQueue.PushLast( Command )
	End
	
	Method AddCommand( ID : Int, CommandType : Int, ParamIntA : Int = 0, ParamIntB : Int = 0, ParamIntC : Int = 0 )
		Local Command := GetNewCommand()
		Command.Type 		= CommandType
		Command.ID	 		= ID
		Command.ParamIntA	= ParamIntA
		Command.ParamIntB	= ParamIntB
		Command.ParamIntC	= ParamIntC
		Self.CommandQueue.PushLast( Command )
	End
	
	Method AddCommand_CreateRenderItem : Int( CommandType : Int, Z : Float )
	
		Local Command 	:= Self.GetNewCommand()
		
		Self.LastRenderItemID += 1
		Command.Type 		= CommandType
		Command.ID			= Self.LastRenderItemID
		Command.ParamFloatA	= Z
		
		Self.CommandQueue.PushLast( Command )
		'Print( "New ID Created: " + Command.ID )
		Return Command.ID
	End
	
	Method PopCommand : CCommand( )
		Return Self.CommandQueue.PopFirst()
	End
	
	Method IsEmpty : Bool()
		Return Self.CommandQueue.IsEmpty()
	End
End