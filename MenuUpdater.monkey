Import MenuComponent

Class CMenuUpdater Extends CComponentUpdater
	Field CurrentSelectedMenu 	: CMenuComponent
	Field ComponentList			: List< CMenuComponent >	= New List< CMenuComponent >()
	
	Method New()
		Super.New( ComponentType_Menu )
	End
	
	Method Update( CommandBuffer : CCommandBuffer )
		Local SelectedMenu : CMenuComponent
		For Local MenuComponent := Eachin Self.ComponentList
			Select MenuComponent.RequestState
				Case MenuState_Neutral
					If MenuComponent.State <> MenuState_Select
						CommandBuffer.AddCommand( MenuComponent.HighlightSquareID, CommandType_ChangeColor, MenuComponent.NeutralColor.R, MenuComponent.NeutralColor.G, MenuComponent.NeutralColor.B )
						MenuComponent.State = MenuComponent.RequestState
					End
				Case MenuState_Highlight
					If MenuComponent.State <> MenuState_Select
						CommandBuffer.AddCommand( MenuComponent.HighlightSquareID, CommandType_ChangeColor, MenuComponent.HighlightColor.R, MenuComponent.HighlightColor.G, MenuComponent.HighlightColor.B )
						MenuComponent.State = MenuComponent.RequestState
					End
				Case MenuState_Select
					SelectedMenu = MenuComponent
					CommandBuffer.AddCommand( MenuComponent.HighlightSquareID, CommandType_ChangeColor, MenuComponent.SelectColor.R, MenuComponent.SelectColor.G, MenuComponent.SelectColor.B )
					MenuComponent.State = MenuComponent.RequestState
			End
			MenuComponent.RequestState = MenuState_Invalid
		End
		
		If SelectedMenu
			For Local MenuComponent := Eachin Self.ComponentList
				If MenuComponent <> SelectedMenu
					CommandBuffer.AddCommand( MenuComponent.HighlightSquareID, CommandType_ChangeColor, MenuComponent.NeutralColor.R, MenuComponent.NeutralColor.G, MenuComponent.NeutralColor.B )
					MenuComponent.State = MenuState_Neutral
				End
			End
		End
	End
	
	Method AddComponent( Component : CComponent )
		Self.ComponentList.AddLast( CMenuComponent( Component ) )
	End
End