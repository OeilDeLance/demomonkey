Import main
Import Movement
Import Unit
Import AnimationGraph

Const TemplateID_HumanFootman 	:= 0
Const TemplateID_OrcFootman 	:= 1

Class CSpawnRequestData
	Field TemplateID 	: Int
	Field Position 		: FVector
	
	Method New( TemplateID : Int, Position : FVector )
		Self.TemplateID = TemplateID
		Self.Position 	= Position
	End
End

Class CSpawnManager
	Global SpawnRequestQueue 	: Deque<CSpawnRequestData> = New Deque<CSpawnRequestData>()
	Global OrcAnimationSet 		: IntMap< Int > = New IntMap< Int >()
	Global HumanAnimationSet 	: IntMap< Int > = New IntMap< Int >()
	
	Function Init()
		HumanAnimationSet.Add( Anim_WalkUp, Anim_HumanFootmanWalkUp )
		HumanAnimationSet.Add( Anim_WalkDown, Anim_HumanFootmanWalkDown )
		HumanAnimationSet.Add( Anim_WalkRight, Anim_HumanFootmanWalkRight )
		HumanAnimationSet.Add( Anim_WalkLeft, Anim_HumanFootmanWalkLeft )
		HumanAnimationSet.Add( Anim_AttackUp, Anim_HumanFootmanAttackUp )
		HumanAnimationSet.Add( Anim_AttackDown, Anim_HumanFootmanAttackDown )
		HumanAnimationSet.Add( Anim_AttackRight, Anim_HumanFootmanAttackRight )
		HumanAnimationSet.Add( Anim_AttackLeft, Anim_HumanFootmanAttackLeft )
		HumanAnimationSet.Add( Anim_WalkUpRight, Anim_HumanFootmanWalkUpRight )
		HumanAnimationSet.Add( Anim_WalkUpLeft, Anim_HumanFootmanWalkUpLeft )
		HumanAnimationSet.Add( Anim_WalkDownRight, Anim_HumanFootmanWalkDownRight )
		HumanAnimationSet.Add( Anim_WalkDownLeft, Anim_HumanFootmanWalkDownLeft )
		HumanAnimationSet.Add( Anim_AttackUpRight, Anim_HumanFootmanAttackUpRight )
		HumanAnimationSet.Add( Anim_AttackUpLeft, Anim_HumanFootmanAttackUpLeft )
		HumanAnimationSet.Add( Anim_AttackDownRight, Anim_HumanFootmanAttackDownRight )
		HumanAnimationSet.Add( Anim_AttackDownLeft, Anim_HumanFootmanAttackDownLeft )
		HumanAnimationSet.Add( Anim_IdleUp, Anim_HumanFootmanIdleUp )
		HumanAnimationSet.Add( Anim_IdleDown, Anim_HumanFootmanIdleDown )
		HumanAnimationSet.Add( Anim_IdleRight, Anim_HumanFootmanIdleRight )
		HumanAnimationSet.Add( Anim_IdleLeft, Anim_HumanFootmanIdleLeft )
		HumanAnimationSet.Add( Anim_IdleUpRight, Anim_HumanFootmanIdleUpRight )
		HumanAnimationSet.Add( Anim_IdleUpLeft, Anim_HumanFootmanIdleUpLeft )
		HumanAnimationSet.Add( Anim_IdleDownRight, Anim_HumanFootmanIdleDownRight )
		HumanAnimationSet.Add( Anim_IdleDownLeft, Anim_HumanFootmanIdleDownLeft )
	
		
		OrcAnimationSet.Add( Anim_WalkUp, Anim_OrcFootmanWalkUp )
		OrcAnimationSet.Add( Anim_WalkDown, Anim_OrcFootmanWalkDown )
		OrcAnimationSet.Add( Anim_WalkRight, Anim_OrcFootmanWalkRight )
		OrcAnimationSet.Add( Anim_WalkLeft, Anim_OrcFootmanWalkLeft )
		OrcAnimationSet.Add( Anim_AttackUp, Anim_OrcFootmanAttackUp )
		OrcAnimationSet.Add( Anim_AttackDown, Anim_OrcFootmanAttackDown )
		OrcAnimationSet.Add( Anim_AttackRight, Anim_OrcFootmanAttackRight )
		OrcAnimationSet.Add( Anim_AttackLeft, Anim_OrcFootmanAttackLeft )
		OrcAnimationSet.Add( Anim_WalkUpRight, Anim_OrcFootmanWalkUpRight )
		OrcAnimationSet.Add( Anim_WalkUpLeft, Anim_OrcFootmanWalkUpLeft )
		OrcAnimationSet.Add( Anim_WalkDownRight, Anim_OrcFootmanWalkDownRight )
		OrcAnimationSet.Add( Anim_WalkDownLeft, Anim_OrcFootmanWalkDownLeft )
		OrcAnimationSet.Add( Anim_AttackUpRight, Anim_OrcFootmanAttackUpRight )
		OrcAnimationSet.Add( Anim_AttackUpLeft, Anim_OrcFootmanAttackUpLeft )
		OrcAnimationSet.Add( Anim_AttackDownRight, Anim_OrcFootmanAttackDownRight )
		OrcAnimationSet.Add( Anim_AttackDownLeft, Anim_OrcFootmanAttackDownLeft )
		OrcAnimationSet.Add( Anim_IdleUp, Anim_OrcFootmanIdleUp )
		OrcAnimationSet.Add( Anim_IdleDown, Anim_OrcFootmanIdleDown )
		OrcAnimationSet.Add( Anim_IdleRight, Anim_OrcFootmanIdleRight )
		OrcAnimationSet.Add( Anim_IdleLeft, Anim_OrcFootmanIdleLeft )
		OrcAnimationSet.Add( Anim_IdleUpRight, Anim_OrcFootmanIdleUpRight )
		OrcAnimationSet.Add( Anim_IdleUpLeft, Anim_OrcFootmanIdleUpLeft )
		OrcAnimationSet.Add( Anim_IdleDownRight, Anim_OrcFootmanIdleDownRight )
		OrcAnimationSet.Add( Anim_IdleDownLeft, Anim_OrcFootmanIdleDownLeft )
	End
	
	Function RequestSpawn( TemplateID : Int, Position : FVector )
		SpawnRequestQueue.PushLast = New CSpawnRequestData( TemplateID, Position )
	End
	Function Update( MyApp : CMyApp, CommandBuffer : CCommandBuffer )
		If SpawnRequestQueue.IsEmpty() = False
			Local SpawnRequestData := SpawnRequestQueue.PopFirst()
			Select SpawnRequestData.TemplateID
				Case TemplateID_HumanFootman
					CreateHumanFootman( MyApp, CommandBuffer, SpawnRequestData.Position )
				Case TemplateID_OrcFootman
					CreateOrcFootman( MyApp, CommandBuffer, SpawnRequestData.Position )
			End
		End
	End
	
	Function CreateHumanFootman( MyApp : CMyApp, CommandBuffer : CCommandBuffer, SpawnPosition : FVector )
		Local SpriteID 	:= CommandBuffer.AddCommand_CreateRenderItem( CommandType_CreateSprite, 0.0 )
		CommandBuffer.AddCommand( SpriteID, CommandType_UpdateScale, CAIManager.MapScale, CAIManager.MapScale )
			
		Local Footman 			:= New CGameObject()
		Local AnimationGraph 	:= New CAnimationGraph( SpriteID, HumanAnimationSet )
		Local Movement 			:= New CMovement( SpriteID, SpawnPosition, HumanAnimationSet, AnimationGraph )
		Local Unit 				:= New CUnit( SpriteID, Movement, AttitudeGroup_Humans, HumanAnimationSet, AnimationGraph )
		Footman.AddComponent( Movement ) 
		Footman.AddComponent( Unit )
		Footman.AddComponent( AnimationGraph )
		MyApp.AddGameObject	( Footman )
	End
	
	Function CreateOrcFootman( MyApp : CMyApp, CommandBuffer : CCommandBuffer, SpawnPosition : FVector )
		Local SpriteID 	:= CommandBuffer.AddCommand_CreateRenderItem( CommandType_CreateSprite, 0.0 )
		CommandBuffer.AddCommand( SpriteID, CommandType_UpdateScale, CAIManager.MapScale, CAIManager.MapScale )
		
		Local Footman 			:= New CGameObject()
		Local AnimationGraph 	:= New CAnimationGraph( SpriteID, OrcAnimationSet )
		Local Movement 			:= New CMovement( SpriteID, SpawnPosition, OrcAnimationSet, AnimationGraph )
		Local Unit 				:= New CUnit( SpriteID, Movement, AttitudeGroup_Orcs, OrcAnimationSet, AnimationGraph )
		
		Footman.AddComponent( Movement ) 
		Footman.AddComponent( Unit )
		Footman.AddComponent( AnimationGraph )
		MyApp.AddGameObject	( Footman )
		
	End
End