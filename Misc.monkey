Const MAX_FLOAT := 1000000000000.0

Class FVector
	Field X : Float
	Field Y : Float
	
	Method New( X : Float = 0.0, Y : Float = 0.0 )
		Self.X = X
		Self.Y = Y
	End
	' Returns between [-PI, PI]
	Method GetOrientationFromNormed : Float()
		Local AngleA := ACos( Self.X )
		Local AngleB := ASin( Self.Y ) 
		If AngleB < 0.0
			Return - AngleA
		Else
			Return AngleA
		End
	End
End

Class IVector
	Field X : Int
	Field Y : Int
	
	Method New( X : Int = 0, Y : Int = 0)
		Self.X = X
		Self.Y = Y
	End
	
	
End

Class CColor
	Field R : Int
	Field G : Int
	Field B : Int
	
	Method New( R : Int, G : Int, B : Int )
		Self.R = R
		Self.G = G
		Self.B = B
	End
End


Function Round : Int ( Value : Float )
 	Return Value + 0.49999997
End

Const FLT_EPSILON := 2e-23 