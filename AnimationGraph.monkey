Import GameObject

Const AnimationVariable_Attack 		:= 0
Const AnimationVariable_EnemyAngle 	:= 1
Const AnimationVariable_UnitAngle	:= 2
Const AnimationVariable_Move		:= 3

Class CAnimationVariable
	Method New ( VariableName : Int )
		Self.VariableName = VariableName
	End
	Field VariableName	:= -1
	Field BoolValue 	:= False
	Field FloatValueA	:= 0.0
	Field FloatValueB	:= 0.0
	Field FloatValueC	:= 0.0
End

Class CAnimationGraph Extends CComponent
	Field VariableMap 				: IntMap< CAnimationVariable > = New IntMap< CAnimationVariable >()
	Field SpriteID 					:= -1
	
	Field AnimationSet				: IntMap< Int >
	Field DefaultState 				: CAnimationState
	Field CurrentState				: CAnimationState
	
	Method New( SpriteID : Int, AnimationSet : IntMap< Int > )
		Super.New( ComponentType_AnimationGraph )
		Self.SpriteID 		= SpriteID
		Self.AnimationSet 	= AnimationSet
		
		Local AttackVariable 		:= New CAnimationVariable( AnimationVariable_Attack )
		AddVariable( AnimationVariable_Attack, AttackVariable )
		Local EnemyAngleVariable 	:= New CAnimationVariable( AnimationVariable_EnemyAngle )
		AddVariable( AnimationVariable_EnemyAngle, EnemyAngleVariable )
		Local UnitAngleVariable		:= New CAnimationVariable( AnimationVariable_UnitAngle ) 
		AddVariable( AnimationVariable_UnitAngle, UnitAngleVariable )
		Local MoveVariable			:= New CAnimationVariable( AnimationVariable_Move )
		AddVariable( AnimationVariable_Move, MoveVariable )
		
		
		Local Slice := 45.0 / 2.0
		
		'** Attack State
		Local AttackState := New CAnimationState( 0 )
		Local AttackRightNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackRight )
		Local AttackUpNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackUp )
		Local AttackLeftNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackLeft )
		Local AttackDownNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackDown )
		Local AttackUpRightNode : CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackUpRight )
		Local AttackUpLeftNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackUpLeft )
		Local AttackDownRightNode : CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackDownRight )
		Local AttackDownLeftNode : CPlayAnimationNode 	= New CPlayAnimationNode( Anim_AttackDownLeft )
		
		Local AttackMultiRangeCondition := New CFloatMultiRangeCondition( EnemyAngleVariable )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -8 * Slice, -7 * Slice , AttackLeftNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -7 * Slice, -5 * Slice , AttackUpLeftNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -5 * Slice, -3 * Slice , AttackUpNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -3 * Slice, -1 * Slice , AttackUpRightNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -1 * Slice, 1 * Slice , AttackRightNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 1 * Slice, 3 * Slice , AttackDownRightNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 3 * Slice, 5 * Slice , AttackDownNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 5 * Slice, 7 * Slice , AttackDownLeftNode ) )
		AttackMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 7 * Slice, 8 * Slice , AttackLeftNode ) )
		
		AttackState.EntryPoint = AttackMultiRangeCondition
		
		'** Idle State
		Local IdleState := New CAnimationState( 1 )
		Local IdleRightNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleRight )
		Local IdleUpNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleUp )
		Local IdleLeftNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleLeft )
		Local IdleDownNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleDown )
		Local IdleUpRightNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleUpRight )
		Local IdleUpLeftNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleUpLeft )
		Local IdleDownRightNode : CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleDownRight )
		Local IdleDownLeftNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_IdleDownLeft )
		
		Local IdleMultiRangeCondition := New CFloatMultiRangeCondition( UnitAngleVariable )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -8 * Slice, -7 * Slice , IdleLeftNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -7 * Slice, -5 * Slice , IdleUpLeftNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -5 * Slice, -3 * Slice , IdleUpNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -3 * Slice, -1 * Slice , IdleUpRightNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -1 * Slice, 1 * Slice , IdleRightNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 1 * Slice, 3 * Slice , IdleDownRightNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 3 * Slice, 5 * Slice , IdleDownNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 5 * Slice, 7 * Slice , IdleDownLeftNode ) )
		IdleMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 7 * Slice, 8 * Slice , IdleLeftNode ) )
		
		IdleState.EntryPoint = IdleMultiRangeCondition
		
		'** Move State
		Local MoveState := New CAnimationState( 2 )
		Local MoveRightNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkRight )
		Local MoveUpNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkUp )
		Local MoveLeftNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkLeft )
		Local MoveDownNode 		: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkDown )
		Local MoveUpRightNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkUpRight )
		Local MoveUpLeftNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkUpLeft )
		Local MoveDownRightNode : CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkDownRight )
		Local MoveDownLeftNode 	: CPlayAnimationNode 	= New CPlayAnimationNode( Anim_WalkDownLeft )
		
		Local MoveMultiRangeCondition := New CFloatMultiRangeCondition( UnitAngleVariable )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -8 * Slice, -7 * Slice , MoveLeftNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -7 * Slice, -5 * Slice , MoveUpLeftNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -5 * Slice, -3 * Slice , MoveUpNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -3 * Slice, -1 * Slice , MoveUpRightNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( -1 * Slice, 1 * Slice , MoveRightNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 1 * Slice, 3 * Slice , MoveDownRightNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 3 * Slice, 5 * Slice , MoveDownNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 5 * Slice, 7 * Slice , MoveDownLeftNode ) )
		MoveMultiRangeCondition.AddMiniCondition( New CFloatRangeMiniCondition( 7 * Slice, 8 * Slice , MoveLeftNode ) )
		
		MoveState.EntryPoint = MoveMultiRangeCondition
		
		Local TransitionIdleCombat := New CStateBoolTransition( AttackVariable, AttackState )
		IdleState.AddTransition( TransitionIdleCombat )
		
		Local TransitionCombatIdle := New CStateBoolTransition( AttackVariable, IdleState, True )
		AttackState.AddTransition( TransitionCombatIdle )
		
		Local TransitionIdleMove := New CStateBoolTransition( MoveVariable, MoveState )
		IdleState.AddTransition( TransitionIdleMove )
		
		Local TransitionMoveIdle := New CStateBoolTransition( MoveVariable, IdleState, True )
		MoveState.AddTransition( TransitionMoveIdle )
		
		Self.DefaultState = IdleState
		Self.CurrentState = Self.DefaultState
	End
	
	Method AddVariable( VariableID : Int, Variable : CAnimationVariable )
		Self.VariableMap.Add( VariableID, Variable )
	End
	
	Method SetVariable( VariableID : Int, BoolValue : Bool )
		Local Variable := Self.VariableMap.Get( VariableID )
		Variable.BoolValue = BoolValue
	End
	
	Method SetVariable( VariableID : Int, FloatValue : Float )
		Local Variable := Self.VariableMap.Get( VariableID )
		'Print( "Variable with ID: " + Variable.VariableName + " has FloatValueA set to " + FloatValue )
		Variable.FloatValueA = FloatValue
	End
	
	Method Update( DeltaTime : Float, CommandBuffer : CCommandBuffer )
		'Print ("Updating Animation graph for game object with ID = " + Self.GameObject.ID )
		'Local AttackVariable := VariableMap.Get( AnimationVariable_Attack )
		'If AttackVariable.BoolValue
			
		'	Local Node := Self.FloatMultiRangeCondition.AssessCondition()
		'	If Node <> Self.ActiveNode
		'		Self.ActiveNode = Node
		'		Node.Activate( Self.SpriteID, Self.AnimationSet, CommandBuffer )
		'	End
		'End
		For Local StateTransition := Eachin Self.CurrentState.TransitionArray
			If StateTransition.Assess()
				If Self.CurrentState
					Self.CurrentState.SignalDead()
				End
				Self.CurrentState = StateTransition.NextState
			End
		End
		
		Self.CurrentState.Signal( Self.SpriteID, Self.AnimationSet, CommandBuffer )
	End
End
Class CStateTransition
	Field NextState		: CAnimationState
	Field Inverted 		: Bool = False
	
	Method New ( NextState : CAnimationState, Inverted : Bool = False )
		Self.NextState		= NextState
		Self.Inverted		= Inverted
	End
	
	Method Assess : Bool ()
	End
End

Class CStateBoolTransition Extends CStateTransition
	Field BoolVariable 	: CAnimationVariable 
	
	Method New ( BoolVariable : CAnimationVariable, NextState : CAnimationState, Inverted : Bool = False )
		Super.New( NextState, Inverted )
		Self.BoolVariable 	= BoolVariable
	End
	
	Method Assess : Bool ()
		If Self.Inverted = False
			Return BoolVariable.BoolValue
		Else
			Return BoolVariable.BoolValue = False
		End
	End
End

Class CAnimationNode
	Method Signal( SpriteID : Int, AnimationSet : IntMap< Int >, CommandBuffer : CCommandBuffer )
	End
	Method SignalDead()
	End
End

Class CPlayAnimationNode Extends CAnimationNode
	Field AnimID : Int = -1
	Method New ( AnimID : Int )
		Self.AnimID = AnimID
	End
	Method Signal( SpriteID : Int, AnimationSet : IntMap< Int >, CommandBuffer : CCommandBuffer )
		Local AnimID := AnimationSet.Get( Self.AnimID )
		'Print( "Playing anim " + AnimID + " on sprite " + SpriteID + " with anim id " + Self.AnimID )
		CommandBuffer.AddCommand( SpriteID, CommandType_PlayAnimation, AnimID )
	End
End

Class CFloatRangeMiniCondition
	Field MinRange 	:= 0.0
	Field MaxRange 	:= 0.0
	Field Link 		: CAnimationNode
	Method New ( MinRange : Float, MaxRange : Float, Link : CAnimationNode )
		Self.MinRange 	= MinRange
		Self.MaxRange 	= MaxRange
		Self.Link 		= Link
	End
	
	Method AssessCondition : Bool ( FloatVariable : CAnimationVariable )
		If Self.MinRange <= FloatVariable.FloatValueA And FloatVariable.FloatValueA <= Self.MaxRange
			Return True
		End
		Return False
	End
End

Class CAnimationState
	Field EntryPoint 		: CAnimationNode
	Field TransitionArray 	: Deque< CStateTransition > = New Deque< CStateTransition >()
	Field ID				: Int
	Method New( ID : Int )
		Self.ID = ID
	End
	Method Signal( SpriteID : Int, AnimationSet : IntMap< Int >, CommandBuffer : CCommandBuffer )
		Self.EntryPoint.Signal( SpriteID, AnimationSet, CommandBuffer )
	End
	Method SignalDead()
		Self.EntryPoint.SignalDead( )
	End
	
	Method AddTransition( StateTransition : CStateTransition )
		Self.TransitionArray.PushLast( StateTransition )
	End
End

Class CFloatMultiRangeCondition Extends CAnimationNode
	Field FloatVariable 	: CAnimationVariable
	Field ActiveNode		: CAnimationNode
	Field MiniConditionArray: Deque< CFloatRangeMiniCondition > = New Deque< CFloatRangeMiniCondition >()
	Method New( FloatVariable : CAnimationVariable )
		Self.FloatVariable = FloatVariable		
	End
	
	Method Signal( SpriteID : Int, AnimationSet : IntMap< Int >, CommandBuffer : CCommandBuffer )
		Local Node := AssessCondition()
		If Node <> Self.ActiveNode
			If Self.ActiveNode
				Self.ActiveNode.SignalDead()
			End
			Self.ActiveNode = Node
			Node.Signal( SpriteID, AnimationSet, CommandBuffer )
		End
	End
	

	
	Method AssessCondition : CAnimationNode ()
		For Local MiniCondition := Eachin Self.MiniConditionArray
			If MiniCondition.AssessCondition( Self.FloatVariable )
				'Print( "Mini condition selected: " + MiniCondition.MinRange + " <= " + Self.FloatVariable.FloatValueA + " <= " + MiniCondition.MaxRange )
				Return MiniCondition.Link
			End
		End
	End
	
	Method AddMiniCondition( MiniCondition : CFloatRangeMiniCondition )
		Self.MiniConditionArray.PushLast( MiniCondition )
	End
	
	Method SignalDead()
		Self.ActiveNode.SignalDead()
		Self.ActiveNode = Null
	End
End